
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mdp` char(128) COLLATE utf8_unicode_ci NOT NULL,
  `mdpNOUV` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdp_var` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_level` tinyint(4) NOT NULL DEFAULT 0,
  `race` enum('humain','covenant') COLLATE utf8_unicode_ci NOT NULL,
  `id_alliance` int(11) DEFAULT NULL,
  `id_grade_alliance` bigint(15) DEFAULT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mailNOUV` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `envoyerMail` smallint(6) NOT NULL DEFAULT 0,
  `last_ip` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mv` tinyint(4) NOT NULL DEFAULT 0,
  `multi` smallint(6) NOT NULL DEFAULT 0,
  `raisonmv` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `operateurmv` int(11) NOT NULL DEFAULT 0,
  `time_inscription` int(11) NOT NULL,
  `last_visite` int(11) NOT NULL DEFAULT 0,
  `points` bigint(20) NOT NULL DEFAULT 0,
  `place_points` smallint(6) NOT NULL DEFAULT 0,
  `batiments` bigint(20) NOT NULL DEFAULT 0,
  `batiments_place` int(11) NOT NULL DEFAULT 0,
  `recherches` bigint(20) NOT NULL DEFAULT 0,
  `place_recherches` smallint(6) NOT NULL DEFAULT 0,
  `flotte` bigint(20) NOT NULL DEFAULT 0,
  `place_flotte` smallint(6) NOT NULL DEFAULT 0,
  `terrestre` bigint(20) NOT NULL DEFAULT 0,
  `place_terrestre` smallint(6) NOT NULL DEFAULT 0,
  `planete_source` int(11) NOT NULL DEFAULT 0,
  `techno_indu` int(11) NOT NULL DEFAULT 0,
  `techno_inge` bigint(20) NOT NULL DEFAULT 0,
  `techno_inge2` int(11) NOT NULL DEFAULT 0,
  `techno_poli` smallint(6) NOT NULL DEFAULT 0,
  `techno_arme` int(11) NOT NULL DEFAULT 0,
  `techno_defe` bigint(20) NOT NULL DEFAULT 0,
  `techno_defe2` int(11) NOT NULL DEFAULT 0,
  `techno_proj` int(11) NOT NULL DEFAULT 0,
  `techno_expansion` int(11) NOT NULL DEFAULT 0,
  `informatique` int(11) NOT NULL DEFAULT 0,
  `detection` int(11) NOT NULL DEFAULT 0,
  `armement` int(11) NOT NULL DEFAULT 0,
  `energie_t` int(11) NOT NULL DEFAULT 0,
  `reacteur` int(11) NOT NULL DEFAULT 0,
  `reacteur_f` int(11) NOT NULL DEFAULT 0,
  `reacteur_ff` int(11) NOT NULL DEFAULT 0,
  `medecine` int(11) NOT NULL DEFAULT 0,
  `tactique` int(11) NOT NULL DEFAULT 0,
  `spartan` int(11) NOT NULL DEFAULT 0,
  `blindage` int(11) NOT NULL DEFAULT 0,
  `prodfile` int(11) NOT NULL DEFAULT 0,
  `nanites` smallint(6) NOT NULL DEFAULT 0,
  `credits` bigint(20) NOT NULL DEFAULT 0,
  `bourse` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `destinationsFavoris` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `amis` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `combatAT_tactique` smallint(6) NOT NULL DEFAULT 0,
  `combatDE_tactique` smallint(6) NOT NULL DEFAULT 0,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `politique` smallint(6) NOT NULL DEFAULT 0,
  `politique_lastchange` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `actualites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_categorie` int(11) NOT NULL DEFAULT 0,
  `id_user` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT 0,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contenu` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `id_categorie` (`id_categorie`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `actualites_ibfk_1` FOREIGN KEY (`id_categorie`) REFERENCES `categorie` (`id`),
  CONSTRAINT `actualites_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `actualites_commentaires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_actu` int(11) NOT NULL DEFAULT 0,
  `id_user` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ip` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT 0,
  `commentaire` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `id_actu` (`id_actu`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `actualites_commentaires_ibfk_1` FOREIGN KEY (`id_actu`) REFERENCES `actualites` (`id`),
  CONSTRAINT `actualites_commentaires_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `alliances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `race` enum('humain','covenant') COLLATE utf8_unicode_ci NOT NULL,
  `fondateur` int(11) NOT NULL,
  `sante` int(11) NOT NULL DEFAULT 0,
  `nom_alliance` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `presentation` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `message_inscription` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `texte_interne` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url_forum` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url_chat` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `port_chat` mediumint(9) NOT NULL DEFAULT 0,
  `pass_chat` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `chan_chat` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `etat_inscription` tinyint(1) NOT NULL DEFAULT 0,
  `defcon` smallint(6) NOT NULL DEFAULT 0,
  `defcon_txt` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `galaxie` int(11) NOT NULL DEFAULT 0,
  `ss` int(11) NOT NULL DEFAULT 0,
  `wing` smallint(6) NOT NULL DEFAULT 0,
  `nom_asteroide` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `image_asteroide` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `debris_met` bigint(20) NOT NULL DEFAULT 0,
  `debris_cri` bigint(20) NOT NULL DEFAULT 0,
  `timestamp` int(11) NOT NULL DEFAULT 0,
  `credits_alliance` bigint(20) NOT NULL DEFAULT 0,
  `metal` bigint(20) NOT NULL DEFAULT 0,
  `cristal` bigint(20) NOT NULL DEFAULT 0,
  `hydrogene` bigint(20) NOT NULL DEFAULT 0,
  `file_bat` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `file_vais` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `centre` smallint(6) NOT NULL DEFAULT 0,
  `port` smallint(6) NOT NULL DEFAULT 0,
  `forge` smallint(6) NOT NULL DEFAULT 0,
  `urgence` smallint(6) NOT NULL DEFAULT 0,
  `propagande` smallint(6) NOT NULL DEFAULT 0,
  `economie` smallint(6) NOT NULL DEFAULT 0,
  `vaisseau_1` int(11) NOT NULL DEFAULT 0,
  `vaisseau_2` int(11) NOT NULL DEFAULT 0,
  `vaisseau_3` int(11) NOT NULL DEFAULT 0,
  `vaisseau_4` int(11) NOT NULL DEFAULT 0,
  `vaisseau_5` int(11) NOT NULL DEFAULT 0,
  `vaisseau_6` int(11) NOT NULL DEFAULT 0,
  `vaisseau_7` int(11) NOT NULL DEFAULT 0,
  `vaisseau_8` int(11) NOT NULL DEFAULT 0,
  `vaisseau_9` int(11) NOT NULL DEFAULT 0,
  `vaisseau_10` int(11) NOT NULL DEFAULT 0,
  `vaisseau_11` int(11) NOT NULL DEFAULT 0,
  `vaisseau_12` int(11) NOT NULL DEFAULT 0,
  `vaisseau_13` int(11) NOT NULL DEFAULT 0,
  `vaisseau_14` int(11) NOT NULL DEFAULT 0,
  `vaisseau_15` int(11) NOT NULL DEFAULT 0,
  `vaisseau_16` int(11) NOT NULL DEFAULT 0,
  `vaisseau_17` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fondateur` (`fondateur`),
  CONSTRAINT `alliances_ibfk_1` FOREIGN KEY (`fondateur`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `alliances_attente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_alliance` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL DEFAULT 0,
  `message` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id_alliance`,`id_user`),
  KEY `id` (`id`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `alliances_attente_ibfk_1` FOREIGN KEY (`id_alliance`) REFERENCES `alliances` (`id`),
  CONSTRAINT `alliances_attente_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `alliances_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_alliance` int(11) NOT NULL,
  `emetteur` int(11) DEFAULT NULL,
  `timestamp` int(11) NOT NULL DEFAULT 0,
  `message` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `id_alliance` (`id_alliance`),
  KEY `emetteur` (`emetteur`),
  CONSTRAINT `alliances_chat_ibfk_1` FOREIGN KEY (`id_alliance`) REFERENCES `alliances` (`id`),
  CONSTRAINT `alliances_chat_ibfk_2` FOREIGN KEY (`emetteur`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `alliances_creation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nom_alliance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fondateur` int(11) NOT NULL,
  `lien` char(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `signatures` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `fondateur` (`fondateur`),
  CONSTRAINT `alliances_creation_ibfk_1` FOREIGN KEY (`fondateur`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `alliances_emprunt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_alliance` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `raison` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `metal` int(11) NOT NULL DEFAULT 0,
  `cristal` int(11) NOT NULL DEFAULT 0,
  `hydrogene` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `id_alliance` (`id_alliance`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `alliances_emprunt_ibfk_1` FOREIGN KEY (`id_alliance`) REFERENCES `alliances` (`id`),
  CONSTRAINT `alliances_emprunt_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `alliances_grade` (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `id_alliance` int(10) NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `auth` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `id_alliance` (`id_alliance`),
  CONSTRAINT `alliances_grade_ibfk_1` FOREIGN KEY (`id_alliance`) REFERENCES `alliances` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `alliances_mail` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `vu` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `destinataire` int(11) DEFAULT NULL,
  `expediteur` int(11) DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT 0,
  `sujet` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contenu` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `temps` int(11) NOT NULL DEFAULT 0,
  `statut` smallint(6) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `destinataire` (`destinataire`),
  KEY `expediteur` (`expediteur`),
  CONSTRAINT `alliances_mail_ibfk_1` FOREIGN KEY (`destinataire`) REFERENCES `alliances` (`id`),
  CONSTRAINT `alliances_mail_ibfk_2` FOREIGN KEY (`expediteur`) REFERENCES `alliances` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `alliances_pactes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_alliance1` int(11) DEFAULT NULL,
  `id_alliance2` int(11) DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT 0,
  `time_demand` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `time_creation` int(11) NOT NULL DEFAULT 0,
  `time_fin` int(11) NOT NULL DEFAULT 0,
  `accepte` tinyint(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `id_alliance1` (`id_alliance1`),
  KEY `id_alliance2` (`id_alliance2`),
  CONSTRAINT `alliances_pactes_ibfk_1` FOREIGN KEY (`id_alliance1`) REFERENCES `alliances` (`id`),
  CONSTRAINT `alliances_pactes_ibfk_2` FOREIGN KEY (`id_alliance2`) REFERENCES `alliances` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `annonces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contenu` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `complet` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `annonces_commentaires` (
  `id` int(11) NOT NULL,
  `id_annonce` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ip` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT 0,
  `commentaire` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `id_annonce` (`id_annonce`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `annonces_commentaires_ibfk_1` FOREIGN KEY (`id_annonce`) REFERENCES `annonces` (`id`),
  CONSTRAINT `annonces_commentaires_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `bourse` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `news` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `timeDesc` int(11) NOT NULL DEFAULT 0,
  `metal` float NOT NULL DEFAULT 0,
  `cristal` float NOT NULL DEFAULT 0,
  `metalAnc` float NOT NULL DEFAULT 0,
  `cristalAnc` float NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `bourse_ressources` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `dispo` bigint(20) NOT NULL DEFAULT 0,
  `graph` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `bug` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `temps` int(11) NOT NULL DEFAULT 0,
  `page` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `cds_beta` (
  `code` char(15) COLLATE utf8_unicode_ci NOT NULL,
  `utilise` tinyint(1) NOT NULL DEFAULT 0,
  `user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `classement0` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `pseudo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `race` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `points` bigint(20) NOT NULL DEFAULT 0,
  `flottes` bigint(20) NOT NULL DEFAULT 0,
  `terrestres` bigint(20) NOT NULL DEFAULT 0,
  `recherches` bigint(20) NOT NULL DEFAULT 0,
  `batiments` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `classement1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `pseudo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `race` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `points` bigint(20) NOT NULL DEFAULT 0,
  `flottes` bigint(20) NOT NULL DEFAULT 0,
  `terrestres` bigint(20) NOT NULL DEFAULT 0,
  `recherches` bigint(20) NOT NULL DEFAULT 0,
  `batiments` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `classement_alliances0` (
  `id` int(11) NOT NULL DEFAULT 1,
  `tag` varchar(255) NOT NULL,
  `nbr_membres` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `points_points_by_members` int(11) NOT NULL,
  `flottes` int(11) NOT NULL,
  `points_flottes_by_members` int(11) NOT NULL,
  `terrestres` int(11) NOT NULL,
  `points_terrestres_by_members` int(11) NOT NULL,
  `batiments` int(11) NOT NULL,
  `points_batiments_by_members` int(11) NOT NULL,
  `recherches` int(11) NOT NULL,
  `points_recherches_by_members` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `classement_alliances1` (
  `id` int(11) NOT NULL DEFAULT 1,
  `tag` varchar(255) NOT NULL,
  `nbr_membres` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `points_points_by_members` int(11) NOT NULL,
  `flottes` int(11) NOT NULL,
  `points_flottes_by_members` int(11) NOT NULL,
  `terrestres` int(11) NOT NULL,
  `points_terrestres_by_members` int(11) NOT NULL,
  `batiments` int(11) NOT NULL,
  `points_batiments_by_members` int(11) NOT NULL,
  `recherches` int(11) NOT NULL,
  `points_recherches_by_members` int(11) NOT NULL,
  `id_alliance` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `flottes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_alliance` int(11) DEFAULT NULL,
  `mission` tinyint(4) NOT NULL DEFAULT 0,
  `statut` tinyint(1) NOT NULL DEFAULT 0,
  `last` int(11) NOT NULL DEFAULT 0,
  `start_time` int(11) NOT NULL DEFAULT 0,
  `start_type` tinyint(4) NOT NULL DEFAULT 0,
  `start_planete` int(11) NOT NULL DEFAULT 0,
  `end_time` int(11) NOT NULL DEFAULT 0,
  `end_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 : ID d''une planète ; 1 : coordonnées d''une planète ; 2 : ID d''une alliance ; 3 : coordonnées d''un système (pour les alliances)',
  `end_planete` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ret_time` int(11) NOT NULL DEFAULT 0,
  `ret_type` tinyint(4) NOT NULL DEFAULT 0,
  `ret_planete` int(11) NOT NULL DEFAULT 0,
  `vitesse` float NOT NULL DEFAULT 0,
  `contenu_metal` int(11) NOT NULL DEFAULT 0,
  `contenu_cristal` int(11) NOT NULL DEFAULT 0,
  `contenu_hydrogene` int(11) NOT NULL DEFAULT 0,
  `nb_vais` int(11) NOT NULL DEFAULT 0,
  `vaisseau_1` int(11) NOT NULL DEFAULT 0,
  `vaisseau_2` int(11) NOT NULL DEFAULT 0,
  `vaisseau_3` int(11) NOT NULL DEFAULT 0,
  `vaisseau_4` int(11) NOT NULL DEFAULT 0,
  `vaisseau_5` int(11) NOT NULL DEFAULT 0,
  `vaisseau_6` int(11) NOT NULL DEFAULT 0,
  `vaisseau_7` int(11) NOT NULL DEFAULT 0,
  `vaisseau_8` int(11) NOT NULL DEFAULT 0,
  `vaisseau_9` int(11) NOT NULL DEFAULT 0,
  `vaisseau_10` int(11) NOT NULL DEFAULT 0,
  `vaisseau_11` int(11) NOT NULL DEFAULT 0,
  `vaisseau_12` int(11) NOT NULL DEFAULT 0,
  `vaisseau_13` int(11) NOT NULL DEFAULT 0,
  `vaisseau_14` int(11) NOT NULL DEFAULT 0,
  `vaisseau_15` int(11) NOT NULL DEFAULT 0,
  `vaisseau_16` int(11) NOT NULL DEFAULT 0,
  `vaisseau_17` int(11) NOT NULL DEFAULT 0,
  `tactique` int(11) NOT NULL DEFAULT 0,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_alliance` (`id_alliance`),
  CONSTRAINT `flottes_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`),
  CONSTRAINT `flottes_ibfk_2` FOREIGN KEY (`id_alliance`) REFERENCES `alliances` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `flottes_combats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `refflotte` int(11) NOT NULL DEFAULT 0,
  `timestamp` int(11) NOT NULL DEFAULT 0,
  `serialize` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tactiqueAT` int(11) NOT NULL DEFAULT 0,
  `tactiqueEN` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `flottes_preparation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `statut` tinyint(1) NOT NULL DEFAULT 0,
  `initiateur` int(11) NOT NULL DEFAULT 0,
  `end_type` tinyint(4) NOT NULL DEFAULT 0,
  `end_planete` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time_end` int(11) NOT NULL DEFAULT 0,
  `flottes` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mission` smallint(6) NOT NULL DEFAULT 0,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `infoshead` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texte` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `mail` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `vu` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `destinataire` int(11) DEFAULT NULL,
  `expediteur` int(11) DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT 0,
  `sujet` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contenu` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `temps` int(11) NOT NULL DEFAULT 0,
  `statut` smallint(6) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `destinataire` (`destinataire`),
  KEY `expediteur` (`expediteur`),
  CONSTRAINT `mail_ibfk_1` FOREIGN KEY (`destinataire`) REFERENCES `user` (`id`),
  CONSTRAINT `mail_ibfk_2` FOREIGN KEY (`expediteur`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `messages_demarrage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contenu` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `ope_faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contenu` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ordre` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `ope_mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contenu` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT 0,
  `statut` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `ope_mail_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `ope_modele` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sujet` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contenu` text COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `ope_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contenu` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `timestamp` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `ope_news_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `phpbb_users` (
  `user_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` tinyint(2) NOT NULL DEFAULT 0,
  `group_id` mediumint(8) unsigned NOT NULL DEFAULT 3,
  `user_permissions` mediumtext COLLATE utf8_bin NOT NULL,
  `user_perm_from` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `user_ip` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_regdate` int(11) unsigned NOT NULL DEFAULT 0,
  `username` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `username_clean` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_password` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_passchg` int(11) unsigned NOT NULL DEFAULT 0,
  `user_pass_convert` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `user_email` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_email_hash` bigint(20) NOT NULL DEFAULT 0,
  `user_birthday` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_lastvisit` int(11) unsigned NOT NULL DEFAULT 0,
  `user_lastmark` int(11) unsigned NOT NULL DEFAULT 0,
  `user_lastpost_time` int(11) unsigned NOT NULL DEFAULT 0,
  `user_lastpage` varchar(200) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_last_confirm_key` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_last_search` int(11) unsigned NOT NULL DEFAULT 0,
  `user_warnings` tinyint(4) NOT NULL DEFAULT 0,
  `user_last_warning` int(11) unsigned NOT NULL DEFAULT 0,
  `user_login_attempts` tinyint(4) NOT NULL DEFAULT 0,
  `user_inactive_reason` tinyint(2) NOT NULL DEFAULT 0,
  `user_inactive_time` int(11) unsigned NOT NULL DEFAULT 0,
  `user_posts` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `user_lang` varchar(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_timezone` decimal(5,2) NOT NULL DEFAULT 0.00,
  `user_dst` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `user_dateformat` varchar(30) COLLATE utf8_bin NOT NULL DEFAULT 'd M Y H:i',
  `user_style` smallint(4) unsigned NOT NULL DEFAULT 0,
  `user_rank` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `user_colour` varchar(6) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_new_privmsg` tinyint(4) NOT NULL DEFAULT 0,
  `user_unread_privmsg` tinyint(4) NOT NULL DEFAULT 0,
  `user_last_privmsg` int(11) unsigned NOT NULL DEFAULT 0,
  `user_message_rules` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `user_full_folder` int(11) NOT NULL DEFAULT -3,
  `user_emailtime` int(11) unsigned NOT NULL DEFAULT 0,
  `user_topic_show_days` smallint(4) unsigned NOT NULL DEFAULT 0,
  `user_topic_sortby_type` varchar(1) COLLATE utf8_bin NOT NULL DEFAULT 't',
  `user_topic_sortby_dir` varchar(1) COLLATE utf8_bin NOT NULL DEFAULT 'd',
  `user_post_show_days` smallint(4) unsigned NOT NULL DEFAULT 0,
  `user_post_sortby_type` varchar(1) COLLATE utf8_bin NOT NULL DEFAULT 't',
  `user_post_sortby_dir` varchar(1) COLLATE utf8_bin NOT NULL DEFAULT 'a',
  `user_notify` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `user_notify_pm` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `user_notify_type` tinyint(4) NOT NULL DEFAULT 0,
  `user_allow_pm` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `user_allow_viewonline` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `user_allow_viewemail` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `user_allow_massemail` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `user_options` int(11) unsigned NOT NULL DEFAULT 895,
  `user_avatar` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_avatar_type` tinyint(2) NOT NULL DEFAULT 0,
  `user_avatar_width` smallint(4) unsigned NOT NULL DEFAULT 0,
  `user_avatar_height` smallint(4) unsigned NOT NULL DEFAULT 0,
  `user_sig` mediumtext COLLATE utf8_bin NOT NULL,
  `user_sig_bbcode_uid` varchar(5) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_sig_bbcode_bitfield` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_from` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_icq` varchar(15) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_aim` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_yim` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_msnm` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_jabber` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_website` varchar(200) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_occ` text COLLATE utf8_bin NOT NULL,
  `user_interests` text COLLATE utf8_bin NOT NULL,
  `user_actkey` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_newpasswd` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username_clean` (`username_clean`),
  KEY `user_birthday` (`user_birthday`),
  KEY `user_email_hash` (`user_email_hash`),
  KEY `user_type` (`user_type`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE IF NOT EXISTS `planete` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `hash_planete` char(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `galaxie` tinyint(4) NOT NULL DEFAULT 0,
  `ss` tinyint(4) NOT NULL DEFAULT 0,
  `position` tinyint(4) NOT NULL DEFAULT 0,
  `nb` tinyint(4) NOT NULL DEFAULT 0,
  `isolement` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `nom_planete` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `moral` float NOT NULL DEFAULT 0.5,
  `image` tinyint unsigned NOT NULL DEFAULT 0,
  `cases` int(11) NOT NULL DEFAULT 0,
  `debris_met` bigint(20) NOT NULL DEFAULT 0,
  `debris_cri` bigint(20) NOT NULL DEFAULT 0,
  `metal` double NOT NULL DEFAULT 0,
  `cristal` double NOT NULL DEFAULT 0,
  `hydrogene` double NOT NULL DEFAULT 0,
  `timestamp` int(11) NOT NULL DEFAULT 0,
  `population` double NOT NULL DEFAULT 1000,
  `coeff` float NOT NULL DEFAULT 0,
  `energie` bigint(20) NOT NULL DEFAULT 0,
  `file_tech` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `file_bat` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mine_m` int(11) NOT NULL DEFAULT 0,
  `coeff_mine_m` float NOT NULL DEFAULT 1,
  `mine_c` int(11) NOT NULL DEFAULT 0,
  `coeff_mine_c` float NOT NULL DEFAULT 1,
  `mine_h` int(11) NOT NULL DEFAULT 0,
  `coeff_mine_h` float NOT NULL DEFAULT 1,
  `centrale_s` int(11) NOT NULL DEFAULT 0,
  `coeff_centrale_s` float NOT NULL DEFAULT 1,
  `centrale_f` int(11) NOT NULL DEFAULT 0,
  `coeff_centrale_f` float NOT NULL DEFAULT 1,
  `radar` int(11) NOT NULL DEFAULT 0,
  `labo` int(11) NOT NULL DEFAULT 0,
  `chantier_terrestre` int(11) NOT NULL DEFAULT 0,
  `chantier_spatial` int(11) NOT NULL DEFAULT 0,
  `caserne` int(11) NOT NULL DEFAULT 0,
  `silo` int(11) NOT NULL DEFAULT 0,
  `centre_info` tinyint(4) NOT NULL DEFAULT 0,
  `habitation` smallint(6) NOT NULL DEFAULT 0,
  `arcologies` smallint(6) NOT NULL DEFAULT 0,
  `bunker` smallint(6) NOT NULL DEFAULT 0,
  `stations` smallint(6) NOT NULL DEFAULT 0,
  `commercial` smallint(6) NOT NULL DEFAULT 0,
  `loisir` smallint(6) NOT NULL DEFAULT 0,
  `administration` smallint(6) NOT NULL DEFAULT 0,
  `file_cas` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `soldat1` int(11) NOT NULL DEFAULT 0,
  `soldat2` int(11) NOT NULL DEFAULT 0,
  `soldat3` int(11) NOT NULL DEFAULT 0,
  `soldat4` int(11) NOT NULL DEFAULT 0,
  `sniper` int(11) NOT NULL DEFAULT 0,
  `spartan` int(11) NOT NULL DEFAULT 0,
  `medecin` int(11) NOT NULL DEFAULT 0,
  `ingenieur` int(11) NOT NULL DEFAULT 0,
  `soldat_lourd` int(11) NOT NULL DEFAULT 0,
  `cons_spatial` int(11) NOT NULL DEFAULT 0,
  `file_vais` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `vaisseau_1` int(11) NOT NULL DEFAULT 0,
  `vaisseau_2` int(11) NOT NULL DEFAULT 0,
  `vaisseau_3` int(11) NOT NULL DEFAULT 0,
  `vaisseau_4` int(11) NOT NULL DEFAULT 0,
  `vaisseau_5` int(11) NOT NULL DEFAULT 0,
  `vaisseau_6` int(11) NOT NULL DEFAULT 0,
  `vaisseau_7` int(11) NOT NULL DEFAULT 0,
  `vaisseau_8` int(11) NOT NULL DEFAULT 0,
  `vaisseau_9` int(11) NOT NULL DEFAULT 0,
  `vaisseau_10` int(11) NOT NULL DEFAULT 0,
  `vaisseau_11` int(11) NOT NULL DEFAULT 0,
  `vaisseau_12` int(11) NOT NULL DEFAULT 0,
  `vaisseau_13` int(11) NOT NULL DEFAULT 0,
  `vaisseau_14` int(11) NOT NULL DEFAULT 0,
  `vaisseau_15` int(11) NOT NULL DEFAULT 0,
  `vaisseau_16` int(11) NOT NULL DEFAULT 0,
  `vaisseau_17` int(11) NOT NULL DEFAULT 0,
  `file_ter` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `vais_0` int(11) NOT NULL DEFAULT 0,
  `vais_1` int(11) NOT NULL DEFAULT 0,
  `vais_2` int(11) NOT NULL DEFAULT 0,
  `vais_3` int(11) NOT NULL DEFAULT 0,
  `vcl_1` int(11) NOT NULL DEFAULT 0,
  `vcl_2` int(11) NOT NULL DEFAULT 0,
  `vcl_3` int(11) NOT NULL DEFAULT 0,
  `vcl_4` int(11) NOT NULL DEFAULT 0,
  `def_1` int(11) NOT NULL DEFAULT 0,
  `def_2` int(11) NOT NULL DEFAULT 0,
  `def_3` int(11) NOT NULL DEFAULT 0,
  `def_4` int(11) NOT NULL DEFAULT 0,
  `def_5` int(11) NOT NULL DEFAULT 0,
  `def_6` int(11) NOT NULL DEFAULT 0,
  `def_7` int(11) NOT NULL DEFAULT 0,
  `def_8` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hash_planete` (`hash_planete`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `planete_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `registre_identification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_util` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `commodo` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `securite_identification` (
  `ip` char(6) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` tinyint(4) NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`ip`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `sessions` (
  `session` binary(32) NOT NULL,
  `uid` binary(16) NOT NULL,
  `time` int(11) NOT NULL,
  `ip` binary(4) NOT NULL,
  `var` varchar(2500) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(2) NOT NULL,
  `active` enum('1','0') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`session`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `test_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mdp` char(128) COLLATE utf8_unicode_ci NOT NULL,
  `mdpNOUV` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `mdp_var` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_level` tinyint(4) NOT NULL DEFAULT 0,
  `race` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `id_alliance` int(11) NOT NULL,
  `id_grade_alliance` bigint(15) NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mailNOUV` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `envoyerMail` smallint(6) NOT NULL,
  `last_ip` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `mv` tinyint(4) NOT NULL DEFAULT 0,
  `multi` smallint(6) NOT NULL,
  `raisonmv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `operateurmv` int(11) NOT NULL,
  `time_inscription` int(11) NOT NULL,
  `last_visite` int(11) NOT NULL DEFAULT 0,
  `points` bigint(20) NOT NULL DEFAULT 0,
  `place_points` smallint(6) NOT NULL DEFAULT 0,
  `batiments` bigint(20) NOT NULL,
  `batiments_place` int(11) NOT NULL,
  `recherches` bigint(20) NOT NULL DEFAULT 0,
  `place_recherches` smallint(6) NOT NULL DEFAULT 0,
  `flotte` bigint(20) NOT NULL DEFAULT 0,
  `place_flotte` smallint(6) NOT NULL DEFAULT 0,
  `terrestre` bigint(20) NOT NULL DEFAULT 0,
  `place_terrestre` smallint(6) NOT NULL DEFAULT 0,
  `planete_source` int(11) NOT NULL,
  `techno_indu` int(11) NOT NULL DEFAULT 0,
  `techno_inge` bigint(20) NOT NULL DEFAULT 0,
  `techno_inge2` int(11) NOT NULL DEFAULT 0,
  `techno_poli` smallint(6) NOT NULL DEFAULT 0,
  `techno_arme` int(11) NOT NULL DEFAULT 0,
  `techno_defe` bigint(20) NOT NULL DEFAULT 0,
  `techno_defe2` int(11) NOT NULL DEFAULT 0,
  `techno_proj` int(11) NOT NULL DEFAULT 0,
  `techno_expansion` int(11) NOT NULL DEFAULT 0,
  `credits` bigint(20) NOT NULL DEFAULT 0,
  `bourse` text COLLATE utf8_unicode_ci NOT NULL,
  `destinationsFavoris` text COLLATE utf8_unicode_ci NOT NULL,
  `amis` text COLLATE utf8_unicode_ci NOT NULL,
  `combatAT_tactique` smallint(6) NOT NULL DEFAULT 0,
  `combatDE_tactique` smallint(6) NOT NULL DEFAULT 0,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `politique` smallint(6) NOT NULL,
  `politique_lastchange` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `user_inscriptions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_activ` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serveur` smallint(6) NOT NULL DEFAULT 1,
  `pseudo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `race` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mdp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `placement` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_inscription` int(11) NOT NULL,
  `last_visite` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `version` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contenu` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `temps` int(11) NOT NULL DEFAULT 0,
  `pseudo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
