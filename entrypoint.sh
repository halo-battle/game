#!/bin/sh

[ -n "$MYSQL_DATABASE" ] && sed -i -r "/db/s/=\s*'([^']*)'/= '${MYSQL_DATABASE}'/" onyx2/db/default.profile.php
# superseed in onyx2/db/default.profile.php
#[ -n "$MYSQL_HOST" ] && sed -i -r "/host/s/=\s*'([^']*)'/= '${MYSQL_HOST}'/" onyx2/db/default.profile.php
[ -n "$MYSQL_USER" ] && sed -i -r "/user/s/=\s*'([^']*)'/= '${MYSQL_USER}'/" onyx2/db/default.profile.php
[ -n "$MYSQL_PASSWORD" ] && sed -i -r "/pass/s/=\s*'([^']*)'/= '${MYSQL_PASSWORD}'/" onyx2/db/default.profile.php

[ -n "$DISABLE_TLS" ] && sed -i -r "/preg_match/s/preg_match\('#^https:/preg_match('#^http:/" onyx2/include/common.php

[ -n "$HB_ALLIANCE_NB_SIGNATURES" ] && sed -i -r "/nb_signatures/s/,\s*[0-9]+)/, ${HB_ALLIANCE_NB_SIGNATURES})/" onyx2/include/common.php
[ -n "$HB_DEBUT_UNIVERS" ] && sed -i -r "/debut_d_univers/s/,\s*(true|false|TRUE|FALSE))/, ${HB_DEBUT_UNIVERS})/" onyx2/include/common.php

[ -n "$HB_SERVER_NAME" ] && sed -i -r "/serveur_name/s/>(.*)</>${HB_SERVER_NAME}</" onyx2/config/root.xml
[ -n "$HB_VITESSE" ] && sed -i -r "/vitesse/s/>(.*)</>${HB_VITESSE}</" onyx2/config/root.xml
[ -n "$HB_ENABLE_ATTAQUES" ] && sed -i -r "/attaques/s/>(.*)</>${HB_ENABLE_ATTAQUES}</" onyx2/config/root.xml
[ -n "$HB_NB_AMAS" ] && sed -i -r "/nb_amas/s/>(.*)</>${HB_NB_AMAS}</" onyx2/config/root.xml
[ -n "$HB_NB_SYSTEMS" ] && sed -i -r "/nb_systeme/s/>(.*)</>${HB_NB_SYSTEMS}</" onyx2/config/root.xml
[ -n "$HB_NB_PLANETES" ] && sed -i -r "/nb_planete/s/>(.*)</>${HB_NB_PLANETES}</" onyx2/config/root.xml
[ -n "$HB_MAIL_ADMIN" ] && sed -i -r "/mail_admin/s/>(.*)</>${HB_MAIL_ADMIN}</" onyx2/config/root.xml
[ -n "$HB_MAIL_SUPPORT" ] && sed -i -r "/mail_support/s/>(.*)</>${HB_MAIL_SUPPORT}</" onyx2/config/root.xml

[ -n "$SMTP_SERVER" ] && sed -i -r "/mail->Host/s/=\s*'(.*)'/='${SMTP_SERVER}'/" onyx2/include/server/inscription.php

exec docker-php-entrypoint $@
