# Halo-Battle

## Développer
HB peut petre lancé localement via un docker. Pour cela, vous devez avoir installé Docker et docker-compose.
une fois installé, lancez un `docker-compouse up` à la racine du repo. Une fois que tout à boot, vous devez
injecter le code SQL avec `docker exec -i game_mariadb_1 mysql --user=hb --password=hb hb_game < schema.sql`

HB est alors disponible sur http://localhost:8080.

À la création du compte, aucun email n'est envoyé. Pour récupérer votre code de confirmation, vous devez
vous connecter au serveur MySQL via `docker exec -it game_mariadb_1 mysql --user=hb --password=hb hb_game`
et récupérer votre jeton d'activation via `select id_activ from user_inscriptions WHERE pseudo LIKE 'VOTRE_PSEUDO_A_REMPLACER';`
une fois l'ID récupérer, rendez-vous à l'adresse http://localhost:8080/?p=validation&i=VOTRE_ID (pensez
bien à modifier l'id dans l'URL).

## Installer
Un cron doit être rajouté sur la machine hôte pour calculer le classement des joueurs, en éxécutant le fichier `cron/classement.php` aussi souvent qu'il vous plaira (ou aussi souvent que votre CPU le permettra).
