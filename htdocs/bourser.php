<?php
//Définition de la constante anti-hacking
define("INDEX", 1);

//Inclusion de l'API Onyx
require_once(trim(file_get_contents('./.onyx')));
require_once("common.php"); //Chargement de tout le nécessaire pour le jeu

require_once("function.php"); //Inclusion des fonctions principales
require_once("tables.php"); //Inclusion des noms des tables de base de données correspondant à l'architecture du serveur

function nomR($string, $race)
{
    if ($race == "covenant") {
        if ($string == 1) {
            return "Cristal";
        } elseif ($string == 2) {
            return "Ions";
        } else {
            return "Metal";
        }
    } else {
        if ($string == 1) {
            return "Cristal";
        } elseif ($string == 2) {
            return "Hydrogene";
        } else {
            return "Metal";
        }
    }
}

$action = intval(gpc('a'));
if (isset($sess->values['race'])) {
    $race = $sess->values['race'];
} else {
    $race = gpc('r');
}

$bdd = new BDD();
$act = $bdd->unique_query("SELECT id, graph, dispo FROM $table_bourse_ressources WHERE id = $action;");
$bdd->deconnexion();
if (empty($act)) {
    exit;
}

$values = array();
$values_label = array();
$values_abscisse = array();

$act['graph'] = unserialize($act['graph']);

for ($i = 1; $i < 8; $i++) {
    if (!isset($act['graph'][(date('w')+$i)%7])) {
        $values[] = 0;
    } else {
        $values[] = ceil(pow($act['graph'][(date('w')+$i)%7], -0.1) * 20000)/10;
    }
}

for ($i = 6; $i >= 0; $i--) {
    $values_label[] = $values[$i];
    $values_abscisse[] = ucfirst(strftime('%a', time() - 86400 * $i));
}

//*
require_once("jpgraph/jpgraph.php");
require_once("jpgraph/jpgraph_line.php");

if ($race != "humain" && $race != "covenant") {
    $race = "humain";
}

$graph = new Graph(282, 200);
$graph->SetScale("textlin");
$graph->img->SetMargin(40, 20, 20, 40);

$lineplot=new LinePlot($values);
$lineplot->mark->SetType(MARK_UTRIANGLE);
$lineplot->value->show();
$lineplot->value->SetFont(FF_FONT1, FS_BOLD);
//$lineplot->value->SetFormat('%0f');

//Définition des couleurs
$graph->SetBackgroundImage(ONYX."include/content/bourse/".$race.".png", BGIMG_COPY);
if ($race == "covenant") {
    $graph->yaxis->SetColor("#FFFFFF");
    $graph->xaxis->SetColor("white");
    $lineplot->SetFillColor("pink@0.8");
    $lineplot->value->SetColor('#FF0000');
    $graph->yaxis->title->SetColor("white");
    $graph->title->SetColor("white");
    $lineplot->SetColor("#c00000");
} else {
    $graph->yaxis->SetColor("#00DD00");
    $lineplot->SetFillColor("cyan@0.8");
    $lineplot->value->SetColor('#FF0000');
    $graph->yaxis->title->SetColor("white");
    $graph->title->SetColor("white");
    $lineplot->SetColor("#3bb3ff");
}

$act['nom'] = nomR($act['id'], $race);

$graph->Add($lineplot);
$graph->title->Set($act['nom']);
$graph->yaxis->title->Set("Evolution (%)");
$graph->xaxis->SetTickLabels($values_abscisse);

$graph->title->SetFont(FF_FONT1, FS_BOLD);
$graph->yaxis->title->SetFont(FF_FONT1, FS_BOLD);
$graph->xaxis->title->SetFont(FF_FONT1, FS_BOLD);

$lineplot->SetWeight(2);

$graph->Stroke();
