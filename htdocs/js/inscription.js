window.onload = function(){
	document.getElementById('pseudo').onfocus = function(){
		document.getElementById('description').innerHTML = '<b>Pseudo</b><br /><br />Votre pseudo est le nom que vous porterez dans le jeu.<br /><br />Vous pouvez utiliser toutes les lettres, les chiffres ainsi que les caract&egrave;res _ et .<br /><br /><p style="color:#FF0000"><b>Attention :</b> ce pseudo est d&eacute;finitif, vous ne pourrez pas le changer par la suite.</p>';
	}
	document.getElementById('mdp').onfocus = function(){
		document.getElementById('description').innerHTML = '<b>Mot de passe</b><br /><br /><p>Le mot de passe sert &agrave; emp&ecirc;cher d&#39;autres joueurs d&#39;acc&egrave;der &egrave; votre compte.<br /><br />Choisissez de pr&eacute;f&eacute;rence un mot de passe compliqu&eacute; avec <b>au minimum 6 caract&egrave;res.</b><br /><br />Une fois dans le jeu, vous pourrez modifier votre mot de passe via la page Options.</p>';
	}
	document.getElementById('conf').onfocus = function(){
		document.getElementById('description').innerHTML = '<b>Confirmez votre mot de passe</b><br /><br /><p>Recopiez le mot de passe que vous venez d&#39;&eacute;crire dans le champs pr&eacute;c&eacute;dent.</p>';
	}
	document.getElementById('mail').onfocus = function(){
		document.getElementById('description').innerHTML = '<b>Adresse &eacute;lectronique</b><p><br /><br />Afin de vous tenir inform&eacute; des principales informations sur le jeu et pour valider votre compte, nous vous demandons de bien vouloir indiquer votre adresse &eacute;lectronique.<br /><br />Nous ne diffusons pas vos adresses et/ou informations personnelles &agrave; des sites tiers ou commerciaux.<br /><br /><p style="color:#FF0000"><b>Attention :</b> nous avons remarqu&eacute; des ralentissements lors de l&#39;envoie de mail pour les adresses @hotmail, @live et @msn. Nous vous recommandons d&#39;utiliser d&#39;autres adresses mail.<br /><br />Les mails envoy&eacute;s sont souvent dans le dossier du courrier ind&eacute;sirable, pensez &agrave; y jeter un oeil !</p></p>';
	}
	document.getElementById('covenant').onmouseover = function(){
		document.getElementById('description').innerHTML = '<b>Covenants</b><br /><br /><p>Ce regroupement religieux de races extraterrestres h&eacute;t&eacute;roclites voue un culte fanatique aux Sept Anneaux Sacr&eacute;s et aux Forerunners depuis la date de fondation de l&#39;empire Covenant. Organis&eacute; en gouvernement th&eacute;ocratique et se d&eacute;pla&ccedil;ant &agrave; bord d&#39;une gigantesque plan&egrave;te artificielle, Grande Bont&eacute;, le collectif Covenant assume une existence mill&eacute;naire. Disposants d&#39;une technologie bien sup&eacute;rieure &agrave; l&#39;Humanit&eacute;, et de territoires vastes et f&eacute;rocement gard&eacute;s, les Covenants se sont lanc&eacute;s dans un combat &agrave; mort sans piti&eacute; contre leurs ennemis impies, esp&eacute;rant bien les rayer de la carte de la galaxie &agrave; jamais, conform&eacute;ment &agrave; la volont&eacute; de leurs "Seigneurs".</p>';
	}
	document.getElementById('covenanti').onfocus = function(){
		document.getElementById('description').innerHTML = '<b>Covenants</b><br /><br /><p>Ce regroupement religieux de races extraterrestres h&eacute;t&eacute;roclites voue un culte fanatique aux Sept Anneaux Sacr&eacute;s et aux Forerunners depuis la date de fondation de l&#39;empire Covenant. Organis&eacute; en gouvernement th&eacute;ocratique et se d&eacute;pla&ccedil;ant &agrave; bord d&#39;une gigantesque plan&egrave;te artificielle, Grande Bont&eacute;, le collectif Covenant assume une existence mill&eacute;naire. Disposants d&#39;une technologie bien sup&eacute;rieure &agrave; l&#39;Humanit&eacute;, et de territoires vastes et f&eacute;rocement gard&eacute;s, les Covenants se sont lanc&eacute;s dans un combat &agrave; mort sans piti&eacute; contre leurs ennemis impies, esp&eacute;rant bien les rayer de la carte de la galaxie &agrave; jamais, conform&eacute;ment &agrave; la volont&eacute; de leurs "Seigneurs".</p>';
	}
	document.getElementById('humain').onmouseover = function(){
		document.getElementById('description').innerHTML = '<b>Humains</b><br /><br /><p>Race audacieuse et conqu&eacute;rante, l&#39;Humanit&eacute; s&#39;est lanc&eacute;e &agrave; la conqu&ecirc;te des &eacute;toiles au 22&egrave;me si&egrave;cle, fondant dans un premier temps quelques colonies &eacute;parses aux abords de la Terre, pour ensuite s&#39;enfoncer de plus en plus loin &agrave; travers la galaxie. Rest&eacute;e isol&eacute;e du reste de l&#39;univers pendant tr&egrave;s longtemps et se croyant seule au monde, la confrontation avec les Covenants a rapidement chang&eacute; la donne. Aujourd&#39;hui, bien que d&eacute;tentrice d&#39;une technologie relativement avanc&eacute;e, l&#39;Humanit&eacute; se voit forc&eacute;e d&#39;engager un combat d&eacute;sesp&eacute;r&eacute; contre les Covenants, ceux-ci renfor&ccedil;ant un peu plus chaque jours leur emprise sur le territoire morcel&eacute; des derniers hommes.</p>';
	}
	document.getElementById('humaini').onfocus = function(){
		document.getElementById('description').innerHTML = '<b>Humains</b><br /><br /><p>Race audacieuse et conqu&eacute;rante, l&#39;Humanit&eacute; s&#39;est lanc&eacute;e &agrave; la conqu&ecirc;te des &eacute;toiles au 22&egrave;me si&egrave;cle, fondant dans un premier temps quelques colonies &eacute;parses aux abords de la Terre, pour ensuite s&#39;enfoncer de plus en plus loin &agrave; travers la galaxie. Rest&eacute;e isol&eacute;e du reste de l&#39;univers pendant tr&egrave;s longtemps et se croyant seule au monde, la confrontation avec les Covenants a rapidement chang&eacute; la donne. Aujourd&#39;hui, bien que d&eacute;tentrice d&#39;une technologie relativement avanc&eacute;e, l&#39;Humanit&eacute; se voit forc&eacute;e d&#39;engager un combat d&eacute;sesp&eacute;r&eacute; contre les Covenants, ceux-ci renfor&ccedil;ant un peu plus chaque jours leur emprise sur le territoire morcel&eacute; des derniers hommes.</p>';
	}
	document.getElementById('servers').onfocus = function(){
		document.getElementById('description').innerHTML = '<b>Serveurs</b><br /><br /><p>Choisissez un serveur dans la liste.</p>';
	}
	document.getElementById('placement').onfocus = function(){
		document.getElementById('description').innerHTML = '<b>Placement pr&eacute;f&eacute;rentiel</b><br /><br /><p>Ce champ est facultatif.<br /><br />Si un de vos amis joue &agrave; Halo&#45;Battle sur le serveur que vous avez s&eacute;lectionn&eacute; ci&#45;dessus, vous pouvez demander d&#39;&ecirc;tre plac&eacute; &agrave; c&ocirc;t&eacute; de l&#39;une de ses plan&egrave;tes en indiquant simplement son pseudo.<br /><br />Sous r&eacute;serve de disponibilit&eacute;.</p>';
	}
	document.getElementById('captcha').onfocus = function(){
		document.getElementById('description').innerHTML = '<b>Captcha</b><br /><br /><p>Pour s&#39;assurer que vous &ecirc;tes un humains et non pas un robot, nous vous demandons de bien vouloir recopier le texte situ&eacute; contre cette case.<br /><br />Vous pouvez g&eacute;n&eacute;rer une autre cha&icirc;ne en cliquant sur l&#39;image<noscript> (requiert d&#39;avoir JavaScript d&#39;activ&eacute;)</noscript>.<br /><br />Si vous &ecirc;tes visuellement d&eacute;ficient et que vous ne pouvez pas voir le texte, <a href="mailto:staff@halo-battle.s-fr.com">contactez un administrateur</a>.</p>';
	}
	document.getElementById('gen').onclick = function(){
		i++;
		rand = Math.random() + i
		document.getElementById('gen').src='captcha/image.php?'+rand;
	}

	var i = 0;
	window.onload = dateTempsReel();
	rand = Math.random() + i
	document.getElementById('gen').src='captcha/image.php?'+rand;
}