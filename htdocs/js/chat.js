var chat_lastTime = 0;
var chat_refresh_time = 1500;
var chat_refresh = setTimeout("chat_MAJ()", 500);

function chat_sendMessage()
	{
		new Ajax.Request(
		 'ajax_chat.php',
		 {
			 method: 'post',
			parameters: {message: $('msg').value},
			onSuccess: function() { $('msg').value = "";  clearTimeout(chat_refresh); chat_MAJ(); },
			onFailure: function() { if (confirm("La requête a échouée, voulez-vous réessayer de renvoyer votre message ?")) chat_sendMessage(); }
		 }
		);
		return false;
	}

function chat_MAJ()
	{
		new Ajax.Request(
		 'ajax_chat.php',
		 {
			method: 'get',
			parameters: {time: chat_lastTime},
			onSuccess: function(transport, json)
				{
					if (json.length > 0)
					{
						for(i=json.length-1; i>=0; i--)
						{
							//On tranforme le timestamp en date correcte
							var date = new Date();
							var now = new Date();
							date.setTime(json[i]["timestamp"]*1000);

							//On affiche tout
							var newRow = $('chat').insertRow(0);
							var newCell = newRow.insertCell(0);
							if (date.getDay() != now.getDay())
								newCell.innerHTML = '[' + json[i]["pseudo"] + '] ' + (date.getDay()<10?"0"+date.getDay():date.getDay()) + "/" + (date.getMonth()<10?"0"+date.getMonth():date.getMonth()) + " " + (date.getHours()<10?"0"+date.getHours():date.getHours()) + ":" + (date.getMinutes()<10?"0"+date.getMinutes():date.getMinutes()) + ":" + (date.getSeconds()<10?"0"+date.getSeconds():date.getSeconds());
							else
								newCell.innerHTML = '[' + json[i]["pseudo"] + '] ' + (date.getHours()<10?"0"+date.getHours():date.getHours()) + ":" + (date.getMinutes()<10?"0"+date.getMinutes():date.getMinutes()) + ":" + (date.getSeconds()<10?"0"+date.getSeconds():date.getSeconds());
	    					newCell = newRow.insertCell(1);
							newCell.innerHTML = json[i]["message"];
							if (chat_lastTime < json[i]["timestamp"])
								chat_lastTime = json[i]["timestamp"];
						}
					}
				},
			onFailure: function() { chat_MAJ(); }
		 }
		);
		chat_refresh = setTimeout("chat_MAJ()", chat_refresh_time);
	}