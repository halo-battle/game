window.onload = function(){
	document.getElementById('nom').onfocus = function(){
		document.getElementById('aide1').innerHTML = 'Pour vous repérer plus facilement entre vos différentes flottes, donnez-lui un nom.';
		document.getElementById('aide2').innerHTML = '';
	}
	document.getElementById('fav_dest').onfocus = function(){
		document.getElementById('aide1').innerHTML = 'Choisissez dans la liste une destination ou entrez les coordonnées directement dans les champs ci-après.';
		document.getElementById('aide2').innerHTML = '';
	}
	document.getElementById('amas').onfocus = function(){
		document.getElementById('aide1').innerHTML = 'Indiquez ici l\'amas de destination de votre flotte.';
		document.getElementById('aide2').innerHTML = '';
	}
	document.getElementById('ss').onfocus = function(){
		document.getElementById('aide1').innerHTML = 'Indiquez ici le système de destination de votre flotte.';
		document.getElementById('aide2').innerHTML = '';
	}
	document.getElementById('plan').onfocus = function(){
		document.getElementById('aide1').innerHTML = 'Indiquez ici la planète de destination de votre flotte.';
		document.getElementById('aide2').innerHTML = '';
	}
	document.getElementById('vitesse').onfocus = function(){
		document.getElementById('aide1').innerHTML = 'Choisissez le taux de vitesse de votre flotte. Plus la flotte ira vite, plus elle consomera de carburant';
		document.getElementById('aide2').innerHTML = '';
	}
	document.getElementById('mission').onfocus = function(){
		document.getElementById('aide1').innerHTML = '';
		document.getElementById('aide2').innerHTML = 'Choisissez la mission que vous voulez donner à votre flotte.';
	}
	document.getElementById('metal').onfocus = function(){
		document.getElementById('aide1').innerHTML = '';
		document.getElementById('aide2').innerHTML = 'Si vous souhaitez transporter des ressources dans les cales de vos vaisseaux, indiquez-le dans ces trois champs';
	}
	document.getElementById('cristal').onfocus = function(){
		document.getElementById('aide1').innerHTML = '';
		document.getElementById('aide2').innerHTML = 'Si vous souhaitez transporter des ressources dans les cales de vos vaisseaux, indiquez-le dans ces trois champs';
	}
	document.getElementById('hydrogene').onfocus = function(){
		document.getElementById('aide1').innerHTML = '';
		document.getElementById('aide2').innerHTML = 'Si vous souhaitez transporter des ressources dans les cales de vos vaisseaux, indiquez-le dans ces trois champs';
	}

	document.getElementById('nom').onkeyup = function(){
		document.getElementById('vp').innerHTML = "Chargement en cours ...";
		tempsFlotte();
	}
	document.getElementById('amas').onkeyup = function(){
		document.getElementById('vp').innerHTML = "Chargement en cours ...";
		tempsFlotte();
	}
	document.getElementById('ss').onkeyup = function(){
		document.getElementById('vp').innerHTML = "Chargement en cours ...";
		tempsFlotte();
	}
	document.getElementById('vitesse').onkeyup = function(){
		document.getElementById('vp').innerHTML = "Chargement en cours ...";
		tempsFlotte();
	}
	document.getElementById('plan').onkeyup = function(){
		document.getElementById('vp').innerHTML = "Chargement en cours ...";
		tempsFlotte();
	}
	document.getElementById('mission').onkeyup = function(){
		document.getElementById('vp').innerHTML = "Chargement en cours ...";
		tempsFlotte();
	}
	document.getElementById('metal').onkeyup = function(){
		document.getElementById('vp').innerHTML = "Chargement en cours ...";
		tempsFlotte();
	}
	document.getElementById('cristal').onkeyup = function(){
		document.getElementById('vp').innerHTML = "Chargement en cours ...";
		tempsFlotte();
	}
	document.getElementById('hydrogene').onkeyup = function(){
		document.getElementById('vp').innerHTML = "Chargement en cours ...";
		tempsFlotte();
	}

	document.getElementById('fav_dest').onchange = function(){
		if (document.getElementById('fav_dest').value == "edit") {
			window.open("?p=destinationsrapides");
			document.getElementById('fav_dest').value = "0";
		}
		else {
			document.getElementById('vp').innerHTML = "Chargement en cours ...";
			tempsFlotte();
		}
	}
}

function tempsFlotte() {
	document.getElementById('deblok').innerHTML = '...';
	var url_string = (window.location.href).toLowerCase();
	var url = new URL(url_string);
	var cds = url.searchParams.get("c");
	new Ajax.Request(
		'ajax_flotte.php',
		{
			method: 'post',
			parameters: {
				cds_temp: cds,
				fav_dest: document.getElementById('fav_dest').value,
				nom: document.getElementById('nom').value,
				mission: document.getElementById('mission').value,
				met: document.getElementById('metal').value,
				cri: document.getElementById('cristal').value,
				hyd: document.getElementById('hydrogene').value,
				galaxie: document.getElementById('amas').value,
				ss: document.getElementById('ss').value,
				pos: document.getElementById('plan').value,
				vitesse: document.getElementById('vitesse').value
			},
			onSuccess: function(transport, json) {
				document.getElementById('destination').innerHTML = json.root.destination;
				document.getElementById('temps').innerHTML = json.root.temps;
				document.getElementById('conso').innerHTML = json.root.conso;
				document.getElementById('deblok').innerHTML = json.root.deblok;
				document.getElementById('tactique').innerHTML = json.root.tactique;
				document.getElementById('placesRest').innerHTML = json.root.places;
				document.getElementById('vp').innerHTML = "";
			}
		}
	);
}
