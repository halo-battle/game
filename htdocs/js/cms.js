﻿function dateTempsReel()
	{
		var days = new Array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
		var months = new Array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
		date = new Date;
		date.setTime(date.getTime());
		var hour = date.getHours() < 10 ? '0'+date.getHours() : date.getHours();
		var min = date.getMinutes() < 10 ? '0'+date.getMinutes() : date.getMinutes();
		var day = days[date.getDay()];
		var day_number = date.getDate();
		var month = months[date.getMonth()];
		var year = date.getFullYear();
		var datetime = day + ' ' + day_number + ' ' + month + ' '+year+' ' + hour + ':' + min;
		if (document.getElementById('date')) document.getElementById('date').innerHTML= datetime;
		setTimeout('dateTempsReel()', (60-date.getSeconds())*1000)
	}

window.onload = dateTempsReel;