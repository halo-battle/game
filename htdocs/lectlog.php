<html>
	<head>
		<title>.: Halo-Battle :: Lecteur de log :.</title>
		<style>
		acronym {
			text-decoration: underline;
			underliner: dot;
		}
		</style>
	</head>
	<body>
		<h2>Lecteur de log HB</h2>
<?php
define('dir', '../onyx2/log');

if (!is_dir(dir)) {
    die('Dossier introuvable !');
}

if (empty($_GET['file']) || is_dir(dir.$_GET['file'])) {
    if (!empty($_GET['file']) && str_replace("..", '', $_GET['file']) != $_GET['file']) {
        die('Accès au fichier non autorisé !');
    } elseif (empty($_GET['file'])) {
        $_GET['file'] = '';
    }

    $pointeur = opendir(dir.$_GET['file']);
    print 'Dossier en cours : '.dir.$_GET['file'].'<br /><br />';
    while ($fichier = readdir($pointeur)) {
        if ($fichier == '.' || $fichier == '..') {
            continue;
        }

        print '<a href="?file='.$_GET['file'].'/'.$fichier.'">'.$fichier.'</a><br />';
    }
} else {
    if (!empty($_GET['file']) && str_replace("..", '', $_GET['file']) != $_GET['file']) {
        die('Accès au fichier non autorisé !');
    }
    $file = dir.$_GET['file'];

    print '<h3><ins>Fichier :</ins> '.$file.'</h3><hr />';

    if (!is_file($file)) {
        die('Fichier introuvable !');
    }

    $fichier = @fopen($file, 'r+');
    $contenu_log = fread($fichier, filesize($file));
    fclose($fichier);

    $exp = "#\[(\d{2}/\d{2}/\d{2} \d{2}:\d{2}:\d{2})\] (\\w{6,13}) : ([^\[]{0,1000}) \[(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\] \[(\w{3,4})\] \[/([^\[]{1,200})\] \[([^\[]{0,200})\] \[([^\[]{0,75})\]#ui";

    preg_match_all($exp, htmlentities(trim(str_replace("\n", ' ', $contenu_log))), $m);

    if (empty($m[0][0])) {
        print '<pre>'.$contenu_log.'</pre>';
    } else {
        print '<table border="1">
			<thead>
				<tr>
					<th>Date</th>
					<th>Type</th>
					<th>Message</th>
					<th>IP + ...</th>
				</tr>
			</thead>
			<tbody>';
        foreach ($m[0] as $key => $lign) {
            if (preg_match('#array#', $m[3][$key])) {
                $exp = "#array \((   '(.{1,10})' =&gt; '([^',]{0,1000})',)(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?(   '(.{1,10})' =&gt; '([^',]{0,1000})',)?#ui";

                preg_match_all($exp, $m[3][$key], $mArray);

                $m[3][$key] = '';
                foreach ($mArray as $k => $lign) {
                    if ($k == 0 || $k%3 == 1) {
                        continue;
                    }

                    if ($lign[0] == "") {
                        break;
                    }
                    if ($k%3 == 2) {
                        $m[3][$key] .= '<b>'.$lign[0].' : </b>';
                    } else {
                        $m[3][$key] .= (strlen($lign[0])>80?'<acronym title="'.$lign[0].'">'.substr($lign[0], 0, 77).'...</acronym>':$lign[0]).'<br />';
                    }
                }
            }

            if ($m[2][$key] == "ERREUR") {
                print '<tr style="background: #FF0000;">';
            } elseif ($m[2][$key] == "AVERTISSEMENT") {
                print '<tr style="background: #FFCC00;">';
            } else {
                print '<tr>';
            }
            print '
				<td>'.$m[1][$key].'<br />'.$m[5][$key].'</td>
				<td>'.$m[2][$key].'<br />'.(strlen($m[6][$key])>30?'<acronym title="'.$m[6][$key].'">'.substr($m[6][$key], 0, 27).'...</acronym>':$m[6][$key]).'</td>
				<td>'.$m[3][$key].'</td>
				<td><acronym title="'.$m[7][$key].'">'.$m[4][$key].'</acronym><br />'.$m[8][$key].'</td>
			</tr>';
        }
        print '</tbody></table>';
    }
}
?>
	</body>
</html>