<?php
//Définition de la constante anti-hacking
define("INDEX", 1);
define("DEBUG", true);

//Inclusion de l'API Onyx
require_once(trim(file_get_contents('../.onyx')));
require_once("common.php"); //Chargement de tout le nécessaire pour le jeu
require_once("donnees.php");
require_once('Class/flotte.php');
require_once('Class/planete.php');

$planete1 = new Planete(0);
$planete1->galaxie = 1;
$planete1->ss = 1;
$planete1->position = 1;

$planete2 = new Planete(0);
$planete2->galaxie = 1;
$planete2->ss = 1;
$planete2->position = 2;

$vaisseaux = array(1);

print Flotte::calc_deplacement($planete1, 1, 1, 2, 1, $vaisseaux, 123456789);
