<?php
//Définition de la constante anti-hacking
define("INDEX", 1);
define("DEBUG", true);

//Inclusion de l'API Onyx
require_once(trim(file_get_contents('../.onyx')));
require_once("common.php"); //Chargement de tout le nécessaire pour le jeu
require_once("donnees.php");
require_once('Class/planete.php');

$SESS = new Session();

$planete = new Planete(7);

$file = unserialize($SESS->values["serial"]);
//$file = new FileSpatial();

$file->ready($planete);
print date('d/m/Y H:i:s', $file->DEBUG__setTime(0, 90));
var_dump($file->checkMaxSize($planete));
//$file->addObjet(0, 1, $planete);

print 'ICI';
var_dump($file->objectInFile(0));
var_dump($file->nbObjectInFile(0));

$SESS->values["serial"] = serialize($file);
$SESS->put();

$file->DEBUG__print();
var_dump($planete->vaisseaux);
var_dump($planete->modif);
