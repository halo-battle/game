<?php
define('INDEX', 1);
//Inclusion de l'API Onyx
require_once(trim(file_get_contents('./.onyx')));

$SESS = new Session();

//On défini la configuration
if (!isset($_GET['nbr_chiffres']) || $_GET['nbr_chiffres'] < 5) {
    $nbr_chiffres = mt_rand(7, 9);
} // Nombre de chiffres qui formerons le nombre par défaut
else {
    $nbr_chiffres = intval(gpc('nbr_chiffres'));
} // Si l'on met dans l'adresse un ?nbr_chiffres=X

header("Content-type: image/png");
//Création de l'image à partir d'un fond
$_img = imagecreatefromgif('fond_verif_img.gif');

//On définit une couleur de fond
//$arriere_plan = imagecolorallocate($_img, 0, 0, 0); // Au cas où on utiliserai pas d'image de fond, on utilise cette couleur là.
//On définit les autres couleurs
$couleur = mt_rand(0, 4);
    if ($couleur == 0) {
        $avant_plan = imagecolorallocate($_img, rand(250, 225), rand(0, 25), rand(0, 25));
    } elseif ($couleur == 1) {
        $avant_plan = imagecolorallocate($_img, rand(0, 25), rand(250, 225), rand(0, 25));
    }
//	elseif ($couleur == 2) $avant_plan = imagecolorallocate($_img, rand(0,55), rand(0,55), rand(250,255));
    elseif ($couleur == 3) {
        $avant_plan = imagecolorallocate($_img, rand(0, 25), rand(250, 225), rand(250, 225));
    } elseif ($couleur == 4) {
        $avant_plan = imagecolorallocate($_img, rand(250, 225), rand(0, 25), rand(250, 225));
    } elseif ($couleur == 5) {
        $avant_plan = imagecolorallocate($_img, rand(250, 225), rand(250, 225), rand(0, 25));
    } else {
        $avant_plan = imagecolorallocate($_img, rand(200, 225), rand(200, 225), rand(200, 225));
    }

//Définition de la liste des caractères
$caracteres = "2345678azertypsdfhjkmwxcbn?";
$nb_caracteres = strlen($caracteres)-1;
$captcha = "";

for ($i = 0; $i < $nbr_chiffres; $i++) {
    $alea = mt_rand(0, $nb_caracteres);
    $captcha .= $caracteres[$alea];
}

$SESS->values['_captcha'] = strtolower($captcha);
$SESS->put();

//On détruit les variables inutiles
unset($chiffre, $i, $caractere, $nbr_chiffres, $nb_caracteres, $caracteres);

//On définit la police
//$fontfile = './'.mt_rand(0, 2).'.TTF';
$fontfile = './2.TTF';

//imagestring($_img, 5, 18, 8, $nombre, $avant_plan);
if ($fontfile == './0.TTF') {
    ImageTTFText($_img, 20, 2, 0, 23, $avant_plan, $fontfile, strtoupper($captcha));
} else {
    ImageTTFText($_img, 20, mt_rand(1, 5), rand(1, 7), 30, $avant_plan, $fontfile, $captcha);
}

imagepng($_img);
