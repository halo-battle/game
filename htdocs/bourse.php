<?php
define("INDEX", 1);
$chrono_start = microtime();
$onyx = @file_get_contents('./.onyx') or die("Configuration introuvable.");
define("_FCORE", trim($onyx));
require_once(_FCORE."kernel.php");
require_once(_FCORE."../game/tables.php");

$action = intval(gpc('a'));
if (isset($sess->values['race'])) {
    $race = $sess->values['race'];
} else {
    $race = gpc('r');
}

$bdd = new BDD();
$bdd->connexion();
$act = $bdd->unique_query("SELECT nom FROM $table_bourse WHERE id = $action;");
$bdd->deconnexion();
if (empty($act)) {
    exit;
}

$values = array();
$values_label = array();
$values_abscisse = array();

//Extraction des derniers pourcentages
$filename = '';
$i = 6;
while (!is_file($filename)) {
    $i++;
    if ($i > 14) {
        break;
    }
    $filename = _FCORE."../game/cache/bourse/".$action.".".strftime('%Y%m%d', time() - 86400 * $i).".bourse";
}
if (!is_file($filename)) {
    $i = 7;
    while (!is_file($filename)) {
        $i--;
        if ($i < 0) {
            break;
        }
        $filename = _FCORE."../game/cache/bourse/".$action.".".strftime('%Y%m%d', time() - 86400 * $i).".bourse";
    }
}
if (is_file($filename)) {
    $handle = fopen($filename, "r");
    while (!feof($handle)) {
        $r = fgets($handle);
        if (preg_match('#;#', $r)) {
            $last = $r;
        }
    }
    fclose($handle);
    $calc = $last = explode(';', $last);
} else {
    $calc = $last = array(1, 1, 1);
}

for ($i = 6; $i >= 0; $i--) {
    $filename = _FCORE."../game/cache/bourse/".$action.".".strftime('%Y%m%d', time() - 86400 * $i).".bourse";
    if (is_file($filename)) {
        $handle = fopen($filename, "r");
        while (!feof($handle)) {
            $r = fgets($handle);
            if (preg_match('#;#', $r)) {
                $last = $r;
            }
        }
        fclose($handle);
        $last = explode(';', $last);
    }
    $evolution = intval(($last[1]/$calc[1] - 1) * 100);
    $values[] = $evolution;
    if ($evolution >= 0) {
        $evolution = '+'.$evolution;
    }
    $values_label[] = $evolution.'%';
    $values_abscisse[] = ucfirst(strftime('%a', time() - 86400 * $i));
}

//*
require_once("../jpgraph/jpgraph.php");
require_once("../jpgraph/jpgraph_line.php");

if ($race != "humain" && $race != "covenant") {
    $race = "humain";
}

$graph = new Graph(282, 200);
$graph->SetScale("textlin");
$graph->img->SetMargin(40, 20, 20, 40);

$lineplot=new LinePlot($values);
$lineplot->mark->SetType(MARK_UTRIANGLE);
$lineplot->value->show();
$lineplot->value->SetFont(FF_FONT1, FS_BOLD);
$lineplot->value->SetFormat('%0.1f');

//Définition des couleurs
$graph->SetBackgroundImage(_FCORE."../game/bourse/".$race.".png", BGIMG_COPY);
if ($race == "covenant") {
    $graph->yaxis->SetColor("#FFFFFF");
    $graph->xaxis->SetColor("white");
    $lineplot->SetFillColor("pink@0.8");
    $lineplot->value->SetColor('#FF0000');
    $graph->yaxis->title->SetColor("white");
    $graph->title->SetColor("white");
    $lineplot->SetColor("#c00000");
} else {
    $graph->yaxis->SetColor("#00DD00");
    $lineplot->SetFillColor("cyan@0.8");
    $lineplot->value->SetColor('#FF0000');
    $graph->yaxis->title->SetColor("white");
    $graph->title->SetColor("white");
    $lineplot->SetColor("#3bb3ff");
}


$graph->Add($lineplot);
$graph->title->Set($act['nom']);
$graph->yaxis->title->Set("Evolution (%)");
$graph->xaxis->SetTickLabels($values_abscisse);

$graph->title->SetFont(FF_FONT1, FS_BOLD);
$graph->yaxis->title->SetFont(FF_FONT1, FS_BOLD);
$graph->xaxis->title->SetFont(FF_FONT1, FS_BOLD);

$lineplot->SetWeight(2);

$graph->Stroke();
exit;
//*/
require_once(_FCORE."../artichow/LinePlot.class.php");

$graph = new Graph(282, 200);
$graph->setAntiAliasing(false);
$graph->border->setColor(new Color(210, 210, 210));
$graph->title->set($act['nom']);
$graph->title->setFont(new TuffyBold(14));
$graph->title->setBackgroundColor(new White(50));
$graph->title->setPadding(5, 5, 2, 2);
$graph->title->border->setColor(new Black());

// Les valeurs à afficher sur la courbe
//$values = array(1, 4, 5, -2.5, 3);
// On créé la courbe
$plot = new LinePlot($values);

$plot->setColor(new Color(210, 210, 210));
$plot->yAxis->title->set("Evolution (%)");

// Ajoute un dégradé de fond
$plot->setBackgroundColor(new Color(240, 240, 240));
$plot->setFillColor(new Color(180, 180, 180, 75));
$plot->label->set($values_label);
$plot->label->setBackgroundGradient(
    new LinearGradient(
        new Color(250, 250, 250, 10),
        new Color(255, 200, 200, 30),
        0
    )
);
$plot->label->border->setColor(new Color(20, 20, 20, 20));
$plot->label->setPadding(3, 1, 1, 0);

$plot->xAxis->setLabelText($values_abscisse);

// On ajoute 5 % d'espace à gauche et à droite de la courbe.
// On ne change pas l'espace du haut et du bas de la courbe.
$plot->setSpace(
    5, /* Gauche */
   5, /* Droite */
   null, /* Haut */
   null /* Bas */
);

$graph->add($plot);

$graph->draw();
