<?php
define("INDEX", 1);
define("DEBUG", false);

//Inclusion de l'API Onyx
require_once(trim(file_get_contents('./.onyx')));
require_once("common.php"); //Chargement de tout le nécessaire pour le jeu

//On vérifie si le client est connecté ou non sur le site
if (isset($SESS) && isset($SESS->values['connected']) && $SESS->values['connected'] && !empty($SESS->values['id']) && !empty($SESS->level) && $SESS->level >= 3) {
    function infoPlan($galaxie, $ss, $pos, $info)
    {
        $base = new BDD();
        $resultat = $base->unique_query("SELECT * FROM planete WHERE galaxie = '$galaxie' AND ss = '$ss' AND position= '$pos';");
        $base->deconnexion();
        return $resultat[$info];
    }

    //Passage des valeurs générales au template
    $titre = "Administration";
    $template->assign('page', 'admin');
    $template->assign('menu', $VAR['menu']);
    $template->assign('premiere_page', $VAR['first_page']);
    $template->assign('race', $SESS->values['race']);
    $template->assign("tpsdejeu", "un certain temps");

    if (!empty($SESS->values['souscontrole'])) {
        $pagea = 'erreur';

        $SESS->values['id'] = $SESS->values['souscontrole'][0];
        $SESS->values['idPlan'] = $SESS->values['souscontrole'][1];
        $SESS->values['souscontrole'] = null;
        $SESS->put();

        $template->assign('message', 'Droits rétablis avec succès !<br />');
    } else {
        $id_user = $SESS->values['id'];

        if (!isset($_GET['p'])) {
            $_GET['p'] = '';
        }
        if ($SESS->level >= 5) {
            switch ($_GET['p']) {
                case 'djoueurs':
                    include("admin/supprimer_joueur.php");
                    break;
                case 'bandeau':
                    include("admin/bandeau.php");
                    break;
                case 'demarrage':
                    include("admin/demarrage.php");
                    break;
                case 'version':
                    include("admin/version.php");
                    break;
                case 'inscription':
                    include("admin/inscription.php");
                    break;
                case 'njoueur':
                    include("server/njoueur.php");
                    break;
                case 'mail_mass':
                    include("admin/mail_mass.php");
                    break;
                case 'bdd':
                    include("admin/bdd.php");
                    break;
                case 'proxy':
                include("admin/proxy.php");
                    break;
            }
        }
        if (empty($pagea)) {
            switch ($_GET['p']) {
                case 'courrier':
                    include("admin/mail.php");
                    break;
                case 'vip':
                    include("admin/ip.php");
                    break;
                case 'vflottes':
                    include("admin/flottes.php");
                    break;
                case 'vplanetes':
                    include("admin/planete.php");
                    break;
                case 'vjoueurs':
                    include("admin/joueur.php");
                    break;
                case 'valliances':
                    include("admin/alliance.php");
                    break;
                case 'snalliances':
                    include("admin/snalliances.php");
                    break;
                case 'vrapports':
                    include("admin/rapport.php");
                    break;
                case 'sjoueurs':
                    include("admin/sanction_joueur.php");
                    break;
                case 'cjoueurs':
                    include("admin/prendre_controle.php");
                    break;
                default:
                    include("admin/accueil.php");
                    break;
            }
        }
    }

    $template->assign('titre', $titre);
    $template->assign('pagea', $pagea);
    $template->display('admin/'.$pagea.'.tpl');
} else {
    header('Location: '.$VAR['first_page']);
}
