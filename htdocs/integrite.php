<html>
	<head>
		<title>.: Halo-Battle :: V&eacute;rification de l'int&eacute;grit&eacute; des fichiers :.</title>
	</head>
	<body>
		<h2>V&eacute;rification de l'int&eacute;grit&eacute; des fichiers</h2>
<?php
define('FILE_SAV', "../onyx2/cache/checksum.php");
define('dir_start', "../");

print 'Chargement du fichier de comparaison ...';
if (is_file('checkSum.php')) {
    print ' Touv&eacute; ...';
    $SUM = sha1(file_get_contents('checkSum.php'));
    print ' Donn&eacute;es charg&eacute;es avec succ&egrave;s<br /><br />';
}

print 'V&eacute;rification du fichier de validation ...';
if (sha1(sha1_file(__FILE__)) != $SUM) {
    die(' INVALIDE !<br />Arr&ecirc;t du processus de validation !');
} else {
    print ' Valide<br /><br />';
}

print 'Recherche du fichier de sauvegarde ...';
if (is_file(FILE_SAV) && !isset($_GET['erase'])) {
    function parcourDir($dir, $sums)
    {
        if (isset($_GET['v'])) {
            print "<br />Parcours du dossier &agrave; la recherche de nouveaux fichiers : ".$dir;
        }

        $new = 0;
        $pointeur = opendir($dir);
        while ($fichier = readdir($pointeur)) {
            if ($fichier == '.' || $fichier == '..' || $fichier == '.svn' || preg_match("#~#", $fichier)) {
                continue;
            }
        
            if (is_dir($dir.$fichier) && is_readable($dir.$fichier)) {
                $new += parcourDir($dir.$fichier.'/', $sums);
            } elseif (is_file($dir.$fichier)) {
                if (!isset($sums[$dir.$fichier])) {
                    $new++;
                    print "<tr style=\"background: #00FFFF;\"><td>".$dir.$fichier."</td><td style=\"text-align: center;\">Nouveau fichier !</td></tr>";
                }
            }
        }
        closedir($pointeur);
        
        return $new;
    }

    print ' Trouv&eacute;<br /><br />';

    if (isset($_GET["check"])) {
        print 'Contr&ocirc;le de la somme de la derni&egrave;re validation ...';
        if (sha1(sha1_file(FILE_SAV).'<^>'.sha1_file(__FILE__).sha1_file('checkSum.php')) == $_GET["check"]) {
            print ' <span style="color: #00FF00;">Authentique</span><br /><br />';
        } else {
            die(' Invalide !<br />Arr&ecirc;t du processus de v&eacute;rification !');
        }
    } else {
        print '<span style="color: #FF0000; font-weigth: bold;">Les r&eacute;sultats pr&eacute;sent&eacute;s ci-dessus ne peuvent pas &ecirc;tre authentifi&eacute;s sans la somme d\'un pr&eacute;c&eacute;dent contr&ocirc;le, rien ne dit que les fichiers de validation n\'ont pas &eacute;t&eacute; modifi&eacute;s !</span><br /><br />';
    }

    $start = unserialize(file_get_contents(FILE_SAV));

    print ' V&eacute;rification des fichiers ...';
    print '<table border="1" style=""><tr><th>Nom du fichier</th><th>D&eacute;tails</th></tr>';
    $alerte = 0;
    foreach ($start as $key => $sum) {
        if (!is_file($key)) {
            $alerte++;
            if (!isset($_GET['a'])) {
                print "<tr style=\"background: #FFCC00;\"><td>".$key."</td><td style=\"text-align: center;\">Fichier supprim&eacute; !</td></tr>";
            }
        } elseif (sha1_file($key) != $sum) {
            $alerte++;
            print "<tr style=\"background: #FF0000;\"><td>".$key."</td><td style=\"text-align: center;\">Alerte de diff&eacute;rence de somme !</td></tr>";
        } elseif (isset($_GET['v'])) {
            print "<tr style=\"background: #00FF00;\"><td>".$key."</td><td style=\"text-align: center;\">Ok</td></tr>";
        }
    }
    $newFiles = parcourDir(dir_start, $start);
    print '</table>';

    print '<br />Fin de la v&eacute;rification : '.$alerte.' alerte(s) sur '.count($start).' fichier(s). '.$newFiles.' nouveau(x) fichier(s).';

    print '<br /><br />Cont&ocirc;le de la prochaine validation : '.sha1(sha1_file(FILE_SAV).'<^>'.sha1_file(__FILE__).sha1_file('checkSum.php'));
} else {
    if (isset($_GET['erase']) && $_GET['erase'] == $SUM) {
        print ' Trouv&eacute;<br />D&eacute;tection d\'une demande d\'effacement<br /><br />';
    } elseif (!is_file(FILE_SAV)) {
        //print ' Introuvable<br /><br />';
        die(' Introuvable<br /><br />Arr&ecirc;t de la v&eacute;rification.');
    } else {
        die(' ?<br /><br />Impossible de d&eacute;terminer l\'origine de la requ&ecirc;te !<br />Arr&ecirc;t de la v&eacute;rification.');
    }

    function extractDir($dir)
    {
        $sums = array();

        if (isset($_GET['v'])) {
            print "<br />G&eacute;n&eacute;ration des sommes de fichiers pour le dossier : ".$dir;
        }

        $pointeur = opendir($dir);
        while ($fichier = readdir($pointeur)) {
            if ($fichier == '.' || $fichier == '..' || $fichier == '.svn' || preg_match("#~#", $fichier)) {
                continue;
            }
        
            if (is_dir($dir.$fichier) && is_readable($dir.$fichier)) {
                $sums = array_merge($sums, extractDir($dir.$fichier.'/'));
            } elseif (is_file($dir.$fichier)) {
                $sums[$dir.$fichier] = sha1_file($dir.$fichier);
            }
        }
        closedir($pointeur);
        
        return $sums;
    }

    print 'G&eacute;n&eacute;ration du tableau ...';
    $sums = array();
        
    $sums = extractDir(dir_start);

    print "&Eacute;criture du fichier.<br />";
    file_put_contents(FILE_SAV, serialize($sums));
    print "Fin de l'op&eacute;ration.<br />";
}
?>
	</body>
</html>