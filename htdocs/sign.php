<?php
//Définition de la constante anti-hacking
define("INDEX", 1);
define("DEBUG", false);
define("xCSRF", true); //On annule la vérification des attaques CSRF car on sait que toutes les requêtes viennent d'un autre serveur, deplus les risques sont minimes

//Inclusion de l'API Onyx
require_once(trim(file_get_contents('./.onyx')));
require_once("common.php"); //Chargement de tout le nécessaire pour le jeu

function separerNombres($int)
{
    return number_format(floor($int), 0, ',', ' ');
}


$id = gpc('i');
if (!is_numeric($id)) {
    $id = 0;
}

$f = gpc('f');
$s = gpc('s');

header("Content-type: image/png");

if (file_exists(ONYX."cache/signatures/".$id.".".$f.$s.".sign") && time() - filemtime(ONYX."cache/signatures/".$id.".".$f.$s.".sign") < 36000) {
    readfile(ONYX."cache/signatures/".$id.".".$f.$s.".sign");
} else {
    $fontfile = ONYX."include/content/signatures/ARIAL.TTF";

    $bdd = new BDD();
    $bdd->escape($id);
    $joueur = $bdd->unique_query("SELECT $table_user.pseudo, $table_user.auth_level, $table_user.race, $table_user.place_points, $table_user.points, $table_user.batiments, $table_user.flotte, $table_alliances.tag, $table_alliances.nom  FROM $table_user INNER JOIN $table_alliances ON $table_user.id_alliance = $table_alliances.id WHERE $table_user.id = $id;");
    if ($joueur == false) {
        $joueur = $bdd->unique_query("SELECT pseudo, auth_level, race, place_points, points, batiments, flotte FROM $table_user WHERE id = $id;");
    }
    $nomjoueur = $joueur['pseudo'];
    $raid = $bdd->unique_query("SELECT COUNT(id) AS nombre FROM $table_mail WHERE destinataire = '$nomjoueur' AND contenu LIKE '<b>Le combat contre % est termi%';");
    $transport = $bdd->unique_query("SELECT COUNT(id) AS nombre FROM $table_mail WHERE contenu LIKE 'Les vaisseaux de $nomjoueur %';");
    $bdd->deconnexion();

    if (!empty($s) && $joueur['auth_level'] >= 1) {
        switch ($s) {
            case 1:
                $img = 'mixte1userbarju7.png';
                $_img = imagecreatefrompng(ONYX."include/content/signatures/".$img);
                $entete = imagecolorallocate($_img, 255, 255, 255);
                $contenu = imagecolorallocate($_img, 255, 255, 255);
                $infos = array(
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($raid['nombre'])
                            ),
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($joueur['flotte'])
                            ),
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($transport['nombre'])
                            ),
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($joueur['batiments'])
                            )
                        );
                break;
            default:
                $img = 'mixte1userbarju7.png';
                $_img = imagecreatefrompng(ONYX."include/content/signatures/".$img);
                $entete = imagecolorallocate($_img, 255, 255, 255);
                $contenu = imagecolorallocate($_img, 255, 255, 255);
                $infos = array(
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($raid['nombre'])
                            ),
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($joueur['flotte'])
                            ),
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($transport['nombre'])
                            ),
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($joueur['batiments'])
                            )
                        );
        }
        if ($joueur['race'] == 'covenant') {
            $_imgavatar = imagecreatefrompng(ONYX."include/content/signatures/covi.png");
        } else {
            $_imgavatar = imagecreatefrompng(ONYX."include/content/signatures/hum.png");
        }
    } elseif ($joueur['race'] == 'covenant') {
        switch ($f) {
            case 1:
                $img = 'userbarallicopygs1.png';
                $_img = imagecreatefrompng(ONYX."include/content/signatures/".$img);
                $entete = imagecolorallocate($_img, 255, 255, 255);
                $contenu = imagecolorallocate($_img, 255, 255, 255);
                $infos = array(
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($raid['nombre'])
                            ),
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($joueur['batiments'])
                            ),
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($joueur['flotte'])
                            ),
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($transport['nombre'])
                            )
                        );
                break;
            default:
                $img = 'covenantuserbarus1.png';
                $_img = imagecreatefrompng(ONYX."include/content/signatures/".$img);
                $entete = imagecolorallocate($_img, 255, 255, 255);
                $contenu = imagecolorallocate($_img, 0, 0, 0);
                $infos = array(
                            array(
                                imagecolorallocate($_img, 0, 0, 0),
                                separerNombres($raid['nombre'])
                            ),
                            array(
                                imagecolorallocate($_img, 0, 0, 0),
                                separerNombres($joueur['flotte'])
                            ),
                            array(
                                imagecolorallocate($_img, 0, 0, 0),
                                separerNombres($transport['nombre'])
                            ),
                            array(
                                imagecolorallocate($_img, 0, 0, 0),
                                separerNombres($joueur['batiments'])
                            )
                        );
        }
        $_imgavatar = imagecreatefrompng(ONYX."include/content/signatures/covi.png");
    } else {
        switch ($f) {
            case 1:
                $img = 'userbarallicopygs1.png';
                $_img = imagecreatefrompng(ONYX."include/content/signatures/".$img);
                $entete = imagecolorallocate($_img, 255, 255, 255);
                $contenu = imagecolorallocate($_img, 255, 255, 255);
                $infos = array(
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($raid['nombre'])
                            ),
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($joueur['batiments'])
                            ),
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($joueur['flotte'])
                            ),
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($transport['nombre'])
                            )
                        );
                break;
            default:
                $img = 'humainuserbarhz2.png';
                $_img = imagecreatefrompng(ONYX."include/content/signatures/".$img);
                $entete = imagecolorallocate($_img, 255, 255, 255);
                //$contenu = imagecolorallocate($_img, 160, 160, 0);
                $contenu = imagecolorallocate($_img, 255, 255, 255);
                $infos = array(
                            array(
                                imagecolorallocate($_img, 0, 0, 0),
                                separerNombres($raid['nombre'])
                            ),
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($joueur['flotte'])
                            ),
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($transport['nombre'])
                            ),
                            array(
                                imagecolorallocate($_img, 255, 255, 255),
                                separerNombres($joueur['batiments'])
                            )
                        );
        }
        $_imgavatar = imagecreatefrompng(ONYX."include/content/signatures/hum.png");
    }

    $blanc = imagecolorallocate($_img, 255, 255, 255);
    $noir = imagecolorallocate($_img, 0, 0, 0);
    $rouge = imagecolorallocate($_img, 255, 0, 0);
    $vert = imagecolorallocate($_img, 0, 255, 0);
    $bleu = imagecolorallocate($_img, 0, 0, 255);
    $jaune = imagecolorallocate($_img, 255, 255, 0);

    if ($joueur == false) {
        ImageTTFText($_img, 20, -20, 123, 17, $rouge, $fontfile, 'Joueur introuvable !');
        if (file_exists(ONYX."/cache/signatures/".$id.".".$f.$s.".sign")) {
            unlink(ONYX."/cache/signatures/".$id.".".$f.$s.".sign");
        }
    } else {
        if ($joueur['auth_level'] > 4) {
            ImageTTFText($_img, 12, 0, 45, 23, $vert, $fontfile, ucfirst($joueur['pseudo']));
        } elseif ($joueur['auth_level'] >= 1) {
            ImageTTFText($_img, 12, 0, 45, 23, $jaune, $fontfile, ucfirst($joueur['pseudo']));
        } else {
            ImageTTFText($_img, 12, 0, 45, 23, $entete, $fontfile, ucfirst($joueur['pseudo']));
        }

        ImageTTFText($_img, 12, 0, 255, 23, $entete, $fontfile, $VAR['serveur_name']);

        if (isset($joueur['tag'])) {
            if (empty($joueur['place_points'])) {
                ImageTTFText($_img, 11, 0, 32, 47, $contenu, $fontfile, "Classement : Non classé");
            } else {
                ImageTTFText($_img, 11, 0, 32, 47, $contenu, $fontfile, "Classement : ".separerNombres($joueur['place_points']).($joueur['place_points']==1?'er':'ème'));
            }
            ImageTTFText($_img, 11, 0, 32, 64, $contenu, $fontfile, "Points : ".separerNombres($joueur['points']));
            ImageTTFText($_img, 11, 0, 32, 82, $contenu, $fontfile, "Alliance : [".$joueur['tag']."] ".substr($joueur['nom'], 0, 9).(strlen($joueur['nom'])>9?'...':''));
        } else {
            if (empty($joueur['place_points'])) {
                ImageTTFText($_img, 11, 0, 32, 55, $contenu, $fontfile, "Classement : Non classé");
            } else {
                ImageTTFText($_img, 11, 0, 32, 55, $contenu, $fontfile, "Classement : ".separerNombres($joueur['place_points']).($joueur['place_points']==1?'er':'ème'));
            }
            ImageTTFText($_img, 11, 0, 32, 75, $contenu, $fontfile, "Points : ".separerNombres($joueur['points']));
        }

        ImageTTFText($_img, 12, 0, 272, 51, $infos[0][0], $fontfile, $infos[0][1]);
        ImageTTFText($_img, 12, 0, 272, 78, $infos[1][0], $fontfile, $infos[1][1]);
        ImageTTFText($_img, 12, 0, 355, 50, $infos[2][0], $fontfile, $infos[2][1]);
        ImageTTFText($_img, 12, 0, 355, 78, $infos[3][0], $fontfile, $infos[3][1]);
    }
    imagecopy($_img, $_imgavatar, 435, 0, 0, 0, 98, 100);

    imagealphablending($_img, false);
    imagesavealpha($_img, true);

    if ($joueur == false) {
        imagepng($_img);
    } else {
        imagepng($_img, ONYX."cache/signatures/".$id.".".$f.$s.".sign");
        readfile(ONYX."cache/signatures/".$id.".".$f.$s.".sign");
    }
}
