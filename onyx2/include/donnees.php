<?php
if (!defined('ONYX')) {
    exit;
}

if (!defined('INDEX') || INDEX != 2) {
    //On inclut les classes de données
    require_once("Class/Donnees/interface.php");
    require_once("Class/Donnees/alliances_batiments.php");
    require_once("Class/Donnees/batiments.php");
    require_once("Class/Donnees/caserne.php");
    require_once("Class/Donnees/technologies.php");
    require_once("Class/Donnees/terrestre.php");
    require_once("Class/Donnees/spatial.php");

    //On inclut les classes de file d'attente
    require_once("Class/File/interface.php");
    require_once("Class/File/alliances_batiments.php");
    require_once("Class/File/batiments.php");
    require_once("Class/File/caserne.php");
    require_once("Class/File/technologies.php");
    require_once("Class/File/terrestre.php");
    require_once("Class/File/spatial.php");
}

//On déffinit les constantes de correspondance Champs/Array
$alli_batimentsVAR = array("centre", "port", "forge", "urgence", "propagande", "economie");
$technologiesVAR = array("techno_indu", "techno_inge", "techno_inge2", "techno_poli", "techno_arme", "techno_defe", "techno_defe2", "techno_proj", "techno_expansion");
$batimentsVAR = array("mine_m", "mine_c", "mine_h", "centrale_s", "centrale_f", "radar", "labo", "chantier_terrestre", "chantier_spatial", "caserne", "silo", "centre_info", "habitation", "arcologies", "bunker", "stations", "commercial", "loisir", "administration");
$caserneVAR = array("soldat1", "soldat2", "soldat3", "soldat4", "sniper", "spartan", "medecin", "ingenieur", "soldat_lourd");
$spatialVAR = array("vaisseau_1", "vaisseau_2", "vaisseau_3", "vaisseau_4", "vaisseau_5", "vaisseau_6", "vaisseau_7", "vaisseau_8", "vaisseau_9", "vaisseau_10", "vaisseau_11", "vaisseau_12", "vaisseau_13", "vaisseau_14", "vaisseau_15", "vaisseau_16", "vaisseau_17");
$terrestreVAR = array();
$terrestreVAR["humain"] = array("vais_0", "vais_1", "vais_2", "vais_3", "vcl_1", "vcl_2", "vcl_3", "vcl_4", "def_1", "def_2", "def_3", "def_4", "def_5", "def_6", "def_7", "def_8");
$terrestreVAR["covenant"] = array("vais_0", "vais_1", "vais_2", "vais_3", "vcl_1", "vcl_2", "vcl_3", "vcl_4", "def_1", "def_2", "def_3", "def_4", "def_5");
$coeffVAR = array("coeff_mine_m", "coeff_mine_c", "coeff_mine_h", "coeff_centrale_s", "coeff_centrale_f");
