<?php
if (!defined('ONYX')) {
    exit;
}

$act = gpc('q');

if ($act == "fonder") {
    $bdd->reconnexion();
    //On regarde si le joueur n'est pas déjà en train de fonder une alliance
    if (!$alli = $bdd->query("SELECT id FROM $table_alliances_creation WHERE fondateur = ".$planete->id_user." OR signatures LIKE '%;".$planete->id_user.";%';")) {
        $bdd->deconnexion();
        $page = 'alliance/nm_fonder';

        $nom = gpc('nom', 'post');
        $tag = gpc('tag', 'post');
        if (!empty($nom) || !empty($tag)) {
            //Vérifications
            if (!preg_match("#^[A-Za-z0-9èéàùûüôöç'_ -]{5,42}$#ui", $nom)) {
                erreur("Le nom d'alliance que vous avez choisi n'est pas valide :<br />il doit avoir entre 5 et 42 caractères (certains accents sont autorisés, mais aucun caractères spéciaux) !", "red");
            } elseif (!preg_match("#^[A-Za-z0-9]{3,5}$#ui", $tag)) {
                erreur("Le tag d'alliance que vous avez choisi n'est pas valide :<br />il doit avoir entre 3 et 5 lettres (sans accents) ou chiffres !", "red");
            }

            $lien = sha1($tag.'Hb$'.$nom.'☺Ø'.$planete->id_user.'‘«'.$planete->race);

            $bdd->reconnexion();
            $bdd->escape($nom);
            $bdd->escape($tag);
            $utilise = $bdd->query("SELECT id FROM $table_alliances WHERE nom_alliance = '$nom' OR tag = '$tag' OR fondateur = ".$planete->id_user." UNION SELECT id FROM $table_alliances_creation WHERE nom_alliance = '$nom' OR tag = '$tag' OR fondateur = ".$planete->id_user.";");
            if (empty($utilise)) {
                $bdd->query("INSERT INTO $table_alliances_creation (tag, nom_alliance, fondateur, lien) VALUES ('$tag', '$nom', ".$planete->id_user.", '$lien');");
            }
            $bdd->deconnexion();

            if (!empty($utilise)) {
                erreur("Le nom ou le tag que vous avez choisi est déjà utilisé par une alliance.", "red");
            } else {
                send_mp($planete->id_user, "Fondation de votre alliance !", 'Pour terminer la création de votre alliance, trouvez au moins '.nb_signatures.' joueurs de cette galaxie sans alliance pour leur faire signer votre traité de fondation d\'alliance.<br /><br />Lien de signature :<br /><a href="'.$VAR["menu"]["alliance"].'&amp;signer='.$lien.'">http://'.$_SERVER['HTTP_HOST'].'/'.$VAR["first_page"].''.$VAR["menu"]["alliance"].'&amp;signer='.$lien.'</a>');
                erreur('Votre alliance a bien &eacute;t&eacute; cr&eacute;&eacute;e.<br />Il ne vous reste plus qu\'à trouver au moins '.nb_signatures.' signatures pour finir la création de votre alliance.<br /><br />Le lien permettant à vos quatres personnes de signer est <a href="'.$VAR["menu"]["alliance"].'&amp;signer='.$lien.'">http://'.$_SERVER['HTTP_HOST'].'/'.$VAR["first_page"].''.$VAR["menu"]["alliance"].'&amp;signer='.$lien.'</a>', "green");
            }
        }
        unset($nom, $tag);
    } else {
        $page = 'alliance/nm_statut';
        $alliance = $bdd->unique_query("SELECT * FROM $table_alliances_creation WHERE id = ".$alli[0]["id"].";");

        $signatures = explode(';', substr($alliance["signatures"], 1), -1);
        $signaturesExport = implode(' OR id = ', $signatures);
        $pseudos = $bdd->query("SELECT pseudo FROM $table_user WHERE id = ".$signaturesExport.";");
        $bdd->deconnexion();

        if (gpc('r') == "quit") {
            if ($alliance["fondateur"] == $planete->id_user) {
                $bdd->reconnexion();
                $bdd->query("DELETE FROM $table_alliances_creation WHERE id = ".$alli[0]["id"].";");
                $bdd->deconnexion();

                foreach ($signatures as $id_user) {
                    send_mp($id_user, "Annulation de la fondation de votre alliance !", "Le fondateur de l'alliance pour laquelle vous avez signée vient d'annuler sa création.");
                }

                send_mp($planete->id_user, "Annulation de la fondation de votre alliance !", "Vous vennez d'annuler la création de votre alliance.");
                erreur("Votre alliance vient d'être supprimée", "green");
            } else {
                $keys = array_keys($signatures, $planete->id_user);
                foreach ($keys as $key) {
                    unset($signatures[$key]);
                }

                $signatures = ";".implode(';', $signatures).";";
                $bdd->reconnexion();
                $bdd->escape($signatures);
                $bdd->query("UPDATE $table_alliances_creation SET signatures = '$signatures' WHERE id = ".$alliance["id"].";");
                $bdd->deconnexion();

                send_mp($planete->id_user, "Annulation de la signature pour votre alliance !", "Vous vennez d'annuler votre signature pour l'alliance ".$alliance["nom"]);
                erreur("Votre signature a été rayée.", "green");
            }
        }

        $template->assign("alliance", $alliance);
        $template->assign("signatures", $signatures);
        $template->assign("pseudos", $pseudos);
        $template->assign("nbSignatures", count($signatures));
    }
} elseif (!empty($_POST['search_tag']) || !empty($_POST['search_nom'])) {
    $page = 'alliance/nm_search';

    $tag = gpc('search_tag', 'post');
    $nom = str_replace("*", "%", gpc('search_nom', 'post'));

    $bdd->reconnexion();
    $bdd->escape($tag);
    $bdd->escape($nom);
    $rech1 = $bdd->query("SELECT id, tag, nom_alliance, race, etat_inscription FROM $table_alliances WHERE tag LIKE '$tag%' AND nom_alliance LIKE '%$nom%';");
    $rech2 = $bdd->query("SELECT lien AS id, tag, nom_alliance, 0 AS race, 2 AS etat_inscription FROM $table_alliances_creation WHERE tag LIKE '$tag%' AND nom_alliance LIKE '%$nom%';");
    $bdd->deconnexion();

    if (!empty($rech1) && !empty($rech2)) {
        $recherche = array_merge($rech1, $rech2);
    } elseif (!empty($rech2)) {
        $recherche = $rech2;
    } else {
        $recherche = $rech1;
    }

    if ($bdd->num_rows == 1) {
        redirection($VAR["menu"]["alliance"]."&v=".$recherche[0]['id']);
    } elseif ($bdd->num_rows == 0) {
        erreur("Aucune alliance ne correspond à ces critères de recherche", "", $VAR["menu"]["alliance"]);
    }

    $template->assign("recherches", $recherche);
    unset($nom, $tag, $recherche);
} else {
    $bdd->reconnexion();
    $template->assign("fondation", $bdd->query("SELECT id FROM $table_alliances_creation WHERE fondateur = ".$planete->id_user." OR signatures LIKE '%;".$planete->id_user.";%';"));
    $page = 'alliance/nm_accueil';
}
