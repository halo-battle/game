<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$page = 'messagerie';
$titre = 'Messagerie d\'alliance';
$template->assign("link_P", "alli_messagerie");
$template->assign("titreR", $titre);

//On interdit l'accès à la messagerie en cas de prise de contrôle d'un joueur
if (!empty($sess->values['souscontrole'][0])) {
    erreur('Vous êtes en mode prise de contrôle, vous ne pouvez pas accèder aux messages privés des joueurs.');
}

if ($id = intval(gpc('avertir'))) {
    $bdd->reconnexion();
    $bdd->query("UPDATE $table_alliances_mail SET statut = 1 WHERE destinataire = ".$planete->id." AND id = $id;");
    if ($bdd->affected()) {
        erreur('La demande de vérification de contenu par les opérateurs a été transmise avec succès.', 'green', '?p=messagerie');
    } else {
        erreur('Une demande est déjà en cours ou vous n\'êtes pas autorisé à avertir ce message.', 'red', '?p=messagerie');
    }
    $bdd->deconnexion();
}

//On récupére l'onglet demandé
$onglet = gpc('n');
$template->assign('onglet', $onglet);

if ($onglet == "rapports") {
    //Demande de supression des messages
    if (!empty($_POST)) {
        $bdd->reconnexion();
        foreach ($_POST as $value) {
            $bdd->query("UPDATE $table_alliances_mail SET vu = 's' WHERE destinataire = ".$planete->id." AND id = ".intval($value).";");
        }
        $bdd->deconnexion();

        header('Location: ?p=messagerie&n='.$onglet);
        exit;
    }

    $bdd->reconnexion();
    $data = $bdd->query("SELECT * FROM $table_alliances_mail WHERE destinataire = ".$planete->id." AND expediteur = false AND vu != 's' ORDER BY id DESC;");
    $bdd->query("UPDATE $table_alliances_mail SET vu = 0 WHERE destinataire = ".$planete->id." AND expediteur = false AND vu != 's';");
    $bdd->deconnexion();

    $template->assign('messages', $data);
} elseif ($onglet == "send") {
    $bdd->reconnexion();
    $data = $bdd->query("SELECT M.*, U.pseudo AS destinataire FROM $table_alliances_mail M INNER JOIN $table_user U ON U.id = M.destinataire WHERE M.expediteur = ".$planete->id." ORDER BY M.id DESC;");
    $bdd->deconnexion();

    $template->assign('messages', $data);
} elseif ($onglet == "envoyer") {
    $page = 'envoyer';
    $titre = 'Envoyer un message';

    if (!empty($_POST['objet']) && !empty($_POST['destinataire']) && !empty($_POST['message'])) {
        $utils = explode(';', gpc('destinataire', 'post'));
        $nbutil = count($utils);

        $time = time();
        $message = htmlspecialchars(gpc('message', 'post'));
        $objet = htmlspecialchars(gpc('objet', 'post'));

        if (strlen($message) > 99999) {
            erreur('Le contenu de votre message est trop long. Limite : 99 999 caractères.');
        }
        if (empty($utils)) {
            erreur('Aucun utilisateur sélectionné.');
        }

        $bdd->reconnexion();
        $bdd->escape($message);
        $bdd->escape($objet);

        $introuv = array();
        foreach ($utils as $util) {
            if (empty($util)) {
                continue;
            }
            $util = trim($util);
            $bdd->escape($util);

            if ($util != $planete->pseudo && $env = $bdd->unique_query("SELECT id, pseudo, mail, envoyerMail FROM $table_user WHERE pseudo = '$util';")) {
                $bdd->query("INSERT INTO $table_alliances_mail (destinataire, expediteur, sujet, contenu, temps) VALUES(".$env['id'].", ".$planete->id_user.", '$objet', '$message', $time);");
                if ($env["envoyerMail"]& 2 && !empty($env["mail"])) {
                    send_mail($env["mail"], "Halo-Battle :: Nouveau message privé", 'Bonjour '.$env["pseudo"].',

						Vous recevez ce courriel suite à l\'arrivée d\'un nouveau message privé reçu sur votre compte de jeu du serveur '.$VAR['serveur_name'].'. Le sujet de ce message est : "'.$objet.'" et a été envoyé par '.$planete->pseudo.'. Vous pouvez utiliser le lien suivant pour voir le message ou vous connecter.

						http://'.$_SERVER['HTTP_HOST'].'/'.$VAR['first_page'].'?p=messagerie

						Vous pouvez désactiver ses notifications via les options de votre compte dans l\'onglet "notifications"

						A bientôt dans Halo-Battle,
						Le Staff');
                }
            } else {
                $introuv[] = htmlentities($util);
            }
        }
        $bdd->deconnexion();

        if (!empty($introuv[0])) {
            erreur('Impossible de trouver le/les destinataire(s) suivant(s) : '.implode(', ', $introuv).'.<br />Les autres ont bien reçu votre message.', "red", '?p=messagerie&n=envoyer');
        } else {
            erreur('Votre message a été envoyé avec succès.', "green", '?p=messagerie');
        }
    }

    $amis = array();
    $amisn = array();
    $bdd->reconnexion();
    foreach ($planete->amis as $ami) {
        $res = $bdd->unique_query("SELECT pseudo FROM $table_user WHERE id = ".$ami.";");
        if ($res != false) {
            $amis[] = $res['pseudo'];
            $amisn[] = $ami;
        }
    }
    $bdd->deconnexion();
    $template->assign('amis', $amis);
    $template->assign('amisn', $amisn);

    if (isset($_GET['d'])) {
        $template->assign('destinataire', gpc('d'));
    }
    if (isset($_GET['o'])) {
        $template->assign('objet', gpc('o'));
    }
} else {
    //Demande de supression des messages
    if (!empty($_POST)) {
        $bdd->reconnexion();
        foreach ($_POST as $value) {
            $bdd->query("DELETE FROM $table_alliances_mail WHERE destinataire = ".$planete->id_user." AND id = ".intval($value).";");
        }
        $bdd->deconnexion();

        header('Location: ?p=messagerie&n='.$onglet);
        exit;
    }

    if ($SESS->level >= 3) {
        $bdd->reconnexion();
        $data = $bdd->query("SELECT M.*, U.pseudo AS expediteur FROM $table_alliances_mail M INNER JOIN $table_user U ON U.id = M.expediteur WHERE M.destinataire = ".$planete->id_user." AND M.expediteur != false ORDER BY M.id DESC;");
        $bdd->query("UPDATE $table_alliances_mail M SET vu = 0 WHERE M.destinataire = ".$planete->id_user." AND M.expediteur != false ORDER BY M.id DESC;");
        $bdd->deconnexion();
    } else {
        $bdd->reconnexion();
        //Calcul du nombre total de message enregistré
        $nbmax = $bdd->query("SELECT COUNT(id) as nb FROM $table_alliances_mail M WHERE M.destinataire = ".$planete->id_user." AND M.expediteur != false ORDER BY M.id DESC;");
        $data = $bdd->query("SELECT M.*, U.pseudo AS expediteur FROM $table_alliances_mail M INNER JOIN $table_user U ON U.id = M.expediteur WHERE M.destinataire = ".$planete->id_user." AND M.expediteur != false ORDER BY M.id DESC LIMIT 50;");
        $bdd->query("UPDATE $table_alliances_mail M SET vu = 0 WHERE M.destinataire = ".$planete->id_user." AND M.expediteur != false ORDER BY M.id DESC LIMIT 50;");
        $bdd->deconnexion();
    }

    if ($SESS->level < 3 && $nbmax > $bdd->num_rows) {
        $template->assign('avertissement', '<p style="color: red;"><b>Vous avez des messages en attente de réception. Libérez de la place dans votre messagerie pour les afficher.</b></p>');
    }

    $template->assign('messages', $data);
}

unset($onglet, $data, $nbmax);

if ($bdd->num_rows) {
    $template->assign('IM', $bdd->num_rows);
}
