<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}

$id = intval(gpc('v'));
$page = 'alliance/view';

$bdd->reconnexion();
$alliance = $bdd->unique_query("SELECT * FROM $table_alliances WHERE id = $id;");
$nbmembres = $bdd->unique_query("SELECT COUNT(id) AS nbmembres FROM $table_user WHERE id_alliance = $id;");
if ($planete->id_alliance == $id) {
    $grade = $bdd->unique_query("SELECT nom FROM $table_alliances_grade WHERE id = ".$planete->id_grade_alliance.";");
}
$bdd->deconnexion();

if (!empty($alliance)) {
    $template->assign("alliance", $alliance);
    $template->assign("nbmembres", $nbmembres['nbmembres']);
    if (empty($grade)) {
        $template->assign("grade", "Invité");
    } else {
        $template->assign("grade", $grade["nom"]);
    }
} else {
    redirection($VAR["menu"]["alliance"]);
}
