<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}

$gestion = strtolower(gpc("g"));
if ($gestion == "grades") {
    $page = 'alliance/gestion_grades';

    //On vérifie que le joueur ait les permissions pour modifier les grades
    if (!($planete->permissions_alliance &128)) {
        erreur("Vous n'avez pas le grade nécessaire pour modifier les grades !");
    }

    $id = intval(gpc("i"));
    $del = intval(gpc("d"));

    if (!empty($id) && gpc("nom", "post")) {
        $nom = gpc("nom", "post");
        $auth = intval(gpc("gbats", "post")) + intval(gpc("gvais", "post"))*2 + intval(gpc("gflot", "post"))*4 + intval(gpc("gdipl", "post"))*8 + intval(gpc("gcred", "post"))*16 + intval(gpc("gmemb", "post"))*32 + intval(gpc("gwing", "post"))*64 + intval(gpc("galli", "post"))*128 + intval(gpc("gchat", "post"))*256 + intval(gpc("gmess", "post"))*512;

        $bdd->reconnexion();
        $bdd->escape($nom);
        $bdd->query("UPDATE $table_alliances_grade SET nom = '$nom', auth = $auth WHERE id = $id AND id_alliance = ".$planete->id.";");
        $bdd->deconnexion();

        header('Location: ?p=gestion&g=grades');
        exit;
    } elseif (gpc("nom", "post")) {
        $nom = gpc("nom", "post");
        $auth = intval(gpc("gbats", "post")) + intval(gpc("gvais", "post"))*2 + intval(gpc("gflot", "post"))*4 + intval(gpc("gdipl", "post"))*8 + intval(gpc("gcred", "post"))*16 + intval(gpc("gmemb", "post"))*32 + intval(gpc("gwing", "post"))*64 + intval(gpc("galli", "post"))*128;

        $bdd->reconnexion();
        $bdd->escape($nom);
        $bdd->query("INSERT INTO $table_alliances_grade (id_alliance, nom, auth) VALUES (".$planete->id.", '$nom', $auth);");
        $bdd->deconnexion();

        header('Location: ?p=gestion&g=grades');
        exit;
    } elseif (!empty($id)) {
        $bdd->reconnexion();
        $grade = $bdd->unique_query("SELECT id, nom, auth FROM $table_alliances_grade WHERE id_alliance = ".$planete->id." AND id = $id;");
        $bdd->deconnexion();
        $template->assign("grade_mod", $grade);
    } elseif (!empty($del)) {
        $bdd->reconnexion();
        $grade = $bdd->query("DELETE FROM $table_alliances_grade WHERE id_alliance = ".$planete->id." AND id = $del;");
        $bdd->deconnexion();

        header('Location: ?p=gestion&g=grades');
        exit;
    }

    $bdd->reconnexion();
    $grades = $bdd->query("SELECT id, id_alliance, nom, auth FROM $table_alliances_grade WHERE id_alliance = ".$planete->id.";");
    $bdd->deconnexion();
    $template->assign("grades", $grades);

    unset($grades, $id, $del);
} elseif ($gestion == "alliance" || ($gestion == "wings" && !empty($planete->wing))) {
    $page = 'alliance/gestion_alliance';

    //On vérifie que le joueur ait les permissions pour modifier les grades
    if (!($planete->permissions_alliance &128)) {
        erreur("Vous n'avez pas le grade nécessaire pour modifier les paramètres de l'alliance !");
    }

    if (isset($_POST["asteroide_name"])) {
        $asteroide_name = gpc("asteroide_name", "post");
        $planete->nom_asteroide = $asteroide_name;
        $planete->modif[] = "nom_asteroide";
    } elseif (isset($_POST["texte_interne"])) {
        $texte_interne = gpc("texte_interne", "post");
        $defcon = intval(gpc("defcon", "post"));
        $defcon_txt = gpc("defcon_txt", "post");

        $bdd->reconnexion();
        $bdd->escape($texte_interne);
        $bdd->escape($defcon_txt);
        $bdd->query("UPDATE $table_alliances SET texte_interne = '$texte_interne', defcon = $defcon, defcon_txt = '$defcon_txt' WHERE id = ".$planete->id.";");
        $bdd->deconnexion();
    } elseif (isset($_POST["url_forum"])) {
        $url_forum = gpc("url_forum", "post");
        $url_chat = gpc("url_chat", "post");
        $port_chat = intval(gpc("port_chat", "post"));
        $chan_chat = gpc("chan_chat", "post");
        $pass_chat = gpc("pass_chat", "post");

        $bdd->reconnexion();
        $bdd->escape($url_forum);
        $bdd->escape($url_chat);
        $bdd->escape($chan_chat);
        $bdd->escape($pass_chat);
        $bdd->query("UPDATE $table_alliances SET url_forum = '$url_forum', url_chat = '$url_chat', port_chat = $port_chat, chan_chat = '$chan_chat', pass_chat = '$pass_chat' WHERE id = ".$planete->id.";");
        $bdd->deconnexion();
    } elseif (isset($_POST["message_inscription"])) {
        $etatinscriptions = intval(gpc("etatinscriptions", "post"));
        $message_inscription = gpc("message_inscription", "post");
        $presentation = gpc("presentation", "post");

        $bdd->reconnexion();
        $bdd->escape($url_forum);
        $bdd->escape($url_chat);
        $bdd->escape($chan_chat);
        $bdd->query("UPDATE $table_alliances SET etatinscriptions = '$etatinscriptions', message_inscription = '$message_inscription', presentation = '$presentation' WHERE id = ".$planete->id.";");
        $bdd->deconnexion();
    } elseif (isset($_POST["newfondateur"]) && $planete->fondateur == $planete->id_user) {
        //On vérifie que le joueur demandé existe bien et fasse bien parti de l'alliance
        $bdd->reconnexion();
        $membre = $bdd->unique_query("SELECT id_alliance FROM $table_user WHERE id != ".intval(gpc('newfondateur', 'post')).";");
        $bdd->deconnexion();

        if (!empty($membre)) {
            $planete->fondateur = intval(gpc('newfondateur', 'post'));
            $planete->modif[] = "fondateur";
            erreur("Fondateur changé avec succès.", "green");
        } else {
            erreur("Impossible de donner les droits de fondateur à ce joueur !");
        }
    }

    $planete->loadDetails();

    //On charge la liste des utilisateurs de l'alliance si besoin
    $bdd->reconnexion();
    $membres = $bdd->query("SELECT id, pseudo FROM $table_user WHERE id_alliance = ".$planete->id." AND id != ".$planete->fondateur.";");
    $bdd->deconnexion();
    $template->assign("membres", $membres);
    unset($membres);
} elseif ($gestion == "wings") {
    $page = 'alliance/gestion_wing';

    //On vérifie que le joueur ait les permissions pour modifier les grades
    if (!($planete->permissions_alliance &64)) {
        erreur("Vous n'avez pas le grade nécessaire pour gérer les wings !");
    }

    if (!empty($_POST["nom"])) {
        $fondateur = gpc("fondateur", "post");
        $nom = gpc("nom", "post");
        $tag = strtoupper(gpc("tag", "post"));
        $bdd->reconnexion();
        $bdd->escape($fondateur);
        $bdd->escape($nom);
        $bdd->escape($tag);
        $user = $bdd->unique_query("SELECT id, pseudo FROM $table_user WHERE pseudo = '$fondateur' AND id_alliance = 0;");
        $alliances = $bdd->query("SELECT id FROM $table_alliances  WHERE nom_alliance = '$nom' OR tag = '$tag';");
        $wings = $bdd->unique_query("SELECT COUNT(id) AS nb FROM $table_alliances WHERE galaxie = ".$planete->galaxie." AND ss = ".$planete->ss.";");
        $bdd->deconnexion();

        if (!empty($alliances)) {
            erreur("Une alliance porte déjà ce nom ou ce tag. Veuillez en choisir un autre.");
        } elseif (!empty($user)) {
            $race = gpc("race", "post");
            if (!preg_match("#^[A-Za-z0-9èéàùûüôöç'_ -]{5,24}$#ui", $nom)) {
                erreur("Le nom d'alliance que vous avez choisi n'est pas valide :<br />il doit avoir entre 5 et 24 caractères (certains accents sont autorisés, mais aucun caractères spéciaux) !", "red");
            } elseif (!preg_match("#^[A-Za-z0-9]{3,5}$#ui", $tag)) {
                erreur("Le tag de wing que vous avez choisi n'est pas valide :<br />il doit avoir entre 3 et 5 lettres (sans accents) ou chiffres !", "red");
            } elseif ($race != "humain" && $race != "covenant") {
                erreur("La race de la wing est incorrecte !");
            }

            $bdd->reconnexion();
            $bdd->escape($race);
            $bdd->query("INSERT INTO $table_alliances (race, sante, nom_alliance, tag, galaxie, ss, wing) VALUES ('$race', 1, '$nom', '$tag', ".$planete->galaxie.", ".$planete->ss.", ".$wings['nb'].")");
            $affected = $bdd->affected();
            $bdd->deconnexion();

            if ($affected) {
                send_mp($user['id'], "Création d'une wing pour l'alliance [".$planete->tag."] ".$planete->nom_alliance, "L'alliance [".$planete->tag."] ".$planete->nom_alliance." vous propose de devenir capitaine de la wing [".$tag."] ".$nom.".<br /><br />Si vous acceptez le poste, cliquez sur le lien suivant : <a href=\"\">ce lien</a>");
                erreur("Un message vient d'être envoyé à ".$user["pseudo"].". Lorsqu'il aura accepté son poste, la création de la wing sera terminée et les premiers membres pourront s'y inscrire.", "green");
            } else {
                erreur("Une erreur s'est produite lors de la création de la wing. Si le problème percisite, contacter un opérateur.");
            }
        } else {
            erreur("Impossible de trouver le nom de ce joueur. Vérifiez que celui-ci n'appartient actuellement à aucune alliance.<br />Impossible de créer une wing sans fondateur valide.");
        }
    }


    //On charge la liste des wings
    $bdd->reconnexion();
    $wings = $bdd->query("SELECT A.id, A.race, A.nom_alliance, A.tag, COUNT(U.id) AS nbMembres, V.id AS id_fondateur, V.pseudo AS pseudo_fondateur FROM $table_alliances A LEFT JOIN $table_user U ON U.id_alliance = A.id LEFT JOIN $table_user V ON V.id = A.fondateur WHERE galaxie = ".$planete->galaxie." AND ss = ".$planete->ss." AND wing != 0 GROUP BY A.id;");
    $bdd->deconnexion();
    $template->assign("wings", $wings);
    unset($wings);
} else {
    $page = 'alliance/gestion_membres';
    $gestion = "membres";

    //On vérifie que le joueur ait les permissions pour modifier les grades
    if (!($planete->permissions_alliance &32)) {
        erreur("Vous n'avez pas le grade nécessaire pour modifier les membres !");
    }

    //Acceptation ou refus des postulants
    if (!empty($_GET["pa"])) {
        $id = intval(gpc("pa"));
        $bdd->reconnexion();
        $user = $bdd->unique_query("SELECT U.id_alliance, U.pseudo, U.id AS id_user FROM $table_alliances_attente A INNER JOIN $table_user U ON A.id_user = U.id WHERE A.id_alliance = ".$planete->id." AND A.id = $id;");

        if (empty($user)) {
            $bdd->deconnexion();
            erreur("Impossible de trouver la candidature. Si le problème perciste, contactez un opérateur.");
        } elseif (!empty($user['id_alliance'])) {
            $bdd->deconnexion();
            erreur("Le joueur pour lequel vous souhaitez valider la condidature est actuellement dans une autre alliance. Vous ne pouvez donc pas l'accepter immédiatement.");
        } else {
            $bdd->query("UPDATE $table_user SET id_alliance = ".$planete->id.", id_grade_alliance = 0 WHERE id = ".$user["id_user"].";");
            if ($bdd->affected()) {
                $bdd->query("DELETE FROM $table_alliances_attente WHERE id_alliance = ".$planete->id." AND id = $id;");
                $bdd->deconnexion();
                send_mp($user["id_user"], "Candidature pour l'alliance [".$planete->tag."] ".$planete->nom_alliance, "Félicitations vous faites maintenant parti de l'alliance [".$planete->tag."] ".$planete->nom_alliance.".<br /><br />Après examen de votre candidature, le responsable du recrutement a jugé bon de vous accepter au sein de l'alliance.<br />Vous en faites donc désormais parti. Il ne vous reste plus qu'à vous démarquer pour monter dans les grades.<br /><br />Bon jeu !");
                erreur("Membre ajouté à l'alliance avec succès.<br />Un message privé vient de lui être envoyé afin de l'informer de la nouvelle.", "green");
            } else {
                $bdd->deconnexion();
                erreur("Une erreur s'est produite lors de l'ajout du joueur dans les membres de l'alliance.<br />Si le problème perciste, contactez un opérateur.");
            }
        }
    } elseif (!empty($_GET["pr"])) {
        $id = intval(gpc("pr"));
        $bdd->reconnexion();
        $user = $bdd->unique_query("SELECT U.id AS id_user FROM $table_alliances_attente A INNER JOIN $table_user U ON A.id_user = U.id WHERE A.id_alliance = ".$planete->id." AND A.id = $id;");
        $bdd->query("DELETE FROM $table_alliances_attente WHERE id_alliance = ".$planete->id." AND id = $id;");
        $bdd->deconnexion();

        send_mp($user["id_user"], "Candidature pour l'alliance [".$planete->tag."] ".$planete->nom_alliance, "Nous avons le regret de vous annoncer que l'alliance [".$planete->tag."] ".$planete->nom_alliance." a refusé votre candidature.<br /><br />Après examen de votre candidature, le responsable du recrutement a jugé bon de vous écarter de l'alliance.<br />Pour plus d'informations, veuillez contacter directement le responsable du recrutement.");
        erreur("Candidature refusée.<br />Un message privé vient d'être envoyé au joueur afin de l'informer de la nouvelle.", "orange");
    }
    //Gestion des exclusions de l'alliance
    elseif (!empty($_GET["u"])) {
        $id = intval(gpc("u"));
        if ($planete->fondateur == $id) {
            erreur("Vous ne pouvez pas exclure le fondateur de sa propre alliance.<br />En cas de problème avec le fondateur, veuillez contacter un opérateur.");
        }
        $bdd->reconnexion();
        $bdd->query("UPDATE $table_user SET id_alliance = 0, id_grade_alliance = 0 WHERE id_alliance = ".$planete->id." AND id = $id;");
        $bdd->deconnexion();

        if ($bdd->affected()) {
            send_mp($id, "Exclusion de votre alliance [".$planete->tag."] ".$planete->nom_alliance, "Vous vennez d'être exclus de votre alliance [".$planete->tag."] ".$planete->nom_alliance."<br />Pour plus d'informations, veuillez contacter directement le responsable du recrutement.");
            erreur("Membre renvoyé.<br />Un message privé vient de lui être envoyé afin de l'informer de la nouvelle.", "orange");
        } else {
            erreur("Membre introuvable !");
        }
    }

    $id = intval(gpc("i"));
    if (!empty($id)) {
        $grade = intval(gpc('grade', 'post'));

        $bdd->reconnexion();
        $gradet = $bdd->unique_query("SELECT id FROM $table_alliances_grade WHERE id_alliance = ".$planete->id." AND id = $grade;");
        if (!empty($gradet) || $grade == 0) {
            $bdd->query("UPDATE $table_user SET id_grade_alliance = $grade WHERE id_alliance = ".$planete->id." AND id = $id;");
        }
        $bdd->deconnexion();

        header('Location: ?p=gestion&g=membres');
        exit;
    }

    $bdd->reconnexion();
    $membres = $bdd->query("SELECT id, pseudo, race, last_visite, id_grade_alliance FROM $table_user WHERE id_alliance = ".$planete->id.";");
    $grades = $bdd->query("SELECT id, id_alliance, nom, auth FROM $table_alliances_grade WHERE id_alliance = ".$planete->id.";");
    $postulants = $bdd->query("SELECT A.id, A.timestamp, A.message, U.pseudo, U.id AS id_user FROM $table_alliances_attente A INNER JOIN $table_user U ON A.id_user = U.id WHERE A.id_alliance = ".$planete->id.";");
    $bdd->deconnexion();
    $template->assign("membres", $membres);
    $template->assign("grades", $grades);
    $template->assign("postulants", $postulants);

    unset($membres, $grades, $grade, $id, $postulants);
}

$template->assign("onglet", $gestion);
unset($gestion);
