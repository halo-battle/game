<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}

$planete->loadDetails();
if (!empty($planete->url_chat)) {
    //On vérifie que le joueur ait les permissions requises
    if ($planete->permissions_alliance &256) {
        $page = "alliance/chat_irc";
    } else {
        erreur("Vous n'avez pas le grade requis pour accéder au chat de l'alliance.");
    }
} else {
    $page = "alliance/chat_ajax";
}
