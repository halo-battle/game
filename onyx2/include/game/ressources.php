<?php
if (!defined('INDEX')) {
    header('Location: ./'.$VAR['first_page']);
    exit;
}
$titre = 'Ressources';

if (SURFACE == "planete") {
    $page = 'ressources';
    if (isset($_POST['anb']) && $planete->batiments[4]) {
        $anb = gpc("anb", "post");
        if ($planete->checkAndRetireRessources(0, 0, $anb*(7000-$planete->batiments[4]*1250), $anb*(7000-$planete->batiments[4]*1250), 0)) {
            $planete->energieCentrale += $anb;
            $planete->addModif("energieCentrale");
        } else {
            erreur("Vous n'avez pas assez de ressources pour augmenter la capacité de votre ".$LANG[$race]["batiments"]["noms_sing"][4]." !");
        }
    } elseif (isset($_POST['coeff_metal']) && isset($_POST['coeff_cs'])) {
        $planete->coeff_bat[0] = floor($_POST['coeff_metal'])/100;
        if (isset($_POST['coeff_cristal'])) {
            $planete->coeff_bat[1] = floor($_POST['coeff_cristal'])/100;
        }
        if (isset($_POST['coeff_hydrogene'])) {
            $planete->coeff_bat[2] = floor($_POST['coeff_hydrogene'])/100;
        }
        $planete->coeff_bat[3] = floor($_POST['coeff_cs'])/100;
        if (isset($_POST['coeff_ce'])) {
            $planete->coeff_bat[4] = floor($_POST['coeff_ce'])/100;
        }
        $planete->addModif("coeff_bat");

        redirection('?p=ressources');
    }

    $tablo = $planete->production(3600, true);

    $template->assign('ressources_prod', $tablo[1]);
    $template->assign('ressources_conso', $tablo[2]);
    $template->assign('ressources_coef', $tablo[0]);
    $template->assign('ressources_silo', array($planete->batiments[10], pow(2, $planete->batiments[10]) * 100000));
    $template->assign('ressources_toto', array($tablo[1][2] - $tablo[2][3], '(0)'));

    $template->assign('ressources_tab', array(0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100));

    unset($tablo);
} else {
    $page = 'ressources_alli';

    $don_credits = intval(gpc("credits", "post"));
    if (!empty($don_credits) && $don_credits > 0) {
        //Si le joueur n'a pas assez de crédits
        if ($don_credits > $planete->credits) {
            erreur('Vous n\'avez pas suffisamment de crédits pour en donner autant.', 'red', $VAR['menu']['ressources']);
        }

        $planete->addCreditsAlliance($don_credits);
        $planete->addCredits(-1 * $don_credits);

        erreur('Votre don de '.$don_credits.' '.$LANG[$race]["ressources"]["noms"]["credits"].' a été effectué avec succès.', 'green', $VAR['menu']['ressources']);
    }

    unset($don_credits);
}
