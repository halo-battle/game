<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}

require_once("Class/class.donnee.php");

//Récupération des informations envoyées
$idPrep = gpc("cds", "post");

$SESS->values["prepFlottes"][$idPrep]['nom'] = gpc('nomflotte', 'post');
$SESS->values["prepFlottes"][$idPrep]['end_galaxie'] = $end_galaxie = intval(gpc('amas', 'post'));
$SESS->values["prepFlottes"][$idPrep]['end_systeme'] = $end_ss = intval(gpc('ss', 'post'));
$end_pos = gpc('pos', 'post');
if ($end_pos != "A") {
    $SESS->values["prepFlottes"][$idPrep]['end_position'] = floor($end_pos);
} else {
    $SESS->values["prepFlottes"][$idPrep]['end_position'] = $end_pos;
}
$SESS->values["prepFlottes"][$idPrep]['embarquer'] = array($EBmetal = floor(str_replace(' ', '', gpc('metal', 'post'))), $EBcristal = floor(str_replace(' ', '', gpc('cristal', 'post'))), $EBhydrogene = floor(str_replace(' ', '', gpc('hydrogene', 'post'))));
$SESS->values["prepFlottes"][$idPrep]['mission'] = $mission = intval(gpc('mission', 'post'));
$SESS->values["prepFlottes"][$idPrep]['vitesse'] = $vitesse = intval(gpc('vitesse', 'post'));

//On met à jour la session
$SESS->put();

//Vérification du nombre de vaisseaux sur la planète
foreach ($SESS->values["prepFlottes"][$idPrep]['vaisseaux'] as $key => $vaisseau) {
    //On vérifie qu'il y a suffisamment de vaisseaux sur la planète
    if ($planete->vaisseaux[$key] < $vaisseau) {
        erreur('Vous n\'avez pas assez de vaisseaux sur cette planète pour envoyer cette flotte !', "red", $VAR["menu"]["flotte"]);
    }
}

//Vérification des conditions de mission
if ($mission <= 0 || $mission > 7 || ($SESS->values["prepFlottes"][$idPrep]['type'] == 3 && $mission != 3 && $mission != 2)) {
    erreur("La mission sélectionnée est incorrecte !", "red", $VAR["menu"]["flotte"]);
}

//Vérification que le nombre de slots ne soit pas dépassé
if (slots($planete->id_user) <= 0) {
    erreur('Vous ne pouvez pas envoyer plus de flottes simultanément.', "red", $VAR["menu"]["flotte"]);
}

//Vérifications en cas de mission colonisation
if ($mission == 2) {
    //On vérifie la mission, si elle est de coloniser, il faut qu'il y ait des vaisseaux de colonisation
    if ($SESS->values["prepFlottes"][$idPrep]['vaisseaux'][2] <= 0) {
        erreur('Vous ne pouvez pas coloniser sans vaisseau de colonisation !', "red", $VAR["menu"]["flotte"]);
    }

    //On vérifie qu'une colonisation d'asteroide soit bien faite par un fondateur d'alliance en cours de création
    if ($end_pos == "A") {
        $bdd->reconnexion();
        $resultat = $bdd->unique_query("SELECT id FROM $table_alliances_creation WHERE fondateur = ".$planete->id_user.";");
        $bdd->deconnexion();
        if (!$resultat) {
            erreur('Fonder d\'abord une alliance avant de coloniser un astéroide !', "red", $VAR["menu"]["flotte"]);
        }
    }

    // on vérifie que le joueur n'essaye pas de coloniser plus de planète que sa techno expansion lui permet
    $bdd->reconnexion();
    $resultat = $bdd->unique_query("SELECT COUNT(id) as nb_planete FROM $table_planete WHERE id_user = ".$planete->id_user.";");
    $bdd->deconnexion();
    $nb_planete = $resultat["nb_planete"];
    $branche = 8;
    $idTechnologie = $nb_planete;
    $neededTechnologies = Donnee::donneeTechnologie($branche, $idTechnologie, "needed", $planete);
    if (((int)$planete->technologies[$branche]& $neededTechnologies) != $neededTechnologies) {
        erreur('Vous ne pouvez pas coloniser plus de planète que votre niveau d\'expansion');
    }
}

//On vérifie que les attaques soient bien activées
if ($mission == 3 && !$VAR["attaques"]) {
    erreur('Les attaques sont désactivées pour le moment. Pour plus d\'informations, <a href="'.$VAR["menu"]["forums"].'">consultez le forum</a>.', "red", $VAR["menu"]["flotte"], 5000);
}

//On vérifie la mission, si elle est de recycler, il faut qu'il y ait des reclycleurs
if ($mission == 4 && $SESS->values["prepFlottes"][$idPrep]['vaisseaux'][3] <= 0) {
    erreur('Vous ne pouvez pas recycler sans recycleur !', "red", $VAR["menu"]["flotte"]);
}

//On vérifie la mission, si elle est d'espionner, il faut qu'il y ait des sondes
if ($mission == 5 && (($planete->race == "humain" && $SESS->values["prepFlottes"][$idPrep]['vaisseaux'][13] <= 0) || ($planete->race == "covenant" && $SESS->values["prepFlottes"][$idPrep]['vaisseaux'][7] <= 0))) {
    erreur('Vous ne pouvez pas espionner sans sonde d\'espionnage !', "red", '?p=flotte');
}

//Vérification que la destination ne soit pas en dehors de la galaxie
if ($end_galaxie > $VAR['nb_amas'] || $end_ss > $VAR['nb_systeme'] || $end_galaxie < 0 || $end_ss < 1 || (($end_pos > $VAR['nb_planete'] || $end_pos < 1) && $end_pos != "A") || ($end_galaxie < 1 && $SESS->level < 6)) {
    erreur('La destination de la flotte n\'est pas correcte.', "red", '?p=flotte');
}

//On vérifie que l'on possède assez de ressources
if ((!empty($EBmetal) && !$EBmetal > $planete->metal) || (!empty($EBcristal) && !$EBcristal > $planete->cristal) || (!empty($EBhydrogene) && !$EBhydrogene > $planete->hydrogene)) {
    erreur('Vous ne pouvez pas envoyer plus de ressources que vous n\'en posséder.', "red", '?p=flotte');
}

//On vérifie que l'on n'envoie pas des ressources négatives
if ((!empty($EBmetal) && $EBmetal < 0) || (!empty($EBcristal) && $EBcristal < 0) || (!empty($EBhydrogene) && $EBhydrogene < 0)) {
    erreur('Vous avez spécifié des valeurs de ressources à embarquer incorrectes !', "red", '?p=flotte', 4000);
}

//On vérifie la vitesse de la flotte
if (!is_numeric($vitesse) || $vitesse < 0 || $vitesse > 100) {
    erreur('La vitesse de votre flotte est incorrecte !', "red", '?p=flotte');
}


//Recherche de la planète ou de l'astéroïde
if ($end_pos == "A") {
    $bdd->reconnexion();
    $resultat = $bdd->unique_query("SELECT id, id AS id_user, debris_met, debris_cri FROM $table_alliances WHERE galaxie = $end_galaxie AND ss = $end_ss;");
    $bdd->deconnexion();
} else {
    $bdd->reconnexion();
    $resultat = $bdd->unique_query("SELECT id, id_user, debris_met, debris_cri FROM $table_planete WHERE galaxie = $end_galaxie AND ss = $end_ss AND position = $end_pos;");
    $bdd->deconnexion();
}

//On vérifie qu'il n'y ait pas une interaction entre deux multi-comptes
if (count($multi) > 1 && ($mission == 1 || $mission == 6 || $mission == 7)) {
    foreach ($multi as $test) {
        if ($test['id_util'] == $resultat['id_user']) {
            erreur('Vous ne pouvez pas avoir d\'interaction avec ce joueur pour raison de multi-compte (voir page d\'accueil).');
        }
    }
}

if ($mission == 1 && !$resultat) {
    erreur('Impossible de transporter des ressources vers la planète ['.$end_galaxie.':'.$end_ss.':'.$end_pos.'] car elle est inhabitée.', "red", '?p=flotte', 4000);
} elseif ($mission == 2 && $resultat) {
    if ($end_pos != "A") {
        erreur('La planète que vous voulez coloniser est déjà habitée.', "red", '?p=flotte', 3000);
    } else {
        erreur('L\'astéroide que vous voulez coloniser est déjà habitée.', "red", '?p=flotte', 3000);
    }
}
//Si la mission est d'attaquer, on vérifie que le joueur cible ne soit pas ne mode vacances ou qu'il soit tout jeune
elseif ($mission == 3 && $end_pos != "A") {
    $bdd->reconnexion();
    $resultatu = $bdd->unique_query("SELECT mv, time_inscription FROM $table_user WHERE id = ".$resultat['id_user'].";");
    $bdd->deconnexion();

    if ($resultatu['mv'] > 0) {
        erreur('Le joueur que vous tentez d\'attaquer est actuellement en mode vacances, vous ne pouvez donc pas l\'attaquer avant son retour de vacances.', "red", '?p=flotte', 3000);
    } //TODO Mettre le numéro du mode vacances et non > 0 !!!
    elseif ($resultatu['time_inscription'] + 604800 > time()) {
        erreur('Le joueur que vous tentez d\'attaquer s\'est inscrit récemment, laissez-lui le temps de se préparer au combat !', "red", '?p=flotte', 3000);
    } elseif (!$resultat) {
        erreur('La planète que vous tentez d\'attaquer est inhabitée.', "red", '?p=flotte');
    } elseif ($resultat['id_user'] == $planete->id_user) {
        erreur('La planète que vous tentez d\'attaquer vous appartient.', "red", '?p=flotte');
    }
} elseif ($mission == 4 && ($resultat['debris_met'] <= 0 || $resultat['debris_cri'] <= 0) && empty($SESS->values['forceFlotte'])) {
    $SESS->values['forceFlotte'] = true;
    $SESS->put();
    erreur('Il n\'y a rien à recycler sur la planète ['.$end_galaxie.':'.$end_ss.':'.$end_pos.'].<br />Vous pouvez forcer le lancement de la flotte en rechargeant cette page.', "orange");
}
//TODO Autoriser l'espionnage sur les planètes inhabités
elseif ($mission == 5) {
    if (!$resultat) {
        erreur('Impossible d\'espionner la planète ['.$end_galaxie.':'.$end_ss.':'.$end_pos.'] car elle est inhabitée.', "red", '?p=flotte', 4000);
    } elseif ($end_pos != "A" && $resultat['id_user'] == $planete->id_user) {
        erreur('La planète que vous désirez espionner vous appartient !', "red", '?p=flotte', 3000);
    }
} elseif ($mission == 6 && $resultat['id_user'] != $planete->id_user && $resultat['id_user'] != $planete->id_alliance) {
    erreur('La planète sur laquelle vous désirez stationner ne vous appartient pas.', "red", '?p=flotte', 3000);
} elseif ($mission == 7 && !$resultat) {
    erreur('La planète sur laquelle vous désirez donner vos vaisseaux n\'existe pas.', "red", '?p=flotte', 3000);
} elseif ($mission == 7) {
    $bdd->reconnexion();
    $resultatu = $bdd->unique_query("SELECT race FROM $table_user WHERE id = ".$resultat['id_user'].";");
    $bdd->deconnexion();

    if ($resultatu['race'] != $planete->race) {
        erreur("Vous ne pouvez pas donner vos vaisseaux a une autre race que la votre", "red");
    }
}



//Création de la flotte
$flotte = new flotte();
$flotte->creer($planete, $SESS->values["prepFlottes"][$idPrep], $resultat);
$SESS->values["prepFlottes"][$idPrep]["statut"] = 2;
$SESS->put();

erreur('Votre flotte a été envoyée avec succès.', "green", '?p=flotte', 4000);
