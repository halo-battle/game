<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}

//Génération d'un ID unique pour identifier la flotte durant sa création
$idPrep = random();

//Supression des précédentes flottes, sinon il y a un risque de remplir le champs de BDD qui sauvegarde les flottes
$SESS->values["prepFlottes"] = null;
$SESS->values["prepFlottes"] = array();

//Création du tableau de session
$SESS->values["prepFlottes"][$idPrep] = array();

//On récupère les vaisseaux à envoyer
$nombreVaisseau = 0;
foreach ($planete->vaisseaux as $key => $vaisseau) {
    $v = gpc('v'.$key, 'post');
    if (!is_numeric($v) || $v < 0) {
        $v = 0;
    }

    $SESS->values["prepFlottes"][$idPrep]['vaisseaux'][$key] = $v;
    $nombreVaisseau += $v;
}
//On vérifie que l'utilisateur a bien envoyé plus d'un vaisseau
if ($nombreVaisseau <= 0) {
    unset($SESS->values["prepFlottes"][$idPrep]);
    erreur('Vous devez envoyer au moins un vaisseau.', "red", '?p=flotte');
}

//On définit le type de la flotte (utilisateur ou alliance)
if (!empty($_POST['envoie'])) {
    $SESS->values["prepFlottes"][$idPrep]['type'] = 1;
} elseif (!empty($_POST['groupe'])) {
    $SESS->values["prepFlottes"][$idPrep]['type'] = 2;
} else {
    die('Erreur !');
}

//On enregistre les paramètres en session
$SESS->values["prepFlottes"][$idPrep]['nbVaisseaux'] = $nombreVaisseau;
$SESS->values['forceFlotte'] = false;
$SESS->values["prepFlottes"][$idPrep]['time'] = time();
$SESS->values["prepFlottes"][$idPrep]['statut'] = 1;
$SESS->values["prepFlottes"][$idPrep]['vitesse'] = 100; // vitesse par défaut: 100%
$SESS->put();

unset($nombreVaisseau, $key, $vaisseau, $v);

header('Location: ?p=flotte&c='.$idPrep);
exit;
