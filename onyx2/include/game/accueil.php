<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$page = 'accueil';
$titre = 'Accueil';

//Affichage de l'alliance du joueur
$bdd->reconnexion();
//$alli = $bdd->unique_query("SELECT * FROM $table_alliances WHERE id = '".$planete->id_alliance."';");

//On regarde si le joueur a une flotte en vue
$radar = array();
$detect = 86400;
foreach ($queryPlanetes as $planeteJoueur) {
    $end_id = $planeteJoueur['id'];
    $radar[] = array($bdd->query("SELECT F.mission, F.start_planete, F.start_time, F.end_time, F.start_time + F.end_time - ".time()." AS arrive_time, P.nom_planete, P.galaxie as start_galaxie, P.ss as start_ss, P.position as start_position, U.pseudo FROM $table_flottes F INNER JOIN $table_planete P ON P.id = F.start_planete INNER JOIN $table_user U ON U.id = P.id_user WHERE F.statut != '1' AND F.end_planete = '$end_id' AND F.id_user != $id_user AND F.start_time + F.end_time - ".time()." <= $detect;"), array($planeteJoueur['nom_planete'], $planeteJoueur['galaxie'], $planeteJoueur['ss'], $planeteJoueur['position']));
}
$bdd->deconnexion();
$template->assign('radar', $radar);
//$template->assign('alliance', $alli);
unset($nbPlan, $end_id, $radar, $alli);

if (SURFACE == "planete") {
    //Affichage des informations sur la planète
    $template->assign('diametre', $planete->cases * 92);
    $template->assign('points', $planete->points);
    $template->assign('fileBat', $planete->file_bat->printFile($planete));
    $template->assign('fileCas', $planete->file_cas->printFile($planete));
    $template->assign('fileVais', $planete->file_vais->printFile($planete));
    $template->assign('fileTer', $planete->file_ter->printFile($planete));
    $template->assign('fileTech', $planete->file_tech->printFile($planete));
} else {
    //On charge l'utilisateur pour récupérer son pseudo et l'afficher sur la page d'accueil
    $planete->fondateur = new User($planete->fondateur);
    $planete->loadDetails();
}
