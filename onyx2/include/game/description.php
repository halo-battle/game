<?php
if (!defined('INDEX') || SURFACE != "planete") {
    header('Location: ./'.$VAR['first_page']);
    exit;
}
$page = 'description';
$titre = 'D&eacute;tails';

function tableauProd($ibat, $nivo)
{
    $return = array();
    $c = $nivo-2 < 1 ? 1 : $nivo-2;
    $a = $c + 6;

    if ($ibat == 0) {
        for ($c; $c < $a; $c++) {
            $production = ceil(pow(1.1, $c) * 52 * $c);
            $production_diff = ceil(pow(1.1, $c) * 52 * $c) - ceil(pow(1.1, $nivo) * 52 * $nivo);

            $energie = ceil(exp(0.28*$c)*10);
            $energie_diff = ceil(exp(0.28*$c)*10) - ceil(exp(0.28*$nivo)*10);

            $return[] = array($c, $production, $energie, $production_diff, $energie_diff);
        }
    } elseif ($ibat == 1) {
        for ($c; $c < $a; $c++) {
            $production = ceil(pow(1.1, $c) * 34 * $c);
            $production_diff = ceil(pow(1.1, $c) * 34 * $c) - ceil(pow(1.1, $nivo) * 52 * $nivo);

            $energie = ceil(exp(0.28*$c)*10);
            $energie_diff = ceil(exp(0.28*$c)*10) - ceil(exp(0.28*$nivo)*10);

            $return[] = array($c, $production, $energie, $production_diff, $energie_diff);
        }
    } elseif ($ibat == 2) {
        for ($c; $c < $a; $c++) {
            $production = ceil(pow(1.1, $c) * 21 * ($c + 0.7));
            $production_diff = ceil(pow(1.1, $c) * 21 * ($c + 0.7)) - ceil(pow(1.1, $nivo) * 21 * ($nivo + 0.7));

            $energie = ceil(exp(0.2849*$c)*13);
            $energie_diff = ceil(exp(0.2849*$c)*13) - ceil(exp(0.2849*$nivo)*13);

            $return[] = array($c, $production, $energie, $production_diff, $energie_diff);
        }
    } elseif ($ibat == 3) {
        for ($c; $c < $a; $c++) {
            $production = ceil(exp(0.28*$c)*22);
            $production_diff = ceil(exp(0.28*$c)*22) - ceil(exp(0.28*$nivo)*22);

            $return[] = array($c, $production, 0, $production_diff, 0);
        }
    } elseif ($ibat == 4) {
        for ($c; $c < $a; $c++) {
            $production = ceil(exp(0.297*$c)*25);
            $production_diff = ceil(exp(0.297*$c)*25) - ceil(exp(0.297*$nivo)*25);

            $energie = ceil(pow(1.34, ($c-1)) * 9);
            $energie_diff = ceil(pow(1.34, ($c-1)) * 9) - ceil(pow(1.34, ($nivo-1)) * 9);

            $return[] = array($c, $production, $energie, $production_diff, $energie_diff);
        }
    } else {
        return false;
    }

    return $return;
}

if (empty($_GET['r']) || ($_GET['r'] != 'humain' && $_GET['r'] != 'covenant')) {
    $_GET['r'] = $planete->race;
}
$race = gpc('r');
$template->assign('raceAff', $race);

if (isset($_GET['t']) && is_numeric($_GET['t']) && !empty($LANG[$race]["technologies"]["noms_sing"][$_GET['b']][$_GET['t']])) {
    $template->assign('type', 'technologies');
    $b = intval(gpc('b'));
    $t = intval(gpc('t'));
    $template->assign('branche', $b);
    $template->assign('id', $t);

    // calcul du niveau de la techno pour le joueur
    $niveau = dTechnologies::niveau_du_joueur($b, $t, $planete);
    $niveau_max = dTechnologies::niveau_max($b, $t, $planete, $LANG);

    // On n'affiche plus que les infos du niveau qu'on as actuellement
    if ($niveau != 0) {
        $t += ($niveau - 1);
    }
    $object = array();
    $object[0] = $b;
    $object[1] = $t;

    $template->assign('ressourcesNext', array(dTechnologies::credits($b, $t, $planete), dTechnologies::temps($b, $t, $planete)));
    $template->assign('niveau', $niveau);
    $template->assign('niveau_max', $niveau_max);
    $template->assign('etat', dTechnologies::needed($object, $planete, $race));
} elseif (isset($_GET['b']) && is_numeric($_GET['b']) && !empty($LANG[$race]["batiments"]["noms_sing"][$_GET['b']])) {
    $template->assign('type', 'batiments');
    $t = intval(gpc('b'));
    $template->assign('id', $t);
    $template->assign('image', dBatiments::image($t, $planete));
    $template->assign('tableau_prod', tableauProd($t, $planete->batiments[$t]));

    $n = $planete->batiments[$t] + 1;
    $template->assign('ressourcesNext', array(dBatiments::metal($t, $n, $planete), dBatiments::cristal($t, $n, $planete), dBatiments::hydrogene($t, $n, $planete), dBatiments::temps($t, $n, $planete)));

    $template->assign('etat', dBatiments::needed($t, $planete, true));
} elseif (isset($_GET['v']) && is_numeric($_GET['v']) && !empty($LANG[$race]["vaisseaux"]["noms_sing"][$_GET['v']])) {
    $template->assign('type', 'vaisseaux');
    $t = intval(gpc('v'));
    $template->assign('id', $t);
    $template->assign('image', dSpatial::image($t, $planete, $race));

    $template->assign('ressourcesNext', array(dSpatial::metal($t, 1, $planete, $race), dSpatial::cristal($t, 1, $planete, $race), dSpatial::hydrogene($t, 1, $planete, $race), dSpatial::temps($t, 1, $planete, $race)));

    $template->assign('etat', dSpatial::needed($t, $planete, $race));
    $template->assign('caract', array(dSpatial::attaque($t, $planete, $race), dSpatial::bouclier($t, $planete, $race), dSpatial::armature($t, $planete, $race), dSpatial::tailleCales($t, $planete, $race)));
} elseif (isset($_GET['d']) && is_numeric($_GET['d']) && !empty($LANG[$race]["terrestre"]["noms_sing"][$_GET['d']])) {
    $template->assign('type', 'terrestre');
    $t = gpc('d');
    $template->assign('id', $t);
    $template->assign('image', dTerrestre::image($t, $planete));

    $template->assign('ressourcesNext', array(dTerrestre::metal($t, 1, $planete), dTerrestre::cristal($t, 1, $planete), dTerrestre::hydrogene($t, 1, $planete), dTerrestre::temps($t, 1, $planete)));

    $template->assign('etat', dTerrestre::needed($t, $planete, true));
    $template->assign('caract', array(dTerrestre::attaque($t, $planete, $race), dTerrestre::bouclier($t, $planete, $race), dTerrestre::armature($t, $planete, $race)));
} elseif (isset($_GET['c']) && is_numeric($_GET['c']) && !empty($LANG[$race]["caserne"]["noms_sing"][$_GET['c']])) {
    $template->assign('type', 'caserne');
    $t = gpc('c');
    $template->assign('id', $t);
    $template->assign('image', dCaserne::image($t, $planete));

    $template->assign('ressourcesNext', array(dCaserne::metal($t, 1, $planete), dCaserne::cristal($t, 1, $planete), dCaserne::hydrogene($t, 1, $planete), dCaserne::temps($t, 1, $planete)));

    $template->assign('etat', dCaserne::needed($t, $planete, true));
    $template->assign('caract', array('!!', '!!', '!!'));
} else {
    redirection('?p=accueil');
}

unset($t, $race);
