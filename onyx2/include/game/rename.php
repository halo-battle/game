<?php
if (!defined('INDEX') || SURFACE != "planete") {
    header('Location: ./'.$VAR['first_page']);
    exit;
}
$page = 'rename';
$titre = 'Renommer une planète';

if (isset($_POST['planete'])) {
    $nouvNom = trim(str_replace('&nbsp;', ' ', $_POST['planete']));
    if (empty($nouvNom)) {
        erreur('Vous n\'avez indiqué aucun nom de planète.', "red", '?p=rename');
    } elseif (limite($nouvNom, 18)) {
        erreur('Le nom de votre planète est trop long.', "red", '?p=rename');
    } elseif (preg_match('#staf#', strtolower($nouvNom)) && $SESS->level < 4) {
        erreur('Vous devez faire parti du staff pour afficher le nom "staff" dans le nom de votre planète !', "red", '?p=rename');
    } else {
        $planete->nom_planete = $nouvNom;
        $planete->addModif("nom_planete");

        header('Location: ?p=accueil');
        exit;
        //erreur('Le nom de votre planète a été modifié avec succès.', "green", '?p=accueil');
    }
    unset($nouvNom);
} elseif (!empty($_GET['a']) && !empty($SESS->values['abandon']) && isset($_GET['i']) && $_GET['a'] == $SESS->values['abandon'] && $planete->id == $_GET['i']) {
    $bdd->reconnexion();
    $bdd->query("DELETE FROM $table_planete WHERE id_user = ".$planete->id_user." AND id = ".$planete->id." AND galaxie = ".$planete->galaxie." AND ss = ".$planete->ss." AND position = ".$planete->position." LIMIT 1;");
    $bdd->query("DELETE FROM $table_flottes WHERE id_user = ".$planete->id_user." AND start_galaxie = ".$planete->galaxie." AND start_ss = ".$planete->ss." AND start_position = ".$planete->position.";");

    $req = $bdd->unique_query("SELECT id FROM $table_planete WHERE id_user = ".$planete->id_user." LIMIT 1;");
    $bdd->deconnexion();
    $SESS->values['abandon'] = 0;
    unset($SESS->values['abandon']);
    $SESS->values['idPlan'] = $req['id'];
    $SESS->put();

    unset($req, $planete);
    erreur('Cette planète n\'est désormais plus sous votre contrôle.', "green", '?p=accueil');
}

if (count($queryPlanetes) > 1) {
    $template->assign('abandonH', $SESS->values['abandon'] = md5(rand(123456789, 9876543210)));
    $SESS->put();
}
unset($queryPlanetes);
