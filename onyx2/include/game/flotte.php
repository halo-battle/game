<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$titre = 'Flottes';
$page = 'flotte'; //Définition d'un nom de page inexistant, mais permet de passer le nom de l'onglet dans une erreur
include_once("Class/flotte.php");

//Création de flotte : envoi final
if (!empty($_POST['cds']) && !empty($SESS->values["prepFlottes"][$_POST['cds']])) {
    require('game/flottes/envoyer.php');
}
//Restauration d'une flotte sauvegardée
elseif (isset($_GET['c']) && !empty($SESS->values["prepFlottes"][$_GET['c']])) {
    require('game/flottes/restaure.php');
}
//Création de flotte : page 2
elseif (!empty($_POST['envoie']) || !empty($_POST['groupe'])) {
    require('game/flottes/preparer.php');
}
//Affichage du détail d'une flotte
elseif (!empty($_GET['n'])) {
    require('game/flottes/details.php');
}
//Affichage de la page générale
else {
    require('game/flottes/principal.php');
}
