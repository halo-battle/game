<?php
if (!defined('INDEX') || SURFACE != "asteroide") {
    header('Location: ../');
    exit;
}

$onglet = strtolower(gpc('o'));
if ($onglet == "new") {
    $page = "diplomatie/nouveau";

    //On vérifie que le joueur ait les permissions pour modifier les grades
    if (!($planete->permissions_alliance &8)) {
        erreur("Vous n'avez pas le grade nécessaire pour créer des pactes !");
    }

    $type = intval(gpc("type", 'post'));
    $nom_alli = gpc("nom_alli", 'post');
    if (isset($_POST["type"]) && !empty($nom_alli) && $type >= 0 && $type < 5) {
        $bdd->reconnexion();
        $bdd->escape($nom_alli);
        $alli = $bdd->unique_query("SELECT id FROM $table_alliances WHERE nom_alliance LIKE '$nom_alli';");
        if (empty($alli)) {
            erreur("Impossible de trouver cette alliance. Vérifiez le nom !");
        }
        //On détecte tous les types de pactes en cas de guerre (pour éviter d'attaquer avec un pacte de non agression !
        if ($type) {
            $pacte = $bdd->query("SELECT id FROM $table_alliances_pactes WHERE type = $type AND time_fin = 0 AND (accepte = 0 OR accepte = 1) AND ((id_alliance1 = ".$planete->id." AND id_alliance2 = ".$alli['id'].") OR (id_alliance2 = ".$planete->id." AND id_alliance1 = ".$alli['id']."));");
        } else {
            $pacte = $bdd->query("SELECT id FROM $table_alliances_pactes WHERE time_fin = 0 AND (accepte = 0 OR accepte = 1) AND ((id_alliance1 = ".$planete->id." AND id_alliance2 = ".$alli['id'].") OR (id_alliance2 = ".$planete->id." AND id_alliance1 = ".$alli['id']."));");
        }

        if (!empty($pacte) && !$type) {
            erreur("Avant de déclarer une guerre, veuillez abroger tous vos pactes de paix !");
        } elseif (!empty($pacte)) {
            erreur("Vous avez déjà un pacte similaire avec cette alliance !");
        }

        if ($type) {
            $bdd->query("INSERT INTO $table_alliances_pactes (id_alliance1, id_alliance2, type, time_creation) VALUES (".$planete->id.", ".$alli['id'].", $type, ".time().");");
        } else {
            $bdd->query("INSERT INTO $table_alliances_pactes (id_alliance1, id_alliance2, type, time_creation, accepte) VALUES (".$planete->id.", ".$alli['id'].", $type, ".time().", 1);");
        }
        $bdd->deconnexion();

        if ($type) {
            erreur("La demande de pacte a bien été enregistrée.", "green");
        } else {
            erreur("La déclaration a bien été enregistrée.", "green");
        }
    } elseif (!empty($_GET['c'])) {
        $id = intval(gpc('c'));
        $bdd->reconnexion();
        $demand = $bdd->unique_query("SELECT id_alliance1, id_alliance2, accepte FROM $table_alliances_pactes WHERE id = $id AND (id_alliance2 = ".$planete->id." OR id_alliance1 = ".$planete->id.") AND type = 0;");
        if (!isset($demand['accepte']) || ($demand['accepte'] == 1 && $planete->id == $demand['id_alliance2']) || ($demand['accepte'] == 2 && $planete->id == $demand['id_alliance1'])) {
            $bdd->deconnexion();
            erreur("Impossible de demander le cesser le feu, n'êtes-vous pas dans la position du demandeur !");
        } elseif ($demand['accepte'] == 0) {
            $bdd->deconnexion();
            erreur("Une demande de cesser le feu est déjà en cours !");
        } elseif ($demand['accepte'] == 1) {
            $bdd->query("UPDATE $table_alliances_pactes SET accepte = 0 WHERE id = $id AND id_alliance1 = ".$planete->id." AND accepte = 1;");
            $bdd->deconnexion();

            erreur("La demande de cesser le feu a bien été transmise.", "green");
        } elseif ($demand['accepte'] == 2) {
            $bdd->query("UPDATE $table_alliances_pactes SET accepte = 0, id_alliance2 = id_alliance1, id_alliance1 = ".$planete->id." WHERE id = $id AND id_alliance2 = ".$planete->id." AND accepte = 2;");
            $bdd->deconnexion();

            erreur("La demande de cesser le feu a bien été transmise.", "green");
        }
    }
} elseif ($onglet == "archives") {
    $page = "diplomatie/archives";

    $bdd->reconnexion();
    $guerres = $bdd->query("SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance2 WHERE P.type = 0 AND P.id_alliance1 = ".$planete->id." AND accepte = 1 AND time_fin != 0
					  UNION SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance1 WHERE P.type = 0 AND P.id_alliance2 = ".$planete->id." AND accepte = 1 AND time_fin != 0 ORDER BY nom_alliance DESC;");
    $pna = $bdd->query("SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance2 WHERE P.type = 1 AND P.id_alliance1 = ".$planete->id." AND accepte = 1 AND time_fin != 0
				  UNION SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance1 WHERE P.type = 1 AND P.id_alliance2 = ".$planete->id." AND accepte = 1 AND time_fin != 0 ORDER BY nom_alliance DESC;");
    $pc = $bdd->query("SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance2 WHERE P.type = 2 AND P.id_alliance1 = ".$planete->id." AND accepte = 1 AND time_fin != 0
				 UNION SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance1 WHERE P.type = 2 AND P.id_alliance2 = ".$planete->id." AND accepte = 1 AND time_fin != 0 ORDER BY nom_alliance DESC;");
    $pm = $bdd->query("SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance2 WHERE P.type = 3 AND P.id_alliance1 = ".$planete->id." AND accepte = 1 AND time_fin != 0
				 UNION SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance1 WHERE P.type = 3 AND P.id_alliance2 = ".$planete->id." AND accepte = 1 AND time_fin != 0 ORDER BY nom_alliance DESC;");
    $bdd->deconnexion();

    $template->assign("pnas", $pna);
    $template->assign("guerres", $guerres);
    $template->assign("pcs", $pc);
    $template->assign("pms", $pm);
} elseif ($onglet == "encours") {
    $page = "diplomatie/encours";

    $bdd->reconnexion();
    $guerres = $bdd->query("SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance2 WHERE P.type = 0 AND P.id_alliance1 = ".$planete->id." AND (time_fin = 0 OR time_fin > ".time().")
					  UNION SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance1 WHERE P.type = 0 AND P.id_alliance2 = ".$planete->id." AND (time_fin = 0 OR time_fin > ".time().")
					  ORDER BY nom_alliance DESC;");
    $pna = $bdd->query("SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance2 WHERE P.type = 1 AND P.id_alliance1 = ".$planete->id." AND accepte = 1 AND (time_fin = 0 OR time_fin > ".time().")
					  UNION SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance1 WHERE P.type = 1 AND P.id_alliance2 = ".$planete->id." AND accepte = 1 AND (time_fin = 0 OR time_fin > ".time().")
					  ORDER BY nom_alliance DESC;");
    $pc = $bdd->query("SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance2 WHERE P.type = 2 AND P.id_alliance1 = ".$planete->id." AND accepte = 1 AND (time_fin = 0 OR time_fin > ".time().")
					   UNION SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance1 WHERE P.type = 2 AND P.id_alliance2 = ".$planete->id." AND accepte = 1 AND (time_fin = 0 OR time_fin > ".time().")
					   ORDER BY nom_alliance DESC;");
    $pm = $bdd->query("SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance2 WHERE P.type = 3 AND P.id_alliance1 = ".$planete->id." AND accepte = 1 AND (time_fin = 0 OR time_fin > ".time().")
					   UNION SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance1 WHERE P.type = 3 AND P.id_alliance2 = ".$planete->id." AND accepte = 1 AND (time_fin = 0 OR time_fin > ".time().")
					   ORDER BY nom_alliance DESC;");
    $bdd->deconnexion();

    $template->assign("pnas", $pna);
    $template->assign("guerres", $guerres);
    $template->assign("pcs", $pc);
    $template->assign("pms", $pm);
} else {
    $onglet = "actus";
    $page = "diplomatie/general";

    if ($planete->permissions_alliance &8) {
        if (!empty($_GET['a'])) {
            $id = intval(gpc('a'));
            $bdd->reconnexion();
            $demand = $bdd->unique_query("SELECT type FROM $table_alliances_pactes WHERE id = $id AND id_alliance2 = ".$planete->id." AND accepte = 0;");
            if (!isset($demand['type'])) {
                $bdd->deconnexion();
                erreur("Impossible de trouver le pacte !");
            } elseif ($demand['type'] == 0) {
                $bdd->query("UPDATE $table_alliances_pactes SET accepte = 1, time_fin = ".time()." WHERE id = $id AND id_alliance2 = ".$planete->id." AND accepte = 0;");
                $bdd->deconnexion();
                erreur("C'est la fin de la guerre, vous vennez d'accepter le cesser le feu !", "orange", $VAR['menu']['diplomatie']);
            } else {
                $bdd->query("UPDATE $table_alliances_pactes SET accepte = 1, time_creation = ".time()." WHERE id = $id AND id_alliance2 = ".$planete->id." AND accepte = 0;");
                $bdd->deconnexion();
                erreur("Le pacte a bien été accepté.", "green", $VAR['menu']['diplomatie']);
            }
        } elseif (!empty($_GET['r'])) {
            $id = intval(gpc('r'));
            $bdd->reconnexion();
            $demand = $bdd->unique_query("SELECT type FROM $table_alliances_pactes WHERE id = $id AND id_alliance2 = ".$planete->id." AND accepte = 0;");
            if (!isset($demand['type'])) {
                $bdd->deconnexion();
                erreur("Impossible de trouver le pacte !");
            } elseif ($demand['type'] == 0) {
                $bdd->query("UPDATE $table_alliances_pactes SET accepte = 2 WHERE id = $id AND id_alliance2 = ".$planete->id." AND accepte = 0;");
                $bdd->deconnexion();
                erreur("La guerre continue, vous vennez de refuser le cesser le feu !<br />Se sera désormais à vous de proposer un cesser le feu à la fin de la guerre.", "orange", $VAR['menu']['diplomatie']);
            } else {
                $bdd->query("UPDATE $table_alliances_pactes SET accepte = 2 WHERE id = $id AND id_alliance2 = ".$planete->id." AND accepte = 0;");
                $bdd->deconnexion();
                erreur("Le pacte a bien été refusé.", "orange", $VAR['menu']['diplomatie']);
            }
        } elseif (!empty($_GET['s'])) {
            $id = intval(gpc('s'));
            $bdd->reconnexion();
            $bdd->query("UPDATE $table_alliances_pactes SET time_fin = ".time()." WHERE id = $id AND (id_alliance2 = ".$planete->id." OR id_alliance1 = ".$planete->id.") AND accepte = 1;");
            $bdd->deconnexion();

            erreur("Le pacte a bien été abrogé.", "orange", $VAR['menu']['diplomatie']);
        }
    }

    $bdd->reconnexion();
    $demand = $bdd->query("SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance1 WHERE P.id_alliance2 = ".$planete->id." AND accepte = 0 ORDER BY time_demand DESC;");
    $actus_alli = $bdd->query("SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance2 WHERE P.id_alliance1 = ".$planete->id."
						 UNION SELECT P.*, A.nom_alliance, A.tag, A.id AS alliance_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance1 WHERE P.id_alliance2 = ".$planete->id." AND P.accepte != 0
						 ORDER BY time_demand DESC LIMIT 5;");
    $actus_world = $bdd->query("SELECT P.*, A.tag AS tag1, A.nom_alliance AS nom_alliance1, A.id AS alliance1_id, B.tag AS tag2, B.nom_alliance AS nom_alliance2, B.id AS alliance2_id FROM $table_alliances_pactes P INNER JOIN $table_alliances A ON A.id = P.id_alliance2 INNER JOIN $table_alliances B ON B.id = P.id_alliance1 WHERE P.accepte = 1 AND P.id_alliance1 != ".$planete->id." AND P.id_alliance2 != ".$planete->id." ORDER BY time_demand DESC LIMIT 5;");
    $bdd->deconnexion();

    $template->assign("demandes", $demand);
    $template->assign("actus_alli", $actus_alli);
    $template->assign("actus_world", $actus_world);
    unset($demand);
}

$template->assign("onglet", $onglet);
unset($onglet);
