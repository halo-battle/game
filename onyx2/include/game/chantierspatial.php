<?php
if (!defined('ONYX')) {
    exit;
}

$page = 'chantierspatial';
if (SURFACE == "planete") {
    $titre = ucfirst($LANG[$planete->race]['batiments']['noms_sing'][8]);
} else {
    $titre = ucfirst($LANG[$planete->race]['alli_batiments']['noms_sing'][3]);
}

//Si l'on est sur un astéroide, on vérifie que le joueur ait les permissions nécessaire
if (SURFACE == "asteroide" && !($planete->permissions_alliance &2)) {
    erreur("Vous n'avez pas le grade requis pour vous occuper des vaisseaux de l'astéroide.");
}

//Vérification que le joueur ait bien un labo avant d'afficher la page
if (SURFACE == "planete" && $planete->batiments[8] <= 0) {
    erreur('Vous devez d\'abord construire un '.$LANG[$planete->race]['batiments']['noms_sing'][8], "red", '?p=batiments', 3500);
} elseif (SURFACE == "asteroide" && $planete->batiments[3] <= 0) {
    erreur('Vous devez d\'abord construire une '.$LANG[$planete->race]['alli_batiments']['noms_sing'][3], "red", '?p=batiments', 3500);
}

if (SURFACE == "planete") {
    $lieu = intval(gpc("k"));
    if ($lieu >= $planete->batiments[8] || $lieu < 0) {
        $lieu = 0;
    }
} elseif (SURFACE == "asteroide") {
    $lieu = intval(gpc("k"));
    if ($lieu >= $planete->batiments[3] || $lieu < 0) {
        $lieu = 0;
    }
}

//Lancement d'une nouvelle construction
if (isset($_GET['v']) && ($id = intval(gpc('v'))) >= 0 && $nbv = floor(gpc('vais'.$id, 'post'))) {
    $planete->file_vais->addObjet($id, $nbv, $planete, $lieu);

    redirection($VAR['menu']['chantierspatial']."&k=".$lieu);
}
//Annulation d'une nouvelle construction
if (isset($_GET['a']) && isset($_GET['b'])) {
    $n = intval(gpc('s'));
    if (empty($n)) {
        $n = 1;
    }
    $planete->file_vais->delObjet(intval(gpc('b')), $n, intval(gpc('a')), $planete);

    redirection($VAR['menu']['chantierspatial']."&k=".$lieu);
}

$ct_max = count($LANG[$planete->race]['vaisseaux']['noms_sing']);
$TEMP_liste = array();
foreach ($planete->vaisseaux as $i => $nombre) {
    // les humains ont moins de modèlé de vaisseau que les covie
    if ($i >= $ct_max) {
        continue;
    }
    if (!empty($LANG[$planete->race]['vaisseaux']['noms_sing'][$i]) && dSpatial::needed($i, $planete)) {
        $TEMP_liste[] = array(
            'id' => $i,
            'image' => dSpatial::image($i, $planete),
            'nombre'  => $nombre,
            'nec_metal' => dSpatial::metal($i, 1, $planete),
            'nec_cristal' => dSpatial::cristal($i, 1, $planete),
            'nec_hydrogene' => dSpatial::hydrogene($i, 1, $planete),
            'temps' => sec(dSpatial::temps($i, 1, $planete)),
            'enfile' => $planete->file_vais->objectInFile($i)
        );
    }
}

$template->assign('vaisseaux', $TEMP_liste);
$template->assign('lieu', $lieu);
$template->assign('files', $planete->file_vais->printFile($planete));

unset($TEMP_liste, $i, $n, $niveau);
