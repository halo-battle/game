<?php
if (!defined('INDEX') || SURFACE != "planete") {
    header('Location: ./'.$VAR['first_page']);
    exit;
}
$titre = 'Bourse';

//Récupération des données
$ressource = intval(gpc("ressource", "post"));

if ($ressource == 0) {
    $var = 'metal';
} elseif ($ressource == 1) {
    $var = 'cristal';
} elseif ($ressource == 2) {
    $var = 'hydrogene';
}

//On gère les achats
if ((isset($_POST["buy"]) || empty($_POST["nbs"])) && !empty($_POST["nbb"]) && $nb = intval($_POST["nbb"])) {
    $bdd->reconnexion();
    $action = $bdd->unique_query("SELECT dispo, graph FROM $table_bourse_ressources WHERE id = $ressource;");
    $bdd->deconnexion();

    if (empty($action)) {
        erreur("Impossible de trouver l'action dans la bourse !");
    }

    $nb = gpc("nbb", "post");

    if ($action['dispo'] <= $nb) {
        erreur("Il n'y a pas assez de ressources dans la galaxie pour que vous puissiez en acheter autant.");
    }

    //On vérifie qu'il reste suffisamment de place dans les silos du joueur
    if ($planete->cap < $planete->$var + $nb) {
        $nb = $planete->cap - $planete->$var;
    }
    if ($nb <= 0) {
        erreur("Vous n'avez pas assez de place pour stocker ces ressources !");
    }

    //On calcul le prix
    $prix = bourse_calcPrixBase($action['dispo'], $nb, 2.2);

    //On vérifie que le joueur ait assez de crédits pour acheter
    if ($prix <= $planete->credits) {
        $planete->addCredits(-1*$prix);
        $planete->addModifUser('credits');
        $planete->$var += $nb;
        $planete->addModif('force');

        if (empty($action['graph'])) {
            $action['graph'] = array();
        } else {
            $action['graph'] = unserialize($action['graph']);
        }
        $action['graph'][date('w')] = $action['dispo'];
        $graph = serialize($action['graph']);

        $bdd->reconnexion();
        $bdd->escape($graph);
        $bdd->query("UPDATE $table_bourse_ressources SET dispo = dispo - $nb, graph = '$graph' WHERE id = $ressource;");
        $bdd->deconnexion();
    } else {
        erreur("Vous n'avez pas assez de crédits pour faire cet achat !");
    }
}
//On gère les ventes
elseif ((isset($_POST["sell"]) || empty($_POST["nbb"])) && !empty($_POST["nbs"])) {
    $nb = gpc("nbs", "post");
    if ($nb <= 1000 || $nb > 99999999) {
        erreur("Nombre de ressource invalide !<br />Vous pouvez vendre au minimum 1000 ressources !");
    }

    //On vérifie que le joueur ait assez de ressources pour vendre, sinon, on ajuste à son maximum
    if ($nb > $planete->$var) {
        $nb = $planete->$var;
    }

    if ($nb < 0) {
        erreur("Vous n'avez pas assez de ressources en vendre autant !");
    }

    $bdd->reconnexion();
    $action = $bdd->unique_query("SELECT dispo, graph FROM $table_bourse_ressources WHERE id = $ressource;");
    $bdd->deconnexion();

    if (empty($action)) {
        erreur("Impossible de trouver l'action dans la bourse !");
    }

    $prix = bourse_calcPrixBase($action['dispo'], $nb, 1.8);
    $planete->addCredits($prix);
    $planete->addModifUser('credits');
    $planete->$var -= $nb;
    $planete->addModif('force');

    if (empty($action['graph'])) {
        $action['graph'] = array();
    } else {
        $action['graph'] = unserialize($action['graph']);
    }
    $action['graph'][date('w')] = $action['dispo'];
    $graph = serialize($action['graph']);

    $bdd->reconnexion();
    $bdd->escape($graph);
    $bdd->query("UPDATE $table_bourse_ressources SET dispo = dispo + $nb, graph = '$graph' WHERE id = $ressource;");
    $bdd->deconnexion();
}
/*
if(is_numeric($a) && is_numeric(gpc('a'.$a, 'post')))
{
    $nb = gpc('a'.$a, 'post');
    if ($nb <= 0 || $nb > 99999999) erreur("Nombre de ressources invalide !");

    if ($a == 0) $var = 'metal';
    elseif ($a == 1) $var = 'cristal';
    elseif ($a == 2) $var = 'hydrogene';

    $bdd->reconnexion();
    $action = $bdd->unique_query("SELECT dispo, graph FROM $table_bourse_ressources WHERE id = $a;");
    $bdd->deconnexion();

    //On vérifie que la bourse ait suffisament de ressources à distribuer :
    if ($action['dispo'] <= $nb) erreur("Il n'y a pas assez de ressources dans la galaxie pour que vous puissiez en acheter autant.");

    //On vérifie qu'il reste suffisamment de place dans les silos du joueur
    if ($planete->cap < $planete->$var + $nb) $nb = $planete->cap - $planete->$var;
    if ($nb <= 0) erreur("Vous n'avez pas assez de place pour stocker ces ressources !");

    $prix = bourse_calcPrixBase($action['dispo'], $nb, 2.2);

    //On vérifie que le joueur ait assez de crédits pour acheter
    if ($prix <= $planete->credits) {
        $planete->addCredits(-1*$prix);
        $planete->addModifUser('credits');
        $planete->$var += $nb;
        $planete->addModif('force');

        if (empty($action['graph'])) $action['graph'] = array();
        else $action['graph'] = unserialize($action['graph']);
        $action['graph'][date('w')] = $action['dispo'];
        $graph = serialize($action['graph']);

        $bdd->reconnexion();
        $bdd->escape($graph);
        $bdd->query("UPDATE $table_bourse_ressources SET dispo = dispo - $nb, graph = '$graph' WHERE id = $a;");
        $bdd->deconnexion();
    }
    else erreur("Vous n'avez pas assez de crédits pour faire cet achat !");
}
elseif(is_numeric($v) && is_numeric(gpc('a'.$v, 'post')))
{
    $nb = gpc('a'.$v, 'post');
    if ($nb <= 1000 || $nb > 99999999) erreur("Nombre de ressources invalide !<br />Vous pouvez vendre au minimum 1000 ressources !");

    if ($v == 0) $var = 'metal';
    elseif ($v == 1) $var = 'cristal';
    elseif ($v == 2) $var = 'hydrogene';

    //On vérifie que le joueur ait assez de ressources pour vendre, sinon, on ajuste à son maximum
    if ($nb > $planete->$var) $nb = $planete->$var;

    $bdd->reconnexion();
    $action = $bdd->unique_query("SELECT dispo, graph FROM $table_bourse_ressources WHERE id = $v;");
    $bdd->deconnexion();

    $prix = bourse_calcPrixBase($action['dispo'], $nb, 1.8);
    $planete->addCredits($prix);
    $planete->addModifUser('credits');
    $planete->$var -= $nb;
    $planete->addModif('force');

    if (empty($action['graph'])) $action['graph'] = array();
    else $action['graph'] = unserialize($action['graph']);
    $action['graph'][date('w')] = $action['dispo'];
    $graph = serialize($action['graph']);

    $bdd->reconnexion();
    $bdd->escape($graph);
    $bdd->query("UPDATE $table_bourse_ressources SET dispo = dispo + $nb, graph = '$graph' WHERE id = $v;");
    $bdd->deconnexion();
}//*/

$bdd->reconnexion();
$bourse = $bdd->query("SELECT id, dispo FROM $table_bourse_ressources;");
$bdd->deconnexion();

foreach ($bourse as $key => $action) {
    $bourse[$key]['prix'] = bourse_calcPrixBase($action['dispo']);
    $bourse[$key]['prixV'] = $bourse[$key]['prix'] * 0.9;
    $bourse[$key]['prixA'] = $bourse[$key]['prix'] * 1.07;
}

$page = 'marche';
$template->assign('bourse', $bourse);
unset($a, $v, $var, $bourse, $graph, $key, $action, $nb, $prix);
