<?php
if (!defined('INDEX') || SURFACE != "planete") {
    header("Location: ./".$VAR["first_page"]);
    exit;
}
$page = "laboratoire";
$titre = ucfirst($LANG[$race]["batiments"]["noms_sing"][6]);

/*
Transformation des niveaux XML en tableau//
for($n = 0; $n < 7; $n++)
{
    foreach ($LANG[$planete->race]["technologies"]["niveau"][$n] as $key => $b)
    {
        $LANG[$planete->race]["technologies"]["niveau"][$n][$key] = intval($b);
    }

    print var_export($LANG[$planete->race]["technologies"]["niveau"][$n], true);
}
exit;
//*/
//Si l'on est pas sur la planète mère, on bloque le laboratoire
if ($queryPlanetes[0]["id"] != $planete->id) {
    erreur("Vous devez être sur votre planète mère pour faire des recherches dans le ".$LANG[$race]["batiments"]["noms_sing"][6]);
}

//Vérification que le joueur ait bien un labo avant d'afficher la page
if ($planete->batiments[6] <= 0) {
    erreur("Vous devez d'abord construire un ".$LANG[$race]["batiments"]["noms_sing"][6], "red", "?p=batiments", 3500);
}

//On vérifie la branche demandée
if (isset($_GET["n"]) && is_numeric($_GET["n"]) && isset($planete->technologies[$_GET["n"]])) {
    $onglet = gpc("n");
} else {
    $onglet = 0;
}


//Lancement d'une nouvelle recherche
if (isset($_GET["n"]) && isset($_GET["t"])) {
    //On vérifie que le laboratoire ne soit pas en construction
    if ($planete->file_bat->objectInFile(6)) {
        erreur("Votre ".$LANG[$race]["batiments"]["noms_sing"][6]." est en travaux, vous ne pouvez pas faire de recherches pendant ce temps !");
    }

    $planete->file_tech->addObjet(intval(gpc("n")), intval(gpc("t")), $planete);

    redirection($VAR["menu"]["laboratoire"]);
}
//Annulation d'une nouvelle recherche
if (isset($_GET["a"]) && isset($_GET["b"])) {
    $planete->file_tech->delObjet(intval(gpc("b")), 1, intval(gpc("a")), $planete);

    redirection($VAR["menu"]["laboratoire"]);
}

function traiterBranche($onglet, $branche, $start = false)
{
    global $LANG, $planete, $template;

    if ($start) {
        $return = "<dl id=\"arbre\">";
    } else {
        $return = "<dl>";
    }

    foreach ($branche as $key => $b) {
        if (is_array($b)) {
            $return .= "<dd>";
            $return .= traiterBranche($onglet, $b);
            $return .= "</dd>";
        } else {
            $origin_b = $b;
            //On recherche s'il s'agit d'une technologie à niveau multiple
            if ((dTechnologies::idToBit($b) & $planete->technologies[$onglet]) && isset($LANG[$planete->race]["technologies"]["noms_sing"][$onglet][$b+1]) && $LANG[$planete->race]["technologies"]["noms_sing"][$onglet][$b] == $LANG[$planete->race]["technologies"]["noms_sing"][$onglet][$b+1]) {
                if ((dTechnologies::idToBit($b+1) & $planete->technologies[$onglet]) && isset($LANG[$planete->race]["technologies"]["noms_sing"][$onglet][$b+2]) && $LANG[$planete->race]["technologies"]["noms_sing"][$onglet][$b] == $LANG[$planete->race]["technologies"]["noms_sing"][$onglet][$b+2]) {
                    $b = $b + 2;
                } else {
                    $b = $b + 1;
                }
            }

            //La technologie Expansion peut monter jusqu'au niveau 18 \o/
            if ($onglet == 8) {
                $i = 1;
                while ($i < 18) {
                    $b = $origin_b;
                    if ((dTechnologies::idToBit($b + ($i - 1)) & $planete->technologies[$onglet]) &&
                        isset($LANG[$planete->race]["technologies"]["noms_sing"][$onglet][$b+$i]) &&
                        $LANG[$planete->race]["technologies"]["noms_sing"][$onglet][$b] ==
                        $LANG[$planete->race]["technologies"]["noms_sing"][$onglet][$b+$i]) {
                        //La techno est déjà a ce niveau, on ne fait rien
                    } else {
                        $b += ($i - 1);
                        break;
                    }
                    $i++;
                }
            }

            if ($key >= 1) {
                $return .= "</dl><dl>";
            }

            //La techno est au plus haut niveau
            if (dTechnologies::idToBit($b) & $planete->technologies[$onglet]) {
                $return .= '<dt><a href="#">';
            //La techno peut encore être augmentée
            } elseif (dTechnologies::idToBit($origin_b) & $planete->technologies[$onglet]) {
                $return .= '<dt class="partial"><a href="?p=laboratoire&amp;n='.$onglet.'&amp;t='.$b.'">';
            //La techno n'a pas encore été recherchée
            } else {
                $return .= '<dt class="lack"><a href="?p=laboratoire&amp;n='.$onglet.'&amp;t='.$b.'">';
            }

            $return .= $LANG[$planete->race]["technologies"]["noms_sing"][$onglet][$b].'</a><div><img src="'.$template->get_template_vars("url_images").'images/technologies/'.dTechnologies::image(array($onglet, $b), $planete).'" width="50" height="50" alt="'.$LANG[$planete->race]["technologies"]["noms_sing"][$onglet][$b].'" /><p><strong>Niveau :</strong> ';
            if (($niv = dTechnologies::niveau($onglet, $b)) > 0) {
                if (dTechnologies::idToBit($b) & $planete->technologies[$onglet]) {
                    $return .= $niv;
                } else {
                    $return .= ($niv-1);
                }
            } else {
                $return .= "unique";
            }

            if (!(dTechnologies::idToBit($b) & $planete->technologies[$onglet])) {
                if (($r = dTechnologies::metal($onglet, $b, $planete)) > 0) {
                    $return .= "<br /><strong>Coût ".$LANG[$planete->race]["ressources"]["noms"][0]." :</strong> ".$r;
                }
                if (($r = dTechnologies::cristal($onglet, $b, $planete)) > 0) {
                    $return .= "<br /><strong>Coût ".$LANG[$planete->race]["ressources"]["noms"][1]." :</strong> ".$r;
                }
                if (($r = dTechnologies::hydrogene($onglet, $b, $planete)) > 0) {
                    $return .= "<br /><strong>Coût ".$LANG[$planete->race]["ressources"]["noms"][2]." :</strong> ".$r;
                }
                if (($r = dTechnologies::credits($onglet, $b, $planete)) > 0) {
                    $return .= "<br /><strong>Coût ".$LANG[$planete->race]["ressources"]["noms"][4]." :</strong> ".$r;
                }

                $return .= "<br /><strong>Temps :</strong> ".sec(dTechnologies::temps($onglet, $b, $planete));
            }

            $return .= "</p></div></dt>";
        }
    }

    return $return."</dl>";
}

if ($onglet == 1 || $onglet == 2) {
    $template->assign("arbre", traiterBranche(1, dTechnologies::type(1, $planete->race), true)."<br />".traiterBranche(2, dTechnologies::type(2, $planete->race), true));
} elseif ($onglet == 5 || $onglet == 6) {
    $template->assign("arbre", traiterBranche(5, dTechnologies::type(5, $planete->race), true)."<br />".traiterBranche(6, dTechnologies::type(6, $planete->race), true));
} else {
    $template->assign("arbre", traiterBranche($onglet, dTechnologies::type($onglet, $planete->race), true));
}

$template->assign("onglet", $onglet);
$template->assign("files", $planete->file_tech->printFile($planete));

unset($TEMP_liste, $niveau, $i);
