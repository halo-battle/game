<?php
if (!defined('ONYX')) {
    exit;
}

$page = 'caserne';
$titre = $LANG[$planete->race]['batiments']['noms_sing'][9];

//Vérification que le joueur ait bien une caserne avant d'afficher la page
if ($planete->batiments[9] <= 0) {
    erreur('Vous devez d\'abord construire une '.$LANG[$planete->race]['batiments']['noms_sing'][9], "red", '?p=batiments', 3500);
}

$lieu = intval(gpc("k"));
if ($lieu >= $planete->batiments[9] || $lieu < 0) {
    $lieu = 0;
}

//Lancement d'un nouvel entrainement
if (isset($_GET['c']) && ($id = intval(gpc('c'))) >= 0 && $nbc = floor(gpc('cas'.$id, 'post'))) {
    $planete->file_cas->addObjet($id, $nbc, $planete, $lieu);

    redirection($VAR['menu']['caserne']."&k=".$lieu);
}
//Annulation d'un entrainement
if (isset($_GET['a']) && isset($_GET['b'])) {
    $n = intval(gpc('s'));
    if (empty($n)) {
        $n = 1;
    }
    $planete->file_cas->delObjet(intval(gpc('b')), $n, intval(gpc('a')), $planete);

    redirection($VAR['menu']['caserne']."&k=".$lieu);
}

$TEMP_liste = array();
foreach ($planete->casernes as $i => $nombre) {
    if (!empty($LANG[$planete->race]['caserne']['noms_sing'][$i]) && dCaserne::needed($i, $planete)) {
        $TEMP_liste[] = array(
            'id' => $i,
            'image' => dCaserne::image($i, $planete),
            'nombre'  => $nombre,
            'nec_metal' => dCaserne::metal($i, 1, $planete),
            'nec_cristal' => dCaserne::cristal($i, 1, $planete),
            'nec_hydrogene' => dCaserne::hydrogene($i, 1, $planete),
            'temps' => sec(dCaserne::temps($i, 1, $planete)),
            'enfile' => $planete->file_cas->objectInFile($i)
        );
    }
}

$template->assign('unites', $TEMP_liste);
$template->assign('lieu', $lieu);
$template->assign('files', $planete->file_cas->printFile($planete));

unset($TEMP_liste, $i, $n, $niveau);
