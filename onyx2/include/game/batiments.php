<?php
if (!defined('ONYX')) {
    exit;
}

$titre = 'Bâtiments';

//Si l'on est sur un astéroide, on vérifie que le joueur ait les permissions nécessaire
if (SURFACE == "asteroide" && !($planete->permissions_alliance &1)) {
    erreur("Vous n'avez pas le grade requis pour vous occuper des bâtiments de l'astéroide.");
}

//Lancement d'une nouvelle construction
if (isset($_GET['c'])) {
    //On vérifie qu'il n'y ait pas de technologie en cours de recherche si l'on veut améliorer le centre de recherche
    if (gpc('c') == 6 && $planete->file_tech->hasObject()) {
        erreur('Une technologie est en cours de recherche dans votre laboratoire, vous ne pouvez pas faire de travaux !');
    }

    $planete->file_bat->addObjet(intval(gpc('c')), 1, $planete);

    redirection($VAR['menu']['batiments']);
}
//Lancement d'une déconstruction
if (isset($_GET['d'])) {
    //On vérifie qu'il n'y ait pas de technologie en cours de recherche si l'on veut améliorer le centre de recherche
    if ($_GET['d'] == 6 && $planete->file_tech->hasObject()) {
        erreur('Une technologie est en cours de recherche dans votre laboratoire, vous ne pouvez pas faire de travaux !');
    }

    $planete->file_bat->addDemolition(intval(gpc('d')), 1, $planete);

    redirection($VAR['menu']['batiments']);
}
//Annulation d'une nouvelle construction
if (isset($_GET['a']) && isset($_GET['b'])) {
    $planete->file_bat->delObjet(intval(gpc('b')), 1, intval(gpc('a')), $planete);

    redirection($VAR['menu']['batiments']);
}

if (SURFACE == "planete") {
    $page = 'batiments';
    $TEMP_liste = array();
    foreach ($planete->batiments as $i => $niveau) {
        //On vérifie le type par rapport à l'onglet
        if (isset($_GET["n"]) && !(intval($_GET["n"])& dBatiments::type($i))) {
            continue;
        }

        //Si l'on est pas sur la planète mère, on désactive le laboratoire
        if ($i == 6 && $queryPlanetes[0]['id'] != $planete->id) {
            continue;
        }

        if (!empty($LANG[$planete->race]['batiments']['noms_sing'][$i]) && dBatiments::needed($i, $planete)) {
            $TEMP_liste[] = array(
                'id' => $i,
                'image' => dBatiments::image($i, $planete),
                'niveau'  => $niveau,
                'nec_metal' => dBatiments::metal($i, $niveau+1, $planete),
                'nec_cristal' => dBatiments::cristal($i, $niveau+1, $planete),
                'nec_hydrogene' => dBatiments::hydrogene($i, $niveau+1, $planete),
                'temps' => sec(dBatiments::temps($i, $niveau+1, $planete)),
                'enfile' => $planete->file_bat->objectInFile($i)
            );
        }
    }
} else {
    $page = 'batiments_alli';
    $TEMP_liste = array();
    foreach ($planete->batiments as $i => $niveau) {
        if (!empty($LANG[$planete->race]['alli_batiments']['noms_sing'][$i]) && dAlliancesBatiments::needed($i, $planete) && $niveau < 6) {
            $TEMP_liste[] = array(
                'id' => $i,
                'image' => dAlliancesBatiments::image($i, $planete),
                'niveau'  => $niveau,
                'nec_metal' => dAlliancesBatiments::metal($i, $niveau+1, $planete),
                'nec_cristal' => dAlliancesBatiments::cristal($i, $niveau+1, $planete),
                'nec_hydrogene' => dAlliancesBatiments::hydrogene($i, $niveau+1, $planete),
                'nec_credits' => dAlliancesBatiments::credits($i, $niveau+1, $planete),
                'temps' => sec(dAlliancesBatiments::temps($i, $niveau+1, $planete)),
                'enfile' => $planete->file_bat->objectInFile($i)
            );
        }
    }
}
$template->assign('batiments', $TEMP_liste);
$template->assign('onglet', gpc("n"));
$template->assign('files', $planete->file_bat->printFile($planete));

unset($TEMP_liste, $niveau, $i);
