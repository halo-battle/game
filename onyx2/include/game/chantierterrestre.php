<?php
if (!defined('ONYX')) {
    exit;
}

$page = $p;
$onglet = gpc('n');
if (!empty($onglet)) {
    $template->assign('onglet', $onglet);
} else {
    $template->assign('onglet', "unites");
}
$titre = ucfirst($LANG[$planete->race]['batiments']['noms_sing'][7]);

//Vérification que le joueur ait bien un labo avant d'afficher la page
if ($planete->batiments[7] <= 0) {
    erreur('Vous devez d\'abord construire un '.$LANG[$planete->race]['batiments']['noms_sing'][7], "red", '?p=batiments', 3500);
}

$lieu = intval(gpc("k"));
if ($lieu >= $planete->batiments[7] || $lieu < 0) {
    $lieu = 0;
}

//Lancement d'une nouvelle construction
if (isset($_GET['v']) && ($id = intval(gpc('v'))) >= 0 && $nbv = floor(gpc('terr'.$id, 'post'))) {
    $planete->file_ter->addObjet($id, $nbv, $planete, $lieu);

    redirection($VAR['menu'][$p].'&k='.$lieu.'&n='.$onglet);
}
//Annulation d'une nouvelle construction
if (isset($_GET['a']) && isset($_GET['b'])) {
    $n = intval(gpc('s'));
    if (empty($n)) {
        $n = 1;
    }
    $planete->file_ter->delObjet(intval(gpc('b')), $n, intval(gpc('a')), $planete);

    redirection($VAR['menu'][$p].'&k='.$lieu.'&n='.$onglet);
}

$ct_max = count($LANG[$planete->race]['terrestre']['noms_sing']);
$TEMP_liste = array();
foreach ($planete->terrestres as $i => $nombre) {
    // les humains ont plus de modèlé de def que les covie
    if ($i >= $ct_max) {
        continue;
    }
    $type = dTerrestre::type($i, $planete);

    if (!empty($LANG[$planete->race]['terrestre']['noms_sing'][$i]) && (($onglet == "defenses" && !$type) || ($onglet != "defenses" && $type)) && dTerrestre::needed($i, $planete)) {
        $TEMP_liste[] = array(
            'id' => $i,
            'image' => dTerrestre::image($i, $planete),
            'nombre'  => $nombre,
            'nec_metal' => dTerrestre::metal($i, 1, $planete),
            'nec_cristal' => dTerrestre::cristal($i, 1, $planete),
            'nec_hydrogene' => dTerrestre::hydrogene($i, 1, $planete),
            'temps' => sec(dTerrestre::temps($i, 1, $planete)),
            'enfile' => $planete->file_ter->objectInFile($i)
        );
    }
}

$template->assign('unites', $TEMP_liste);
$template->assign('lieu', $lieu);
$template->assign('files', $planete->file_ter->printFile($planete));

unset($TEMP_liste, $i, $n, $niveau, $onglet, $type);
