<?php
if (!defined('ONYX')) {
    exit;
}

if (empty($sess->values["connected"]) && !defined("xCSRF")) {
    define("xCSRF", true);
}

//A passer a True pour dev tranquillou (pas d'envois de mail, pas de https,...)
define("DEV", getenv("DEV"));

require_once("function.php"); //Inclusion des fonctions principales
require_once("tables.php"); //Inclusion des noms des tables de base de données correspondant à l'architecture du serveur

//On prépare le gestionnaire de templates
$template = new Template();
$template->assign("link", array_map("url", $VAR["link"]));
$template->assign("url_serveur", $_SERVER["HTTP_HOST"]);
$template->assign("url_images", $VAR["url_images"]);

//On charge la session
$SESS = new Session();

//Extraction des données en cache pour le header, sinon création du cache
$header = Cache::read("headerNB");
if (empty($header)) {
    $bdd = new BDD();

    $nbcovie = $bdd->unique_query("SELECT COUNT(id) AS covenants FROM $table_user WHERE race = 'covenant';");
    $nbhumain = $bdd->unique_query("SELECT COUNT(id) AS humains FROM $table_user WHERE race = 'humain';");
    $enligne = $bdd->unique_query("SELECT COUNT(session) AS enligne FROM sessions WHERE active = true AND var != '0';");
    $infos = $bdd->query("SELECT * FROM $table_infoshead ORDER BY id DESC;");
    $msgdem = $bdd->unique_query("SELECT titre FROM $table_messages_demarrage ORDER BY id DESC LIMIT 1;");

    $bdd->deconnexion();

    Cache::set("headerNB", array("count" => array($nbcovie["covenants"], $nbhumain["humains"], "cette", $enligne["enligne"]), "infos" => $infos, "messagedemarrage" => $msgdem["titre"]));
    unset($nbcovie, $nbhumain, $enligne, $infos, $msgdem, $bdd);

    $header = Cache::read("headerNB");
}
$template->assign("header", $header);
$template->assign("version", $VAR["version"]);
$template->assign("serveur_name", $VAR["serveur_name"]);
$template->assign("first_page", $VAR["first_page"]);
$template->assign("LANG", $LANG);

define("VITESSE", $VAR["vitesse"]);
define("debut_d_univers", true); //Constante pour savoir si l'on offre ou pas un vaisseau de colonisation et s'il est possible de se désangager d'une signature
define("nb_signatures", 4);

//Si l'on est pas connecté, on garde le header pour comparer lors de la connexion
if (!empty($sess->values["connected"])) {
    unset($header);
}

//Evite les attaques CSRF
if (DEV) {
    $protocole = "http";
} else {
    $protocole = "https";
}
if (!empty($_SERVER["HTTP_REFERER"]) && !(preg_match('#^'.$protocole.'://'.$_SERVER['HTTP_HOST'].'#', $_SERVER["HTTP_REFERER"]) && defined("xCSRF"))) {
    elog("Possibilité d'attaque CSRF\n".var_export($_REQUEST, true), 2);
    unset($_POST, $_GET);
    $_GET = $_POST = array();
}
