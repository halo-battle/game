<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}

$bdd->reconnexion();
$sql = "";
if ($planete->id_user != null) {
    $sql = "id_user = " . $planete->id_user . " ";
}
if ($planete->id != null) {
    if ($sql != "") {
        $sql .= " OR ";
    }
    $sql .= "end_planete = " . $planete->id . " ";
}
if ($planete->id_alliance != null) {
    if ($sql != "") {
        $sql .= " OR ";
    }
    $sql .= "id_alliance = " . $planete->id_alliance . " ";
}
if ($planete->id_alliance != null) {
    if ($sql != "") {
        $sql .= " OR ";
    }
    $sql .= "end_planete = " . $planete->id_alliance . " ";
}
$flottes = $bdd->query("SELECT id FROM $table_flottes WHERE (" . $sql . ") AND (start_time + end_time) <= ".time()." AND last < ".(time()-10).";");
$bdd->deconnexion();

if (!empty($flottes)) {
    foreach ($flottes as $flotte) {
        $flotte = new Flotte($flotte['id']);
        $flotte->check_mission();
    }
}

unset($flottes, $flotte);
