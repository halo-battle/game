<?php
//Gestion des dépendances, on importe les classes nécessaires à la classe en cours
include_once("Class/surface.php");
include_once("donnees.php");

/***************************************************************************
 *                            class.planete.php
 *                           -------------------
 *   begin                : Jeudi 21 août 2008
 *   update               : Vendredi 27 février 2009
 *   email                : nemunaire@gmail.com
 *
 *
 ***************************************************************************/
class Planete extends Surface
{
    private $timestamp_lastSilo;
    private $timestamp_lastMineM;
    private $timestamp_lastMineC;
    private $timestamp_lastMineH;

    public $position;
    public $nom_planete;
    public $cases;
    public $casesRest;
    public $cap;
    public $population;
    public $population_max;
    public $moral;
    public $energie;
    public $energieConso;
    public $energieCentrale;
    public $file_tech;
    public $file_cas;
    public $file_ter;
    public $coeff_bat = array();
    public $casernes = array();
    public $terrestres = array();

    /**
     * Constructeur
     * @param    mixed	$id	id de la planète à importer/coordonnées
     * @param    bool	$first	Bloquer l'affichage des messages d'erreurs
     *
     * @return   void
     * @access   public
     */
    public function __construct($id, $first = false)
    {
        //Récupération du nom des tables utilisées et connexion à la base de données
        global $table_planete;
        $bdd = new BDD();

        //On traite le cas où l'on recoit l'ID ou les coordonnées de la planète
        if ($id === false) {
            $bdd->deconnexion();
        } elseif (is_numeric($id)) {
            $plan = $bdd->unique_query("SELECT * FROM $table_planete WHERE id = $id;");
            $bdd->deconnexion();
        } elseif (preg_match('#^\[?([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})\]?$#', $id, $position)) {
            $plan = $bdd->unique_query("SELECT * FROM $table_planete WHERE galaxie = ".$position[1]." AND ss = ".$position[2]." AND position = ".$position[3].";");
            $bdd->deconnexion();
        } else {
            trigger_error('Erreur #04 : Format de recherche de planete incorrect !', E_USER_ERROR);
        }

        if (!empty($plan)) {
            //Chargement des données depuis le résultat de la base de données
            $this->id = $plan["id"];
            parent::User($plan["id_user"]);
            $this->galaxie = $plan["galaxie"];
            $this->ss = $plan["ss"];
            $this->position = $plan["position"];
            if (!empty($plan["isolement"])) {
                $this->isolement = unserialize($plan["isolement"]);
            } else {
                $this->isolement = array();
            }
            $this->nom_planete = $plan["nom_planete"];
            $this->image = $plan["image"];
            $this->cases = $plan["cases"];
            $this->energieCentrale = @$plan["energieCentrale"];
            $this->debris_met = $plan["debris_met"];
            $this->debris_cri = $plan["debris_cri"];
            $this->metal = $plan["metal"];
            $this->cristal = $plan["cristal"];
            $this->hydrogene = $plan["hydrogene"];
            $this->population = $plan["population"];
            $this->moral = $plan["moral"];
            $this->timestamp = $plan["timestamp"];

            //Chargement des variables de conversion
            global $batimentsVAR, $caserneVAR, $terrestreVAR, $spatialVAR;

            $this->casesRest = $this->cases; //Calcul du nombre de cases en même temps
            foreach ($batimentsVAR as $bat) {
                $this->batiments[] = $plan[$bat];
                $this->casesRest -= $plan[$bat];
            }
            if (!empty($plan["file_bat"])) {
                $this->file_bat = unserialize($plan["file_bat"]);
            } else {
                $this->file_bat = new FileBatiments();
            }

            $this->coeff_bat = array($plan["coeff_mine_m"], $plan["coeff_mine_c"], $plan["coeff_mine_h"], $plan["coeff_centrale_s"], $plan["coeff_centrale_f"]);
            //On vérifie que les coefficient ne soient pas supérieurs à 1 ou inférieurs à 0
            for ($i = 0; $i < 5; $i++) {
                if ($this->coeff_bat[$i] > 1) {
                    $this->coeff_bat[$i] = 1;
                } elseif ($this->coeff_bat[$i] < 0) {
                    $this->coeff_bat[$i] = 0;
                }
            }

            if (!empty($plan["file_tech"])) {
                $this->file_tech = unserialize($plan["file_tech"]);
            } else {
                $this->file_tech = new FileTechnologies();
            }

            foreach ($caserneVAR as $cas) {
                $this->casernes[] = $plan[$cas];
            }
            if (!empty($plan["file_cas"])) {
                $this->file_cas = unserialize($plan["file_cas"]);
            } else {
                $this->file_cas =  new FileCaserne();
            }

            foreach ($terrestreVAR[$this->race] as $ter) {
                $this->terrestres[] = $plan[$ter];
            }
            if (!empty($plan["file_ter"])) {
                $this->file_ter = unserialize($plan["file_ter"]);
            } else {
                $this->file_ter =  new FileTerrestre();
            }

            foreach ($spatialVAR as $vais) {
                $this->vaisseaux[] = $plan[$vais];
            }
            if (!empty($plan["file_vais"])) {
                $this->file_vais = unserialize($plan["file_vais"]);
            } else {
                $this->file_vais =  new FileSpatial();
            }

            //Calcul de la population logée
            $this->population_max = (pow($this->batiments[12], 1.5)+pow($this->batiments[13], 2.1))*1000+3000;
            //Détermination des capacités maximales
            $this->cap = pow(2, $this->batiments[10]) * 100000;
        }
    }

    /**
     * Actualise les ressources de la planète en fonction de la production et termine les files d'attentes.
     *
     * @return   void
     * @access   public
     */
    public function actualiser($actuFile = true, $first = false)
    {
        //Actualisation des files d'attentes
        if ($actuFile) {
            $this->file_bat->ready($this);
            $this->file_tech->ready($this);
            $this->file_cas->ready($this);
            $this->file_ter->ready($this);
            $this->file_vais->ready($this);
        }

        if (!empty($this->timestamp_lastSilo)) {
            $this->cap = pow(2, $this->batiments[10]-1) * 100000;
            $capnouv = pow(2, $this->batiments[10]) * 100000;
            $this->addModif("force");
        }

        //Calcul du temps écoulé depuis la dernière mise à jour de la planète
        $temps_ecoule = time() - $this->timestamp;
        $ressources = $this->production($temps_ecoule);

        if ($this->metal + $ressources[0] < $this->cap) {
            $this->metal += $ressources[0];
        } else {
            //Si les capacités de stockage ont changé depuis la dernière actualisation
            if (isset($capnouv)) {
                $ressources = $this->production($this->timestamp_lastSilo - $this->timestamp);
                if ($this->metal + $ressources[0] < $this->cap) {
                    $this->metal += $ressources[0];
                } else {
                    $this->metal = $this->cap;
                }

                $ressources = $this->production(time() - $this->timestamp_lastSilo);
                if ($this->metal + $ressources[0] < $capnouv) {
                    $this->metal += $ressources[0];
                } else {
                    $this->alert_ressources[0] = true;
                    $this->metal = $capnouv;
                }
            } else {
                $this->alert_ressources[0] = true;
                $this->metal = $this->cap;
            }
        }
        if ($this->cristal + $ressources[1] < $this->cap) {
            $this->cristal += $ressources[1];
        } else {
            //Si les capacités de stockage ont changé depuis la dernière actualisation
            if (isset($capnouv)) {
                $ressources = $this->production($this->timestamp_lastSilo - $this->timestamp);
                if ($this->cristal + $ressources[0] < $this->cap) {
                    $this->cristal += $ressources[0];
                } else {
                    $this->cristal = $this->cap;
                }

                $ressources = $this->production(time() - $this->timestamp_lastSilo);
                if ($this->cristal + $ressources[0] < $capnouv) {
                    $this->cristal += $ressources[0];
                } else {
                    $this->alert_ressources[1] = true;
                    $this->cristal = $capnouv;
                }
            } else {
                $this->alert_ressources[1] = true;
                $this->cristal = $this->cap;
            }
        }
        if ($this->hydrogene + $ressources[2] < $this->cap) {
            $this->hydrogene += $ressources[2];
        } else {
            //Si les capacités de stockage ont changé depuis la dernière actualisation
            if (isset($capnouv)) {
                $ressources = $this->production($this->timestamp_lastSilo - $this->timestamp);
                if ($this->hydrogene + $ressources[0] < $this->cap) {
                    $this->hydrogene += $ressources[0];
                } else {
                    $this->hydrogene = $this->cap;
                }

                $ressources = $this->production(time() - $this->timestamp_lastSilo);
                if ($this->hydrogene + $ressources[0] < $capnouv) {
                    $this->hydrogene += $ressources[0];
                } else {
                    $this->alert_ressources[2] = true;
                    $this->hydrogene = $capnouv;
                }
            } else {
                $this->alert_ressources[2] = true;
                $this->hydrogene = $this->cap;
            }
        }

        //Vérification de la date pour faire les actions journalières
        if (date('zya') != date('zya', $this->timestamp)) { //Mise à jour trois fois par jour : 0h, 1h, 13h
            //if (date('zy') != date('zy', $this->timestamp)) //Mise à jour une fois par jour : 0h
            //On évalue le moral
            $this->evalMoral($first);

            //Si la population est à 0, on ajoute des habitants
            if ($this->population <= 0) {
                $this->population = 1000;
            }

            $popPlus = $this->population * 0.0153^max(1, floor((time()-$this->timestamp)/86400));

            if ($this->politique == 2) {
                $popPlus *= 1.1;
            } //Communisme : 10 % de population qui arrive en plus.
            elseif ($this->politique == 3) {
                $popPlus *= 1.05;
            } //Démocratie : 5 % de population qui arrive en plus.

            //Bonus techno immigration coloniale
            if ($this->technologies[3] & 4) {
                $popPlus *= 1.15;
            } elseif ($this->technologies[3] & 2) {
                $popPlus *= 1.10;
            } elseif ($this->technologies[3] & 1) {
                $popPlus *= 1.05;
            }

            $this->population += $popPlus;
            $this->addCredits(($this->population/100*exp(0.01)*25) + ($this->population*0.01*$this->batiments[15])); //Première partie : production normale; seconde : batiment commercial
            $this->addModif("population");
        }

        $this->timestamp = time();
    }

    public function setMoral($difference)
    {
        $this->moral += $difference;

        //Ajustement du moral
        if ($this->moral > 1) {
            $this->moral = 1;
        } elseif ($this->moral < 0) {
            $this->moral = 0;
        }

        $this->addModif("moral");
    }

    public function evalMoral($first = false)
    {
        $evolution = array();
        //Cas de sous-production
        if (($this->coeff_bat[0] + $this->coeff_bat[1] + $this->coeff_bat[2])/3 < 0.9) {
            if ($this->politique == 2) {
                $this->moral -= 0.10;
            } //Communisme : démoralise 2x plus
            else {
                $this->moral -= 0.05;
            }

            $this->addModif("moral");
        }

        //Surpopulation

        //Surlogement

        //Effets des batiments modules résidentiels, centre commercials et centre de loisir
        $this->moral += 0.0010*$this->batiments[12] + 0.0025*$this->batiments[15] + 0.1*$this->batiments[16];

        //Ajustement du moral en fonction de la politique
        if ($this->politique == 1 && $this->moral > 0.7) {
            $this->moral = 0.7;
        }

        //On vérifie qu'on ne dépasse pas le maximum
        if ($this->moral > 1) {
            $this->moral = 1;
        }
        if ($this->moral < 0) {
            $this->moral = 0;
        }

        //Isolement si besoin
        if ($this->moral < 0.1) {
            //On vérifie qu'il ne s'agit pas de la planète mère
            global $table_planete;
            $bdd = new Bdd();
            $bdd->reconnexion();
            $res = $bdd->unique_query("SELECT id FROM $table_planete WHERE id_user = ".$this->id_user." ORDER BY id LIMIT 1;");
            $bdd->deconnexion();
            if ($res['id'] != $this->id) {
                if ($this->moral <= 0.04) {
                    if ($this->moral <= 0.01) {
                        $rand = rand(0, 4);
                    } else {
                        $rand = rand(0, 20);
                    }
                    //Perte de la planète
                    if ($rand == 1) {
                        $bdd->reconnexion();
                        $bdd->query("DELETE FROM $table_planete WHERE id = ".$this->id.";");
                        $bdd->deconnexion();
                        send_mp($this->id_user, 'Perte de contrôle de '.$this->nom_planete, "Suite à une démoralisation persistante de la population sur la planète ".$this->nom_planete." [".$this->galaxie.":".$this->ss.":".$this->position."], la population a renversé votre gouvernement en tuant tous vos gouverneurs. Vous perdez donc définitivement le contrôle de cette planète.");
                        if (!$first) {
                            $sess->values['idPlan'] = $res['id'];
                            erreur('La population de cette planète est tellement démoralisée qu\'elle s\'est révolté contre vous. Vous ne contrôlez plus cette planète.');
                        }
                    }
                } elseif ($this->moral <= 0.1) {
                    if ($this->moral <= 0.06) {
                        $rand = rand(0, 2);
                    } else {
                        $rand = rand(0, 10);
                    }
                    //Perte de contrôle temporaire
                    if ($rand == 1) {
                        $debut = time() - rand(0, 3600)*4;
                        $fin = $debut + 86400;
                        $this->isolement = array($debut, $fin);
                        $this->addModif("isolement");

                        send_mp($this->id_user, 'Perte de contrôle temporaire de '.$this->nom_planete, "Suite à une démoralisation percistante de la population sur la planète ".$this->nom_planete." [".$this->galaxie.":".$this->ss.":".$this->position."], la population a pris le contrôle de votre planète. Vous perdez le contrôle de cette planète le temps que vos gouverneurs reprennent le pouvoir.");
                        if (!$first) {
                            $sess->values['idPlan'] = $res['id'];
                            erreur('La population de cette planète est tellement démoralisée qu\'elle s\'est révoltée contre vous. Vous perdez temporairement le contrôle de cette planète.');
                        }
                    }
                }
            }
        }
    }

    public function checkAndRetireRessources($metal, $cristal, $hydrogene, $credits)
    {
        if ($this->metal >= $metal && $this->cristal >= $cristal && $this->hydrogene >= $hydrogene && $this->credits >= $credits) {
            $this->metal -= $metal;
            $this->cristal -= $cristal;
            $this->hydrogene -= $hydrogene;

            if (!empty($credits)) {
                $this->credits -= $credits;
                $this->addModifUser("credits");
            }

            return true;
        } else {
            return false;
        }
    }

    public function addRessources($metal, $cristal, $hydrogene, $credits = 0)
    {
        $perte = 0;

        $this->metal += $metal;
        if ($this->metal > $this->cap) {
            $perte += $this->metal - $this->cap;
            $this->metal = $this->cap;
        }

        $this->cristal += $cristal;
        if ($this->cristal > $this->cap) {
            $perte += $this->cristal - $this->cap;
            $this->cristal = $this->cap;
        }

        $this->hydrogene += $hydrogene;
        if ($this->hydrogene > $this->cap) {
            $perte += $this->hydrogene - $this->cap;
            $this->hydrogene = $this->cap;
        }

        if (!empty($credits)) {
            $this->credits += $credits;
            $this->addModifUser("credits");
        }

        return $perte;
    }

    /**
     * Vérifie si la planète est isolée ou non
     *
     * @return   boolean
     * @access   public
     */
    public function isolement()
    {
        $return = false;

        global $queryPlanetes;

        //Détermination du numéro de la planète par rapport aux autres, dans l'ordre de colonisation
        $numP = 0;
        foreach ($queryPlanetes as $key => $p) {
            if ($p['id'] == $this->id) {
                $numP = $key + 1;
            }
        }

        if ($numP >= 11) {
            if (!isset($this->isolement[0]) || (time() > $this->isolement[0] && (!isset($this->isolement[1]) || (time() > $this->isolement[1] && date('dmY') != date('dmY', $this->isolement[0]))))) {
                switch ($numP) {
                    case 11:
                        $tps = 2;
                        break;
                    case 12:
                        $tps = 4;
                        break;
                    case 13:
                        $tps = 6;
                        break;
                    case 14:
                        $tps = 8;
                        break;
                    case 15:
                        $tps = 12;
                        break;
                    case 16:
                        $tps = 16;
                        break;
                    case 17:
                        $tps = 20;
                        break;
                    default:
                        $tps = 24;
                }
                $debut = mktime(rand(0, 24-$tps), 0, 0, date('n'), date('j'), date('Y'));
                $fin = $debut + $tps * 3600;

                $this->isolement[0] = $debut;
                if (time() > $this->isolement[0]) {
                    $this->isolement[1] = $fin;
                }
                $this->addModif("isolement");
            }
            if (isset($this->isolement[1]) && time() < $this->isolement[1]) {
                $return = true;
            }
        } elseif (!isset($this->isolement[0])) {
            $this->isolement = array(0,0);
            $this->addModif("isolement");
        }

        return $return;
    }

    /**
     * Calcul les ressources produites en fonction de $temps_ecoule
     * @param    int	$temps_ecoule	Temps écoulé depuis la dernière actualisation
     *
     * @return   array
     * @access   public
     */
    public function production($temps_ecoule, $retarray = false)
    {
        //Accélération de la production
        $temps_ecoule *= VITESSE;

        //Si l'on est en auto contrôle des mines, on fixe le rendement de la centrale solaire à 1
        if ($this->options& 8) {
            $this->coeff_bat[3] = 1;
            $this->coeff_bat[4] = 1;
        }

        //Calcul de la consomation d'énergie
        if ($this->batiments[0] > 0) {
            $energie_m = ceil(exp(0.28*$this->batiments[0])*10);
        } else {
            $energie_m = 0;
        }
        if ($this->batiments[1] > 0) {
            $energie_c = ceil(exp(0.28*$this->batiments[1])*10);
        } else {
            $energie_c = 0;
        }
        if ($this->batiments[2] > 0) {
            $energie_h = ceil(exp(0.2849*$this->batiments[2])*13);
        } else {
            $energie_h = 0;
        }
        if ($this->batiments[3] > 0) {
            $energie_s = ceil(exp(0.28*$this->batiments[3])*22);
        } else {
            $energie_s = 0;
        }

        $energie_f = 100 + $this->energieCentrale;

        //Calcul de la consomation d'énergie
        $this->energieConso = $energie_m * $this->coeff_bat[0]  + $energie_c * $this->coeff_bat[1] + $energie_h * $this->coeff_bat[2];
        $nrjmx = $energie_m  + $energie_c + $energie_h;

        //Calcul de la production d'énergie
        $this->energie = $energie_s * $this->coeff_bat[3] + $energie_f * $this->coeff_bat[4];

        if ($this->energieConso == 0) {
            $coeff = 0;
        } else {
            $coeff = $this->energie / $this->energieConso;
        }

        if ($coeff < 0) {
            $coeff = 0;
        } elseif ($coeff > 1 && !($this->options& 8)) {
            $coeff = 1;
        }

        $Ncoeff = array();
        for ($i = 0; $i < 3; $i++) {
            $Ncoeff[$i] = $coeff * $this->coeff_bat[$i];
            if ($Ncoeff[$i] > 1) {
                $Ncoeff[$i] = 1;
            }
            if ($Ncoeff[$i] < $this->coeff_bat[$i] || ($Ncoeff[$i] > $this->coeff_bat[$i] && $this->options& 16) && $this->batiments[$i] != 0) {
                $this->coeff_bat[$i] = $Ncoeff[$i];
                $this->addModif("coeff_bat");
            }
        }

        //Calcul de la consomation d'hydrogène
        //if ($this->batiments[4] > 0)
        //	$conso_h = ((ceil(pow(1.34,($this->batiments[4]-1))*9)/3600)*$temps_ecoule) * $this->coeff_bat[4];
        //else
        $conso_h = 0;

        //Calcul des production de ressources
        if ($this->batiments[0] <= 0 || $this->batiments[3] <= 0) {
            $prod_met = 0.011 * $temps_ecoule;
        } else {
            $prod_met = ((ceil(pow(1.1, $this->batiments[0]) * 52 * $this->batiments[0]) / 3600) * $temps_ecoule) * $this->coeff_bat[0];
        }

        if ($this->batiments[1] <= 0 || $this->batiments[3] <= 0) {
            $prod_cri = 0.0055 * $temps_ecoule;
        } else {
            $prod_cri = ((ceil(pow(1.1, $this->batiments[1]) * 34 * $this->batiments[1]) / 3600) * $temps_ecoule) * $this->coeff_bat[1];
        }

        if ($this->batiments[2] <= 0) {
            $prod_hy = 0;
        } else {
            $prod_hy = ((ceil(pow(1.1, $this->batiments[2]) * 21 * ($this->batiments[2] + 0.7)) / 3600) * $temps_ecoule) * $this->coeff_bat[2];
        }

        $this->coeff_bat[3] = 1;
        //Augmentation de la production en fonction des technologies
        if ($this->technologies[0] &4) {
            $prod_met *= 1.15;
            $prod_cri *= 1.15;
            $prod_hy *= 1.15;
        } elseif ($this->technologies[0] &2) {
            $prod_met *= 1.10;
            $prod_cri *= 1.10;
            $prod_hy *= 1.10;
        } elseif ($this->technologies[0] &1) {
            $prod_met *= 1.05;
            $prod_cri *= 1.05;
            $prod_hy *= 1.05;
        }
        //Rendement métal
        if ($this->technologies[0] &64) {
            $prod_met *= 1.3;
        } elseif ($this->technologies[0] &32) {
            $prod_met *= 1.2;
        } elseif ($this->technologies[0] &16) {
            $prod_met *= 1.1;
        }
        //Rendemant cristal
        if ($this->technologies[0] &512) {
            $prod_cri *= 1.3;
        } elseif ($this->technologies[0] &256) {
            $prod_cri *= 1.2;
        } elseif ($this->technologies[0] &128) {
            $prod_cri *= 1.1;
        }
        //Rendement hydrogène
        if ($this->technologies[0] &4096) {
            $prod_hy *= 1.3;
        } elseif ($this->technologies[0] &2048) {
            $prod_hy *= 1.2;
        } elseif ($this->technologies[0] &1024) {
            $prod_hy *= 1.1;
        }

        //Augmentation de la production en fonction du moral
        if ($this->moral > 0.9) {
            $prod_met *= 1.05;
            $prod_cri *= 1.05;
            $prod_hy *= 1.05;
        } elseif ($this->moral > 0.75) {
            $prod_met *= 1.02;
            $prod_cri *= 1.02;
            $prod_hy *= 1.02;
        } elseif ($this->moral < 0.45) {
            $prod_met *= 0.97;
            $prod_cri *= 0.97;
            $prod_hy *= 0.97;
        } elseif ($this->moral < 0.25) {
            $prod_met *= 0.94;
            $prod_cri *= 0.94;
            $prod_hy *= 0.94;
        }

        //Augmentation de la production en fonction de la politique
        if ($this->politique == 2) {
            $prod_met *= 1.10;
            $prod_cri *= 1.10;
            $prod_hy *= 1.10;
        }

        //On enlève la consomation d'hydrogène
        $prod_hy -= $conso_h;

        if ($retarray) {
            return array(
                array(
                    ceil($this->coeff_bat[0]*100),
                    ceil($this->coeff_bat[1]*100),
                    ceil($this->coeff_bat[2]*100),
                    ceil($this->coeff_bat[3]*100),
                    ceil($this->coeff_bat[4]*100)),
                    array(
                        $prod_met,
                        $prod_cri,
                        $prod_hy + $conso_h,
                        $energie_s*$this->coeff_bat[3],
                        $energie_f*$this->coeff_bat[4]),
                    array(
                        $energie_m*$this->coeff_bat[0],
                        $energie_c*$this->coeff_bat[1],
                        $energie_h*$this->coeff_bat[2],
                        $conso_h,
                        ($energie_s*$this->coeff_bat[3] + $energie_f*$this->coeff_bat[4])-($energie_m*$this->coeff_bat[0] + $energie_c*$this->coeff_bat[1] + $energie_h*$this->coeff_bat[2])
                    )
                );
        } else {
            return array($prod_met, $prod_cri, $prod_hy);
        }
    }

    public function creer($id_user, $mere = false)
    {
        global $VAR;
        //Définition des paramètres de l'utilisateur pour la planète
        $this->id_user = $id_user;

        //Génération du nombre de case et de l'image en fonction de la position dans le système
        $this->image = mt_rand(1, 150);
        if ($mere) {
            $this->cases = 225;
        } elseif ($this->position > $VAR['nb_planete']*0.75) {
            $this->cases = mt_rand(200, 255);
        } elseif ($this->position > $VAR['nb_planete']/2) {
            $this->cases = mt_rand(250, 300);
        } elseif ($this->position > $VAR['nb_planete']/4) {
            $this->cases = mt_rand(175, 260);
        } else {
            $this->cases = mt_rand(150, 220);
        }

        //Définition des principaux paramètres de la planète
        if ($mere) {
            $this->nom_planete = 'Planète mère';
        } else {
            $this->nom_planete = 'Planète colonisée';
        }

        $this->metal = 1000;
        $this->cristal = 700;
        $this->hydrogene = 0;

        // on met le moment où la planete a été crée, comme ça, à la première actualisation, on ne remplit pas les silos
        $this->timestamp = time();

        $this->modif = array("id_user", "nom_planete", "galaxie", "ss", "position", "image", "cases", "timestamp", "metal", "cristal", "hydrogene");
    }

    /**
     * Destructeur
     *
     * @return   void
     * @access   public
     */
    public function __destruct()
    {
        if (empty($this->ss) || empty($this->id_user)) {
            return;
        }
        if (DEBUG) {
            var_dump($this);
        }

        global $table_planete;
        if (empty($this->id)) {
            $outNomChamps = array();
            $outValeurs = array();
            $bdd = new BDD();
            foreach ($this->modif as $modif) {
                //On gère les champs variables tableaux
                if (is_array($modif)) {
                    $calc = dDonnees::nameVAR($modif[0]);

                    if (!isset(${$calc.'VAR'})) {
                        global ${$calc.'VAR'};
                    }

                    if ($calc == "terrestre") {
                        $outNomChamps[] = ${$calc.'VAR'}[$this->race][$modif[1]];
                    } else {
                        $outNomChamps[] = ${$calc.'VAR'}[$modif[1]];
                    }
                    $outValeurs[] = $this->{$modif[0]}[$modif[1]];
                } elseif ($modif == "force") {
                    continue;
                } elseif (!is_array($this->{$modif})) {
                    $bdd->escape($this->{$modif});
                    $outNomChamps[] = $modif;
                    if (is_int($this->{$modif}) || is_float($this->{$modif})) {
                        $outValeurs[] = $this->{$modif};
                    } else {
                        $outValeurs[] = "'".$this->{$modif}."'";
                    }
                } else {
                    if (is_array($this->{$modif}) && $modif != "coeff_bat" && $modif != "vaisseaux" && $modif != "terrestres" && $modif != "casernes" && $modif != "technologies" && $modif != "batiments") {
                        $prep = serialize($this->{$modif});
                        $bdd->escape($prep);
                        $outNomChamps[] = $modif;
                        $outValeurs[] = "'$prep'";
                    } else {
                        $calc = dDonnees::nameVAR($modif);

                        if (!isset(${$calc.'VAR'})) {
                            global ${$calc.'VAR'};
                        }

                        foreach ($this->{$modif} as $j => $value) {
                            if ($calc == "terrestre") {
                                $outNomChamps[] = ${$calc.'VAR'}[$this->race][$j];
                            } else {
                                $outNomChamps[] = ${$calc.'VAR'}[$j];
                            }
                            $outNomChamps[] = ${$calc.'VAR'}[$j];
                            $outValeurs[] = $value;
                        }
                    }
                }
            }
            $bdd->query("INSERT INTO $table_planete (".implode(', ', $outNomChamps).", hash_planete) VALUES (".implode(', ', $outValeurs).", SHA1(CONCAT('g',planete.galaxie,'s',planete.ss,'p',planete.position)))");
            if ($bdd->affected() != 1) {
                elog($bdd->affected()." champ(s) affecté(s) par la requête du fichier ".__FILE__." à la ligne ".__LINE__.", données : outNomChamps = ".serialize($outNomChamps)." ; outValeurs = ".serialize($outValeurs), 2);
            }
            $bdd->deconnexion();
        } else {
            $out = array();
            $bdd = new BDD();
            foreach ($this->modif as $modif) {
                //On gère les champs variables tableaux
                if (is_array($modif)) {
                    $calc = dDonnees::nameVAR($modif[0]);

                    if (!isset(${$calc.'VAR'})) {
                        global ${$calc.'VAR'};
                    }
                    if (empty(${$calc.'VAR'})) {
                        trigger_error('Impossible de trouver les données pour '.$modif[0], E_USER_ERROR);
                    }

                    if ($calc == "terrestre") {
                        $out[] = ${$calc.'VAR'}[$this->race][$modif[1]]." = ".$this->{$modif[0]}[$modif[1]];
                    } else {
                        $out[] = ${$calc.'VAR'}[$modif[1]]." = ".$this->{$modif[0]}[$modif[1]];
                    }
                } elseif ($modif == "force") {
                    $out[] = "timestamp = timestamp";
                } elseif (!is_array($this->{$modif}) && !is_object($this->{$modif})) {
                    $bdd->escape($this->{$modif});
                    if (is_int($this->{$modif}) || is_float($this->{$modif})) {
                        $out[] = $modif." = ".$this->{$modif};
                    } else {
                        $out[] = $modif." = '".$this->{$modif}."'";
                    }
                } else {
                    if ($modif != "coeff_bat" && $modif != "vaisseaux" && $modif != "terrestres" && $modif != "casernes" && $modif != "technologies" && $modif != "batiments") {
                        $prep = serialize($this->{$modif});
                        $bdd->escape($prep);
                        $out[] = $modif." = '$prep'";
                    } else {
                        $calc = dDonnees::nameVAR($modif);

                        if (!isset(${$calc.'VAR'})) {
                            global ${$calc.'VAR'};
                        }

                        foreach ($this->{$modif} as $j => $value) {
                            if ($calc == "terrestre") {
                                $out[] = ${$calc.'VAR'}[$this->race][$j]." = ".$value;
                            } else {
                                $out[] = ${$calc.'VAR'}[$j]." = ".$value;
                            }
                        }
                    }
                }
            }
            if (!empty($out)) {
                $sql = "UPDATE $table_planete SET timestamp = ".time().", metal = ".$this->metal.", cristal = ".$this->cristal.", hydrogene = ".$this->hydrogene.", ".implode(', ', $out)." WHERE id = ".$this->id.";";
                if (DEBUG) {
                    echo '<br /><br />'.$sql;
                }
                $bdd->query($sql);
                if ($bdd->affected() != 1) {
                    elog($bdd->affected()." champ(s) affecté(s) par la requête du fichier ".__FILE__." à la ligne ".__LINE__.", données : out = ".serialize($out)." avec l'ID : ".$this->id, 2);
                }
            }

            $bdd->deconnexion();
            parent::__destruct();
        }
    }
}
