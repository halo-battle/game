<?php
include_once("Class/rapport.php");
/***************************************************************************
 *                            class.flotte.php
 *                           ------------------
 *   begin                : Samedi 20 septembre 2008
 *   update               : Samedi 20 septembre 2008
 *   email                : nemunaire@gmail.com
 *
 *
 ***************************************************************************/
class Flotte
{
    public $id_flotte = 0;
    public $nom;
    public $start_planete;
    public $start_time;
    public $start_type;
    public $end_planete;
    public $end_time;
    public $end_type;
    public $ret_planete;
    public $ret_type;
    public $ret_time;
    public $nb_vais;
    public $vaisseaux = array();
    public $tactique = 0;
    public $mission;
    public $vitesse;
    public $statut = 0;
    public $last;
    public $contenu = array(0,0,0);
    public $contenuMax = 0;
    public $modifFlotte = array();

    /**
     * Constructeur
     * @param    int	$id	id de la flotte à importer
     * @param    bool	$verrou	Booléen disant si la classe doit obtenir préalablement un verrou pour la flotte.
     *
     * @return   void
     * @access   public
     */
    public function __construct($id = 0, $verrou = true)
    {
        if (!empty($id)) {
            global $table_flottes;
            global $ressoVAR, $spatialVAR;

            $id = intval($id);
            $bdd = new BDD();
            $flotte = $bdd->unique_query("SELECT * FROM $table_flottes WHERE id = $id;");
            if ($verrou) {
                $bdd->query("UPDATE $table_flottes SET last = ".time()." WHERE id = $id;");
            } //Obtention d'un vérrou de 10 seconde sur la flotte
            $bdd->deconnexion();

            if (!empty($flotte)) {
                $this->id_flotte = $flotte["id"];
                $this->nom = $flotte["nom"];
                $this->start_planete = $flotte["start_planete"];
                $this->start_type = $flotte["start_type"];
                $this->start_time = $flotte["start_time"];
                $this->end_planete = $flotte["end_planete"];
                $this->end_type = $flotte["end_type"];
                $this->end_time = $flotte["end_time"];
                $this->ret_planete = $flotte["ret_planete"];
                $this->ret_type = $flotte["ret_type"];
                $this->ret_time = $flotte["ret_time"];
                $this->tactique = $flotte["tactique"];
                $this->mission = $flotte["mission"];
                $this->vitesse = $flotte["vitesse"];
                $this->statut = $flotte["statut"];
                $this->last = $flotte["last"];
                $this->nb_vais = $flotte["nb_vais"];

                foreach ($spatialVAR as $vais) {
                    $this->vaisseaux[] = $flotte[$vais];
                }

                $this->contenu = array($flotte["contenu_metal"], $flotte["contenu_cristal"], $flotte["contenu_hydrogene"]);

                $this->calculer();
            }
        }
    }


    public function calculer()
    {
        global $planete;
        $this->nb_vais = 0;

        //Calcul de la capacité maximale d'embarquement de la flotte
        foreach ($this->vaisseaux as $key => $vais) {
            $this->nb_vais += $vais;
            $this->contenuMax += dSpatial::tailleCales($key, $planete) * $vais;
        }
    }


    public function load_planete()
    {
        if (!empty($this->start_planete) && is_numeric($this->start_planete)) {
            //On traite l'importation en fonction des types fournis
            if ($this->start_type == 0) {
                global $planete;
                //Si la planète est la même que celle du joueur actuel, on l'utilise, sinon, on la crée
                if (SURFACE == "planete" && $planete->id == $this->start_planete) {
                    $this->start_planete = $planete;
                } else {
                    $this->start_planete = new Planete($this->start_planete);
                }
            } elseif ($this->start_type == 2) {
                global $planete;
                //Si la planète est la même que celle du joueur actuel, on l'utilise, sinon, on la crée
                if (SURFACE == "asteroide" && $planete->id == $this->start_planete) {
                    $this->start_planete = $planete;
                } else {
                    $this->start_planete = new Asteroide($this->start_planete);
                }
            }
        }
        if (!empty($this->end_planete) && is_numeric($this->end_planete)) {
            //On traite l'importation en fonction des types fournis
            if ($this->end_type == 0 || $this->ret_type == 1) {
                global $planete;
                //Si la planète est la même que celle du joueur actuel, on l'utilise, sinon, on la crée
                if (SURFACE == "planete" && $planete->id == $this->end_planete) {
                    $this->end_planete = $planete;
                } else {
                    $this->end_planete = new Planete($this->end_planete);
                }
            } elseif ($this->end_type == 2) {
                global $planete;
                //Si la planète est la même que celle du joueur actuel, on l'utilise, sinon, on la crée
                if (SURFACE == "asteroide" && $planete->id == $this->end_planete) {
                    $this->end_planete = $planete;
                } else {
                    $this->end_planete = new Asteroide($this->end_planete);
                }
            }
        }
        if (!empty($this->ret_planete) && is_numeric($this->ret_planete)) {
            //On traite l'importation en fonction des types fournis
            if ($this->ret_type == 0) {
                global $planete;
                //Si la planète est la même que celle du joueur actuel, on l'utilise, sinon, on la crée
                if (SURFACE == "planete" && $planete->id == $this->ret_planete) {
                    $this->ret_planete = $planete;
                } else {
                    $this->ret_planete = new Planete($this->ret_planete);
                }
            } elseif ($this->ret_type == 2) {
                global $planete;
                //Si la planète est la même que celle du joueur actuel, on l'utilise, sinon, on la crée
                if (SURFACE == "asteroide" && $planete->id == $this->ret_planete) {
                    $this->ret_planete = $planete;
                } else {
                    $this->ret_planete = new Asteroide($this->ret_planete);
                }
            }
        }
    }

    public function creer(surface $start_planete, $tableau, $resultat)
    {
        $sauv = $start_planete->vaisseaux; //Sauvegarde les vaisseaux de la planète avant le lancement
        foreach ($tableau['vaisseaux'] as $key => $vaisseau) {
            $this->vaisseaux[$key] = $vaisseau;
            $sauv[$key] -= $vaisseau;

            if ($sauv[$key] < 0) {
                erreur('Vous n\'avez pas assez de vaisseaux sur cette planète pour envoyer cette flotte !', "red", $VAR["menu"]["flotte"]);
            }
        }

        //Définition des paramètres de la flotte en fonction des données enregistrées en session
        $this->nom = $tableau["nom"];
        $this->vitesse = $tableau["vitesse"];
        $this->mission = $tableau["mission"];
        $this->start_planete = $start_planete;
        if (SURFACE == "planete") {
            $this->start_type = 0;
        } elseif (SURFACE == "asteroide") {
            $this->start_type = 2;
        }
        $this->start_time = time();

        //On traite le cas des planètes qui n'existent pas (dans le cas de la mission colonisation)
        if ($tableau["mission"] == 2) {
            //On traite le cas des colonisation d'astéroïdes
            if ($tableau["end_position"] == "A") {
                $this->end_type = 3;
            } else {
                $this->end_type = 1;
            }

            $this->end_planete = $tableau["end_galaxie"].':'.$tableau["end_systeme"].':'.$tableau["end_position"];
        }
        //On traite le cas des envoie vers les alliances
        elseif ($tableau["end_position"] == "A") {
            $this->end_type = 2;
            $this->end_planete = $resultat['id'];
        } else {
            $this->end_type = 0;
            $this->end_planete = $resultat['id'];
        }

        if (empty($this->end_planete)) {
            erreur("Planète d'arriver incorrecte !");
        }

        //On vérifie qu'il y a assez de place dans les cales des vaisseaux avant l'envoie ainsi que de ressources sur la planète
        $this->calculer();

        $calc = Flotte::calc_deplacement($start_planete, $tableau["end_galaxie"], $tableau["end_systeme"], $tableau["end_position"], $tableau["vitesse"]/100, $tableau["vaisseaux"], $this->contenuMax - $tableau["embarquer"][0] - $tableau["embarquer"][1] - $tableau["embarquer"][2], false, true);

        $this->end_time = $calc[0];
        //On double la consomation si on fait un aller-retour
        if ($tableau["mission"] == 1 || $tableau["mission"] == 2 || $tableau["mission"] == 4 || $tableau["mission"] == 5) {
            $calc[1] *= 2;
        }

        if ($this->contenuMax < $tableau["embarquer"][0] + $tableau["embarquer"][1] + $tableau["embarquer"][2] + $calc[1]) {
            erreur('Vous ne pouvez pas embarquer autant de ressources, les cales débordent. '.$this->contenuMax, "red", '?p=flotte', 3500);
        } elseif ($start_planete->metal < $tableau["embarquer"][0] || $start_planete->cristal < $tableau["embarquer"][1] || $start_planete->hydrogene < $tableau["embarquer"][2] + $calc[1]) {
            erreur('Vous n\'avez pas assez de ressources sur cette planète pour envoyer cette flotte.', "red", '?p=flotte', 3500);
        }

        $this->contenu = $tableau["embarquer"];
        $this->modifFlotte = "INSERT";

        //On actualise la planète
        $start_planete->metal -= $tableau["embarquer"][0];
        $start_planete->cristal -= $tableau["embarquer"][1];
        $start_planete->hydrogene -= floor($tableau["embarquer"][2] + $calc[1]);
        $start_planete->vaisseaux = $sauv;
        $start_planete->addModif("vaisseaux");
        $start_planete->addModif("force");
    }

    public static function calc_deplacement($start_planete, $end_galaxie, $end_systeme, $end_position, $vitesse, $vaisseaux, $contenuDispo, $returnArray = false, $returnConso = false)
    {
        global $VAR;

        //Calcul de la longueur du déplacement
        $diff_galaxie = abs($start_planete->galaxie - $end_galaxie);
        $diff_systeme = abs($start_planete->ss - $end_systeme);
        $diff_position = abs($start_planete->position - $end_position);

        $diff_centre_position_start = abs(ceil($VAR['nb_amas']/2) - $start_planete->position);
        $diff_centre_systeme_start = abs(ceil($VAR['nb_systeme']/2) - $start_planete->ss);

        $diff_centre_position_end = abs(ceil($VAR['nb_amas']/2) - $end_position);
        $diff_centre_systeme_end = abs(ceil($VAR['nb_systeme']/2) - $end_systeme);

        //Calcul du temps de déplacement pour chaque vaisseau
        $temps = array();
        $conso = array(0, 0, 0);
        foreach ($vaisseaux as $key => $vais) {
            //S'il n'y a pas de vaisseaux de ce type, on ne calcul pas leur vitesse
            if ($vais == 0) {
                continue;
            }

            //Calcul du temps de déplacement entre planètes
            if ($start_planete->ss == $end_systeme && $start_planete->galaxie == $end_galaxie) {
                $temps[0][$key] = (1000/dSpatial::vitesseP($key, $start_planete)) * (1 + 0.1 * $diff_position);
                $temps[1][$key] = $temps[2][$key] = 0;
            }
            //Calcul du temps de déplacement entre système
            elseif ($start_planete->galaxie == $end_galaxie) {
                $temps[0][$key] = (1000/dSpatial::vitesseP($key, $start_planete)) * (1 + 0.1 * ($diff_centre_position_start + $diff_centre_position_end));
                $temps[1][$key] = (2000/dSpatial::vitesseS($key, $start_planete)) * (2 + 1 * $diff_systeme);
                $temps[2][$key] = 0;
            }
            //Calcul du temps de déplacement entre galaxies
            else {
                $temps[0][$key] = (1000/dSpatial::vitesseP($key, $start_planete)) * (1 + 0.1 * ($diff_centre_position_start + $diff_centre_position_end));
                $temps[1][$key] = (2000/dSpatial::vitesseS($key, $start_planete)) * (2 + 1 * ($diff_centre_systeme_start + $diff_centre_systeme_end));
                $temps[2][$key] = (5000/dSpatial::vitesseG($key, $start_planete)) * (2 + 1.5 * $diff_galaxie);
            }

            //Calcul du bonus pour le réacteur à combustion
            $techR = $start_planete->technologies[1];
            if ($techR & 56) {
                $bonus = 0.7;
            } elseif ($techR & 24) {
                $bonus = 0.8;
            } elseif ($techR & 8) {
                $bonus = 0.9;
            } else {
                $bonus = 1;
            }
            $temps[0][$key] *= $bonus * 1/$vitesse;
            $conso[0] += $vais * $temps[0][$key] * $bonus / exp($vitesse/5);

            //Calcul du bonus pour le réacteur à fusion
            $techR = $start_planete->technologies[1];
            if ($techR &448) {
                $bonus = 0.7;
            } elseif ($techR &192) {
                $bonus = 0.8;
            } elseif ($techR &64) {
                $bonus = 0.9;
            } else {
                $bonus = 1;
            }
            $temps[1][$key] *= $bonus * 1/$vitesse;
            $conso[1] += $vais * $temps[1][$key] * $bonus / exp($vitesse/7.5);

            //Calcul du bonus pour le réacteur à fusion de type II
            $techR = $start_planete->technologies[1];
            if ($techR &3584) {
                $bonus = 0.7;
            } elseif ($techR &1536) {
                $bonus = 0.8;
            } elseif ($techR &512) {
                $bonus = 0.9;
            } else {
                $bonus = 1;
            }
            $temps[2][$key] *= $bonus * 1/$vitesse;
            $conso[2] += $vais * $temps[2][$key] * $bonus / exp($vitesse/10);
        }

        if (!isset($vaisseaux[4])) {
            $vaisseaux[4] = 0;
        }
        if (!isset($vaisseaux[5])) {
            $vaisseaux[5] = 0;
        }

        //Si les chasseurs peuvent rentrer dans les cales des vaisseaux, on les enlèves
        if ($contenuDispo - ceil($conso[0]+$conso[1]+$conso[2]) - ($vaisseaux[4] * dSpatial::tailleCales(4, $start_planete) + $vaisseaux[5] * dSpatial::tailleCales(5, $start_planete)) >= ($vaisseaux[4] + $vaisseaux[5]) * 200) {
            $temps[2][4] = $temps[2][5] = $temps[1][4] = $temps[1][5] = $temps[0][4] = $temps[0][5] = 0;
        }

        //On calcul le temps de déplacement maximal
        if ($returnArray) {
            return $temps;
        } elseif ($returnConso) {
            return array(max($temps[0]) + max($temps[1]) + max($temps[2]), ceil($conso[0]+$conso[1]+$conso[2]));
        } else {
            return (max($temps[0]) + max($temps[1]) + max($temps[2]));
        }
    }


    public function calcStockage($vaisseaux, surface $planete)
    {
        global $LANG;

        $contenuMax = 0;
        foreach ($vaisseaux as $key => $vais) {
            if (!isset($LANG[$planete->race]["vaisseaux"]["noms_sing"][$key])) {
                continue;
            }
            $contenuMax += dSpatial::tailleCales($key, $planete) * $vais;
        }
        return $contenuMax;
    }


    public function check_mission()
    {
        //On vérifie qu'un calcul ne soit pas déjà en cours
        if ($this->last >= time() - 10) {
            return false;
        } elseif ($this->start_time + $this->end_time > time()) {
            return false;
        }

        if ($this->statut == 0) {
            switch ($this->mission) {
                case 6:
                case 7:
                    $this->stationner();
                    break;
                case 1:
                    $this->transporter();
                    break;
                case 2:
                    $this->coloniser();
                    break;
                case 3:
                    $this->attaquer();
                    break;
                case 4:
                    $this->recycler();
                    break;
                case 5:
                    $this->espionner();
                    break;
            }
        }

        if ($this->statut == 1 && ($this->ret_time > time() || $this->start_time + $this->end_time * 2 <= time())) {
            $this->retourner();
        }

        return true;
    }


    public function stationner()
    {
        //On décharge les ressources éventuellement contenue
        $this->decharger();

        //On fait atterir les vaisseaux
        foreach ($this->vaisseaux as $key => $vais) {
            $this->end_planete->vaisseaux[$key] += $vais;
        }

        if (!in_array("vaisseaux", $this->end_planete->modif)) {
            $this->end_planete->modif[] = "vaisseaux";
        }
        $this->modifFlotte = "DELETE";
    }


    public function transporter()
    {
        $max = $this->decharger();

        //Envoie du MP de confirmation au joueur
        $send = new Rapport(2, $this->start_planete, $this->end_planete, $this->start_time + $this->end_time);
        $send->addInfo($this->end_planete, 0);
        $send->addInfo($max, 1);
        $send->sendTransport();

        $this->statut = 1;
        $this->addModifFlotte("statut");
    }


    public function coloniser()
    {
        //On vérifie que les coordonnées de la planètes sont bien enregistrée
        if (empty($this->end_planete) || is_object($this->end_planete) || !preg_match('#^\[?([0-9]{1,2}):([0-9]{1,2}):([A0-9]{1,2})\]?$#', $this->end_planete)) {
            $this->load_planete();
            file_log("Erreur de colonisation de la planète : ".$this->end_planete." pour le joueur : ".$this->start_planete->id_user, 2);
            send_mp($this->start_planete->id_user, "Erreur de colonisation [F#01]", "Une erreur s'est produite lors de la tentative de colonisation de votre flotte, elle a fait demi-tour.", $this->start_time + $this->end_time);
            $this->rappeler();
        }

        //On vérifie que la planète ne soit pas déjà colonisée
        global $table_planete, $table_alliances, $table_alliances_creation, $table_user;
        preg_match('#^\[?([0-9]{1,2}):([0-9]{1,2}):([A0-9]{1,2})\]?$#', $this->end_planete, $position);
        if (is_numeric($position[3])) {
            $bdd = new BDD();
            $p = $bdd->query("SELECT * FROM $table_planete WHERE galaxie = ".$position[1]." AND ss = ".$position[2]." AND position = ".$position[3].";");
            $bdd->deconnexion();
        } else {
            $bdd = new BDD();
            $p = $bdd->query("SELECT * FROM $table_alliances WHERE galaxie = ".$position[1]." AND ss = ".$position[2].";");
            $alliance = $bdd->unique_query("SELECT signatures FROM $table_alliances_creation WHERE fondateur = ".$this->start_planete->id_user.";");
            $signaturesExport = implode(' OR id = ', explode(';', substr($alliance["signatures"], 1), -1));
            $bdd->deconnexion();
        }

        $this->load_planete();

        if ($p && (!isset($signaturesExport) || count($signaturesExport) < nb_signatures)) {
            $rapport = new Rapport(2, $this->start_planete, 0, $this->start_time + $this->end_time);
            $rapport->addInfo(array($position[1], $position[2], $position[3]), 0);
            $rapport->addInfo(false, 1);
            $rapport->send();

            $this->statut = 1;
            $this->addModifFlotte("statut");
        } else {
            if (is_numeric($position[3])) {
                //On crée la planète
                $this->end_planete = new Planete(false);
                $this->end_planete->galaxie = $position[1];
                $this->end_planete->ss = $position[2];
                $this->end_planete->position = $position[3];
                $this->end_planete->creer($this->start_planete->id_user);

                //On définit la limite de ressources pour permettre le déchargement de celles contenues dans les vaisseaux
                $this->end_planete->cap = 100000;
            } else {
                //On crée l'astéroide
                $this->end_planete = new Asteroide(false);
                $this->end_planete->galaxie = $position[1];
                $this->end_planete->ss = $position[2];
                $this->end_planete->creer($this->start_planete);
            }

            //Rembousement du carburant non utilisé (la colonisation prévois au départ un allé/retour)
            $conso = $this->calc_deplacement($this->start_planete, $position[1], $position[2], $position[3], $this->vitesse, $this->vaisseaux, 0, false, true);
            $this->end_planete->hydrogene += $conso[1];

            //On enlève un vaisseau de colonisation de la liste
            $this->vaisseaux[2]--;

            //On fait atterir les vaisseaux et décharger les ressources
            $this->decharger();
            $this->atterir();

            //On envoie un rapport
            $rapport = new Rapport(2, $this->start_planete, 0, $this->start_time + $this->end_time);
            $rapport->addInfo(array($position[1], $position[2], $position[3]), 0);
            $rapport->addInfo(true, 1);
            $rapport->send();

            if (!is_numeric($position[3])) {
                unset($this->end_planete);
                $bdd = new BDD();
                $p = $bdd->unique_query("SELECT id FROM $table_alliances WHERE galaxie = ".$position[1]." AND ss = ".$position[2]." AND fondateur = ".$this->start_planete->id_user.";");
                print "UPDATE $table_user SET alliance = '".$p['id']."' WHERE id = $signaturesExport OR id = ".$this->start_planete->id_user.";";
                $bdd->query("UPDATE $table_user SET id_alliance = '".$p['id']."' WHERE id = $signaturesExport OR id = ".$this->start_planete->id_user.";");
                $bdd->deconnexion();
            }
        }
    }


    public function recycler()
    {
        //Si la planète d'arrivé n'est pas chargée, on charge les planètes
        if (is_numeric($this->end_planete)) {
            $this->load_planete();
        }

        $coeff = ($this->contenuMax - $this->contenu[0] - $this->contenu[1] - $this->contenu[2])/($this->end_planete->debris_met + $this->end_planete->debris_cri);
        if ($coeff > 1) {
            $coeff = 1;
        }

        $a = floor($this->end_planete->debris_met * $coeff);
        $b = floor($this->end_planete->debris_cri * $coeff);

        $this->contenu[0] += $a;
        $this->contenu[1] += $b;
        if (!in_array('contenu', $this->modifFlotte)) {
            $this->modifFlotte[] = 'contenu';
        }

        $this->end_planete->debris_met -= $a;
        $this->end_planete->debris_cri -= $b;
        if (!in_array('debris_met', $this->end_planete->modif)) {
            $this->end_planete->modif[] = 'debris_met';
        }
        if (!in_array('debris_cri', $this->end_planete->modif)) {
            $this->end_planete->modif[] = 'debris_cri';
        }

        //Send link
        $rapport = new Rapport(4, $this->start_planete, 0, $this->start_time + $this->end_time);
        $rapport->addInfo($this->end_planete, 0);
        $rapport->addInfo(array($a, $b), 1);
        $rapport->send();

        $this->statut = 1;
        if (!in_array('statut', $this->modifFlotte)) {
            $this->modifFlotte[] = 'statut';
        }
    }


    public function attaquer()
    {
        //Si la planète d'arrivé n'est pas chargée, on charge les planètes
        if (is_numeric($this->end_planete)) {
            $this->load_planete();
        }

        //On charge la classe des combats

        //On réalise le combat

        //On envoit le résultat

        //On update la flotte avec les pertes et gains

        //On dis à la flotte de rentrer
        $this->statut = 1;
        $this->addModifFlotte("statut");
    }

    public function espionner()
    {
        //Si la planète d'arrivé n'est pas chargée, on charge les planètes
        if (is_numeric($this->end_planete)) {
            $this->load_planete();
        }

        //Extraction des niveaux technologique des deux adversaires
        if (($this->start_planete->technologies[1]& 67108864) == 67108864) {
            $espionnage_A = 3;
        } elseif (($this->start_planete->technologies[1]& 33554432) == 33554432) {
            $espionnage_A = 2;
        } elseif (($this->start_planete->technologies[1]& 16777216) == 16777216) {
            $espionnage_A = 1;
        } else {
            $espionnage_A = 0;
        }
        if (($this->end_planete->technologies[1]& 536870912) == 536870912) {
            $contreespionnage_B = 3;
        } elseif (($this->end_planete->technologies[1]& 268435456) == 268435456) {
            $contreespionnage_B = 2;
        } elseif (($this->end_planete->technologies[1]& 134217728) == 134217728) {
            $contreespionnage_B = 1;
        } else {
            $contreespionnage_B = 0;
        }

        //Création du rapport
        $rapport = new Rapport(5, $this->start_planete, $this->end_planete, $this->start_time + $this->end_time);
        $rapport->addInfo($this->end_planete, 0);
        //Rend aléatoire le niveau affiché des bâtiments
        $diff_esp =  $espionnage_A - $contreespionnage_B;
        //Il y a au moins 2 niveaux d'erreur (soit 1 positif et 1 négatif)
        if ($diff_esp == 0) {
            $diff_esp = 1;
        }
        $rapport->addInfo($diff_esp, 1);
        //Où x est le résultat de cette formule, si x >= y, ça donne accès a plus d'informations
        // si y >= 1 : ressources
        // si y >= 2 : batiment
        // si y >= 3 : défenses
        // si y >= 4 : flottes
        $rapport->addInfo($espionnage_A + 2 - $contreespionnage_B + 1, 2);
        //Si le joueur a au moins le niveau 1 du contre-espionnage, on le prévient qu'il a été espionné, sinon il n'en sait rien
        $rapport->addInfo($contreespionnage_B, 3);
        $rapport->send();

        $this->statut = 1;
        $this->addModifFlotte("statut");
    }


    public function decharger($plan = "end_planete")
    {
        //Si la planète d'arrivé n'est pas chargée, on charge les planètes
        if (is_numeric($this->$plan)) {
            $this->load_planete();
        }
        $max = array(0, 0, 0);

        //Si on dépasse les capacités, on laisse les ressources en trop dans le cargo
        if ($this->$plan->metal + $this->contenu[0] > $this->$plan->cap) {
            $max[0] = $this->$plan->cap - $this->$plan->metal;
            if ($max[0] < 0) {
                $max[0] = 0;
            }
        } else {
            $max[0] = $this->contenu[0];
        }
        $this->$plan->metal += $max[0];
        $this->contenu[0] -= $max[0];

        if ($this->$plan->cristal + $this->contenu[1] > $this->$plan->cap) {
            $max[1] = $this->$plan->cap - $this->$plan->cristal;
            if ($max[1] < 0) {
                $max[1] = 0;
            }
        } else {
            $max[1] = $this->contenu[1];
        }
        $this->$plan->cristal += $max[1];
        $this->contenu[1] -= $max[1];

        if ($this->$plan->hydrogene + $this->contenu[2] > $this->$plan->cap) {
            $max[2] = $this->$plan->cap - $this->$plan->hydrogene;
            if ($max[2] < 0) {
                $max[2] = 0;
            }
        } else {
            $max[2] = $this->contenu[2];
        }
        $this->$plan->hydrogene += $max[2];
        $this->contenu[2] -= $max[2];

        $this->$plan->addModif("force");

        $this->addModifFlotte("contenu");
        return $max;
    }


    public function atterir($plan = "end_planete")
    {
        //Si la planète d'arrivé n'est pas chargée, on charge les planètes
        if (is_numeric($this->$plan)) {
            $this->load_planete();
        }

        if (isset($this->$plan->vaisseaux[0])) {
            foreach ($this->vaisseaux as $key => $vais) {
                $this->$plan->vaisseaux[$key] += $vais;
            }
        } else {
            $this->$plan->vaisseaux = $this->vaisseaux;
        }

        $this->$plan->addModif("vaisseaux");

        $this->modifFlotte = "DELETE";
    }


    public function rappeler()
    {
        if ($this->start_time + $this->end_time >= time()) {
            return false;
        } else {
            $this->end_time = time() - $this->start_time + 10;
            $this->mission = 5;
            if (!in_array('mission', $this->modifFlotte)) {
                $this->modifFlotte[] = 'mission';
            }
            if (!in_array('end_time', $this->modifFlotte)) {
                $this->modifFlotte[] = 'end_time';
            }
            return true;
        }
    }


    public function retourner()
    {
        //Si la planète de départ n'est pas chargée, on charge les planètes
        if (is_numeric($this->start_planete)) {
            $this->load_planete();
        }

        //Si on a demandé une planète particulière au retour
        if (!empty($this->ret_time) && !empty($this->ret_planete) && !is_numeric($this->ret_planete)) {
            $this->decharger("ret_planete");
            $this->atterir("ret_planete");
        }
        //Si le retour se fait sur la planète source
        else {
            $this->decharger("start_planete");
            $this->atterir("start_planete");
        }

        $this->modifFlotte = "DELETE";
    }


    public function addModifFlotte($modif)
    {
        if (!in_array($modif, $this->modifFlotte)) {
            $this->modifFlotte[] = $modif;
        }
    }


    public function printTime_end()
    {
        return $this->start_time + $this->end_time;
    }

    public function printTime_ret()
    {
        if (!empty($this->ret_time)) {
            return $this->ret_time;
        } elseif ($this->mission == 1 || $this->mission == 4 || $this->mission == 5 || (($this->mission == 3 || $this->mission == 2) && $this->statut != 0)) {
            return $this->start_time + $this->end_time*2;
        } else {
            return 0;
        }
    }


    public static function txtMission($mission)
    {
        switch ($mission) {
            case 1:
                return "Transporter";
                break;
            case 2:
                return "Coloniser";
                break;
            case 3:
                return "Attaquer";
                break;
            case 4:
                return "Recycler";
                break;
            case 5:
                return "Espionner";
                break;
            case 6:
            case 7:
                return "Stationner";
                break;
            default:
                return "Inconnu";
                break;
        }
    }

    /**
     * Destructeur
     *
     * @return   void
     * @access   public
     */
    public function __destruct()
    {
        global $table_flottes;

        if ($this->modifFlotte === "DELETE") {
            $bdd = new BDD();
            $bdd->query("DELETE FROM $table_flottes WHERE id = ".$this->id_flotte.";");
            if ($bdd->affected() != 1) {
                elog($bdd->affected()." champ(s) affecté(s) par la requête du fichier ".__FILE__." à la ligne ".__LINE__.", données : suppr ID = ".$this->id_flotte, 2);
            }
            $bdd->deconnexion();
        } else {
            if (empty($this->id_flotte) && $this->modifFlotte == "INSERT") {
                $out1 = '';
                $out2 = '';
                global $spatialVAR;
                foreach ($this->vaisseaux as $key => $vais) {
                    if ($vais > 0) {
                        $out1 .= ', '.$spatialVAR[$key];
                        $out2 .= ', '.$vais;
                    }
                }
                if (SURFACE == "asteroide") {
                    $sql = "INSERT INTO $table_flottes (id_user, id_alliance, mission, start_time, start_type, start_planete, end_time, end_type, end_planete, vitesse, contenu_metal, contenu_cristal, contenu_hydrogene, tactique, nom, nb_vais$out1) VALUES (0, ".$this->start_planete->id.", '".$this->mission."', '".$this->start_time."', '".$this->start_type."', '".$this->start_planete->id."', '".$this->end_time."', '".$this->end_type."', '".$this->end_planete."', '".$this->vitesse."', '".$this->contenu[0]."', '".$this->contenu[1]."', '".$this->contenu[2]."', '".$this->tactique."', '".$this->nom."', ".$this->nb_vais."$out2);";
                } else {
                    $sql = "INSERT INTO $table_flottes (id_user, mission, start_time, start_type, start_planete, end_time, end_type, end_planete, vitesse, contenu_metal, contenu_cristal, contenu_hydrogene, tactique, nom, nb_vais$out1) VALUES ('".$this->start_planete->id_user."', '".$this->mission."', '".$this->start_time."', '".$this->start_type."', '".$this->start_planete->id."', '".$this->end_time."', '".$this->end_type."', '".$this->end_planete."', '".$this->vitesse."', '".$this->contenu[0]."', '".$this->contenu[1]."', '".$this->contenu[2]."', '".$this->tactique."', '".$this->nom."', ".$this->nb_vais."$out2);";
                }
                if (DEBUG) {
                    echo $sql;
                }
                $bdd = new BDD();
                $bdd->query($sql);
                if ($bdd->affected() != 1) {
                    elog($bdd->affected()." champ(s) affecté(s) par la requête du fichier ".__FILE__." à la ligne ".__LINE__.", données : insertion flotte", 2);
                }
                $bdd->deconnexion();
            } elseif (isset($this->modifFlotte[0])) {
                $out = array();
                $bdd = new BDD();
                foreach ($this->modifFlotte as $modif) {
                    if (!is_array($this->{$modif})) {
                        if (is_int($this->{$modif}) || is_float($this->{$modif})) {
                            $out[] .= $modif." = ".$this->{$modif};
                        } else {
                            $bdd->escape($this->{$modif});
                            $out[] .= $modif." = '".$this->{$modif}."'";
                        }
                    } else {
                        if ($modif == "contenu") {
                            $calc = "resso";
                        } elseif ($modif == "vaisseaux") {
                            $calc = "nomvaisn";
                        }

                        if (!isset(${$calc.'VAR'})) {
                            global ${$calc.'VAR'};
                        }

                        foreach (${$calc.'VAR'} as $key => $var) {
                            $bdd->escape($this->{$modif}[$key]);
                            $out[] = ${$calc.'VAR'}[$key]." = ".$this->{$modif}[$key];
                        }
                    }
                }

                if (!empty($out)) {
                    $sql = "UPDATE $table_flottes SET ".implode(', ', $out).", last = 0 WHERE id = ".$this->id_flotte.";";
                    if (DEBUG) {
                        var_dump($sql);
                    }
                    $bdd->query($sql);
                    if ($bdd->affected() != 1) {
                        elog($bdd->affected()." champ(s) affecté(s) par la requête du fichier ".__FILE__." à la ligne ".__LINE__.", données : out = ".serialize($out)." avec l'ID : ".$this->id_flotte, 2);
                    }
                }

                $bdd->deconnexion();
            }
        }
    }
}
