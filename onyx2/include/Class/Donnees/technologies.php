<?php
require_once("Class/Donnees/interface.php");

class dTechnologies implements Donnees
{
    public static function metal($branche, $idTechnologie, surface $planete)
    {
        return 0;
    }

    public static function cristal($branche, $idTechnologie, surface $planete)
    {
        return 0;
    }

    public static function hydrogene($branche, $idTechnologie, surface $planete)
    {
        return 0;
    }

    public static function credits($branche, $idTechnologie, surface $planete)
    {
        if ($branche == 0) {
            switch ($idTechnologie) {
                case 0:
                    return 1000;
                    break;
                case 1:
                    return 2000;
                    break;
                case 2:
                    return 4000;
                    break;
                case 3:
                    return 4000;
                    break;
                case 4:
                    return 2000;
                    break;
                case 5:
                    return 4000;
                    break;
                case 6:
                    return 8000;
                    break;
                case 7:
                    return 2000;
                    break;
                case 8:
                    return 4000;
                    break;
                case 9:
                    return 8000;
                    break;
                case 10:
                    return 2000;
                    break;
                case 11:
                    return 4000;
                    break;
                case 12:
                    return 8000;
                    break;
                case 13:
                    return 3000;
                    break;
                case 14:
                    return 6000;
                    break;
                case 15:
                    return 12000;
                    break;
                case 16:
                    return 3000;
                    break;
                case 17:
                    return 6000;
                    break;
                case 18:
                    return 12000;
                    break;
                default:
                    trigger_error("Technologie (".$branche.";".$idTechnologie.") introuvable dans les données", E_USER_ERROR);
            }
        } elseif ($branche == 1) {
            switch ($idTechnologie) {
                case 0:
                    return 1000;
                    break;
                case 1:
                    return 2000;
                    break;
                case 2:
                    return 4000;
                    break;
                case 3:
                    return 2000;
                    break;
                case 4:
                    return 4000;
                    break;
                case 5:
                    return 8000;
                    break;
                case 6:
                    return 3000;
                    break;
                case 7:
                    return 6000;
                    break;
                case 8:
                    return 12000;
                    break;
                case 9:
                    return 4000;
                    break;
                case 10:
                    return 8000;
                    break;
                case 11:
                    return 16000;
                    break;
                case 12:
                    return 1000;
                    break;
                case 13:
                    return 2000;
                    break;
                case 14:
                    return 4000;
                    break;
                case 15:
                    return 2000;
                    break;
                case 16:
                    return 4000;
                    break;
                case 17:
                    return 8000;
                    break;
                case 18:
                    return 2000;
                    break;
                case 19:
                    return 4000;
                    break;
                case 20:
                    return 8000;
                    break;
                case 21:
                    return 2000;
                    break;
                case 22:
                    return 4000;
                    break;
                case 23:
                    return 8000;
                    break;
                case 24:
                    return 2000;
                    break;
                case 25:
                    return 4000;
                    break;
                case 26:
                    return 8000;
                    break;
                case 27:
                    return 3000;
                    break;
                case 28:
                    return 6000;
                    break;
                case 29:
                    return 12000;
                    break;
                default:
                    trigger_error("Technologie (".$branche.";".$idTechnologie.") introuvable dans les données", E_USER_ERROR);
            }
        } elseif ($branche == 2) {
            switch ($idTechnologie) {
                case 0:
                    return 1000;
                    break;
                case 1:
                    return 2000;
                    break;
                case 2:
                    return 4000;
                    break;
                case 3:
                    return 4000;
                    break;
                case 4:
                    return 4000;
                    break;
                case 5:
                    return 4000;
                    break;
                case 6:
                    return 8000;
                    break;
                case 7:
                    return 16000;
                    break;
                default:
                    trigger_error("Technologie (".$branche.";".$idTechnologie.") introuvable dans les données", E_USER_ERROR);
            }
        } elseif ($branche == 3) {
            switch ($idTechnologie) {
                case 0:
                    return 1000;
                    break;
                case 1:
                    return 2000;
                    break;
                case 2:
                    return 4000;
                    break;
                case 3:
                    return 2000;
                    break;
                case 4:
                    return 4000;
                    break;
                case 5:
                    return 8000;
                    break;
                case 6:
                    return 4000;
                    break;
                case 7:
                    return 6000;
                    break;
                case 8:
                    return 6000;
                    break;
                case 9:
                    return 1000;
                    break;
                case 10:
                    return 2000;
                    break;
                case 11:
                    return 4000;
                    break;
                case 12:
                    return 2000;
                    break;
                case 13:
                    return 4000;
                    break;
                case 14:
                    return 8000;
                    break;
                default:
                    trigger_error("Technologie (".$branche.";".$idTechnologie.") introuvable dans les données", E_USER_ERROR);
            }
        } elseif ($branche == 4) {
            switch ($idTechnologie) {
                case 0:
                    return 1000;
                    break;
                case 1:
                    return 2000;
                    break;
                case 2:
                    return 4000;
                    break;
                case 3:
                    return 1000;
                    break;
                case 4:
                    return 2000;
                    break;
                case 5:
                    return 4000;
                    break;
                case 6:
                    return 2000;
                    break;
                case 7:
                    return 4000;
                    break;
                case 8:
                    return 8000;
                    break;
                case 9:
                    return 2000;
                    break;
                case 10:
                    return 4000;
                    break;
                case 11:
                    return 8000;
                    break;
                case 12:
                    return 3000;
                    break;
                case 13:
                    return 6000;
                    break;
                case 14:
                    return 12000;
                    break;
                case 15:
                    return 3000;
                    break;
                case 16:
                    return 6000;
                    break;
                case 17:
                    return 12000;
                    break;
                default:
                    trigger_error("Technologie (".$branche.";".$idTechnologie.") introuvable dans les données", E_USER_ERROR);
            }
        } elseif ($branche == 5) {
            switch ($idTechnologie) {
                case 0:
                    return 1000;
                    break;
                case 1:
                    return 2000;
                    break;
                case 2:
                    return 4000;
                    break;
                case 3:
                    return 2000;
                    break;
                case 4:
                    return 4000;
                    break;
                case 5:
                    return 8000;
                    break;
                case 6:
                    return 3000;
                    break;
                case 7:
                    return 6000;
                    break;
                case 8:
                    return 12000;
                    break;
                case 9:
                    return 1000;
                    break;
                case 10:
                    return 2000;
                    break;
                case 11:
                    return 4000;
                    break;
                case 12:
                    return 2000;
                    break;
                case 13:
                    return 4000;
                    break;
                case 14:
                    return 8000;
                    break;
                case 15:
                    return 3000;
                    break;
                case 16:
                    return 6000;
                    break;
                case 17:
                    return 12000;
                    break;
                case 18:
                    return 1000;
                    break;
                case 19:
                    return 2000;
                    break;
                case 20:
                    return 4000;
                    break;
                case 21:
                    return 2000;
                    break;
                case 22:
                    return 4000;
                    break;
                case 23:
                    return 8000;
                    break;
                case 24:
                    return 3000;
                    break;
                case 25:
                    return 6000;
                    break;
                case 26:
                    return 12000;
                    break;
                default:
                    trigger_error("Technologie (".$branche.";".$idTechnologie.") introuvable dans les données", E_USER_ERROR);
            }
        } elseif ($branche == 6) {
            switch ($idTechnologie) {
                case 0:
                    return 2000;
                    break;
                case 1:
                    return 4000;
                    break;
                case 2:
                    return 6000;
                    break;
                case 3:
                    return 2000;
                    break;
                case 4:
                    return 4000;
                    break;
                case 5:
                    return 6000;
                    break;
                case 6:
                    return 8000;
                    break;
                default:
                    trigger_error("Technologie (".$branche.";".$idTechnologie.") introuvable dans les données", E_USER_ERROR);
            }
        } elseif ($branche == 7) {
            switch ($idTechnologie) {
                case 0:
                    return 2000;
                    break;
                case 1:
                    return 4000;
                    break;
                case 2:
                    return 4000;
                    break;
                case 3:
                    return 6000;
                    break;
                case 4:
                    return 6000;
                    break;
                case 5:
                    return 8000;
                    break;
                case 6:
                    return 10000;
                    break;
                case 7:
                    return 10000;
                    break;
                case 8:
                    return 12000;
                    break;
                case 9:
                    return 14000;
                    break;
                case 10:
                    return 14000;
                    break;
                case 11:
                    return 16000;
                    break;
                case 12:
                    return 18000;
                    break;
                case 13:
                    return 20000;
                    break;
                case 14:
                    return 22000;
                    break;
                default:
                    trigger_error("Technologie (".$branche.";".$idTechnologie.") introuvable dans les données", E_USER_ERROR);
            }
        } elseif ($branche == 8) {
            return (4 + 2 * $idTechnologie) * 1000;
        }
        return 0;
    }

    public static function temps($branche, $idTechnologie, surface $planete, $demolition = false)
    {
        if ($branche == 0) {
            switch ($idTechnologie) {
                case 0:
                    $temps = 54000;
                    break;
                case 1:
                    $temps = 90000;
                    break;
                case 2:
                    $temps = 126000;
                    break;
                case 3:
                    $temps = 126000;
                    break;
                case 4:
                    $temps = 72000;
                    break;
                case 5:
                    $temps = 108000;
                    break;
                case 6:
                    $temps = 144000;
                    break;
                case 7:
                    $temps = 72000;
                    break;
                case 8:
                    $temps = 108000;
                    break;
                case 9:
                    $temps = 144000;
                    break;
                case 10:
                    $temps = 72000;
                    break;
                case 11:
                    $temps = 108000;
                    break;
                case 12:
                    $temps = 144000;
                    break;
                case 13:
                    $temps = 126000;
                    break;
                case 14:
                    $temps = 162000;
                    break;
                case 15:
                    $temps = 198000;
                    break;
                case 16:
                    $temps = 126000;
                    break;
                case 17:
                    $temps = 162000;
                    break;
                case 18:
                    $temps = 198000;
                    break;
                default:
                    trigger_error("Technologie (".$branche.";".$idTechnologie.") introuvable dans les données", E_USER_ERROR);
            }
        } elseif ($branche == 1) {
            switch ($idTechnologie) {
                case 0:
                    $temps = 54000;
                    break;
                case 1:
                    $temps = 90000;
                    break;
                case 2:
                    $temps = 126000;
                    break;
                case 3:
                    $temps = 90000;
                    break;
                case 4:
                    $temps = 126000;
                    break;
                case 5:
                    $temps = 162000;
                    break;
                case 6:
                    $temps = 126000;
                    break;
                case 7:
                    $temps = 162000;
                    break;
                case 8:
                    $temps = 198000;
                    break;
                case 9:
                    $temps = 162000;
                    break;
                case 10:
                    $temps = 198000;
                    break;
                case 11:
                    $temps = 234000;
                    break;
                case 12:
                    $temps = 54000;
                    break;
                case 13:
                    $temps = 90000;
                    break;
                case 14:
                    $temps = 126000;
                    break;
                case 15:
                    $temps = 54000;
                    break;
                case 16:
                    $temps = 90000;
                    break;
                case 17:
                    $temps = 126000;
                    break;
                case 18:
                    $temps = 72000;
                    break;
                case 19:
                    $temps = 108000;
                    break;
                case 20:
                    $temps = 144000;
                    break;
                case 21:
                    $temps = 54000;
                    break;
                case 22:
                    $temps = 90000;
                    break;
                case 23:
                    $temps = 126000;
                    break;
                case 24:
                    $temps = 54000;
                    break;
                case 25:
                    $temps = 90000;
                    break;
                case 26:
                    $temps = 126000;
                    break;
                case 27:
                    $temps = 90000;
                    break;
                case 28:
                    $temps = 126000;
                    break;
                case 29:
                    $temps = 162000;
                    break;
                default:
                    trigger_error("Technologie (".$branche.";".$idTechnologie.") introuvable dans les données", E_USER_ERROR);
            }
        } elseif ($branche == 2) {
            switch ($idTechnologie) {
                case 0:
                    $temps = 36000;
                    break;
                case 1:
                    $temps = 72000;
                    break;
                case 2:
                    $temps = 108000;
                    break;
                case 3:
                    $temps = 126000;
                    break;
                case 4:
                    $temps = 126000;
                    break;
                case 5:
                    $temps = 126000;
                    break;
                case 6:
                    $temps = 162000;
                    break;
                case 7:
                    $temps = 198000;
                    break;
                default:
                    trigger_error("Technologie (".$branche.";".$idTechnologie.") introuvable dans les données", E_USER_ERROR);
            }
        } elseif ($branche == 3) {
            switch ($idTechnologie) {
                case 0:
                    $temps = 36000;
                    break;
                case 1:
                    $temps = 72000;
                    break;
                case 2:
                    $temps = 108000;
                    break;
                case 3:
                    $temps = 72000;
                    break;
                case 4:
                    $temps = 108000;
                    break;
                case 5:
                    $temps = 144000;
                    break;
                case 6:
                    $temps = 126000;
                    break;
                case 7:
                    $temps = 162000;
                    break;
                case 8:
                    $temps = 162000;
                    break;
                case 9:
                    $temps = 90000;
                    break;
                case 10:
                    $temps = 90000;
                    break;
                case 11:
                    $temps = 90000;
                    break;
                case 12:
                    $temps = 72000;
                    break;
                case 13:
                    $temps = 108000;
                    break;
                case 14:
                    $temps = 144000;
                    break;
                default:
                    trigger_error("Technologie (".$branche.";".$idTechnologie.") introuvable dans les données", E_USER_ERROR);
            }
        } elseif ($branche == 4) {
            switch ($idTechnologie) {
                case 0:
                    $temps = 36000;
                    break;
                case 1:
                    $temps = 72000;
                    break;
                case 2:
                    $temps = 108000;
                    break;
                case 3:
                    $temps = 54000;
                    break;
                case 4:
                    $temps = 90000;
                    break;
                case 5:
                    $temps = 126000;
                    break;
                case 6:
                    $temps = 72000;
                    break;
                case 7:
                    $temps = 108000;
                    break;
                case 8:
                    $temps = 144000;
                    break;
                case 9:
                    $temps = 90000;
                    break;
                case 10:
                    $temps = 126000;
                    break;
                case 11:
                    $temps = 162000;
                    break;
                case 12:
                    $temps = 108000;
                    break;
                case 13:
                    $temps = 144000;
                    break;
                case 14:
                    $temps = 180000;
                    break;
                case 15:
                    $temps = 126000;
                    break;
                case 16:
                    $temps = 162000;
                    break;
                case 17:
                    $temps = 198000;
                    break;
                default:
                    trigger_error("Technologie (".$branche.";".$idTechnologie.") introuvable dans les données", E_USER_ERROR);
            }
        } elseif ($branche == 5) {
            switch ($idTechnologie) {
                case 0:
                    $temps = 36000;
                    break;
                case 1:
                    $temps = 72000;
                    break;
                case 2:
                    $temps = 108000;
                    break;
                case 3:
                    $temps = 72000;
                    break;
                case 4:
                    $temps = 108000;
                    break;
                case 5:
                    $temps = 144000;
                    break;
                case 6:
                    $temps = 108000;
                    break;
                case 7:
                    $temps = 144000;
                    break;
                case 8:
                    $temps = 180000;
                    break;
                case 9:
                    $temps = 36000;
                    break;
                case 10:
                    $temps = 72000;
                    break;
                case 11:
                    $temps = 108000;
                    break;
                case 12:
                    $temps = 72000;
                    break;
                case 13:
                    $temps = 108000;
                    break;
                case 14:
                    $temps = 144000;
                    break;
                case 15:
                    $temps = 108000;
                    break;
                case 16:
                    $temps = 144000;
                    break;
                case 17:
                    $temps = 180000;
                    break;
                case 18:
                    $temps = 54000;
                    break;
                case 19:
                    $temps = 90000;
                    break;
                case 20:
                    $temps = 126000;
                    break;
                case 21:
                    $temps = 90000;
                    break;
                case 22:
                    $temps = 126000;
                    break;
                case 23:
                    $temps = 162000;
                    break;
                case 24:
                    $temps = 126000;
                    break;
                case 25:
                    $temps = 162000;
                    break;
                case 26:
                    $temps = 198000;
                    break;
                default:
                    trigger_error("Technologie (".$branche.";".$idTechnologie.") introuvable dans les données", E_USER_ERROR);
            }
        } elseif ($branche == 6) {
            switch ($idTechnologie) {
                case 0:
                    $temps = 72000;
                    break;
                case 1:
                    $temps = 144000;
                    break;
                case 2:
                    $temps = 216000;
                    break;
                case 3:
                    $temps = 72000;
                    break;
                case 4:
                    $temps = 144000;
                    break;
                case 5:
                    $temps = 216000;
                    break;
                case 6:
                    $temps = 288000;
                    break;
                default:
                    trigger_error("Technologie (".$branche.";".$idTechnologie.") introuvable dans les données", E_USER_ERROR);
            }
        } elseif ($branche == 7) {
            switch ($idTechnologie) {
                case 0:
                    $temps = 36000;
                    break;
                case 1:
                    $temps = 72000;
                    break;
                case 2:
                    $temps = 108000;
                    break;
                case 3:
                    $temps = 144000;
                    break;
                case 4:
                    $temps = 144000;
                    break;
                case 5:
                    $temps = 180000;
                    break;
                case 6:
                    $temps = 216000;
                    break;
                case 7:
                    $temps = 234000;
                    break;
                case 8:
                    $temps = 288000;
                    break;
                case 9:
                    $temps = 324000;
                    break;
                case 10:
                    $temps = 342000;
                    break;
                case 11:
                    $temps = 360000;
                    break;
                case 12:
                    $temps = 396000;
                    break;
                case 13:
                    $temps = 432000;
                    break;
                case 14:
                    $temps = 504000;
                    break;
                default:
                    trigger_error("Technologie (".$branche.";".$idTechnologie.") introuvable dans les données", E_USER_ERROR);
            }
        } elseif ($branche == 8) {
            $temps = (54 + 18 * $idTechnologie) * 1000;
        }

        return $temps/VITESSE;
    }

    public static function type($branche, $race)
    {
        switch ($branche) {
            case 0:
                return array(
                    0, array(3, array(13), 4, 7, array(16), 10)
                );
            case 1:
                return array(
                    array(0, array(3, array(6, array(9))), 12, array(15, 18, array(21), 24, 27))
                );
            case 2:
                return array(
                    array(0, array(3, 4), 5)
                );
            case 3:
                return array(
                    array(0, array(3, array(6, 7, 8)), 9, array(12))
                );
            case 4:
                return array(
                    array(0, array(3, array(6, array(9)), 12, array(15)))
                );
            case 5:
                return array(
                    array(0, array(3, array(6)), 9, array(12, array(15), 18, array(21, array(24))))
                );
            case 6:
                return array(
                    array(0, array(1, array(2)), 3, array(4, array(5, array(6))))
                );
            case 7:
                return array(
                    array(0, array(1, array(2, array(3, array(4, array(5, array(6, array(7, array(8, array(9), 11)), 10)))))), 12)
                );
            case 8:
                return array(
                    array(0)
                );
            default:
                trigger_error("Branche ".$branche." introuvable dans les données", E_USER_ERROR);
        }
    }

    public static function niveau($branche, $id)
    {
        switch ($branche) {
            case 0:
                $tableau = array( 0 => 1, 1 => 2, 2 => 3, 3 => 0, 4 => 1, 5 => 2, 6 => 3, 7 => 1, 8 => 2, 9 => 3, 10 => 1, 11 => 2, 12 => 3, 13 => 1, 14 => 2, 15 => 3, 16 => 1, 17 => 2, 18 => 3);
                break;
            case 1:
                $tableau = array( 0 => 1, 1 => 2, 2 => 3, 3 => 1, 4 => 2, 5 => 3, 6 => 1, 7 => 2, 8 => 3, 9 => 1, 10 => 2, 11 => 3, 12 => 1, 13 => 2, 14 => 3, 15 => 1, 16 => 2, 17 => 3, 18 => 1, 19 => 2, 20 => 3, 21 => 1, 22 => 2, 23 => 3, 24 => 1, 25 => 2, 26 => 3, 27 => 1, 28 => 2, 29 => 3);
                break;
            case 2:
                $tableau = array( 0 => 1, 1 => 2, 2 => 3, 3 => 0, 4 => 0, 5 => 1, 6 => 2, 7 => 3);
                break;
            case 3:
                $tableau = array( 0 => 1, 1 => 2, 2 => 3, 3 => 1, 4 => 2, 5 => 3, 6 => 0, 7 => 0, 8 => 0, 9 => 1, 10 => 2, 11 => 3, 12 => 1, 13 => 2, 14 => 3);
                break;
            case 4:
                $tableau = array( 0 => 1, 1 => 2, 2 => 3, 3 => 1, 4 => 2, 5 => 3, 6 => 1, 7 => 2, 8 => 3, 9 => 1, 10 => 2, 11 => 3, 12 => 1, 13 => 2, 14 => 3, 15 => 1, 16 => 2, 17 => 3);
                break;
            case 5:
                $tableau = array( 0 => 1, 1 => 2, 2 => 3, 3 => 1, 4 => 2, 5 => 3, 6 => 1, 7 => 2, 8 => 3, 9 => 1, 10 => 2, 11 => 3, 12 => 1, 13 => 2, 14 => 3, 15 => 1, 16 => 2, 17 => 3, 18 => 1, 19 => 2, 20 => 3, 21 => 1, 22 => 2, 23 => 3, 24 => 1, 25 => 2, 26 => 3);
                break;
            case 6:
                $tableau = array( 0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0);
                break;
            case 7:
                $tableau = array( 0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 1, 13 => 2, 14 => 3);
                break;
            case 8:
                $tableau = array( 0 => 1, 1 => 2, 2 => 3, 3 => 4, 4 => 5, 5 => 6, 6 => 7, 7 => 8, 8 => 9, 9 => 10, 10 => 11, 11 => 12, 12 => 13, 13 => 14, 14 => 15, 15 => 16, 16 => 17, 17 => 18);
                break;
            default:
                trigger_error("Branche ".$branche." introuvable dans les données", E_USER_ERROR);
        }

        return $tableau[$id];
    }


    public static function image($object, surface $planete)
    {
        $branche = $object[0];
        $idTechnologie = $object[1];

        if ($planete->race == "covenant") {
            return Donnees::image_covenant_default;
        } elseif ($planete->race == "humain") {
            return Donnees::image_humain_default;
        } else {
            trigger_error("Impossible de trouver la race pour ".$planete->race, E_USER_ERROR);
        }
    }


    public static function needed($object, surface $planete, $print = false, $race = null)
    {
        if ($race == null) {
            $race = $planete->race;
        }
        switch ($object[0]) {
            case 0:
                $tableau = array(
                    array(
                        array('batiments', 6, 1)
                    ),
                    array(
                        array("technologies", 0, 0)
                    ),
                    array(
                        array("technologies", 0, 1)
                    ),
                    array(
                        array("technologies", 0, 0),
                        array('batiments', 6, 2)
                    ),
                    array(
                        array("technologies", 0, 0),
                        array('batiments', 6, 2)
                    ),
                    array(
                        array("technologies", 0, 4)
                    ),
                    array(
                        array("technologies", 0, 5)
                    ),
                    array(
                        array("technologies", 0, 0),
                        array('batiments', 6, 2)
                    ),
                    array(
                        array("technologies", 0, 7)
                    ),
                    array(
                        array("technologies", 0, 8)
                    ),
                    array(
                        array("technologies", 0, 0),
                        array('batiments', 6, 2)
                    ),
                    array(
                        array("technologies", 0, 10)
                    ),
                    array(
                        array("technologies", 0, 11)
                    ),
                    array(
                        array("technologies", 0, 3),
                        array('batiments', 6, 5)
                    ),
                    array(
                        array("technologies", 0, 13)
                    ),
                    array(
                        array("technologies", 0, 14)
                    ),
                    array(
                        array("technologies", 0, 4),
                        array("technologies", 0, 7),
                        array("technologies", 0, 10),
                        array('batiments', 6, 10)
                    ),
                    array(
                        array("technologies", 0, 16)
                    ),
                    array(
                        array("technologies", 0, 17)
                    )
                );
                break;
            case 1:
                $tableau = array(
                    array(
                        array('batiments', 6, 1)
                    ),
                    array(
                        array("technologies", 1, 0)
                    ),
                    array(
                        array("technologies", 1, 1)
                    ),
                    array(
                        array("technologies", 1, 0),
                        array('batiments', 6, 2)
                    ),
                    array(
                        array("technologies", 1, 3)
                    ),
                    array(
                        array("technologies", 1, 4)
                    ),
                    array(
                        array("technologies", 1, 3),
                        array('batiments', 6, 5)
                    ),
                    array(
                        array("technologies", 1, 6)
                    ),
                    array(
                        array("technologies", 1, 7)
                    ),
                    array(
                        array("technologies", 1, 6),
                        array('batiments', 6, 10)
                    ),
                    array(
                        array("technologies", 1, 9)
                    ),
                    array(
                        array("technologies", 1, 10)
                    ),
                    array(
                        array('batiments', 6, 1)
                    ),
                    array(
                        array("technologies", 1, 12)
                    ),
                    array(
                        array("technologies", 1, 13)
                    ),
                    array(
                        array("technologies", 1, 12),
                        array('batiments', 6, 2)
                    ),
                    array(
                        array("technologies", 1, 15)
                    ),
                    array(
                        array("technologies", 1, 16)
                    ),
                    array(
                        array("technologies", 1, 12),
                        array('batiments', 6, 5)
                    ),
                    array(
                        array("technologies", 1, 18)
                    ),
                    array(
                        array("technologies", 1, 19)
                    ),
                    array(
                        array("technologies", 1, 18),
                        array('batiments', 6, 5)
                    ),
                    array(
                        array("technologies", 1, 21)
                    ),
                    array(
                        array("technologies", 1, 22)
                    ),
                    array(
                        array("technologies", 1, 12),
                        array('batiments', 6, 2)
                    ),
                    array(
                        array("technologies", 1, 24)
                    ),
                    array(
                        array("technologies", 1, 25)
                    ),
                    array(
                        array("technologies", 1, 12),
                        array('batiments', 6, 2)
                    ),
                    array(
                        array("technologies", 1, 27)
                    ),
                    array(
                        array("technologies", 1, 28)
                    )
                );
                break;
            case 2:
                $tableau = array(
                    array(
                        array('batiments', 6, 1)
                    ),
                    array(
                        array("technologies", 2, 0)
                    ),
                    array(
                        array("technologies", 2, 1)
                    ),
                    array(
                        array("technologies", 2, 0),
                        array('batiments', 6, 2)
                    ),
                    array(
                        array("technologies", 2, 0),
                        array('batiments', 6, 2)
                    ),
                    array(
                        array('batiments', 6, 20)
                    ),
                    array(
                        array("technologies", 2,5)
                    ),
                    array(
                        array("technologies", 2, 6)
                    )
                  );
                break;
            case 3:
                $tableau = array(
                    array(
                        array('batiments', 6, 1)
                    ),
                    array(
                        array("technologies", 3, 0)
                    ),
                    array(
                        array("technologies", 3, 1)
                    ),
                    array(
                        array("technologies", 3, 0),
                        array('batiments', 6, 2)
                    ),
                    array(
                        array("technologies", 3, 3)
                    ),
                    array(
                        array("technologies", 3, 4)
                    ),
                    array(
                        array("technologies", 3, 3),
                        array('batiments', 6, 5)
                    ),
                    array(
                        array("technologies", 3, 3),
                        array('batiments', 6, 5)
                    ),
                    array(
                        array("technologies", 3, 3),
                        array('batiments', 6, 5)
                    ),
                    array(
                        array('batiments', 6, 1)
                    ),
                    array(
                        array("technologies", 3, 9)
                    ),
                    array(
                        array("technologies", 3, 10)
                    ),
                    array(
                        array('batiments', 6, 1)
                    ),
                    array(
                        array("technologies", 3, 12)
                    ),
                    array(
                        array("technologies", 3, 13)
                    )
                );
                break;
            case 4:
                $tableau = array(
                    array(
                        array('batiments', 6, 2)
                    ),
                    array(
                        array("technologies", 4, 0)
                    ),
                    array(
                        array("technologies", 4, 1)
                    ),
                    array(
                        array('batiments', 6, 2)
                    ),
                    array(
                        array("technologies", 4, 3)
                    ),
                    array(
                        array("technologies", 4, 4)
                    ),
                    array(
                        array("technologies", 4, 3),
                        array('batiments', 6, 5)
                    ),
                    array(
                        array("technologies", 4, 6)
                    ),
                    array(
                        array("technologies", 4, 7)
                    ),
                    array(
                        array("technologies", 4, 6),
                        array('batiments', 6, 10)
                    ),
                    array(
                        array("technologies", 4, 9)
                    ),
                    array(
                        array("technologies", 4, 10)
                    ),
                    array(
                        array("technologies", 4, 0),
                        array('batiments', 6, 5)
                    ),
                    array(
                        array("technologies", 4, 12)
                    ),
                    array(
                        array("technologies", 4, 13)
                    ),
                    array(
                        array("technologies", 4, 12),
                        array('batiments', 6, 10)
                    ),
                    array(
                        array("technologies", 4, 15)
                    ),
                    array(
                        array("technologies", 4, 16)
                    ),
                  );
                break;
            case 5:
                $tableau = array(
                    array(
                        array('batiments', 6, 1)
                    ),
                    array(
                        array("technologies", 5, 0)
                    ),
                    array(
                        array("technologies", 5, 1)
                    ),
                    array(
                        array("technologies", 5, 0),
                        array('batiments', 6, 5)
                    ),
                    array(
                        array("technologies", 5, 3)
                    ),
                    array(
                        array("technologies", 5, 4)
                    ),
                    array(
                        array("technologies", 5, 3),
                        array('batiments', 6, 10)
                    ),
                    array(
                        array("technologies", 5, 6)
                    ),
                    array(
                        array("technologies", 5, 7)
                    ),
                    array(
                        array('batiments', 6, 1)
                    ),
                    array(
                        array("technologies", 5, 9)
                    ),
                    array(
                        array("technologies", 5, 10)
                    ),
                    array(
                        array("technologies", 5, 9),
                        array('batiments', 6, 5)
                    ),
                    array(
                        array("technologies", 5, 12)
                    ),
                    array(
                        array("technologies", 5, 13)
                    ),
                    array(
                        array("technologies", 5, 12),
                        array('batiments', 6, 10)
                    ),
                    array(
                        array("technologies", 5, 15)
                    ),
                    array(
                        array("technologies", 5, 16)
                    ),
                    array(
                        array('batiments', 6, 1)
                    ),
                    array(
                        array("technologies", 5, 18)
                    ),
                    array(
                        array("technologies", 5, 19)
                    ),
                    array(
                        array("technologies", 5, 18),
                        array('batiments', 6, 5)
                    ),
                    array(
                        array("technologies", 5, 21)
                    ),
                    array(
                        array("technologies", 5, 22)
                    ),
                    array(
                        array("technologies", 5, 21),
                        array('batiments', 6, 10)
                    ),
                    array(
                        array("technologies", 5, 24)
                    ),
                    array(
                        array("technologies", 5, 25)
                    ),
                  );
                break;
            case 6:
                $tableau = array(
                    array(
                        array('batiments', 6, 1)
                    ),
                    array(
                        array("technologies", 6, 0),
                        array('batiments', 6, 5)
                    ),
                    array(
                        array("technologies", 6, 1),
                        array('batiments', 6, 10)
                    ),
                    array(
                        array('batiments', 6, 1)
                    ),
                    array(
                        array("technologies", 6, 3),
                        array('batiments', 6, 5)
                    ),
                    array(
                        array("technologies", 6, 4),
                        array('batiments', 6, 10)
                    ),
                    array(
                        array("technologies", 6, 2),
                        array("technologies", 6, 5),
                        array('batiments', 6, 20)
                    )
                );
                break;
            case 7:
                if ($race == "humain") {
                    $tableau = array(
                        array(
                            array('batiments', 6, 1)
                        ),
                        array(
                            array("technologies", 7, 0),
                            array('batiments', 6, 2)
                        ),
                        array(
                            array("technologies", 7, 1),
                            array('batiments', 6, 3)
                        ),
                        array(
                            array("technologies", 7, 2),
                            array('batiments', 6, 5)
                        ),
                        array(
                            array("technologies", 7, 1),
                            array('batiments', 6, 3)
                        ),
                        array(
                            array("technologies", 7, 3),
                            array('batiments', 6, 8)
                        ),
                        array(
                            array("technologies", 7, 5),
                            array('batiments', 6, 10)
                        ),
                        array(
                            array("technologies", 7, 6),
                            array('batiments', 6, 15)
                        ),
                        array(
                            array("technologies", 7, 7),
                            array('batiments', 6, 20)
                        ),
                        array(
                            array("technologies", 7, 8),
                            array('batiments', 6, 30)
                        ),
                        array(
                            array("technologies", 7, 5),
                            array('batiments', 6, 10)
                        ),
                        array(
                            array("technologies", 7, 4),
                            array('batiments', 6, 3)
                        ),
                        array(
                            array('batiments', 6, 5)
                        ),
                        array(
                            array("technologies", 7, 12)
                        ),
                        array(
                            array("technologies", 7, 13)
                        ),
                    );
                } else {
                    $tableau = array(
                        array(
                            array('batiments', 6, 1)
                        ),
                        array(
                            array("technologies", 7, 0),
                            array('batiments', 6, 2)
                        ),
                        array(
                            array("technologies", 7, 1),
                            array('batiments', 6, 3)
                        ),
                        array(
                            array("technologies", 7, 2),
                            array('batiments', 6, 5)
                        ),
                        array(
                            array("technologies", 7, 1),
                            array('batiments', 6, 3)
                        ),
                        array(
                            array("technologies", 7, 3),
                            array('batiments', 6, 8)
                        ),
                        array(
                            array("technologies", 7, 5),
                            array('batiments', 6, 10)
                        ),
                        array(
                            array("technologies", 7, 6),
                            array('batiments', 6, 15)
                        ),
                        array(
                            array("technologies", 7, 7),
                            array('batiments', 6, 20)
                        ),
                        array(
                            array("technologies", 7, 8),
                            array('batiments', 6, 30)
                        ),
                        array(
                            array("technologies", 7, 5),
                            array('batiments', 6, 10)
                        ),
                        array(
                            array("technologies", 7, 5),
                            array('batiments', 6, 10)
                        ),
                        array(
                            array('batiments', 6, 5)
                        ),
                        array(
                            array("technologies", 7, 12)
                        ),
                        array(
                            array("technologies", 7, 13)
                        ),
                    );
                }
                break;
            case 8:
                $tableau = array(
                    array(
                        array("technologies", 1, 0),
                        array('batiments', 6, 2)
                    ),
                    array(
                        array("technologies", 8, 0)
                    ),
                    array(
                        array("technologies", 8, 1)
                    ),
                    array(
                        array("technologies", 8, 2)
                    ),
                    array(
                        array("technologies", 8, 3)
                    ),
                    array(
                        array("technologies", 8, 4)
                    ),
                    array(
                        array("technologies", 8, 5)
                    ),
                    array(
                        array("technologies", 8, 6)
                    ),
                    array(
                        array("technologies", 8, 7)
                    ),
                    array(
                        array("technologies", 8, 8)
                    ),
                    array(
                        array("technologies", 8, 9)
                    ),
                    array(
                        array("technologies", 8, 10)
                    ),
                    array(
                        array("technologies", 8, 11)
                    ),
                    array(
                        array("technologies", 8, 12)
                    ),
                    array(
                        array("technologies", 8, 13)
                    ),
                    array(
                        array("technologies", 8, 14)
                    ),
                    array(
                        array("technologies", 8, 15)
                    ),
                    array(
                        array("technologies", 8, 16)
                    ),
                    array(
                        array("technologies", 8, 17)
                    ),
                );
                break;
            default:
                trigger_error("Branche ".$object[0]." introuvable dans les données", E_USER_ERROR);
        }

        if ($print) {
            return dDonnees::print_neededCheck($tableau[$object[1]], $planete, $race);
        } else {
            return dDonnees::neededCheck($tableau[$object[1]], $planete);
        }
    }

    public static function niveau_du_joueur($branche, $id, surface $planete)
    {
        $niveau = 0;
        // Si jamais cette techno a été recherchée au niveau 2 ou 3, aditionne les niveaux des trois technos
        if (($niv = self::niveau($branche, $id)) > 0 && (self::idToBit($id+1) & $planete->technologies[$branche])) {
            if ((self::idToBit($id+2) & $planete->technologies[$branche])) {
                $niveau += $niv;
            }
            $niveau += $niv;
        }
        if (self::idToBit($id) & $planete->technologies[$branche]) {
            // Gère les technos qui n'ont qu'un seul niveau
            $ret = self::niveau($branche, $id);
            if ($ret == 0) {
                $niv = 1;
            } else {
                $niv = $ret;
            }
            $niveau += $niv;
        }
        return $niveau;
    }


    public static function niveau_max($branche, $id, surface $planete, $LANG)
    {
        $niveau_max = 1;
        if (isset($LANG[$planete->race]["technologies"]["noms_sing"][$branche][$id+1]) && $LANG[$planete->race]["technologies"]["noms_sing"][$branche][$id] == $LANG[$planete->race]["technologies"]["noms_sing"][$branche][$id+1]) {
            if (isset($LANG[$planete->race]["technologies"]["noms_sing"][$branche][$id+2]) && $LANG[$planete->race]["technologies"]["noms_sing"][$branche][$id+1] == $LANG[$planete->race]["technologies"]["noms_sing"][$branche][$id+2]) {
                $niveau_max = 3;
            } else {
                $niveau_max = 2;
            }
        }
        return $niveau_max;
    }

    /* Fonction qui transpforme l'id d'une technologie en son équivalent en bit dans la liste */
    public static function idToBit($id)
    {
        return pow(2, $id);
    }
    public static function bitToId($bit)
    {
        return log($bit, 2);
    }
}
