<?php
require_once("Class/Donnees/interface.php");

class dAlliancesBatiments implements Donnees
{
    public static function metal($id, $niveau, surface $planete)
    {
        switch ($id) {
            case 0:
                $metal = ceil(pow(1.5, $niveau)*68);
                break;
            case 1:
                $metal = ceil(pow(1.6, $niveau)*53);
                break;
            case 2:
                $metal = ceil(pow(1.5, $niveau)*242);
                break;
            case 3:
                $metal = ceil(pow(1.5, $niveau)*92);
                break;
            case 4:
                $metal = ceil(pow(1.73, $niveau)*800);
                break;
            case 5:
                $metal = ceil(pow(2, $niveau)*750);
                break;
            default:
                trigger_error("Batiment d'alliance ".$id." introuvable dans les données", E_USER_ERROR);
        }

        return $metal;
    }

    public static function cristal($id, $niveau, surface $planete)
    {
        switch ($id) {
            case 0:
                $cristal = ceil(pow(1.5, $niveau)*17);
                break;
            case 1:
                $cristal = ceil(pow(1.6, $niveau)*27);
                break;
            case 2:
                $cristal = ceil(pow(1.5, $niveau)*72);
                break;
            case 3:
                $cristal = ceil(pow(1.5, $niveau)*37);
                break;
            case 4:
                $cristal = ceil(pow(1.73, $niveau)*420);
                break;
            case 5:
                $cristal = ceil(pow(2, $niveau)*500);
                break;
            default:
                trigger_error("Batiment d'alliance ".$id." introuvable dans les données", E_USER_ERROR);
        }

        return $cristal;
    }

    public static function hydrogene($id, $niveau, surface $planete)
    {
        switch ($id) {
            case 0:
                $hydrogene = 0;
                break;
            case 1:
                $hydrogene = 0;
                break;
            case 2:
                $hydrogene = 0;
                break;
            case 3:
                $hydrogene = 0;
                break;
            case 4:
                $hydrogene = ceil(pow(1.68, $niveau)*285);
                break;
            case 5:
                $hydrogene = 0;
                break;
            default:
                trigger_error("Batiment d'alliance ".$id." introuvable dans les données", E_USER_ERROR);
        }

        return $hydrogene;
    }

    public static function credits($id, $niveau, surface $planete)
    {
        switch ($id) {
            case 0:
                $credits = ceil(pow(1.68, $niveau)*25);
                break;
            case 1:
                $credits = ceil(pow(1.68, $niveau)*85);
                break;
            case 2:
                $credits = ceil(pow(1.68, $niveau)*185);
                break;
            case 3:
                $credits = ceil(pow(1.68, $niveau)*285);
                break;
            case 4:
                $credits = ceil(pow(1.68, $niveau)*385);
                break;
            case 5:
                $credits = ceil(pow(1.68, $niveau)*485);
                break;
            default:
                trigger_error("Batiment d'alliance ".$id." introuvable dans les données", E_USER_ERROR);
        }

        return $credits;
    }

    public static function temps($id, $niveau, surface $planete)
    {
        switch ($id) {
            case 0:
                $sec = pow(1.5, $niveau)*6;
                break;
            case 1:
                $sec = pow(1.55, $niveau)*6;
                break;
            case 2:
                $sec = pow(1.624, $niveau)*6;
                break;
            case 3:
                $sec = pow(1.597, $niveau)*6;
                break;
            case 4:
                $sec = pow(1.7, $niveau)*6;
                break;
            case 5:
                $sec = ceil(pow(2, $niveau)*720);
                break;
            default:
                trigger_error("Batiment d'alliance ".$id." introuvable dans les données", E_USER_ERROR);
        }

        //Accélération du temps de construction
        $sec /= VITESSE;

        return $sec;
    }


    public static function image($id, surface $planete)
    {
        if ($planete->race == "covenant") {
            switch ($id) {
                case 0:
                    //return "batimentcovieux4.jpg";
                    break;
            }
            return Donnees::image_covenant_default;
        } elseif ($planete->race == "humain") {
            switch ($id) {
                case 0:
                    //return "batimentcovieux4.jpg";
                    break;
            }
            return Donnees::image_humain_default;
        } else {
            trigger_error("Impossible de trouver la race pour ".$planete->race, E_USER_ERROR);
        }
    }


    public static function needed($id, surface $planete, $print = false)
    {
        $neededAlliancesBatiments =
            array(
                0,
                array(
                    array('batiments', 0, 1)
                ),
                array(
                    array('batiments', 0, 3)
                ),
                array(
                    array('batiments', 0, 5)
                ),
                array(
                    array('batiments', 0, 4)
                ),
                array(
                    array('batiments', 0, 2)
                )
            );

        if ($print) {
            return dDonnees::print_neededCheck($neededAlliancesBatiments[$id], $planete);
        } else {
            return dDonnees::neededCheck($neededAlliancesBatiments[$id], $planete);
        }
    }
}
