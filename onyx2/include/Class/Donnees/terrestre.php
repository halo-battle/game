<?php
require_once("Class/Donnees/interface.php");

class dTerrestre implements Donnees
{
    public static function metal($id, $nombre, surface $planete)
    {
        if ($planete->race == "humain") {
            switch ($id) {
                case 0:
                    $metal = 300;
                    break;
                case 1:
                    $metal = 420;
                    break;
                case 2:
                    $metal = 600;
                    break;
                case 3:
                    $metal = 950;
                    break;
                case 4:
                    $metal = 240;
                    break;
                case 5:
                    $metal = 260;
                    break;
                case 6:
                    $metal = 420;
                    break;
                case 7:
                    $metal = 500;
                    break;
                case 8:
                    $metal = 230;
                    break;
                case 9:
                    $metal = 650;
                    break;
                case 10:
                    $metal = 1750;
                    break;
                case 11:
                    $metal = 3750;
                    break;
                case 12:
                    $metal = 9500;
                    break;
                case 13:
                    $metal = 9500;
                    break;
                case 14:
                    $metal = 9500;
                    break;
                case 15:
                    $metal = 9500;
                    break;
                default:
                    trigger_error("Unité ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            // Covenant
            switch ($id) {
                case 0:
                    $metal = 300;
                    break;
                case 1:
                    $metal = 420;
                    break;
                case 2:
                    $metal = 600;
                    break;
                case 3:
                    $metal = 950;
                    break;
                case 4:
                    $metal = 240;
                    break;
                case 5:
                    $metal = 260;
                    break;
                case 6:
                    $metal = 420;
                    break;
                case 7:
                    $metal = 500;
                    break;
                case 8:
                    $metal = 100;
                    break;
                case 9:
                    $metal = 300;
                    break;
                case 10:
                    $metal = 4000;
                    break;
                case 11:
                    $metal = 7000;
                    break;
                case 12:
                    $metal = 14000;
                    break;
                default:
                    trigger_error("Unité ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }

        //On tient compte des bonus
        if (isset($planete->politique) && $planete->politique == 1) {
            $metal *= 0.9;
        }

        return $metal * $nombre;
    }

    public static function cristal($id, $nombre, surface $planete)
    {
        if ($planete->race == "humain") {
            switch ($id) {
                case 0:
                    $cristal = 300;
                    break;
                case 1:
                    $cristal = 420;
                    break;
                case 2:
                    $cristal = 600;
                    break;
                case 3:
                    $cristal = 950;
                    break;
                case 4:
                    $cristal = 240;
                    break;
                case 5:
                    $cristal = 260;
                    break;
                case 6:
                    $cristal = 420;
                    break;
                case 7:
                    $cristal = 500;
                    break;
                case 8:
                    $cristal = 230;
                    break;
                case 9:
                    $cristal = 650;
                    break;
                case 10:
                    $cristal = 1750;
                    break;
                case 11:
                    $cristal = 3750;
                    break;
                case 12:
                    $cristal = 9500;
                    break;
                case 13:
                    $cristal = 9500;
                    break;
                case 14:
                    $cristal = 9500;
                    break;
                case 15:
                    $cristal = 9500;
                    break;
                default:
                    trigger_error("Unité ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            // Covenant
            switch ($id) {
                case 0:
                    $cristal = 300;
                    break;
                case 1:
                    $cristal = 420;
                    break;
                case 2:
                    $cristal = 600;
                    break;
                case 3:
                    $cristal = 950;
                    break;
                case 4:
                    $cristal = 240;
                    break;
                case 5:
                    $cristal = 260;
                    break;
                case 6:
                    $cristal = 420;
                    break;
                case 7:
                    $cristal = 500;
                    break;
                case 8:
                    $cristal = 80;
                    break;
                case 9:
                    $cristal = 240;
                    break;
                case 10:
                    $cristal = 5200;
                    break;
                case 11:
                    $cristal = 10000;
                    break;
                case 12:
                    $cristal = 17000;
                    break;
                default:
                    trigger_error("Unité ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }
        //On tient compte des bonus
        if (isset($planete->politique) && $planete->politique == 1) {
            $cristal *= 0.9;
        }

        return $cristal * $nombre;
    }

    public static function hydrogene($id, $nombre, surface $planete)
    {
        if ($planete->race == "humain") {
            switch ($id) {
                case 0:
                    $hydrogene = 0;
                    break;
                case 1:
                    $hydrogene = 0;
                    break;
                case 2:
                    $hydrogene = 0;
                    break;
                case 3:
                    $hydrogene = 0;
                    break;
                case 4:
                    $hydrogene = 0;
                    break;
                case 5:
                    $hydrogene = 0;
                    break;
                case 6:
                    $hydrogene = 0;
                    break;
                case 7:
                    $hydrogene = 0;
                    break;
                case 8:
                    $hydrogene = 0;
                    break;
                case 9:
                    $hydrogene = 80;
                    break;
                case 10:
                    $hydrogene = 100;
                    break;
                case 11:
                    $hydrogene = 120;
                    break;
                case 12:
                    $hydrogene = 1500;
                    break;
                case 13:
                    $hydrogene = 1500;
                    break;
                case 14:
                    $hydrogene = 1500;
                    break;
                case 15:
                    $hydrogene = 1500;
                    break;
                default:
                    trigger_error("Unité ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            // Covenant
            switch ($id) {
                case 0:
                    $hydrogene = 0;
                    break;
                case 1:
                    $hydrogene = 0;
                    break;
                case 2:
                    $hydrogene = 0;
                    break;
                case 3:
                    $hydrogene = 0;
                    break;
                case 4:
                    $hydrogene = 0;
                    break;
                case 5:
                    $hydrogene = 0;
                    break;
                case 6:
                    $hydrogene = 0;
                    break;
                case 7:
                    $hydrogene = 0;
                    break;
                case 8:
                    $hydrogene = 10;
                    break;
                case 9:
                    $hydrogene = 30;
                    break;
                case 10:
                    $hydrogene = 600;
                    break;
                case 11:
                    $hydrogene = 900;
                    break;
                case 12:
                    $hydrogene = 1400;
                    break;
                  default:
                      trigger_error("Unité ".$id." introuvable dans les données", E_USER_ERROR);
              }
        }

        //On tient compte des bonus
        if (isset($planete->politique) && $planete->politique == 1) {
            $hydrogene *= 0.9;
        }

        return $hydrogene * $nombre;
    }

    public static function credits($id, $nombre, surface $planete)
    {
        return 0;
    }

    public static function temps($id, $nombre, surface $planete)
    {
        if ($planete->race == "humain") {
            switch ($id) {
                case 0:
                    $temps = 720;
                    $moins = 1;
                    break;
                case 1:
                    $temps = 2040;
                    $moins = 3;
                    break;
                case 2:
                    $temps = 7200;
                    $moins = 4;
                    break;
                case 3:
                    $temps = 3960;
                    $moins = 6;
                    break;
                case 4:
                    $temps = 600;
                    $moins = 1;
                    break;
                case 5:
                    $temps = 1080;
                    $moins = 2;
                    break;
                case 6:
                    $temps = 2160;
                    $moins = 3;
                    break;
                case 7:
                    $temps = 4680;
                    $moins = 5;
                    break;
                case 8:
                    $temps = 1080;
                    $moins = 1;
                    break;
                case 9:
                    $temps = 2040;
                    $moins = 3;
                    break;
                case 10:
                    $temps = 7200;
                    $moins = 4;
                    break;
                case 11:
                    $temps = 3960;
                    $moins = 7;
                    break;
                case 12:
                    $temps = 3960;
                    $moins = 9;
                    break;
                case 13:
                    $temps = 3960;
                    $moins = 9;
                    break;
                case 14:
                    $temps = 3960;
                    $moins = 9;
                    break;
                case 15:
                    $temps = 3960;
                    $moins = 9;
                    break;
                default:
                    trigger_error("Unité ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            // Covenant
            switch ($id) {
                case 0:
                    $temps = 720;
                    $moins = 1;
                    break;
                case 1:
                    $temps = 2040;
                    $moins = 3;
                    break;
                case 2:
                    $temps = 7200;
                    $moins = 4;
                    break;
                case 3:
                    $temps = 3960;
                    $moins = 6;
                    break;
                case 4:
                    $temps = 600;
                    $moins = 1;
                    break;
                case 5:
                    $temps = 1080;
                    $moins = 2;
                    break;
                case 6:
                    $temps = 2160;
                    $moins = 3;
                    break;
                case 7:
                    $temps = 4680;
                    $moins = 5;
                    break;
                case 8:
                    $temps = 83;
                    $moins = 1;
                    break;
                case 9:
                    $temps = 250;
                    $moins = 3;
                    break;
                case 10:
                    $temps = 6800;
                    $moins = 4;
                    break;
                case 11:
                    $temps = 12350;
                    $moins = 7;
                    break;
                case 12:
                    $temps = 29333;
                    $moins = 9;
                    break;
                default:
                    trigger_error("Unité ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }
        //On tient compte de la vitesse
        $temps /= VITESSE;

        //On tient compte des bonus
        //Bonus technologique de vitesse de construction : Constructions Planétaires
        if ($planete->technologies[2]& dTechnologies::idToBit(2)) {
            $temps /= 1.15;
        } elseif ($planete->technologies[2]& dTechnologies::idToBit(1)) {
            $temps /= 1.10;
        } elseif ($planete->technologies[2]& dTechnologies::idToBit(0)) {
            $temps /= 1.05;
        }

        return ceil($temps/pow(1.25, ($planete->batiments[7] - $moins))) * $nombre;
    }

    public static function type($id, surface $planete)
    {
        if ($planete->race == "humain") {
            switch ($id) {
              case 0:
              case 1:
              case 2:
              case 3:
              case 4:
              case 5:
              case 6:
              case 7:
                  return true;
              case 8:
              case 9:
              case 10:
              case 11:
              case 12:
              case 13:
              case 14:
              case 15:
                  return false;
              default:
                  trigger_error("Unité ".$id." introuvable dans les données", E_USER_ERROR);
          }
        } else {
            // Covenant
            switch ($id) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    return true;
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                    return false;
                default:
                    trigger_error("Unité ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }
    }


    public static function image($id, surface $planete)
    {
        if ($planete->race == "humain") {
            switch ($id) {
                //Unités terrestres
                case 0:
                    return "csnusparrowhawkkp4.jpg";
                     break;
                 case 1:
                     return "pelican.jpg";
                     break;
                 case 2:
                     return "csnushortswordad3.jpg";
                     break;
                 case 3:
                     return "albatross.jpg";
                     break;
                 case 4:
                     return "warthog-vrl.jpg";
                     break;
                 case 5:
                     return "M12G1_LAAV_Warthog.jpg";
                     break;
                 case 6:
                     return "M12A1_LAAV_Warthog.jpg";
                     break;
                 case 7:
                     return "scorpionN.jpg";
                     break;

                 //Défenses
                 case 8:
                     //return "";
                     break;
                 case 9:
                     //return "";
                     break;
                 case 10:
                     //return "";
                     break;
                 case 11:
                     //return "";
                     break;
                      }
            return Donnees::image_humain_default;
        } elseif ($planete->race == "covenant") {
            switch ($id) {
              //Unités terrestres
              case 0:
                  return "bansheeqp0.jpg";
                  break;
              case 1:
                  return "spirit1.jpg";
                  break;
              case 2:
                  return "phantomfu2.jpg";
                  break;
              case 3:
                  return "boardingcraft.jpg";
                  break;
              case 4:
                  return "ghostic1.jpg";
                  break;
              case 5:
                  return "shadow.jpg";
                  break;
              case 6:
                  return "spectre1.jpg";
                  break;
              case 7:
                  return "wraith.jpg";
                  break;

                  //Défenses
              case 8:
                  return "shade.jpg";
                  break;
              case 9:
                  return "defcovie.jpg";
                  break;
              case 10:
                  return "tourellebarreau.jpg";
                  break;
              case 11:
                  return "tourelle.jpg";
                  break;
              case 12:
                  return "lanceur_torpilles.jpg";
                  break;
          }
            return Donnees::image_covenant_default;
        } else {
            trigger_error("Impossible de trouver la race pour ".$planete->race, E_USER_ERROR);
        }
    }


    public static function needed($id, surface $planete, $print = false, $race = null)
    {
        if ($race == null) {
            $race = $planete->race;
        }
        if ($race == "humain") {
            $neededTerrestre = array(
                array(
                    array('batiments', 7, 1)
                ),
                array(
                    array('batiments', 7, 3)
                ),
                array(
                    array('batiments', 7, 4)
                ),
                array(
                    array('batiments', 7, 6)
                ),
                array(
                    array('batiments', 7, 1)
                ),
                array(
                    array('batiments', 7, 2)
                ),
                array(
                    array('batiments', 7, 3)
                ),
                array(
                    array('batiments', 7, 5)
                ),
                //Défenses
                array(
                    array('batiments', 7, 1),
                    array('technologies', 6, 0)
                ),
                array(
                    array('batiments', 7, 3),
                    array('technologies', 6, 3)
                ),
                array(
                    array('batiments', 7, 4),
                    array('technologies', 6, 1)
                ),
                array(
                    array('batiments', 7, 4),
                    array('technologies', 6, 4)
                ),
                array(
                    array('batiments', 7, 8),
                    array('technologies', 6, 2)
                ),
                array(
                    array('batiments', 7, 8),
                    array('technologies', 6, 5)
                ),
                array(
                    array('batiments', 7, 8),
                    array('technologies', 6, 6)
                ),
                array(
                    array('batiments', 7, 10),
                    array('technologies', 7, 11)
                )
            );
        } else {
            // Covenant
            $neededTerrestre = array(
                array(
                    array('batiments', 7, 1)
                ),
                array(
                    array('batiments', 7, 3)
                ),
                array(
                    array('batiments', 7, 4)
                ),
                array(
                    array('batiments', 7, 6)
                ),
                array(
                    array('batiments', 7, 1)
                ),
                array(
                    array('batiments', 7, 2)
                ),
                array(
                    array('batiments', 7, 3)
                ),
                array(
                    array('batiments', 7, 5)
                ),
                //Défenses
                array(
                    array('batiments', 7, 1),
                    array('technologies', 6, 0)
                ),
                array(
                    array('batiments', 7, 3),
                    array('technologies', 6, 3)
                ),
                array(
                    array('batiments', 7, 4),
                    array('technologies', 6, 1)
                ),
                array(
                    array('batiments', 7, 4),
                    array('technologies', 6, 4)
                ),
                array(
                    array('batiments', 7, 8),
                    array('technologies', 6, 2)
                )
            );
        }

        if ($print) {
            return dDonnees::print_neededCheck($neededTerrestre[$id], $planete, $race);
        } else {
            return dDonnees::neededCheck($neededTerrestre[$id], $planete);
        }
    }


    public static function attaque($id, surface $planete, $race = null)
    {
        if ($race == null) {
            $race = $planete->race;
        }
        //Les véhicules ne sont pas implémenté pour le moment
        if ($id < 8) {
            return 0;
        }
        if ($race == "humain") {
            switch ($id) {
                case 8:
                    $attaque = 15;
                    break;
                case 9:
                    $attaque = 50;
                    break;
                case 10:
                    $attaque = 1000;
                    break;
                case 11:
                    $attaque = 2000;
                    break;
                case 12:
                    $attaque = 3000;
                    break;
                case 13:
                    $attaque = 3500;
                    break;
                case 14:
                    $attaque = 6000;
                    break;
                case 15:
                    $attaque = 15000;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            switch ($id) {
                case 8:
                    $attaque = 15;
                    break;
                case 9:
                    $attaque = 50;
                    break;
                case 10:
                    $attaque = 1000;
                    break;
                case 11:
                    $attaque = 2000;
                    break;
                case 12:
                    $attaque = 3000;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }

        //On tient compte des bonus
        //Bonus pour les défenses légère
        if ($id == 8 || $id == 9) {
            //Bonus Projectiles haute densité
            if ($planete->technologies[4]& dTechnologies::idToBit(2)) {
                $attaque *= 1.30;
            } elseif ($planete->technologies[4]& dTechnologies::idToBit(1)) {
                $attaque *= 1.20;
            } elseif ($planete->technologies[4]& dTechnologies::idToBit(0)) {
                $attaque *= 1.10;
            }
        }

        //Bonus pour les défenses moyenne
        if ($id == 10 || $id == 11) {
            //Bonus Ballistique avancée
            if ($planete->technologies[4]& dTechnologies::idToBit(14)) {
                $attaque *= 1.30;
            } elseif ($planete->technologies[4]& dTechnologies::idToBit(13)) {
                $attaque *= 1.20;
            } elseif ($planete->technologies[4]& dTechnologies::idToBit(12)) {
                $attaque *= 1.10;
            }
        }

        //Bonus pour les défenses lourdes
        if ($id >= 12) {
            //Bonus Accélération Magnétique
            if ($planete->technologies[4]& dTechnologies::idToBit(17)) {
                $attaque *= 1.30;
            } elseif ($planete->technologies[4]& dTechnologies::idToBit(16)) {
                $attaque *= 1.20;
            } elseif ($planete->technologies[4]& dTechnologies::idToBit(15)) {
                $attaque *= 1.10;
            }
        }

        return $attaque;
    }

    public static function bouclier($id, surface $planete, $race = null)
    {
        if ($race == null) {
            $race = $planete->race;
        }
        //Les véhicules ne sont pas implémenté pour le moment
        if ($id < 8) {
            return 0;
        }
        if ($race == "humain") {
            switch ($id) {
                case 8:
                    $bouclier = 25;
                    break;
                case 9:
                    $bouclier = 75;
                    break;
                case 10:
                    $bouclier = 1500;
                    break;
                case 11:
                    $bouclier = 1800;
                    break;
                case 12:
                    $bouclier = 4850;
                    break;
                case 13:
                    $bouclier = 5000;
                    break;
                case 14:
                    $bouclier = 5000;
                    break;
                case 15:
                    $bouclier = 7500;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            switch ($id) {
                case 8:
                    $bouclier = 40;
                    break;
                case 9:
                    $bouclier = 120;
                    break;
                case 10:
                    $bouclier = 2600;
                    break;
                case 11:
                    $bouclier = 5000;
                    break;
                case 12:
                    $bouclier = 8600;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }

        //On tient compte des bonus
        return $bouclier;
    }

    public static function armature($id, surface $planete, $race = null)
    {
        if ($race == null) {
            $race = $planete->race;
        }
        //Les véhicules ne sont pas implémenté pour le moment
        if ($id < 8) {
            return 0;
        }
        if ($race == "humain") {
            switch ($id) {
                case 8:
                    $armature = 25;
                    break;
                case 9:
                    $armature = 75;
                    break;
                case 10:
                    $armature = 1500;
                    break;
                case 11:
                    $armature = 1800;
                    break;
                case 12:
                    $armature = 4850;
                    break;
                case 13:
                    $armature = 5000;
                    break;
                case 14:
                    $armature = 5000;
                    break;
                case 15:
                    $armature = 7500;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            switch ($id) {
                case 8:
                    $armature = 10;
                    break;
                case 9:
                    $armature = 30;
                    break;
                case 10:
                    $armature = 400;
                    break;
                case 11:
                    $armature = 700;
                    break;
                case 12:
                    $armature = 1400;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }

        //On tient compte des bonus
        //Bonus pour les défenses légères
        if ($id == 8 || $id == 9) {
            //Bonus Alliage
            if ($planete->technologies[5]& dTechnologies::idToBit(20)) {
                $armature *= 1.30;
            } elseif ($planete->technologies[5]& dTechnologies::idToBit(19)) {
                $armature *= 1.20;
            } elseif ($planete->technologies[5]& dTechnologies::idToBit(18)) {
                $armature *= 1.10;
            }
        }

        //Bonus pour les défense moyennes
        if ($id == 10 || $id == 11) {
            //Bonus Alliage avancé
            if ($planete->technologies[5]& dTechnologies::idToBit(23)) {
                $armature *= 1.30;
            } elseif ($planete->technologies[5]& dTechnologies::idToBit(22)) {
                $armature *= 1.20;
            } elseif ($planete->technologies[5]& dTechnologies::idToBit(21)) {
                $armature *= 1.10;
            }
        }

        //Bonus pour les défenses lourdes
        if ($id >= 12) {
            //Bonus Armure Moléculaire
            if ($planete->technologies[5]& dTechnologies::idToBit(26)) {
                $armature *= 1.30;
            } elseif ($planete->technologies[5]& dTechnologies::idToBit(25)) {
                $armature *= 1.20;
            } elseif ($planete->technologies[5]& dTechnologies::idToBit(24)) {
                $armature *= 1.10;
            }
        }

        return $armature;
    }
}
