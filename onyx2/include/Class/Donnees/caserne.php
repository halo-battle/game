<?php
require_once("Class/Donnees/interface.php");

class dCaserne implements Donnees
{
    public static function metal($id, $nombre, surface $planete)
    {
        switch ($id) {
            case 0:
                $metal = 80;
                break;
            case 1:
                $metal = 110;
                break;
            case 2:
                $metal = 150;
                break;
            case 3:
                $metal = 220;
                break;
            case 4:
                $metal = 180;
                break;
            case 5:
                $metal = 25000;
                break;
            case 6:
                $metal = 100;
                break;
            case 7:
                $metal = 90;
                break;
            case 8:
                $metal = 300;
                break;
            default:
                trigger_error("Unité ".$id." introuvable dans les données", E_USER_ERROR);
        }

        //On tient compte des bonus
        if (isset($planete->politique) && $planete->politique == 1) {
            $metal *= 0.9;
        }

        return $metal * $nombre;
    }

    public static function cristal($id, $nombre, surface $planete)
    {
        switch ($id) {
            case 0:
                $cristal = 45;
                break;
            case 1:
                $cristal = 90;
                break;
            case 2:
                $cristal = 105;
                break;
            case 3:
                $cristal = 150;
                break;
            case 4:
                $cristal = 100;
                break;
            case 5:
                $cristal = 10000;
                break;
            case 6:
                $cristal = 100;
                break;
            case 7:
                $cristal = 105;
                break;
            case 8:
                $cristal = 250;
                break;
            default:
                trigger_error("Unité ".$id." introuvable dans les données", E_USER_ERROR);
        }

        //On tient compte des bonus
        if (isset($planete->politique) && $planete->politique == 1) {
            $cristal *= 0.9;
        }

        return $cristal * $nombre;
    }

    public static function hydrogene($id, $nombre, surface $planete)
    {
        switch ($id) {
            case 0:
                $hydrogene = 0;
                break;
            case 1:
                $hydrogene = 0;
                break;
            case 2:
                $hydrogene = 20;
                break;
            case 3:
                $hydrogene = 0;
                break;
            case 4:
                $hydrogene = 0;
                break;
            case 5:
                $hydrogene = 0;
                break;
            case 6:
                $hydrogene = 0;
                break;
            case 7:
                $hydrogene = 0;
                break;
            case 8:
                $hydrogene = 0;
                break;
            default:
                trigger_error("Unité ".$id." introuvable dans les données", E_USER_ERROR);
        }

        //On tient compte des bonus
        if (isset($planete->politique) && $planete->politique == 1) {
            $hydrogene *= 0.9;
        }

        return $hydrogene * $nombre;
    }

    public static function credits($id, $nombre, surface $planete)
    {
        return 0;
    }

    public static function temps($id, $nombre, surface $planete)
    {
        switch ($id) {
            case 0:
                $temps = 60;
                $moins = 1;
                break;
            case 1:
                $temps = 240;
                $moins = 2;
                break;
            case 2:
                $temps = 240;
                $moins = 3;
                break;
            case 3:
                $temps = 720;
                $moins = 5;
                break;
            case 4:
                $temps = 300;
                $moins = 3;
                break;
            case 5:
                $temps = 29700;
                $moins = 10;
                break;
            case 6:
                $temps = 90;
                $moins = 2;
                break;
            case 7:
                $temps = 90;
                $moins = 2;
                break;
            case 8:
                $temps = 900;
                $moins = 5;
                break;
            default:
                trigger_error("Unité ".$id." introuvable dans les données", E_USER_ERROR);
        }

        //On tient compte de la vitesse
        $temps /= VITESSE;

        //On tient compte des bonus
        return ceil($temps/pow(1.25, ($planete->batiments[9] - $moins))) * $nombre;
    }


    public static function image($id, surface $planete)
    {
        if ($planete->race == "covenant") {
            array('','','','','','','','','');
            switch ($id) {
                case 0:
                    return "grunt1.jpg";
                    break;
                case 1:
                    return "jackal.jpg";
                    break;
                case 2:
                    return "94990342wb4.jpg";
                    break;
                case 3:
                    return "98004530fx3.jpg";
                    break;
                case 4:
                    return "88091275ja8.jpg";
                    break;
                case 5:
                    return "hunter1.jpg";
                    break;
                case 6:
                    return "81770345oo4.jpg";
                    break;
                case 7:
                    return "88218731ts1.jpg";
                    break;
                case 8:
                    return "72188202fg9.jpg";
                    break;
            }
            return Donnees::image_covenant_default;
        } elseif ($planete->race == "humain") {
            switch ($id) {
                case 0:
                    return "marines.jpg";
                    break;
                case 1:
                    return "marinehf0.jpg";
                    break;
                case 2:
                    return "grenadier.jpg";
                    break;
                case 3:
                    return "TCAO2.jpg";
                    break;
                case 4:
                    return "sniper.jpg";
                    break;
                case 5:
                    return "spartan.jpg";
                    break;
                case 6:
                    return "ingenieurs.jpg";
                    break;
                case 7:
                    return "exosquelettehbpb2.jpg";
                    break;
            }
            return Donnees::image_humain_default;
        } else {
            trigger_error("Impossible de trouver la race pour ".$planete->race, E_USER_ERROR);
        }
    }


    public static function needed($id, surface $planete, $print = false)
    {
        $neededCaserne =
            array(
                array(
                    array('batiments', 9, 1)
                ),
                array(
                    array('batiments', 9, 2)
                ),
                array(
                    array('batiments', 9, 3)
                ),
                array(
                    array('batiments', 9, 5)
                ),
                array(
                    array('batiments', 9, 3)
                ),
                array(
                    array('batiments', 9, 10)
                ),
                array(
                    array('batiments', 9, 2)
                ),
                array(
                    array('batiments', 9, 2)
                ),
                array(
                    array('batiments', 9, 5)
                )
            );

        if ($print) {
            return dDonnees::print_neededCheck($neededCaserne[$id], $planete);
        } else {
            return dDonnees::neededCheck($neededCaserne[$id], $planete);
        }
    }
}
