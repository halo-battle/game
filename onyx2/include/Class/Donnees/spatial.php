<?php
require_once("Class/Donnees/interface.php");

class dSpatial implements Donnees
{
    public static function metal($id, $nombre, surface $planete, $race = null)
    {
        if ($race == null) {
            $race = $planete->race;
        }
        if ($race == "humain") {
            switch ($id) {
                case 0:
                    $metal = 1000;
                    break;
                case 1:
                    $metal = 4000;
                    break;
                case 2:
                    $metal = 8000;
                    break;
                case 3:
                    $metal = 3000;
                    break;
                case 4:
                    $metal = 1500;
                    break;
                case 5:
                    $metal = 500;
                    break;
                case 6:
                    $metal = 2000;
                    break;
                case 7:
                    $metal = 3500;
                    break;
                case 8:
                    $metal = 6000;
                    break;
                case 9:
                    $metal = 10000;
                    break;
                case 10:
                    $metal = 18000;
                    break;
                case 11:
                    $metal = 20000;
                    break;
                case 12:
                    $metal = 20500;
                    break;
                case 13:
                    $metal = 23500;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            switch ($id) {
                case 0:
                    $metal = 1000;
                    break;
                case 1:
                    $metal = 3000;
                    break;
                case 2:
                    $metal = 9000;
                    break;
                case 3:
                    $metal = 600;
                    break;
                case 4:
                    $metal = 500;
                    break;
                case 5:
                    $metal = 500;
                    break;
                case 6:
                    $metal = 1500;
                    break;
                case 7:
                    $metal = 3000;
                    break;
                case 8:
                    $metal = 3000;
                    break;
                case 9:
                    $metal = 7000;
                    break;
                case 10:
                    $metal = 14000;
                    break;
                case 11:
                    $metal = 15000;
                    break;
                case 12:
                    $metal = 25000;
                    break;
                case 13:
                    $metal = 18000;
                    break;
                case 14:
                    $metal = 18000;
                    break;
                case 15:
                    $metal = 17500;
                    break;
                case 16:
                    $metal = 150000;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }

        //On tient compte des bonus
        if (isset($planete->politique) && $planete->politique == 1) {
            $metal *= 0.9;
        }

        return $metal * $nombre;
    }

    public static function cristal($id, $nombre, surface $planete, $race = null)
    {
        if ($race == null) {
            $race = $planete->race;
        }
        if ($race == "humain") {
            switch ($id) {
                case 0:
                    $cristal = 800;
                    break;
                case 1:
                    $cristal = 2500;
                    break;
                case 2:
                    $cristal = 8000;
                    break;
                case 3:
                    $cristal = 3000;
                    break;
                case 4:
                    $cristal = 3000;
                    break;
                case 5:
                    $cristal = 400;
                    break;
                case 6:
                    $cristal = 1450;
                    break;
                case 7:
                    $cristal = 2000;
                    break;
                case 8:
                    $cristal = 3600;
                    break;
                case 9:
                    $cristal = 7000;
                    break;
                case 10:
                    $cristal = 12000;
                    break;
                case 11:
                    $cristal = 13500;
                    break;
                case 12:
                    $cristal = 17200;
                    break;
                case 13:
                    $cristal = 17900;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            switch ($id) {
                case 0:
                    $cristal = 800;
                    break;
                case 1:
                    $cristal = 3600;
                    break;
                case 2:
                    $cristal = 8000;
                    break;
                case 3:
                    $cristal = 800;
                    break;
                case 4:
                    $cristal = 400;
                    break;
                case 5:
                    $cristal = 600;
                    break;
                case 6:
                    $cristal = 1800;
                    break;
                case 7:
                    $cristal = 3000;
                    break;
                case 8:
                    $cristal = 6000;
                    break;
                case 9:
                    $cristal = 9200;
                    break;
                case 10:
                    $cristal = 16000;
                    break;
                case 11:
                    $cristal = 18000;
                    break;
                case 12:
                    $cristal = 30000;
                    break;
                case 13:
                    $cristal = 20800;
                    break;
                case 14:
                    $cristal = 19400;
                    break;
                case 15:
                    $cristal = 23000;
                    break;
                case 16:
                    $cristal = 100000;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }

        //On tient compte des bonus
        if (isset($planete->politique) && $planete->politique == 1) {
            $cristal *= 0.9;
        }

        return $cristal * $nombre;
    }

    public static function hydrogene($id, $nombre, surface $planete, $race = null)
    {
        if ($race == null) {
            $race = $planete->race;
        }
        if ($race == "humain") {
            switch ($id) {
                case 0:
                    $hydrogene = 180;
                    break;
                case 1:
                    $hydrogene = 650;
                    break;
                case 2:
                    $hydrogene = 1600;
                    break;
                case 3:
                    $hydrogene = 600;
                    break;
                case 4:
                    $hydrogene = 450;
                    break;
                case 5:
                    $hydrogene = 90;
                    break;
                case 6:
                    $hydrogene = 350;
                    break;
                case 7:
                    $hydrogene = 550;
                    break;
                case 8:
                    $hydrogene = 960;
                    break;
                case 9:
                    $hydrogene = 1700;
                    break;
                case 10:
                    $hydrogene = 3000;
                    break;
                case 11:
                    $hydrogene = 3350;
                    break;
                case 12:
                    $hydrogene = 3770;
                    break;
                case 13:
                    $hydrogene = 4340;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            switch ($id) {
                case 0:
                    $hydrogene = 180;
                    break;
                case 1:
                    $hydrogene = 660;
                    break;
                case 2:
                    $hydrogene = 1600;
                    break;
                case 3:
                    $hydrogene = 140;
                    break;
                case 4:
                    $hydrogene = 90;
                    break;
                case 5:
                    $hydrogene = 110;
                    break;
                case 6:
                    $hydrogene = 330;
                    break;
                case 7:
                    $hydrogene = 600;
                    break;
                case 8:
                    $hydrogene = 900;
                    break;
                case 9:
                    $hydrogene = 1620;
                    break;
                case 10:
                    $hydrogene = 3000;
                    break;
                case 11:
                    $hydrogene = 3300;
                    break;
                case 12:
                    $hydrogene = 5500;
                    break;
                case 13:
                    $hydrogene = 3880;
                    break;
                case 14:
                    $hydrogene = 3740;
                    break;
                case 15:
                    $hydrogene = 4050;
                    break;
                case 16:
                    $hydrogene = 25000;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }

        //On tient compte des bonus
        if (isset($planete->politique) && $planete->politique == 1) {
            $hydrogene *= 0.9;
        }

        return $hydrogene * $nombre;
    }

    public static function credits($id, $nombre, surface $planete, $race = null)
    {
        return 0;
    }

    public static function temps($id, $nombre, surface $planete, $race = null)
    {
        if ($race == null) {
            $race = $planete->race;
        }
        if ($race == "humain") {
            switch ($id) {
                case 0:
                    $temps = 733;
                    $moins = 1;
                    break;
                case 1:
                    $temps = 2583;
                    $moins = 5;
                    break;
                case 2:
                    $temps = 6667;
                    $moins = 5;
                    break;
                case 3:
                    $temps = 2500;
                    $moins = 5;
                    break;
                case 4:
                    $temps = 2000;
                    $moins = 5;
                    break;
                case 5:
                    $temps = 367;
                    $moins = 3;
                    break;
                case 6:
                    $temps = 1392;
                    $moins = 5;
                    break;
                case 7:
                    $temps = 2167;
                    $moins = 1;
                    break;
                case 8:
                    $temps = 3800;
                    $moins = 3;
                    break;
                case 9:
                    $temps = 10250;
                    $moins = 6;
                    break;
                case 10:
                    $temps = 18000;
                    $moins = 6;
                    break;
                case 11:
                    $temps = 22125;
                    $moins = 8;
                    break;
                case 12:
                    $temps = 30867;
                    $moins = 12;
                    break;
                case 13:
                    $temps = 2000;
                    $moins = 12;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            switch ($id) {
                case 0:
                    $temps = 733;
                    $moins = 1;
                    break;
                case 1:
                    $temps = 2800;
                    $moins = 5;
                    break;
                case 2:
                    $temps = 6667;
                    $moins = 5;
                    break;
                case 3:
                    $temps = 600;
                    $moins = 5;
                    break;
                case 4:
                    $temps = 367;
                    $moins = 5;
                    break;
                case 5:
                    $temps = 467;
                    $moins = 3;
                    break;
                case 6:
                    $temps = 1400;
                    $moins = 5;
                    break;
                case 7:
                    $temps = 2500;
                    $moins = 1;
                    break;
                case 8:
                    $temps = 4000;
                    $moins = 3;
                    break;
                case 9:
                    $temps = 10000;
                    $moins = 6;
                    break;
                case 10:
                    $temps = 19000;
                    $moins = 6;
                    break;
                case 11:
                    $temps = 21000;
                    $moins = 8;
                    break;
                case 12:
                    $temps = 46667;
                    $moins = 12;
                    break;
                case 13:
                    $temps = 32800;
                    $moins = 12;
                    break;
                case 14:
                    $temps = 31400;
                    $moins = 14;
                    break;
                case 15:
                    $temps = 34667;
                    $moins = 16;
                    break;
                case 16:
                    $temps = 200000;
                    $moins = 20;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }

        //On tient compte de la vitesse
        $temps /= VITESSE;

        //On tient compte des bonus
        if (SURFACE == "planete") {
            return ceil($temps/pow(1.25, ($planete->batiments[8] - $moins))) * $nombre;
        } else {
            return ($temps * $nombre);
        }
    }

    public static function image($id, surface $planete, $race = null)
    {
        if ($race == null) {
            $race = $planete->race;
        }
        if ($race == "humain") {
            switch ($id) {
                case 0:
                    return "csnucargoparabola2mc9.jpg";
                    break;
                case 1:
                    return "csnucargoladen2al8.jpg";
                    break;
                case 2:
                    return "colonisation.jpg";
                    break;
                case 3:
                    //return "";
                    break;
                case 4:
                    return "longsworduf9.jpg";
                    break;
                case 5:
                    //return "";
                    break;
                case 6:
                    //return "";
                    break;
                case 7:
                    return "frgatecopiegw1.jpg";
                    break;
                case 8:
                    //return "";
                    break;
                case 9:
                    return "halcyo15.jpg";
                    break;
                case 10:
                    return "qsu169.jpg";
                    break;
            }
            return Donnees::image_humain_default;
        } else {
            switch ($id) {
                case 0:
                    return "cargo2pb6.jpg";
                    break;
                case 1:
                    return "sanstitre2copiegw9.jpg";
                    break;
                case 2:
                    return "colocopiers4.jpg";
                    break;
                case 6:
                    return "chasseurlourd7id.jpg";
                    break;
                case 7:
                    return "sonde_despionnage1.jpg";
                    break;
                case 8:
                    return "contactharvestbynameleszk3.jpg";
                    break;
                case 9:
                    return "vaisseauuu0.jpg";
                    break;
                case 10:
                    return "vaisseaudebataille9na.jpg";
                    break;
                case 14:
                    return "pv.jpg";
                    break;
                case 16:
                    return "citecovenant.jpg";
                    break;
            }
            return Donnees::image_covenant_default;
        }
    }

    public static function needed($id, surface $planete, $print = false, $race = null)
    {
        if ($race == null) {
            $race = $planete->race;
        }
        if ($race == "humain") {
            $neededSpatial =
                array(
                    array(
                        array('batiments', 8, 1),
                        array('technologies', 0,3)
                    ),
                    array(
                        array('batiments', 8, 5),
                        array('technologies', 0,3),
                        array('technologies', 1,6)
                    ),
                    array(
                        array('batiments', 8, 5),
                        array('technologies', 8,0)
                    ),
                    array(
                        array('batiments', 8, 5),
                        array('technologies', 7,4)
                    ),
                    array(
                        array('batiments', 8, 5),
                        array('technologies', 7,0)
                    ),
                    array(
                        array('batiments', 8, 3),
                        array('technologies', 7,1)
                    ),
                    array(
                        array('batiments', 8, 5),
                        array('technologies', 7,2)
                    ),
                    array(
                        array('batiments', 8, 1),
                        array('technologies', 7,3)
                    ),
                    array(
                        array('batiments', 8, 3),
                        array('technologies', 7,5)
                    ),
                    array(
                        array('batiments', 8, 6),
                        array('technologies', 7,6)
                    ),
                    array(
                        array('batiments', 8, 6),
                        array('technologies', 7,1)
                    ),
                    array(
                        array('batiments', 8, 8),
                        array('technologies', 7,8)
                    ),
                    array(
                        array('batiments', 8, 10),
                        array('technologies', 7,9)
                    ),
                    array(
                        array('batiments', 8, 3),
                        array('technologies', 7, 4)
                    )
                );
        } else {
            $neededSpatial =
                array(
                    array(
                        array('batiments', 8, 1),
                        array('technologies', 0, 3)
                    ),
                    array(
                        array('batiments', 8, 4),
                        array('technologies', 0, 3)
                    ),
                    array(
                        array('batiments', 8, 3),
                        array('technologies', 8, 0)
                    ),
                    array(
                        array('batiments', 8, 3),
                        array('technologies', 7, 4)
                    ),
                    array(
                        array('batiments', 8, 1),
                        array('technologies', 7, 1)
                    ),
                    array(
                        array('batiments', 8, 1),
                        array('technologies', 7, 2)
                    ),
                    array(
                        array('batiments', 8, 2),
                        array('technologies', 7, 3)
                    ),
                    array(
                        array('batiments', 8, 3),
                        array('technologies', 7, 4)
                    ),
                    array(
                        array('batiments', 8, 4),
                        array('technologies', 7, 5)
                    ),
                    array(
                        array('batiments', 8, 5),
                        array('technologies', 7, 6)
                    ),
                    array(
                        array('batiments', 8, 5),
                        array('technologies', 7, 7)
                    ),
                    array(
                        array('batiments', 8, 6),
                        array('technologies', 7, 8)
                    ),
                    array(
                        array('batiments', 8, 8),
                        array('technologies', 7, 9)
                    ),
                    array(
                        array('batiments', 8, 7),
                        array('technologies', 7, 10)
                    ),
                    array(
                        array('batiments', 8, 7),
                        array('technologies', 7, 11)
                    ),
                    array(
                        array('batiments', 8, 8),
                        array('technologies', 7, 12)
                    ),
                    array(
                        array('batiments', 8, 9),
                        array('technologies', 7, 13)
                    )
                );
        }

        if ($print) {
            return dDonnees::print_neededCheck($neededSpatial[$id], $planete, $race);
        } else {
            return dDonnees::neededCheck($neededSpatial[$id], $planete);
        }
    }

    public static function tailleCales($id, surface $planete, $race = null)
    {
        if ($race == null) {
            $race = $planete->race;
        }
        if ($race == "humain") {
            switch ($id) {
                case 0:
                    $vitesse = 10000;
                    break;
                case 1:
                    $vitesse = 52000;
                    break;
                case 2:
                    $vitesse = 10000;
                    break;
                case 3:
                    $vitesse = 25000;
                    break;
                case 4:
                    $vitesse = 60;
                    break;
                case 5:
                    $vitesse = 25;
                    break;
                case 6:
                    $vitesse = 35;
                    break;
                case 7:
                    $vitesse = 200;
                    break;
                case 8:
                    $vitesse = 600;
                    break;
                case 9:
                    $vitesse = 600;
                    break;
                case 10:
                    $vitesse = 1000;
                    break;
                case 11:
                    $vitesse = 1500;
                    break;
                case 12:
                    $vitesse = 5000;
                    break;
                case 13:
                    $vitesse = 15000;
                    break;
                default:
                    $vitesse = 0;
            }
        } else {
            switch ($id) {
                case 0:
                    $vitesse = 12000;
                    break;
                case 1:
                    $vitesse = 50000;
                    break;
                case 2:
                    $vitesse = 10000;
                    break;
                case 3:
                    $vitesse = 25000;
                    break;
                case 4:
                    $vitesse = 25;
                    break;
                case 5:
                    $vitesse = 35;
                    break;
                case 6:
                    $vitesse = 35;
                    break;
                case 7:
                    $vitesse = 200;
                    break;
                case 8:
                    $vitesse = 600;
                    break;
                case 9:
                    $vitesse = 700;
                    break;
                case 10:
                    $vitesse = 1000;
                    break;
                case 11:
                    $vitesse = 1500;
                    break;
                case 12:
                    $vitesse = 1000;
                    break;
                case 13:
                    $vitesse = 3000;
                    break;
                case 14:
                    $vitesse = 7000;
                    break;
                case 15:
                    $vitesse = 15000;
                    break;
                case 16:
                    $vitesse = 20000000;
                    break;
                default:
                    $vitesse = 0;
            }
        }

        //On tient compte des bonus
        return $vitesse;
    }

    public static function vitesseP($id, surface $planete)
    {
        if ($planete->race == "humain") {
            switch ($id) {
                case 0:
                    $vitesse = 9;
                    break;
                case 1:
                    $vitesse = 6;
                    break;
                case 2:
                    $vitesse = 4;
                    break;
                case 3:
                    $vitesse = 6;
                    break;
                case 4:
                    $vitesse = 10;
                    break;
                case 5:
                    $vitesse = 11;
                    break;
                case 6:
                    $vitesse = 10.5;
                    break;
                case 7:
                    $vitesse = 7;
                    break;
                case 8:
                    $vitesse = 6;
                    break;
                case 9:
                    $vitesse = 5;
                    break;
                case 10:
                    $vitesse = 4;
                    break;
                case 11:
                    $vitesse = 3.6;
                    break;
                case 12:
                    $vitesse = 3;
                    break;
                case 13:
                    $vitesse = 2;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            switch ($id) {
                case 0:
                    $vitesse = 9;
                    break;
                case 1:
                    $vitesse = 6;
                    break;
                case 2:
                    $vitesse = 4;
                    break;
                case 3:
                    $vitesse = 6;
                    break;
                case 4:
                    $vitesse = 11;
                    break;
                case 5:
                    $vitesse = 10;
                    break;
                case 6:
                    $vitesse = 10.5;
                    break;
                case 7:
                    $vitesse = 7;
                    break;
                case 8:
                    $vitesse = 6.2;
                    break;
                case 9:
                    $vitesse = 5.2;
                    break;
                case 10:
                    $vitesse = 4.2;
                    break;
                case 11:
                    $vitesse = 3.7;
                    break;
                case 12:
                    $vitesse = 2.5;
                    break;
                case 13:
                    $vitesse = 3.6;
                    break;
                case 14:
                    $vitesse = 3;
                    break;
                case 15:
                    $vitesse = 2;
                    break;
                case 16:
                    $vitesse = 0.8;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }

        //On tient compte des bonus
        return $vitesse;
    }

    public static function vitesseS($id, surface $planete)
    {
        if ($planete->race == "humain") {
            switch ($id) {
                case 0:
                    $vitesse = 9;
                    break;
                case 1:
                    $vitesse = 8;
                    break;
                case 2:
                    $vitesse = 6;
                    break;
                case 3:
                    $vitesse = 7;
                    break;
                case 4:
                    $vitesse = 21;
                    break;
                case 5:
                    $vitesse = 10;
                    break;
                case 6:
                    $vitesse = 9;
                    break;
                case 7:
                    $vitesse = 8;
                    break;
                case 8:
                    $vitesse = 8;
                    break;
                case 9:
                    $vitesse = 8;
                    break;
                case 10:
                    $vitesse = 5;
                    break;
                case 11:
                    $vitesse = 5.5;
                    break;
                case 12:
                    $vitesse = 4.6;
                    break;
                case 13:
                    $vitesse = 4;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            switch ($id) {
                case 0:
                    $vitesse = 9;
                    break;
                case 1:
                    $vitesse = 8;
                    break;
                case 2:
                    $vitesse = 6;
                    break;
                case 3:
                    $vitesse = 7;
                    break;
                case 4:
                    $vitesse = 10;
                    break;
                case 5:
                    $vitesse = 9;
                    break;
                case 6:
                    $vitesse = 9;
                    break;
                case 7:
                    $vitesse = 8;
                    break;
                case 8:
                    $vitesse = 8.2;
                    break;
                case 9:
                    $vitesse = 8.2;
                    break;
                case 10:
                    $vitesse = 5.2;
                    break;
                case 11:
                    $vitesse = 5.6;
                    break;
                case 12:
                    $vitesse = 4.3;
                    break;
                case 13:
                    $vitesse = 5.5;
                    break;
                case 14:
                    $vitesse = 4.6;
                    break;
                case 15:
                    $vitesse = 4;
                    break;
                case 16:
                    $vitesse = 2;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }

        //On tient compte des bonus
        return $vitesse;
    }

    public static function vitesseG($id, surface $planete)
    {
        if ($planete->race == "humain") {
            switch ($id) {
                case 0:
                    $vitesse = 8;
                    break;
                case 1:
                    $vitesse = 10;
                    break;
                case 2:
                    $vitesse = 5;
                    break;
                case 3:
                    $vitesse = 7;
                    break;
                case 4:
                    $vitesse = 11;
                    break;
                case 5:
                    $vitesse = 0.1;
                    break;
                case 6:
                    $vitesse = 0.09;
                    break;
                case 7:
                    $vitesse = 10;
                    break;
                case 8:
                    $vitesse = 9.5;
                    break;
                case 9:
                    $vitesse = 9.5;
                    break;
                case 10:
                    $vitesse = 9;
                    break;
                case 11:
                    $vitesse = 8;
                    break;
                case 12:
                    $vitesse = 8;
                    break;
                case 13:
                    $vitesse = 7;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            switch ($id) {
                case 0:
                    $vitesse = 8;
                    break;
                case 1:
                    $vitesse = 10;
                    break;
                case 2:
                    $vitesse = 5;
                    break;
                case 3:
                    $vitesse = 7;
                    break;
                case 4:
                    $vitesse = 0.1;
                    break;
                case 5:
                    $vitesse = 0.1;
                    break;
                case 6:
                    $vitesse = 0.09;
                    break;
                case 7:
                    $vitesse = 10;
                    break;
                case 8:
                    $vitesse = 9.5;
                    break;
                case 9:
                    $vitesse = 9.5;
                    break;
                case 10:
                    $vitesse = 9;
                    break;
                case 11:
                    $vitesse = 8;
                    break;
                case 12:
                    $vitesse = 8.5;
                    break;
                case 13:
                    $vitesse = 8;
                    break;
                case 14:
                    $vitesse = 8;
                    break;
                case 15:
                    $vitesse = 7;
                    break;
                case 16:
                    $vitesse = 7;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }

        //On tient compte des bonus
        return $vitesse;
    }

    public static function attaque($id, surface $planete, $race = null)
    {
        if ($race == null) {
            $race = $planete->race;
        }
        if ($race == "humain") {
            switch ($id) {
                case 0:
                    $attaque = 10;
                    break;
                case 1:
                    $attaque = 20;
                    break;
                case 2:
                    $attaque = 20;
                    break;
                case 3:
                    $attaque = 20;
                    break;
                case 4:
                    $attaque = 50;
                    break;
                case 5:
                    $attaque = 250;
                    break;
                case 6:
                    $attaque = 720;
                    break;
                case 7:
                    $attaque = 1240;
                    break;
                case 8:
                    $attaque = 1990;
                    break;
                case 9:
                    $attaque = 3250;
                    break;
                case 10:
                    $attaque = 4250;
                    break;
                case 11:
                    $attaque = 8500;
                    break;
                case 12:
                    $attaque = 16000;
                    break;
                case 13:
                    $attaque = 20;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            switch ($id) {
                case 0:
                    $attaque = 0;
                    break;
                case 1:
                    $attaque = 25;
                    break;
                case 2:
                    $attaque = 25;
                    break;
                case 3:
                    $attaque = 25;
                    break;
                case 4:
                    $attaque = 50;
                    break;
                case 5:
                    $attaque = 100;
                    break;
                case 6:
                    $attaque = 250;
                    break;
                case 7:
                    $attaque = 740;
                    break;
                case 8:
                    $attaque = 1300;
                    break;
                case 9:
                    $attaque = 2000;
                    break;
                case 10:
                    $attaque = 3400;
                    break;
                case 11:
                    $attaque = 4400;
                    break;
                case 12:
                    $attaque = 9300;
                    break;
                case 13:
                    $attaque = 8400;
                    break;
                case 14:
                    $attaque = 8300;
                    break;
                case 15:
                    $attaque = 11500;
                    break;
                case 16:
                    $attaque = 0;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }

        //On tient compte des bonus
        //Bonus pour les vaisseaux léger
        if ($id <= 3) {
            //Bonus Assistance de visée
            if ($planete->technologies[4]& dTechnologies::idToBit(5)) {
                $attaque *= 1.30;
            } elseif ($planete->technologies[4]& dTechnologies::idToBit(4)) {
                $attaque *= 1.20;
            } elseif ($planete->technologies[4]& dTechnologies::idToBit(3)) {
                $attaque *= 1.10;
            }
        }

        //Bonus pour les vaisseaux moyen
        if ($id >= 4 && $id <= 7) {
            //Bonus Guidage laser
            if ($planete->technologies[4]& dTechnologies::idToBit(8)) {
                $attaque *= 1.30;
            } elseif ($planete->technologies[4]& dTechnologies::idToBit(7)) {
                $attaque *= 1.20;
            } elseif ($planete->technologies[4]& dTechnologies::idToBit(6)) {
                $attaque *= 1.10;
            }
        }

        //Bonus pour les vaisseaux lourds
        if ($id >= 8) {
            //Bonus Condensateurs Rapides
            if ($planete->technologies[4]& dTechnologies::idToBit(11)) {
                $attaque *= 1.30;
            } elseif ($planete->technologies[4]& dTechnologies::idToBit(10)) {
                $attaque *= 1.20;
            } elseif ($planete->technologies[4]& dTechnologies::idToBit(9)) {
                $attaque *= 1.10;
            }
        }

        return $attaque;
    }

    public static function bouclier($id, surface $planete, $race = null)
    {
        if ($race == null) {
            $race = $planete->race;
        }
        if ($race == "humain") {
            switch ($id) {
                case 0:
                    $bouclier = 500;
                    break;
                case 1:
                    $bouclier = 2000;
                    break;
                case 2:
                    $bouclier = 2000;
                    break;
                case 3:
                    $bouclier = 1500;
                    break;
                case 4:
                    $bouclier = 250;
                    break;
                case 5:
                    $bouclier = 1000;
                    break;
                case 6:
                    $bouclier = 1750;
                    break;
                case 7:
                    $bouclier = 3000;
                    break;
                case 8:
                    $bouclier = 5000;
                    break;
                case 9:
                    $bouclier = 9000;
                    break;
                case 10:
                    $bouclier = 10000;
                    break;
                case 11:
                    $bouclier = 10250;
                    break;
                case 12:
                    $bouclier = 12750;
                    break;
                case 13:
                    $bouclier = 750;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            switch ($id) {
                case 0:
                    $bouclier = 400;
                    break;
                case 1:
                    $bouclier = 1800;
                    break;
                case 2:
                    $bouclier = 2000;
                    break;
                case 3:
                    $bouclier = 400;
                    break;
                case 4:
                    $bouclier = 200;
                    break;
                case 5:
                    $bouclier = 300;
                    break;
                case 6:
                    $bouclier = 900;
                    break;
                case 7:
                    $bouclier = 1500;
                    break;
                case 8:
                    $bouclier = 3000;
                    break;
                case 9:
                    $bouclier = 4600;
                    break;
                case 10:
                    $bouclier = 8000;
                    break;
                case 11:
                    $bouclier = 9000;
                    break;
                case 12:
                    $bouclier = 15000;
                    break;
                case 13:
                    $bouclier = 10400;
                    break;
                case 14:
                    $bouclier = 9700;
                    break;
                case 15:
                    $bouclier = 11500;
                    break;
                case 16:
                    $bouclier = 50000;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }

        //On tient compte des bonus
        return $bouclier;
    }

    public static function armature($id, surface $planete, $race = null)
    {
        if ($race == null) {
            $race = $planete->race;
        }
        if ($race == "humain") {
            switch ($id) {
                case 0:
                    $armature = 100;
                    break;
                case 1:
                    $armature = 400;
                    break;
                case 2:
                    $armature = 400;
                    break;
                case 3:
                    $armature = 300;
                    break;
                case 4:
                    $armature = 50;
                    break;
                case 5:
                    $armature = 200;
                    break;
                case 6:
                    $armature = 350;
                    break;
                case 7:
                    $armature = 600;
                    break;
                case 8:
                    $armature = 1000;
                    break;
                case 9:
                    $armature = 1800;
                    break;
                case 10:
                    $armature = 2000;
                    break;
                case 11:
                    $armature = 2050;
                    break;
                case 12:
                    $armature = 2550;
                    break;
                case 13:
                    $armature = 150;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        } else {
            switch ($id) {
                case 0:
                    $armature = 200;
                    break;
                case 1:
                    $armature = 600;
                    break;
                case 2:
                    $armature = 800;
                    break;
                case 3:
                    $armature = 120;
                    break;
                case 4:
                    $armature = 100;
                    break;
                case 5:
                    $armature = 150;
                    break;
                case 6:
                    $armature = 300;
                    break;
                case 7:
                    $armature = 600;
                    break;
                case 8:
                    $armature = 600;
                    break;
                case 9:
                    $armature = 1400;
                    break;
                case 10:
                    $armature = 2800;
                    break;
                case 11:
                    $armature = 3000;
                    break;
                case 12:
                    $armature = 3000;
                    break;
                case 13:
                    $armature = 3600;
                    break;
                case 14:
                    $armature = 3600;
                    break;
                case 15:
                    $armature = 3500;
                    break;
                case 16:
                    $armature = 30000;
                    break;
                default:
                    trigger_error("Vaisseau ".$id." introuvable dans les données", E_USER_ERROR);
            }
        }

        //On tient compte des bonus
        //Bonus pour les vaisseaux léger
        if ($id <= 3) {
            //Bonus Alliage
            if ($planete->technologies[5]& dTechnologies::idToBit(20)) {
                $armature *= 1.30;
            } elseif ($planete->technologies[5]& dTechnologies::idToBit(19)) {
                $armature *= 1.20;
            } elseif ($planete->technologies[5]& dTechnologies::idToBit(18)) {
                $armature *= 1.10;
            }
        }

        //Bonus pour les vaisseaux moyen
        if ($id >= 4 && $id <= 7) {
            //Bonus Alliage avancé
            if ($planete->technologies[5]& dTechnologies::idToBit(23)) {
                $armature *= 1.30;
            } elseif ($planete->technologies[5]& dTechnologies::idToBit(22)) {
                $armature *= 1.20;
            } elseif ($planete->technologies[5]& dTechnologies::idToBit(21)) {
                $armature *= 1.10;
            }
        }

        //Bonus pour les vaisseaux lourds
        if ($id >= 8) {
            //Bonus Armure Moléculaire
            if ($planete->technologies[5]& dTechnologies::idToBit(26)) {
                $armature *= 1.30;
            } elseif ($planete->technologies[5]& dTechnologies::idToBit(25)) {
                $armature *= 1.20;
            } elseif ($planete->technologies[5]& dTechnologies::idToBit(24)) {
                $armature *= 1.10;
            }
        }

        return $armature;
    }
}
