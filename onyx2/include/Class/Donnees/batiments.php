<?php
require_once("Class/Donnees/interface.php");

class dBatiments implements Donnees
{
    public static function metal($id, $niveau, surface $planete)
    {
        switch ($id) {
            case 0:
                $metal = ceil(pow(1.5, $niveau)*68);
                break;
            case 1:
                $metal = ceil(pow(1.6, $niveau)*53);
                break;
            case 2:
                $metal = ceil(pow(1.5, $niveau)*242);
                break;
            case 3:
                $metal = ceil(pow(1.5, $niveau)*92);
                break;
            case 4:
                //$metal = ceil(pow(1.73, $niveau)*800);
                $metal = ceil(pow(3, $niveau)*5000);
                break;
            case 5:
                $metal = ceil(pow(2, $niveau)*750);
                break;
            case 6:
                $metal = ceil(pow(2, $niveau)*200);
                break;
            case 7:
                $metal = ceil(pow(2, $niveau)*520);
                break;
            case 8:
                $metal = ceil(pow(2, $niveau)*600);
                break;
            case 9:
                $metal = ceil(pow(2, $niveau)*200);
                break;
            case 10:
                $metal = ceil(pow(2, $niveau)*400);
                break;
            case 11:
                $metal = ceil(pow(1.7, $niveau)*600);
                break;

            case 12:
                $metal = ceil(pow($niveau, 2.075)*1000+1500);
                break;
            case 13:
                $metal = ceil(pow($niveau, 2.8)*1000+299000);
                break;
            case 14:
                $metal = ceil(pow($niveau, 2)*1200+140000);
                break;
            case 15:
                $metal = ceil(pow($niveau, 2)*1000+80000);
                break;
            case 16:
                $metal = ceil(pow($niveau, 2)*850+60000);
                break;
            case 17:
                $metal = ceil(pow($niveau, 2.5)*500 -300);
                break;
            default:
                trigger_error("Batiment ".$id." introuvable dans les données", E_USER_ERROR);
        }

        //On tient compte des bonus
        //Bonus technologique pour les mines
        if ($id <= 3) {
            if ($planete->technologies[0] &262144) {
                $metal *= 0.85;
            } elseif ($planete->technologies[0] &131072) {
                $metal *= 0.9;
            } elseif ($planete->technologies[0] &65536) {
                $metal *= 0.95;
            }
        }

        return $metal;
    }

    public static function cristal($id, $niveau, surface $planete)
    {
        switch ($id) {
            case 0:
                $cristal = ceil(pow(1.5, $niveau)*17);
                break;
            case 1:
                $cristal = ceil(pow(1.6, $niveau)*27);
                break;
            case 2:
                $cristal = ceil(pow(1.5, $niveau)*72);
                break;
            case 3:
                $cristal = ceil(pow(1.5, $niveau)*37);
                break;
            case 4:
                //$cristal = ceil(pow(1.73, $niveau)*420);
                $cristal = ceil(pow(3, $niveau)*3000);
                break;
            case 5:
                $cristal = ceil(pow(2, $niveau)*500);
                break;
            case 6:
                $cristal = ceil(pow(2, $niveau)*150);
                break;
            case 7:
                $cristal = ceil(pow(2, $niveau)*380);
                break;
            case 8:
                $cristal = ceil(pow(2, $niveau)*450);
                break;
            case 9:
                $cristal = ceil(pow(2, $niveau)*100);
                break;
            case 10:
                $cristal = ceil(pow(2, $niveau)*260);
                break;
            case 11:
                $cristal = ceil(pow(1.7, $niveau)*420);
                break;

            case 12:
                $cristal = ceil(pow($niveau, 1.8)*1000+1000);
                break;
            case 13:
                $cristal = ceil(pow($niveau, 2.5)*1000+149000);
                break;
            case 14:
                $cristal = ceil(pow($niveau, 2)*900+99000);
                break;
            case 15:
                $cristal = ceil(pow($niveau, 2)*750+65000);
                break;
            case 16:
                $cristal = ceil(pow($niveau, 2)*650+50000);
                break;
            case 17:
                $cristal = ceil(pow($niveau, 2.4)*400-250);
                break;
            default:
                trigger_error("Batiment ".$id." introuvable dans les données", E_USER_ERROR);
        }

        //On tient compte des bonus
        //Bonus technologique pour les mines
        if ($id <= 3) {
            if ($planete->technologies[0] &262144) {
                $cristal *= 0.85;
            } elseif ($planete->technologies[0] &131072) {
                $cristal *= 0.9;
            } elseif ($planete->technologies[0] &65536) {
                $cristal *= 0.95;
            }
        }

        return $cristal;
    }

    public static function hydrogene($id, $niveau, surface $planete)
    {
        switch ($id) {
            case 0:
                $hydrogene = 0;
                break;
            case 1:
                $hydrogene = 0;
                break;
            case 2:
                $hydrogene = 0;
                break;
            case 3:
                $hydrogene = 0;
                break;
            case 4:
                //$hydrogene = ceil(pow(1.68, $niveau)*285);
                $hydrogene = 0;
                break;
            case 5:
                $hydrogene = 0;
                break;
            case 6:
                $hydrogene = 0;
                break;
            case 7:
                $hydrogene = 0;
                break;
            case 8:
                $hydrogene = 0;
                break;
            case 9:
                $hydrogene = 0;
                break;
            case 10:
                $hydrogene = 0;
                break;
            case 11:
                $hydrogene = ceil(pow(1.7, $niveau)*100);
                break;

            case 12:
                $hydrogene = 0;
                break;
            case 13:
                $hydrogene = 0;
                break;
            case 14:
                $hydrogene = ceil(pow($niveau, 2)*500+30000);
                break;
            case 15:
                $hydrogene = 0;
                break;
            case 16:
                $hydrogene = 0;
                break;
            case 17:
                $hydrogene = 0;
                break;
            default:
                trigger_error("Batiment ".$id." introuvable dans les données", E_USER_ERROR);
        }

        //On tient compte des bonus
        //Bonus technologique pour les mines
        if ($id <= 3) {
            if ($planete->technologies[0] &262144) {
                $hydrogene *= 0.85;
            } elseif ($planete->technologies[0] &131072) {
                $hydrogene *= 0.9;
            } elseif ($planete->technologies[0] &65536) {
                $hydrogene *= 0.95;
            }
        }

        return $hydrogene;
    }

    public static function credits($id, $niveau, surface $planete)
    {
        return 0;
    }

    public static function temps($id, $niveau, surface $planete, $demolition = false)
    {
        switch ($id) {
            case 0:
                $sec = pow(1.5, $niveau)*6;
                break;
            case 1:
                $sec = pow(1.55, $niveau)*6;
                break;
            case 2:
                $sec = pow(1.624, $niveau)*6;
                break;
            case 3:
                $sec = pow(1.597, $niveau)*6;
                break;
            case 4:
                $sec = pow(1.7, $niveau)*6;
                break;
            case 5:
                $sec = ceil(pow(2, $niveau)*720);
                break;
            case 6:
                $sec = ceil(pow(2, $niveau)*720);
                break;
            case 7:
                $sec = ceil(pow(2, $niveau)*420);
                break;
            case 8:
                $sec = ceil(pow(2, $niveau)*600);
                break;
            case 9:
                $sec = ceil(pow(2, $niveau)*300);
                break;
            case 10:
                $sec = ceil(pow(2, $niveau)*1200);
                break;
            case 11:
                $sec = ceil((pow(1.9, $niveau)*800));
                break;

            case 12:
                $sec = ceil(pow($niveau, 2.5)*60+4)*60;
                break;
            case 13:
                $sec = ceil(pow($niveau, 1.6)*60+60)*60;
                break;
            case 14:
                $sec = ceil(pow($niveau, 2)*5+3)*60;
                break;
            case 15:
                $sec = pow($niveau, 2)*240;
                break;
            case 16:
                $sec = pow($niveau, 2)*180;
                break;
            case 17:
                $sec = pow($niveau, 1.65)*3600;
                break;
            default:
                trigger_error("Batiment ".$id." introuvable dans les données", E_USER_ERROR);
        }

        //On applique le bonus de temps des ingénieurs et des centrales informatiques
        if ($id == 11 && $planete->casernes[7]) {
            $sec /= (1.0025 * $planete->casernes[7]);
        } elseif ($id != 11 && ($planete->casernes[7] || $planete->batiments[11])) {
            $sec /= (pow(1.23, $planete->batiments[11]) + 1.0025 * $planete->casernes[7]);
        }

        //Bonus technologique de vitesse de construction : Constructions Planétaires
        if ($planete->technologies[2]& dTechnologies::idToBit(2)) {
            $sec /= 1.15;
        } elseif ($planete->technologies[2]& dTechnologies::idToBit(1)) {
            $sec /= 1.10;
        } elseif ($planete->technologies[2]& dTechnologies::idToBit(0)) {
            $sec /= 1.05;
        }

        //Accélération du temps de construction
        $sec /= VITESSE;

        if ($demolition) {
            return $sec * Donnees::coeff_demolition;
        } else {
            return $sec;
        }
    }

    public static function type($id)
    {
        switch ($id) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
                return 1;
            case 6:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
                return 2;
            case 5:
            case 7:
            case 8:
            case 9:
                return 4;
                return 6;
            default:
                trigger_error("Bâtiment ".$id." introuvable dans les données", E_USER_ERROR);
        }
    }


    public static function image($id, surface $planete)
    {
        if ($planete->race == "covenant") {
            switch ($id) {
                case 0:
                    return "batimentcovieux4.jpg";
                    break;
                case 1:
                    return "cristaloo3.png";
                    break;
                case 2:
                    return "powersupplycoviejq1.jpg";
                    break;
                case 3:
                    return "solaire.jpg";
                    break;
                case 4:
                    return "sanstitrevi7.jpg";
                    break;
                case 5:
                    return "oeilduprophetewj6.jpg";
                    break;
                case 6:
                    return "covielabocn5.jpg";
                    break;
                case 7:
                    return "chantierterrestrehg1.jpg";
                    break;
                case 8:
                    return "sanstitretruecolor09zn6.jpg";
                    break;
                case 9:
                    return "caserncov0ry.jpg";
                    break;
                case 10:
                    return "stockagebasement1cs10bl.jpg";
                    break;
                case 11:
                    return "ordianteur_hologramme.jpg";
                    break;
            }
            return Donnees::image_covenant_default;
        } elseif ($planete->race == "humain") {
            switch ($id) {
                case 0:
                    return "mine_m.jpg";
                    break;
                case 1:
                    return "mine_c.png";
                    break;
                case 2:
                    return "synchroniseur.jpg";
                    break;
                case 3:
                    return "centrale solaire.jpg";
                    break;
                case 4:
                    return "centrale electrique.jpg";
                    break;
                case 5:
                    return "baseradardl3.jpg";
                    break;
                case 6:
                    return "recherches.jpg";
                    break;
                case 7:
                    return "chantierterrestrecopybj8.jpg";
                    break;
                case 8:
                    return "chantier spatial.jpg";
                    break;
                case 9:
                    return "ecole militaire.jpg";
                    break;
                case 10:
                    return "stockage.jpg";
                    break;
                case 11:
                    return "search0yp.jpg";
                    break;
            }
            return Donnees::image_humain_default;
        } else {
            trigger_error("Impossible de trouver la race pour ".$planete->race, E_USER_ERROR);
        }
    }


    public static function needed($id, surface $planete, $print = false)
    {
        $neededBatiments =
            array(
                0,
                0,
                0,
                0,
                array(
                    array("batiments", 3, 12),
                    array("batiments_max", 4, 4)
                ),
                0,
                0,
                array(
                    array("technologies", 2,3)
                ),
                array(
                    array("technologies", 2,4)
                ),
                0,
                0,
                array(
                    array("technologies", 1,12)
                ),
                0,
                array(
                    array("technologies", 3,8)
                ),
                array(
                    array("technologies", 7,12)
                ),
                array(
                    array("technologies", 3,6)
                ),
                array(
                    array("technologies", 3,7)
                ),
                0
            );

        if ($print) {
            return dDonnees::print_neededCheck($neededBatiments[$id], $planete);
        } else {
            return dDonnees::neededCheck($neededBatiments[$id], $planete);
        }
    }
}
