<?php
/***************************************************************************
 *                            class.var.php
 *                           ---------------
 *   begin                : Jeudi 29 janvier 2009
 *   update               : Vendredi 27 février 2009
 *   email                : nemunaire@gmail.com
 *
 *
 ***************************************************************************/
class Donnee
{
    public static function donneeTechnologie($branche, $idTechnologie, $retour, Planete $planete)
    {
        //TODO : Faire un switch pour ne renvoyer le tableau que de la branche plutôt que ce tableau avec l'intégralité des branches.
        $technologies_donnees = array(
            //Industrie
            array(
                array(1, 0, 1000, 54000),
                array(2, 1, 2000, 90000),
                array(4, 3, 4000, 126000),
                array(8, 1, 4000, 126000),
                array(16, 1, 2000, 72000),
                array(32, 17, 4000, 108000),
                array(64, 49, 8000, 144000),
                array(128, 1, 2000, 72000),
                array(256, 129, 4000, 108000),
                array(512, 385, 8000, 144000),
                array(1024, 1, 2000, 72000),
                array(2048, 1025, 4000, 108000),
                array(4096, 3073, 8000, 144000),
                array(8192, 9, 3000, 126000),
                array(16384, 8201, 6000, 162000),
                array(32768, 24585, 12000, 198000),
                array(65536, 1169, 3000, 126000),
                array(131072, 66705, 6000, 162000),
                array(262144, 197777, 12000, 198000)
            ),
            //Ingénieurie
            array(
                array(1, 0, 1000, 54000),
                array(2, 1, 2000, 90000),
                array(4, 3, 4000, 126000),
                array(8, 1, 2000, 90000),
                array(16, 9, 4000, 126000),
                array(32, 25, 8000, 162000),
                array(64, 9, 3000, 126000),
                array(128, 73, 6000, 162000),
                array(256, 201, 12000, 198000),
                array(512, 73, 4000, 162000),
                array(1024, 585, 8000, 198000),
                array(2048, 1609, 16000, 234000),
                array(4096, 0, 1000, 54000),
                array(8192, 4096, 2000, 90000),
                array(16384, 12288, 4000, 126000),
                array(32768, 4096, 2000, 54000),
                array(65536, 36864, 4000, 90000),
                array(131072, 102400, 8000, 126000),
                array(262144, 12288, 2000, 72000),
                array(524288, 274432, 4000, 108000),
                array(1048576, 798720, 8000, 144000),
                array(2097152, 274432, 2000, 54000),
                array(4194304, 2371584, 4000, 90000),
                array(8388608, 6565888, 8000, 126000),
                array(16777216, 28672, 2000, 54000),
                array(33554432, 16805888, 4000, 90000),
                array(67108864, 50360320, 8000, 126000),
                array(134217728, 28672, 3000, 90000),
                array(268435456, 134246400, 6000, 126000),
                array(536870912, 402681856, 12000, 162000)
            ),
            //Ingénieurie 2
            array(
                array(1, 0, 1000, 36000),
                array(2, 1, 2000, 72000),
                array(4, 3, 4000, 108000),
                array(8, 1, 4000, 126000),
                array(16, 1, 4000, 126000),
                array(32, 25, 4000, 126000),
                array(64, 57, 8000, 162000),
                array(128, 121, 16000, 198000)
            ),
            //Politique
            array(
                array(1, 0, 1000, 36000),
                array(2, 1, 2000, 72000),
                array(4, 3, 4000, 108000),
                array(8, 1, 2000, 72000),
                array(16, 9, 4000, 108000),
                array(32, 25, 8000, 144000),
                array(64, 9, 4000, 126000),
                array(128, 8, 6000, 162000),
                array(256, 9, 6000, 162000),
                array(512, 0, 1000, 90000),
                array(1024, 512, 2000, 90000),
                array(2048, 1536, 4000, 90000),
                array(4096, 512, 2000, 72000),
                array(8192, 4608, 4000, 108000),
                array(16384, 12800, 8000, 144000)
            ),

            //Armement
            array(
                array(1, 0, 1000, 36000),
                array(2, 1, 2000, 72000),
                array(4, 3, 4000, 108000),
                array(8, 1, 1000, 54000),
                array(16, 9, 2000, 90000),
                array(32, 25, 4000, 126000),
                array(64, 9, 2000, 72000),
                array(128, 73, 4000, 108000),
                array(256, 201, 8000, 144000),
                array(512, 73, 2000, 90000),
                array(1024, 585, 4000, 126000),
                array(2048, 1609, 8000, 162000),
                array(4096, 1, 3000, 108000),
                array(8192, 4097, 6000, 144000),
                array(16384, 12289, 12000, 180000),
                array(32768, 4097, 3000, 126000),
                array(65536, 36865, 6000, 162000),
                array(131072, 102401, 12000, 198000)
            ),
            //Défense
            array(
                array(1, 0, 1000, 36000),
                array(2, 1, 2000, 72000),
                array(4, 3, 4000, 108000),
                array(8, 1, 2000, 72000),
                array(16, 9, 4000, 108000),
                array(32, 25, 8000, 144000),
                array(64, 9, 3000, 108000),
                array(128, 73, 6000, 144000),
                array(256, 201, 12000, 180000),
                array(512, 0, 1000, 36000),
                array(1024, 512, 2000, 72000),
                array(2048, 1536, 4000, 108000),
                array(4096, 512, 2000, 72000),
                array(8192, 4608, 4000, 108000),
                array(16384, 12800, 8000, 144000),
                array(32768, 4608, 3000, 108000),
                array(65536, 37376, 6000, 144000),
                array(131072, 102912, 12000, 180000),
                array(262144, 512, 1000, 54000),
                array(524288, 262656, 2000, 90000),
                array(1048576, 786944, 4000, 126000),
                array(2097152, 262656, 2000, 90000),
                array(4194304, 2359808, 4000, 126000),
                array(8388608, 6554112, 8000, 162000),
                array(16777216, 2359808, 3000, 126000),
                array(33554432, 19137024, 6000, 162000),
                array(67108864, 52691456, 12000, 198000)
            ),
            //Défense 2
            array(
                array(1, 0, 2000, 72000),
                array(2, 1, 4000, 144000),
                array(4, 3, 6000, 216000),
                array(8, 0, 2000, 72000),
                array(16, 8, 4000, 144000),
                array(32, 24, 6000, 216000),
                array(64, 27, 8000, 288000)
            ),
            //Projets expérimentaux
            array(
                array(1, 0, 2000, 36000),
                array(2, 1, 4000, 72000),
                array(4, 2, 4000, 108000),
                array(8, 4, 6000, 144000),
                array(16, 8, 6000, 144000),
                array(32, 8, 8000, 180000),
                array(64, 32, 10000, 216000),
                array(128, 64, 10000, 234000),
                array(256, 128, 12000, 288000),
                array(512, 256, 14000, 324000),
                array(1024, 64, 14000, 342000),
                array(2048, 256, 16000, 360000),
                array(4096, 576, 18000, 396000),
                array(8192, 4096, 20000, 432000),
                array(16384, 8192, 22000, 504000)
            ),
            //Technologie expansion
            array(
                array(1, 0, 4000, 54000),
                array(2, 1, 6000, 72000),
                array(4, 3, 8000, 90000),
                array(8, 7, 10000, 108000),
                array(16, 15, 12000, 126000),
                array(32, 31, 14000, 144000),
                array(64, 63, 16000, 162000),
                array(128, 127, 18000, 180000),
                array(256, 255, 20000, 198000),
                array(512, 511, 22000, 216000),
                array(1024, 1023, 24000, 234000),
                array(2048, 2047, 26000, 252000),
                array(4096, 4095, 28000, 270000),
                array(8192, 8191, 30000, 288000),
                array(16384, 16383, 32000, 306000),
                array(32768, 32767, 34000, 324000),
                array(65536, 65535, 36000, 342000),
                array(131072, 131071, 38000, 360000)
            )
        );

        if ($retour == "metal") {
            return 0;
        } elseif ($retour == "cristal") {
            return 0;
        } elseif ($retour == "hydrogene") {
            return 0;
        } elseif ($retour == "credits") {
            return $technologies_donnees[$branche][$idTechnologie][2];
        } elseif ($retour == "temps") {
            return $technologies_donnees[$branche][$idTechnologie][3]/VITESSE;
        } elseif ($retour == "needed") {
            return $technologies_donnees[$branche][$idTechnologie][1];
        } elseif ($retour == "id") {
            return $technologies_donnees[$branche][$idTechnologie][0];
        } elseif ($retour == "array") {
            return array(0, 0, 0, $technologies_donnees[$branche][$idTechnologie][2], $technologies_donnees[$branche][$idTechnologie][3]/VITESSE);
        }
    }

    public static function metalTechnologie($branche, $idTechnologie, Planete $planete)
    {
        return Donnee::donneeTechnologie($branche, $idTechnologie, "metal", $planete);
    }

    public static function cristalTechnologie($branche, $idTechnologie, Planete $planete)
    {
        return Donnee::donneeTechnologie($branche, $idTechnologie, "cristal", $planete);
    }

    public static function hydrogeneTechnologie($branche, $idTechnologie, Planete $planete)
    {
        return Donnee::donneeTechnologie($branche, $idTechnologie, "hydrogene", $planete);
    }

    public static function creditsTechnologie($branche, $idTechnologie, Planete $planete)
    {
        return Donnee::donneeTechnologie($branche, $idTechnologie, "credits", $planete);
    }

    public static function tempsTechnologie($branche, $idTechnologie, Planete $planete)
    {
        return Donnee::donneeTechnologie($branche, $idTechnologie, "temps", $planete);
    }

    public static function idTechnologie($branche, $idTechnologie, Planete $planete)
    {
        return Donnee::donneeTechnologie($branche, $idTechnologie, "id", $planete);
    }

    public static function neededTechnologie($branche, $idTechnologie, Planete $planete)
    {
        $neededTechnologies = Donnee::donneeTechnologie($branche, $idTechnologie, "needed", $planete);
        if (empty($neededTechnologies)) {
            return true;
        }
        if (is_array($neededTechnologies)) {
            //TODO prévoir le besoin de plus d'une technologie pour la validation, dans une autre branche, batiment (?) ...
            die('TODO '.__LINE__.' in '.__FILE__);
        } else {
            if (((int)$planete->technologies[$branche]& $neededTechnologies) == $neededTechnologies) {
                return true;
            } else {
                return false;
            }
        }
    }
}
