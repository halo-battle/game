<?php
/***************************************************************************
 *                            class.flotteGroupee.php
 *                           -------------------------
 *   begin                : Samedi 21 mars 2009
 *   update               : Samedi 21 mars 2009
 *   email                : nemunaire@gmail.com
 *
 *
 ***************************************************************************/
class FlotteGroupee
{
    public $id_flotte = 0;
    public $nom;
    public $end_planete;
    public $time_end;
    public $end_type;
    public $ret_planete;
    public $flottes = array();
    public $mission;
    public $modifFlotte = array();

    /**
     * Constructeur
     * @param    int	$id	id de la flotte à importer
     * @param    bool	$verrou	Booléen disant si la classe doit obtenir préalablement un verrou pour la flotte.
     *
     * @return   void
     * @access   public
     */
    public function __construct($id = 0, $verrou = true)
    {
        if (!empty($id)) {
            global $table_flottes_preparation;
            global $spatialVAR, $ressoVAR;

            $id = intval($id);
            $bdd = new BDD();
            $flotte = $bdd->unique_query("SELECT * FROM $table_flottes_preparation WHERE id = $id;");
            $bdd->deconnexion();

            if (!empty($flotte)) {
                $this->id_flotte = $flotte["id"];
                $this->nom = $flotte["nom"];
                $this->end_planete = $flotte["end_planete"];
                $this->end_type = $flotte["end_type"];
                $this->time_end = $flotte["time_end"];
                $this->mission = $flotte["mission"];

                if (empty($this->flottes)) {
                    $this->flottes = array();
                } else {
                    $this->flottes = unserialize($flotte["flottes"]);
                }
            }
        }
    }


    public function load_planete()
    {
        if (is_numeric($this->end_planete) && !empty($this->end_planete)) {
            //On traite l'importation en fonction des types fornis
            if ($this->end_type == 0 || $this->end_type == 1) {
                global $planete;
                //Si la planète est la même que celle du joueur actuel, on l'utilise, sinon, on la crée
                if (SURFACE == "planete" && $planete->id == $this->end_planete) {
                    $this->end_planete = $planete;
                } else {
                    $this->end_planete = new Planete($this->end_planete);
                }
            } else {
                global $planete;
                //Si la planète est la même que celle du joueur actuel, on l'utilise, sinon, on la crée
                if (SURFACE == "asteroide" && $planete->id == $this->end_planete) {
                    $this->end_planete = $planete;
                } else {
                    $this->end_planete = new Asteroide($this->end_planete);
                }
            }
        }
    }

    public function addFlotte($start_planete, $vitesse, $vaisseaux, $contenu_metal, $contenu_cristal, $contenu_hydrogene)
    {
        $this->flottes[] = array($start_planete->id_user, $start_planete->id, $vitesse, $vaisseaux, $contenu_metal, $contenu_cristal, $contenu_hydrogene);
        addModifFlotte("flottes");
    }

    public function lancer()
    {
    }

    public function addModifFlotte($modif)
    {
        if (!in_array($modif, $this->modifFlotte)) {
            $this->modifFlotte[] = $modif;
        }
    }

    /**
     * Destructeur
     *
     * @return   void
     * @access   public
     */
    public function __destruct()
    {
        global $table_flottes;

        if ($this->modifFlotte === "DELETE") {
            $bdd = new BDD();
            $bdd->query("DELETE FROM $table_flottes WHERE id = ".$this->id_flotte.";");
            $bdd->deconnexion();
        } else {
            if (empty($this->id_flotte)) {
                if ($this->modifFlotte == "INSERT") {
                    $out1 = '';
                    $out2 = '';
                    global $spatialVAR;
                    foreach ($this->vaisseaux as $key => $vais) {
                        $out1 .= ', '.$spatialVAR[$key];
                        $out2 .= ', '.$vais;
                    }
                    $sql = "INSERT INTO $table_flottes (id_user, mission, start_time, start_type, start_planete, end_time, end_type, end_planete, vitesse, contenu_metal, contenu_cristal, contenu_hydrogene, tactique, nom, nb_vais$out1) VALUES ('".$this->start_planete->id_user."', '".$this->mission."', '".$this->start_time."', '".$this->start_type."', '".$this->start_planete->id."', '".$this->end_time."', '".$this->end_type."', '".$this->end_planete."', '".$this->vitesse."', '".$this->contenu[0]."', '".$this->contenu[1]."', '".$this->contenu[2]."', '".$this->tactique."', '".$this->nom."', ".$this->nb_vais."$out2);";
                    if (DEBUG) {
                        echo $sql;
                    }
                    $bdd = new BDD();
                    $bdd->query($sql);
                    $bdd->deconnexion();
                }
            } elseif (isset($this->modifFlotte[0])) {
                $out = array();
                $bdd = new BDD();
                foreach ($this->modifFlotte as $modif) {
                    if (!is_array($this->{$modif})) {
                        if (is_int($this->{$modif}) || is_float($this->{$modif})) {
                            $out[] .= $modif." = ".$this->{$modif};
                        } else {
                            $bdd->escape($this->{$modif});
                            $out[] .= $modif." = '".$this->{$modif}."'";
                        }
                    } else {
                        if ($modif == "contenu") {
                            $calc = "resso";
                        } elseif ($modif == "vaisseaux") {
                            $calc = "nomvaisn";
                        }

                        if (!isset(${$calc.'VAR'})) {
                            global ${$calc.'VAR'};
                        }

                        foreach (${$calc.'VAR'} as $key => $var) {
                            $bdd->escape($this->{$modif}[$key]);
                            $out[] = ${$calc.'VAR'}[$key]." = ".$this->{$modif}[$key];
                        }
                    }
                }

                if (!empty($out)) {
                    $sql = "UPDATE $table_flottes SET ".implode(', ', $out).", last = 0 WHERE id = ".$this->id_flotte.";";
                    if (DEBUG) {
                        var_dump($sql);
                    }
                    $bdd->query($sql);
                }

                $bdd->deconnexion();
            }
        }
    }
}
