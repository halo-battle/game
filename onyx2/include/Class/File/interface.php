<?php
if (!defined('ONYX')) {
    exit;
}

interface File
{
    public function objectInFile($object);
    public function nbObjectInFile($object);
    public function addObjet($object, $nombre, surface $planete);
    public function addDemolition($object, $nombre, surface $planete);
    public function delObjet($id, $nombre, $lieu, surface $planete);
    public function ready(surface $planete);

    public function printFile(surface $planete, $lieu = null);
}

class FileCommun
{
    protected $files = array();
    protected $times = array();
    protected $simultane;

    public function __construct()
    {
        $this->times[] = time();
        $this->simultane = 1;
    }

    public function refreshTime($lieu)
    {
        if (empty($this->files[$lieu]) || count($this->files[$lieu]) < 1) {
            $this->times[$lieu] = time();
        }
    }

    public function reajusteVacances($timelost)
    {
        foreach ($this->times as $key => $time) {
            $this->times[$key] += $timelost;
        }

        return serialize($this);
    }

    public function hasObject()
    {
        foreach ($this->files as $file) {
            if (count($file) > 0) {
                return true;
            }
        }

        return false;
    }

    public function checkMaxSize(surface $planete, $lieu = null)
    {
        $nombre = 0;
        if (isset($lieu)) {
            if (!empty($this->files[$lieu])) {
                $nombre = count($this->files[$lieu]);
            }
        } else {
            foreach ($this->files as $file) {
                $nombre += count($file);
            }
        }


        return ($nombre < dDonnees::tailleFile($planete));
    }

    public function findShorter()
    {
        $short = 0;
        $value = 99;
        foreach ($this->files as $key => $file) {
            if ($key >= $this->simultane) {
                break;
            }

            $count = count($file);
            if ($value > $count) {
                $value = $count;
                $short = $key;
            }
        }

        return $short;
    }

    public function findFirstKey($lieu)
    {
        foreach ($this->files[$lieu] as $key => $file) {
            return $key;
        }
    }


    public function DEBUG__print()
    {
        var_dump($this);
        var_dump($this->files);
    }

    public function DEBUG__setTime($lieu, $moins)
    {
        $this->times[$lieu] -= $moins;

        return $this->times[$lieu];
    }
}
