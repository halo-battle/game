<?php
/**
 * Classe Bourse par Pierre-Olivier MERCIER
 * Derni�re �dition le 21 Juillet 2008
 * Copyright Halo-Battle Tous droits r�serv�s
 */
class Bourse
{
    public $bd;

    public $id;
    public $nom;
    public $taxeA = 1.1;
    public $taxeV = 1.5;
    public $metal;
    public $cristal;
    public $actionsUser;
    public $user;

    /**
     * Constructor
     *
     * @param String $nom Nom de l'action
     * @param int $user ID du joueur � charger automatiquement
     *
     * @access protected
     */
    public function Bourse($nom = "", $user = 0)
    {
        global $var___db, $config;
        $db = new bdd();
        $db->connexion();
        $this->bd = $db;

        if (!empty($nom)) {
            $this->loadAction($nom, "id");
            if (!empty($user)) {
                $this->loadUser($user);
            }
        }
    }

    public function __destruct()
    {
        $this->bd->deconnexion();
    }

    public function loadAction($nom, $type = "nom")
    {
        global $table_bourse;
        $this->bd->escape($nom);
        $act = $this->bd->unique_query("SELECT * FROM $table_bourse WHERE $type = '$nom';");
        if ($act) {
            $this->id = $act['id'];
            $this->nom = $act['nom'];
            $this->metal = $act['metal'];
            $this->cristal = $act['cristal'];
        } else {
            erreur('Impossible de trouver cette action !', "red", '?p=bourse');
        }
    }

    public function loadUser($user, $type = "id")
    {
        global $table_user;
        $this->bd->escape($user);
        $act = $this->bd->unique_query("SELECT id, bourse FROM $table_user WHERE $type = '$user';");
        $this->user = $act['id'];
        $this->traitUser($act['bourse']);
    }

    public function traitUser($start)
    {
        $end = array();
        $start = explode(';', $start);
        $cnt = count($start);
        for ($i = 0; $i < $cnt; $i++) {
            $tmp = explode(':', $start[$i]);
            if (!empty($tmp[1])) {
                $end[$tmp[0]] = explode(',', $tmp[1]);
            } else {
                $end[$tmp[0]] = array();
            }
        }
        $this->actionsUser = $end;
    }

    public function prixAchat($nb)
    {
        return array(floor($this->metal * $nb * $this->taxeA), floor($this->cristal * $nb * $this->taxeA));
    }

    public function prixVente($nb)
    {
        if ($this->action() < $nb) {
            $nb = $this->action();
        }

        return array(floor($this->metal * $nb / $this->taxeV), floor($this->cristal * $nb / $this->taxeV));
    }

    public function addAction($nb)
    {
        $ret = array(floor($this->metal * $nb * $this->taxeA), floor($this->cristal * $nb * $this->taxeA));

        $this->metal *= pow(1.1, $nb);
        $this->cristal *= pow(1.1, $nb);

        for ($i = 0; $i < $nb; $i++) {
            $this->actionsUser[$this->id][] = time();
        }

        $this->maj();
        return $ret;
    }

    public function delAction($nb)
    {
        if ($this->action() < $nb) {
            $nb = $this->action();
        }

        $ret = array(floor($this->metal * $nb / $this->taxeV), floor($this->cristal * $nb / $this->taxeV));

        $this->metal /= pow(1.1, $nb);
        $this->cristal /= pow(1.1, $nb);

        for ($i = 0; $i < $nb; $i++) {
            unset($this->actionsUser[$this->id][$i]);
        }

        $this->maj();
        return $ret;
    }

    public function actionIn24Hours()
    {
        $nb = 0;
        if (isset($this->actionsUser[$this->id])) {
            $cnt = count($this->actionsUser[$this->id]);
            for ($i = 0; $i < $cnt; $i++) {
                if ($this->actionsUser[$this->id][$i] > time() - 86400) {
                    $nb++;
                }
            }
        }
        return $nb;
    }

    public function action()
    {
        if (isset($this->actionsUser[$this->id])) {
            return count($this->actionsUser[$this->id]);
        } else {
            return 0;
        }
    }

    public function maj()
    {
        $this->majBourse();
        $this->majUser();
        $this->fileSave();
    }

    public function majBourse()
    {
        global $table_bourse;

        $this->bd->query("UPDATE $table_bourse SET nom = '".$this->nom."', metal = '".$this->metal."', cristal = '".$this->cristal."' WHERE id = ".$this->id.";");
    }

    public function majUser()
    {
        global $table_user;

        $champ = '';
        foreach ($this->actionsUser as $key => $cell) {
            if (count($cell) > 0) {
                if (empty($champ)) {
                    $champ .= $key.':'.implode(',', $cell);
                } else {
                    $champ .= ';'.$key.':'.implode(',', $cell);
                }
            }
        }

        $this->bd->query("UPDATE $table_user SET bourse = '$champ' WHERE id = ".$this->user.";");
    }

    public function delUser($id = "")
    {
        if (!empty($id)) {
            $this->loadUser($id);
        }

        $champ = '';
        foreach ($this->actionsUser as $key => $cell) {
            $cnt = count($cell);
            if ($cnt > 0) {
                $this->loadAction($key, "id");
                $this->delAction($cnt);
            }
        }
    }


    public function fileSave()
    {
        $fichier = fopen(_FCORE."../game/cache/bourse/".$this->id.".".strftime('%Y%m%d').".bourse", 'a+');
        fwrite($fichier, time().';'.$this->metal.';'.$this->cristal."\n");
        fclose($fichier);
    }


    public function newGroupe($nom, $metal, $cristal, $description = "")
    {
        global $table_bourse;

        $this->bd->query("INSERT INTO $table_bourse (nom, metal, cristal, description) VALUES('$nom', '$metal', '$cristal', '$description');");
    }

    public function editGroupe($description)
    {
        //TODO toute cette fonction !!
    }
}
