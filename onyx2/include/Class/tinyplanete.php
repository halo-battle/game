<?php
class TinyPlanete
{
    public $id = 0;
    public $galaxie;
    public $ss;
    public $position;
    public $nom_planete;

    /**
     * Constructeur
     * @param    int	$id	id de la planète à importer
     *
     * @return   void
     * @access   public
     */
    public function __construct($id)
    {
        //Récupération du nom des tables utilisées et connexion à la base de données
        global $table_planete;
        $bdd = new bdd();

        //On traite le cas où l'on recoit l'ID ou les coordonnées de la planète
        if (is_numeric($id)) {
            $plan = $bdd->unique_query("SELECT  id, galaxie, ss, position, nom_planete  FROM $table_planete WHERE id = $id;");
            $bdd->deconnexion();
        } elseif (preg_match('#^\[?([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})\]?$#', $id, $position)) {
            $plan = $bdd->unique_query("SELECT  id, galaxie, ss, position, nom_planete  FROM $table_planete WHERE galaxie = ".$position[1]." AND ss = ".$position[2]." AND position = ".$position[3].";");
            $bdd->deconnexion();
        } else {
            trigger_error('Erreur #04 : Format de recherche de planete incorrect !', E_USER_ERROR);
        }

        if (!empty($plan)) {
            //Chargement des données depuis le résultat de la base de données
            $this->id = $plan["id"];
            $this->galaxie = $plan["galaxie"];
            $this->ss = $plan["ss"];
            $this->position = $plan["position"];
            $this->nom_planete = $plan["nom_planete"];
        }
    }
}
