<?php
/**
 * Class Rapport
 *
 * Génération et envoie de rapport de mission de flotte
 *
 */
class Rapport
{
    public $type = 0;
    public $var = array();
    public $utilA = 0;
    public $utilB = 0;
    public $timestamp = 0;

    public $titreA = "";
    public $titreB = "";
    public $rapportA = "";
    public $rapportB = "";

    public $table = "mail";

    /**
     * Constructor
     * @access protected
     */
    public function Rapport($type, $utilA, $utilB, $time)
    {
        $this->type = $type;
        $this->utilA = $utilA;
        $this->utilB = $utilB;
        $this->timestamp = $time;
    }

    public function addInfo($info, $id)
    {
        $this->var[$id] = $info;
    }

    public function sendRapport()
    {
        if (!empty($this->utilA) && !empty($this->rapportA)) {
            send_mp($this->utilA->id_user, $this->titreA, $this->rapportA, $this->timestamp);
        }
        if (!empty($this->utilB) && !empty($this->rapportB)) {
            send_mp($this->utilB->id_user, $this->titreB, $this->rapportB, $this->timestamp);
        }
    }

    public function send()
    {
        if ($this->type == '3') {
            $this->sendCombat();
        } elseif ($this->type == '1') {
            $this->sendTransport();
        } elseif ($this->type == '2') {
            $this->sendColonisation();
        } elseif ($this->type == '4') {
            $this->sendRecyclage();
        } elseif ($this->type == '5') {
            $this->sendEspionnage();
        } elseif ($this->type == '6') {
            $this->sendAlliance();
        } elseif ($this->type == '7') {
            $this->sendAlliance2();
        }
    }

    public function sendEspionnage()
    {
        global $LANG;

        //Différentiation entre les planètes et les astéroides
        if (!isset($this->var[0]->nom_planete)) {
            $this->titreA = "Rapport d'espionnage de ".$this->var[0]->nom_asteroide." [".$this->var[0]->galaxie.":".$this->var[0]->ss.":A".$this->var[0]->position."]";
            $this->rapportA = "<strong>Espionnage de ".$this->var[0]->pseudo." sur ".$this->var[0]->nom_asteroide."[".$this->var[0]->galaxie.":".$this->var[0]->ss.":A".$this->var[0]->position."]</strong><br /><br />";
        } else {
            $this->titreA = "Rapport d'espionnage de ".$this->var[0]->nom_planete." [".$this->var[0]->galaxie.":".$this->var[0]->ss.":".$this->var[0]->position."]";
            $this->rapportA = "<strong>Espionnage de ".$this->var[0]->pseudo." sur ".$this->var[0]->nom_planete."[".$this->var[0]->galaxie.":".$this->var[0]->ss.":".$this->var[0]->position."]</strong><br /><br />";
        }

        if ($this->var[2] < 1) {
            $this->rapportA .= "<em>Nos sondes n'ont pas pu récolter d'informations sur cette planète.</em>";
        } else {
            //Ressources
            if ($this->var[2] > 1) {
                $this->rapportA .= "<table style=\"margin: auto;\"><tr><th colspan=\"2\">Ressources sur la planète :</th></tr><tr><td>".$LANG[$this->var[0]->race]["ressources"]["noms"]["metal"]." :</td><td>".$this->var[0]->metal."</td></tr><tr><td>".$LANG[$this->var[0]->race]["ressources"]["noms"]["cristal"]." :</td><td>".$this->var[0]->cristal."</td></tr><tr><td>".$LANG[$this->var[0]->race]["ressources"]["noms"]["hydrogene"]." :</td><td>".$this->var[0]->hydrogene."</td></tr></table><br />";
            }

            //Bâtiments
            if ($this->var[2] > 2) {
                $this->rapportA .= "<table style=\"margin: auto;\"><tr><th>Niveau</th><th>B&acirc;timent</th></tr>";
                foreach ($this->var[0]->batiments as $key => $batiment) {
                    $this->rapportA .= "<tr><td>".rand(($batiment - $this->var[1]), ($batiment + $this->var[1]))."</td><td>".$LANG[$this->var[0]->race]["batiments"]["noms_sing"][$key]."</td></tr>";
                }
                $this->rapportA .= "</table><br />";
            }

            //Flottes au sol
            if ($this->var[2] > 4) {
                $this->rapportA .= "<table style=\"margin: auto;\"><tr><th>Nombre</th><th>Vaisseaux</th></tr>";
                foreach ($this->var[0]->vaisseaux as $key => $vaisseau) {
                    $this->rapportA .= "<tr><td>".rand(($vaisseau - $this->var[1]), ($vaisseau + $this->var[1]))."</td><td>".$LANG[$this->var[0]->race]["vaisseaux"]["noms_sing"][$key]."</td></tr>";
                }
                $this->rapportA .= "</table><br />";
            }

            //Défenses
            if ($this->var[2] > 3) {
                $this->rapportA .= "<table style=\"margin: auto;\"><tr><th>Nombre</th><th>Défenses</th></tr>";
                foreach ($this->var[0]->terrestres as $key => $unite) {
                    if (!dTerrestre::type($key, $this->var[0])) {
                        $this->rapportA .= "<tr><td>".rand($unite * floor(1 - $this->var[1]), $unite * ceil(1 + $this->var[1]))."</td><td>".$LANG[$this->var[0]->race]["terrestre"]["noms_sing"][$key]."</td></tr>";
                    }
                }
                $this->rapportA .= "</table><br />";
            }
        }

        //On envoit le rapport d'espionnage
        $temps = $this->timestamp;
        $db = new BDD();
        $db->escape($this->titreA);
        $db->escape($this->rapportA);

        $db->query("INSERT INTO ".$this->table." (destinataire, sujet, contenu, temps) VALUES(".$this->utilA->id_user.", '$this->titreA', '$this->rapportA', '$temps');");
        $db->deconnexion();

        //On envoit le rapport de cdontre-espionnage
        if ($this->var[3] >= 1) {
            $this->titreB = "Rapport de contre-espionnage";
            if (!isset($this->var[0]->nom_planete)) {
                $this->rapportB = "Nous venons d'apprendre que notre planète : ".$this->var[0]->nom_asteroide."[".$this->var[0]->galaxie.":".$this->var[0]->ss.":A] a été la cible d'un espionnage de la part de ".$this->utilA->pseudo;
            } else {
                $this->rapportB = "Nous venons d'apprendre que notre planète : ".$this->var[0]->nom_planete."[".$this->var[0]->galaxie.":".$this->var[0]->ss.":".$this->var[0]->position."] a été la cible d'un espionnage de la part de ".$this->utilA->pseudo;
            }

            $temps = $this->timestamp;
            $db = new BDD();
            $db->escape($this->titreB);
            $db->escape($this->rapportB);

            $db->query("INSERT INTO ".$this->table." (destinataire, sujet, contenu, temps) VALUES(".$this->utilB->id_user.", '$this->titreB', '$this->rapportB', '$temps');");
            $db->deconnexion();
        }
    }

    public function sendTransport()
    {
        global $LANG;
        if (!isset($this->var[0]->nom_planete)) {
            $titre = 'Transport vers '.$this->var[0]->nom_asteroide.' ['.$this->var[0]->galaxie.':'.$this->var[0]->ss.':A]';
            $this->rapportA = 'Vos vaisseaux ont déposé '.$this->var[1][0].' de '.$LANG[$this->utilA->race]["ressources"]["noms"]["metal"].', '.$this->var[1][1].' de '.$LANG[$this->utilA->race]["ressources"]["noms"]["cristal"].' et '.$this->var[1][2].' d\''.$LANG[$this->utilA->race]["ressources"]["noms"]["hydrogene"].' sur '.$this->var[0]->nom_asteroide.'['.$this->var[0]->galaxie.':'.$this->var[0]->ss.':A]</b><br /><br />';
            $this->rapportB = 'Les vaisseaux de '.$this->utilA->pseudo.' ont déposé '.$this->var[1][0].' de '.$LANG[$this->utilB->race]["ressources"]["noms"]["metal"].', '.$this->var[1][1].' de '.$LANG[$this->utilB->race]["ressources"]["noms"]["cristal"].' et '.$this->var[1][2].' d\''.$LANG[$this->utilB->race]["ressources"]["noms"]["hydrogene"].' sur '.$this->var[0]->nom_asteroide.'['.$this->var[0]->galaxie.':'.$this->var[0]->ss.':A]</b><br /><br />';
        } else {
            $titre = 'Transport vers '.$this->var[0]->nom_planete.' ['.$this->var[0]->galaxie.':'.$this->var[0]->ss.':'.$this->var[0]->position.']';
            $this->rapportA = 'Vos vaisseaux ont déposé '.$this->var[1][0].' de '.$LANG[$this->utilA->race]["ressources"]["noms"]["metal"].', '.$this->var[1][1].' de '.$LANG[$this->utilA->race]["ressources"]["noms"]["cristal"].' et '.$this->var[1][2].' d\''.$LANG[$this->utilA->race]["ressources"]["noms"]["hydrogene"].' sur '.$this->var[0]->nom_planete.'['.$this->var[0]->galaxie.':'.$this->var[0]->ss.':'.$this->var[0]->position.']</b><br /><br />';
            $this->rapportB = 'Les vaisseaux de '.$this->utilA->pseudo.' ont déposé '.$this->var[1][0].' de '.$LANG[$this->utilB->race]["ressources"]["noms"]["metal"].', '.$this->var[1][1].' de '.$LANG[$this->utilB->race]["ressources"]["noms"]["cristal"].' et '.$this->var[1][2].' d\''.$LANG[$this->utilB->race]["ressources"]["noms"]["hydrogene"].' sur '.$this->var[0]->nom_planete.'['.$this->var[0]->galaxie.':'.$this->var[0]->ss.':'.$this->var[0]->position.']</b><br /><br />';
        }

        $temps = $this->timestamp;
        $db = new BDD();
        $db->escape($titre);
        $db->escape($this->rapportA);
        $db->escape($this->rapportB);

        $db->query("INSERT INTO ".$this->table." (destinataire, sujet, contenu, temps) VALUES(".$this->utilA->id_user.", '$titre', '$this->rapportA', '$temps');");
        if ($this->utilA->id_user != $this->utilB->id_user) {
            $db->query("INSERT INTO ".$this->table." (destinataire, sujet, contenu, temps) VALUES(".$this->utilB->id_user.", '$titre', '$this->rapportB', '$temps');");
        }
        $db->deconnexion();
    }

    public function sendColonisation()
    {
        $titre = 'Colonisation de ['.$this->var[0][0].':'.$this->var[0][1].':'.$this->var[0][2].']';
        if (is_numeric($this->var[0][2])) {
            if ($this->var[1]) {
                $rapport = 'Votre vaisseau a atteint la planète ['.$this->var[0][0].':'.$this->var[0][1].':'.$this->var[0][2].'] et commence la colonisation.';
            } else {
                $rapport = 'Nous n\'avons pas pu coloniser la planète ['.$this->var[0][0].':'.$this->var[0][1].':'.$this->var[0][2].'] car lorsque nous sommes arrivé sur place, elle était déjà colonisée.';
            }
        } else {
            if ($this->var[1]) {
                $rapport = 'Votre vaisseau a atteint l\'astéroide ['.$this->var[0][0].':'.$this->var[0][1].':'.$this->var[0][2].'] et commence la colonisation.';
            } else {
                $rapport = 'Nous n\'avons pas pu coloniser la planète ['.$this->var[0][0].':'.$this->var[0][1].':'.$this->var[0][2].'] car lorsque nous sommes arrivé sur place, elle était déjà colonisée.';
            }
        }

        $temps = $this->timestamp;
        $db = new BDD();
        $db->escape($titre);
        $db->escape($rapport);
        $db->query("INSERT INTO ".$this->table." (destinataire, sujet, contenu, temps) VALUES(".$this->utilA->id_user.", '$titre', '$rapport', '$temps');");
        $db->deconnexion();
    }

    public function sendRecyclage()
    {
        global $LANG;
        if (!isset($this->var[0]->nom_planete)) {
            $titre = 'Recyclage de '.$this->var[0]->nom_asteroide.' ['.$this->var[0]->galaxie.':'.$this->var[0]->ss.':A]';
            $rapport = 'Vos vaisseaux récoltent '.$this->var[1][0].' de '.$LANG[$this->utilA->race]["ressources"]["noms"]["metal"].' et '.$this->var[1][1].' de '.$LANG[$this->utilA->race]["ressources"]["noms"]["cristal"].' sur '.$this->var[0]->nom_asteroide.'['.$this->var[0]->galaxie.':'.$this->var[0]->ss.':A]</b><br /><br />';
        } else {
            $titre = 'Recyclage de '.$this->var[0]->nom_planete.' ['.$this->var[0]->galaxie.':'.$this->var[0]->ss.':'.$this->var[0]->position.']';
            $rapport = 'Vos vaisseaux récoltent '.$this->var[1][0].' de '.$LANG[$this->utilA->race]["ressources"]["noms"]["metal"].' et '.$this->var[1][1].' de '.$LANG[$this->utilA->race]["ressources"]["noms"]["cristal"].' sur '.$this->var[0]->nom_planete.'['.$this->var[0]->galaxie.':'.$this->var[0]->ss.':'.$this->var[0]->position.']</b><br /><br />';
        }

        $temps = $this->timestamp;

        $db = new BDD();
        $db->escape($titre);
        $db->escape($rapport);
        $db->query("INSERT INTO ".$this->table." (destinataire, sujet, contenu, temps) VALUES(".$this->utilA->id_user.", '$titre', '$rapport', '$temps');");
        $db->deconnexion();
    }

    public function sendCombat()
    {
        include(_FCORE."../game/vars.php");
        include_once(_FCORE."../game/function.php");
        require_once(SMARTY_DIR."Smarty.class.php");

        $this->rapportA = new Smarty();
        $this->rapportB = new Smarty();

        $this->rapportA->template_dir = _FCORE.'templates/templates/';
        $this->rapportA->compile_dir =  _FCORE.'templates/templates_c/';
        $this->rapportA->config_dir =  _FCORE.'templates/configs/';
        $this->rapportA->cache_dir =  _FCORE.'templates/cache/';

        $this->rapportB->template_dir = _FCORE.'templates/templates/';
        $this->rapportB->compile_dir =  _FCORE.'templates/templates_c/';
        $this->rapportB->config_dir =  _FCORE.'templates/configs/';
        $this->rapportB->cache_dir =  _FCORE.'templates/cache/';

        $this->var[4]['pseudo'] = trouvNom($this->var[4]['id_user']);
        $this->var[5]['pseudo'] = trouvNom($this->var[5]['id_user']);

        $this->rapportA->assign('tour', $this->var[3]);
        $this->rapportA->assign('EN', $this->var[4]);
        $this->rapportA->assign('flotte', $this->var[5]);
        $this->rapportA->assign('vaisseaux1', $this->var[0]);
        $this->rapportA->assign('vaisseaux2', $this->var[1]);
        $this->rapportA->assign('defenses1', $this->var[2]);
        $this->rapportA->assign('vaisseaux3', $this->var[7]);
        $this->rapportA->assign('vaisseaux4', $this->var[8]);
        $this->rapportA->assign('defenses2', $this->var[9]);

        $this->rapportA->assign('termine', $this->var[6][0]);
        $this->rapportA->assign('attaquantG', $this->var[6][1]);
        $this->rapportA->assign('pillage', $this->var[11]);

        $this->rapportA->assign('vaisBC', $nomvais_bc);
        $this->rapportA->assign('vaisPV', $nomvais_pv);
        $this->rapportA->assign('defBC', $defense_bc);
        $this->rapportA->assign('defPV', $defense_pv);
        $this->rapportA->assign('nextTour', $this->var[10]);

        $race = trouvInfo($this->utilA, 'race');
        include(_FCORE."../game/noms.php");
        $this->rapportA->assign('ressources', $ressourc);
        $this->rapportA->assign('nomvaisAT', $nomvaisa);

        $race = trouvInfo($this->var[4]['id_user'], 'race');
        include(_FCORE."../game/noms.php");
        $this->rapportA->assign('nomvaisEN', $nomvaisa);
        array_splice($nomterra, 0, 8);
        $this->rapportA->assign('nomdefEN', $nomterra);

        $this->rapportA = $this->rapportA->fetch('game/ATrapport_combat.tpl');


        $this->rapportB->assign('tour', $this->var[3]);
        $this->rapportB->assign('EN', $this->var[4]);
        $this->rapportB->assign('flotte', $this->var[5]);
        $this->rapportB->assign('vaisseaux1', $this->var[0]);
        $this->rapportB->assign('vaisseaux2', $this->var[1]);
        $this->rapportB->assign('defenses1', $this->var[2]);
        $this->rapportB->assign('vaisseaux3', $this->var[7]);
        $this->rapportB->assign('vaisseaux4', $this->var[8]);
        $this->rapportB->assign('defenses2', $this->var[9]);

        $this->rapportB->assign('termine', $this->var[6][0]);
        $this->rapportB->assign('attaquantG', $this->var[6][1]);
        $this->rapportB->assign('matchnul', $this->var[6][2]);
        $this->rapportB->assign('pillage', $this->var[11]);

        $this->rapportB->assign('vaisBC', $nomvais_bc);
        $this->rapportB->assign('vaisPV', $nomvais_pv);
        $this->rapportB->assign('defBC', $defense_bc);
        $this->rapportB->assign('defPV', $defense_pv);
        $this->rapportB->assign('nextTour', $this->var[10]);

        $race = trouvInfo($this->utilA, 'race');
        include(_FCORE."../game/noms.php");
        $this->rapportB->assign('ressources', $ressourc);
        $this->rapportB->assign('nomvaisAT', $nomvaisa);

        $race = trouvInfo($this->var[4]['id_user'], 'race');
        include(_FCORE."../game/noms.php");
        $this->rapportB->assign('nomvaisEN', $nomvaisa);
        array_splice($nomterra, 0, 8);
        $this->rapportB->assign('nomdefEN', $nomterra);

        $this->rapportB = $this->rapportB->fetch('game/ENrapport_combat.tpl');


        $this->titreA = 'Combat contre '.$this->var[4]['pseudo'];
        $this->titreB = 'Combat contre '.$this->var[5]['pseudo'];
        $temps = $this->timestamp;
        $db = new bdd();
        $db->connexion();
        $db->escape($this->titreA);
        $db->escape($this->titreB);
        $db->escape($this->rapportA);
        $db->escape($this->rapportB);
        $db->query("INSERT INTO ".$this->table." (destinataire, sujet, contenu, temps) VALUES(".$this->utilA->id_user.", '$this->titreA', '$this->rapportA', '$temps')");
        $db->query("INSERT INTO ".$this->table." (destinataire, sujet, contenu, temps) VALUES(".$this->utilB->id_user."', '$this->titreB', '$this->rapportB', '$temps')");
        $db->deconnexion();
    }

    public function show()
    {
        include(_FCORE."../game/vars.php");
        include_once(_FCORE."../game/function.php");
        require_once(SMARTY_DIR."Smarty.class.php");

        $this->rapportA = new Smarty();

        $this->rapportA->template_dir = _FCORE.'templates/templates/';
        $this->rapportA->compile_dir =  _FCORE.'templates/templates_c/';
        $this->rapportA->config_dir =  _FCORE.'templates/configs/';
        $this->rapportA->cache_dir =  _FCORE.'templates/cache/';

        $this->rapportA->assign('tour', $this->var[3]);
        $this->rapportA->assign('EN', $this->var[4]);
        $this->rapportA->assign('flotte', $this->var[5]);
        $this->rapportA->assign('vaisseaux1', $this->var[0]);
        $this->rapportA->assign('vaisseaux2', $this->var[1]);
        $this->rapportA->assign('defenses1', $this->var[2]);
        $this->rapportA->assign('vaisseaux3', $this->var[7]);
        $this->rapportA->assign('vaisseaux4', $this->var[8]);
        $this->rapportA->assign('defenses2', $this->var[9]);

        $this->rapportA->assign('termine', $this->var[6][0]);
        $this->rapportA->assign('attaquantG', $this->var[6][1]);
        $this->rapportA->assign('pillage', $this->var[11]);
        $this->rapportA->assign('debris', $this->var[12]);
        $this->rapportA->assign('infoPLUS', $this->var[14]);
        //$this->rapportA->assign('infoPLUS2', $this->var[15]);

        $this->rapportA->assign('page', 'simulation');
        $this->rapportA->assign('enligne', $this->var[13][0]);
        $this->rapportA->assign('infos', $this->var[13][1]);
        $this->rapportA->assign('nbinfos', $this->var[13][2]);
        $this->rapportA->assign('count', $this->var[13][3]);
        $this->rapportA->assign('version', $this->var[13][4]);
        $this->rapportA->assign('tpsdejeu', $this->var[13][5]);

        $this->rapportA->assign('vaisBC', $nomvais_bc);
        $this->rapportA->assign('vaisPV', $nomvais_pv);
        $this->rapportA->assign('defBC', $defense_bc);
        $this->rapportA->assign('defPV', $defense_pv);
        $this->rapportA->assign('nextTour', $this->var[10]);

        $race = trouvInfo($this->utilA, 'race');
        include(_FCORE."../game/noms.php");
        $this->rapportA->assign('ressources', $ressourc);
        $this->rapportA->assign('nomvaisAT', $nomvaisa);
        $this->rapportA->assign('nomvaisEN', $nomvaisa);
        array_splice($nomterra, 0, 8);
        $this->rapportA->assign('nomdefEN', $nomterra);
        $this->rapportA->assign('race', $race);

        return $this->rapportA->fetch('game/SIMrapport_combat.tpl');
        return $this->rapportA;
    }

    public function sendAlliance()
    {
        $this->titreA = 'Déclaration officielle de votre alliance !';
        $this->rapportA = 'Félicitations, votre alliance a recueilli suffisament de signature, sa déclaration est maintenant officielle !<br /><br />Vous pouvez dès maintenant administrer votre alliance en vous rendant sur la page Alliance.';

        $temps = $this->timestamp;

        $db = new BDD();
        $db->escape($this->titreA);
        $db->escape($this->rapportA);
        $db->query("INSERT INTO ".$this->table." (destinataire, sujet, contenu, temps) VALUES(".$this->utilA->id_user.", '$this->titreA', '$this->rapportA', '$temps')");
        $db->deconnexion();
    }

    public function sendAlliance2()
    {
        $this->titreA = 'Fondation de votre alliance !';
        $this->rapportA = 'Pour terminer la création de votre alliance, trouvez 4 joueurs de cette galaxie sans alliance pour leur faire signer votre traité de fondation d\'alliance.<br /><br />Lien de signature : <a href="?p=alliances&amp;q=signer&amp;i='.$this->var[0].'">http://'.$_SERVER['HTTP_HOST'].'/?p=alliances&amp;q=signer&amp;i='.$this->var[0].'</a>';

        $temps = $this->timestamp;

        $db = new BDD();
        $db->escape($this->titreA);
        $db->escape($this->rapportA);
        $db->query("INSERT INTO ".$this->table." (destinataire, sujet, contenu, temps) VALUES(".$this->utilA->id_user.", '$this->titreA', '$this->rapportA', '$temps')");
        $db->deconnexion();
    }
}
