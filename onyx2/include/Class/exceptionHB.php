<?php
/***************************************************************************
 *                            class.exceptionHB.php
 *                           -----------------------
 *   begin                : Lundi 9 février 2009
 *   update               : Vendredi 27 février 2009
 *   email                : nemunaire@gmail.com
 *
 *
 ***************************************************************************/

class ExceptionHB extends Exception
{
    public function __construct($branche, $code = 0, $debug = true, $gerer = false)
    {
        if ($gerer) {
            parent::__construct($branche, $code);
        } else {
            if (is_numeric($branche)) {
                switch ($branche) {
                    case 1:
                        switch ($code) {
                            case 0:
                                $message = "La planète est pleine, vous ne pouvez plus construire de batiment dessus !";
                                break;
                            case 1:
                                $message = "La file d'attente est pleine, vous ne pouvez pas rajouter plus de batiments.";
                                break;
                            case 2:
                                $message = "Vous n'avez pas les bâtiments et/ou technologies nécessaires pour construire ce bâtiment.";
                                break;
                            case 3:
                                $message = "Le bâtiment dont vous demandez la construction est actuellement en démolition. Annulez la démolition pour lui ajouter l'aggrandir.";
                                break;
                            case 4:
                                $message = "Vous n'avez pas les ressources nécessaire pour construire ce bâtiment !";
                                break;

                            case 5:
                                $message = "Impossible d'annuler la construction de ce bâtiment, il n'a pas été trouvé dans la file !";
                                break;

                            case 6:
                                $message = "Vous ne pouvez pas démolir ce batiment, il n'est pas encore construit !";
                                break;
                            case 7:
                                $message = "Le bâtiment dont vous demandez la démolition est actuellement en travaux. Annulez les travaux en cours pour pouvoir le démolir.";
                                break;
                        }
                        break;
                    case 2:
                        switch ($code) {
                            case 0:
                                $message = "Cette technologie est déjà en file d'attente, soyez patient !";
                                break;
                            case 1:
                                $message = "La file d'attente est pleine, vous ne pouvez pas rajouter plus de technologies.";
                                break;
                            case 2:
                                $message = "Vous n'avez pas les bâtiments et/ou technologies nécessaires pour rechercher cette technologie.";
                                break;
                            case 3:
                                $message = "Vous possédez déjà cette technologie !";
                                break;
                            case 4:
                                $message = "Vous n'avez pas les crédits ou ressources nécessaires pour rechercher cette technologie !";
                                break;

                            case 5:
                                $message = "Impossible d'annuler la recherche de cette technologie, elle n'a pas été trouvée dans la file !";
                                break;
                        }
                        break;
                    case 3:
                        switch ($code) {
                            case 1:
                                $message = "La file d'attente est pleine, vous ne pouvez pas rajouter plus d'unités.";
                                break;
                            case 2:
                                $message = "Vous n'avez pas les bâtiments et/ou technologies nécessaires pour entraîner ces unités.";
                                break;
                            case 3:
                                $message = "L'unité dont vous demandez l'entraînement est actuellement en démentellement. Annulez le démentellement pour l'entraîner de nouveau.";
                                break;
                            case 4:
                                $message = "Vous n'avez pas les ressources nécessaire pour entraîner cette unité !";
                                break;

                            case 5:
                                $message = "Impossible d'annuler l'entraînement de cette unité, elle n'a pas été trouvé dans la file !";
                                break;

                            case 6:
                                $message = "Vous ne pouvez pas démenteler autant d'unités !";
                                break;
                            case 7:
                                $message = "L'unité dont vous demandez le démentellement est actuellement en entraînement. Annulez l'entraînement en cours pour pouvoir la démenteller.";
                                break;

                            case 8:
                                $message = "Dépassement de capacité.<br />Vous ne pouvez pas demander la construction d'autant d'unités en même temps.";
                                break;
                        }
                        break;
                    case 4:
                        switch ($code) {
                            case 1:
                                $message = "La file d'attente est pleine, vous ne pouvez pas rajouter plus de recherches.";
                                break;
                            case 2:
                                $message = "Vous n'avez pas les bâtiments et/ou technologies nécessaires pour rechercher cette technologie.";
                                break;
                            case 4:
                                $message = "Vous n'avez pas les ressources nécessaire pour rechercher cette technologie !";
                                break;

                            case 5:
                                $message = "Impossible d'annuler la recherche de cette technologie, elle n'a pas été trouvé dans la file !";
                                break;

                            case 6:
                                $message = "Il n'est pas possible d'annuler une technologie connue";
                                break;
                            case 7:
                                $message = "Vous recherchez déjà cette technologie !";
                                break;
                        }
                        break;
                }
            } else {
                $message = $branche;
            }

            if ($debug || empty($message)) {
                $message = "Erreur #".intval($branche)."/".$code." :<br />".$message;
            }

            global $template, $page;
            if (!empty($page)) {
                $template->assign('page', $page);
            }
            $template->assign('message', $message);
            $template->assign('couleur', 'red');
            $template->display('game/erreur.tpl');
            exit;
        }
    }
}
