<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$titre = 'Aide';

if (empty($_GET['q'])) {
    $_GET['q'] = '';
}

switch ($_GET['q']) {
    case '1':
        $page = '../aide/avertissements'; break;
    case '2':
        $page = '../aide/config'; break;
    case '3':
        $page = '../aide/intro'; break;
    case '4':
        $page = '../aide/commentjouer'; break;
    default:
        $page = '../aide/sommaire';
}
