<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
require_once("Class/phpmailer.php");
$page = 'inscription';

$erreurs = '';
$inscriptOk = true;
if (isset($_POST['HB_pseudo']) && isset($_POST['HB_mdp']) && isset($_POST['HB_conf']) && isset($_POST['HB_mail']) && isset($_POST['servers']) && isset($_POST['HB_captcha']) && isset($_POST['HB_placement'])) {
    $pseudo = htmlentities($_POST['HB_pseudo']);
    $mdp = $_POST['HB_mdp'];
    $codeact = strtolower(gpc('codeact', 'post'));
    $mdpconf = $_POST['HB_conf'];
    $mail = htmlentities($_POST['HB_mail']);
    if (!isset($_POST['race'])) {
        $_POST['race'] = '';
    }
    $race = htmlentities($_POST['race']);
    $server = htmlentities($_POST['servers']);
    $captcha = htmlentities($_POST['HB_captcha']);
    $placement = htmlentities($_POST['HB_placement']);
    if (!isset($_POST['regles'])) {
        $_POST['regles'] = '';
    }
    $regles = htmlentities($_POST['regles']);

    $base = new BDD();
    $base->escape($pseudo);
    $base->escape($codeact);
    $base->escape($mail);
    $base->escape($race);
    $base->escape($server);
    $base->escape($captcha);
    $base->escape($placement);
    $base->escape($regles);

    //Béta et code d'activation
    if (false) {
        $reponsecds = $base->unique_query("SELECT * FROM cds_beta WHERE code LIKE '$codeact' AND utilise = 0;");
        if (empty($reponsecds)) {
            $inscriptOk = false;
            $erreurs .= '<p style="color: #FF0000"><b>Le code d\'activation est incorrect ou est déjà utilisé !</p><br />';
        }
    }

    //Vérification relative au pseudo : caractéres utilisés + disponibilité
    if (addslashes(preg_replace('@[^a-zA-Z0-9_]@i', '', $pseudo)) != $pseudo || $pseudo == '') {
        $inscriptOk = false;
        $erreurs .= '<p style="color: #FF0000"><b>Le pseudo contient des caractères incorrects.</b><br />Les caractères autorisés sont les lettres minuscules ou majuscules de A à Z sans les accents, ainsi que les chiffres et les caractères . _</p><br />';
    } else {
        $result = $base->query("SELECT pseudo FROM user_inscriptions WHERE pseudo = '$pseudo';");
        if ($base->num_rows >= 1) {
            $inscriptOk = false;
            $erreurs .= '<p style="color: #FF0000"><b>Le pseudo est déjà utilisé par un joueur.</b><br />Vous devez choisir un autre pseudo.</p><br />';
        }
    }

    //Vérification du mot de passe :
    if ($mdp == '' || $mdp != $mdpconf) {
        $inscriptOk = false;
        $erreurs .= '<p style="color: #FF0000"><b>Mots de passe différents ou ne comportant pas assez de caractères</b><br />Vous devez impérativement choisir un mot de passe pour sécuriser votre compte. Si vous avez spécifié un mot de passe, il se peut que vous n\'ayez pas indiqué le même mot de passe dans la case de confirmation.</p><br />';
    }
    if (strlen($mdp) < 6) { //Longueur minimale du mdp
        $inscriptOk = false;
        $erreurs .= '<p style="color: #FF0000"><b>Mot de passe trop court</b><br />Votre mot de passe doit contenir au minimum 6 caractères</p><br />';
    }

    //Vérification de la longueur de nom
    if (strlen($pseudo) < 3) {
        $inscriptOk = false;
        $erreurs .= '<p style="color: #FF0000"><b>Pseudo trop court</b><br />Votre pseudo doit contenir au minimum 4 caractères</p><br />';
    } elseif (strlen($pseudo) > 20) {
        $inscriptOk = false;
        $erreurs .= '<p style="color: #FF0000"><b>Pseudo trop long</b><br />Votre pseudo doit contenir au maximum 19 caractères</p><br />';
    }

    //Vérification relative au mail : caractéres utilisés + disponibilité
    if (!preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i', $mail) || empty($mail)) {
        $inscriptOk = false;
        $erreurs .= '<p style="color: #FF0000"><b>L\'adresse électronique n\'est pas valide.</b><br />L\'adresse électronique que vous avez tappé est invalide ou contient des caractères interdits, recommencez.</p><br />';
    } else {
        $result = $base->query("SELECT mail FROM user_inscriptions WHERE mail = '$mail';");
        if ($base->num_rows >= 1) {
            $inscriptOk = false;
            $erreurs .= '<p style="color: #FF0000"><b>L\'adresse électronique que vous avez indiqué est déjà associée à un compte.</b><br>Vous devez choisir une autre adresse électronique.</p><br />';
        }
    }

    //Vérification de la race choisie
    if ($race != 'covenant' && $race != 'humain') {
        $inscriptOk = false;
        $erreurs .= '<p style="color: #FF0000"><b>Aucune race choisie</b><br />Vous devez choisir la race que vous désirez incarner dans le jeu.</p><br />';
    }

    //Vérification du serveur
    if (is_numeric($server) && $server > 1) {
        $inscriptOk = false;
        $erreurs .= '<p style="color: #FF0000"><b>Galaxie incorrecte</b><br />Vous devez choisir une galaxie (un serveur) dans la liste ci-dessous pour pouvoir jouer.</p><br />';
    }

    //Vérification du partenaire
    if (!empty($placement)) {
        $result = $base->unique_query("SELECT pseudo FROM user_inscriptions WHERE pseudo = '$placement';");
        if (!$result) {
            $inscriptOk = false;
            $erreurs .= '<p style="color: #FF0000"><b>Impossible de trouver le joueur ami.</b><br />Vous devez choisir un autre pseudo.</p><br />';
        }
    }

    //Vérification du captcha
    if (empty($SESS->values['_captcha']) || empty($captcha) || $SESS->values['_captcha'] != strtolower($captcha)) {
        $inscriptOk = false;
        $erreurs .= '<p style="color: #FF0000"><b>Code de vérification incorrect :</b><br />Le texte que vous avez recopier ne correspondait pas au texte de l\'image, veuillez recommencer.</p><br />';
    }

    //Vérification du captcha
    if (empty($regles) || $regles != '1') {
        $inscriptOk = false;
        $erreurs .= '<p style="color: #FF0000"><b>Vous devez accepter les conditions générales</b><br />';
    }

    if ($inscriptOk) {
        $time = time();
        $ip = $_SERVER["REMOTE_ADDR"];

        // Activer la ligne suivante pour crypter les mots de passe dans la base de données
        //$mdp = sha1(strtoupper($pseudo).':'.$mdp); // Cryptage du mot de passe pour éviter le piratage de compte
        $mdp = cxor($mdp, sha1($pseudo.'£'.$race));

        $id_activ = sha1('H'.rand().'/|\\'.rand().'B£');
        $Nmail = $mail;

        //On envoie le mail de confirmation
        $mail = new PHPMailer();
        $mail->SetLanguage('fr', ONYX."/include/Class/");
        $mail->IsSMTP();
        $mail->IsHTML(true);
        $mail->Host='thot.pomail.fr';
        $mail->From='no-reply@halo-battle.fr';
        $mail->FromName='Halo-Battle';
        //$mail->SMTPAuth=true;
        //$mail->Username='no-reply@halo-battle.s-fr.com';
        //$mail->Password='hD3e2nXu';

        $mail->AddAddress($Nmail);
        $mail->AddReplyTo('no-reply@halo-battle.fr');
        $mail->Subject='Halo-Battle :: Activation de votre compte';
        $mail->Body='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><title>Halo-Battle :: Activation de votre compte</title></head><body><p>Bonjour '.$pseudo.',<br />Vous recevez ce mail suite &aacute; votre demande d\'inscription sur le jeu <a href="http://'.$_SERVER['HTTP_HOST'].'/">Halo-Battle</a>. Pour confirmer votre adresse mail et ainsi pouvoir acc&eacute;der au jeu, cliquez sur le lien ci-apr&egrave;s :<br /><a href="http://'.$_SERVER['HTTP_HOST'].'/?p=validation&i='.$id_activ.'">http://'.$_SERVER['HTTP_HOST'].'/?p=validation&i='.$id_activ.'</a><br /><br />A bient&ocirc;t dans Halo-Battle,<br />Le staff</p></body></html>';
        //$mail->Subject='Halo-Battle :: Pre-Inscription';
        //$mail->Body='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><title>Halo-Battle :: Pr&eacute;-Inscription</title></head><body><p>F&eacute;licitations '.$pseudo.' !<br />Votre compte vient d&#39;&ecirc;tre enregistr&eacute; sur notre serveur. Un mail de confirmation vous sera envoy&eacute; lors de la sortie du jeu en ligne Halo-Battle. Si vous ne recevez pas l\'email de confirmation, <a href="mailto:technique@halo-battle.s-fr.com">contactez un administrateur</a>.<br /><br />A tr&egrave;s bient&ocirc;t pour la sortie d&#39;Halo-Battle !</p></body></html>';

        if (DEV) {
            $ret = true;
        } else {
            $ret = $mail->Send();
        }

        if (!$ret) {
            $template->assign('message', 'Erreur lors de l\'envoie du courriel de confirmation !<br /><br /><small><i>'.$mail->ErrorInfo.'</i></small><br /><br />Si le problème perciste, <a href="mailto:technique@halo-battle.fr">contactez un administrateur</a>.');
            $template->assign('couleur', 'red');
            $template->display('cms/erreur.tpl');
            exit;
        } else {
            //Si on a un code d'activation, on l'annule
            if (!empty($reponsecds)) {
                $base->query("UPDATE cds_beta SET utilise = 1, user = '$pseudo' WHERE code = '$codeact';");
            }

            $base->query("INSERT INTO user_inscriptions (id_activ, pseudo, race, mdp, mail, last_ip, placement, time_inscription, last_visite) VALUES ('$id_activ', '$pseudo', '$race', '$mdp', '$Nmail', '$ip', '$placement', '$time', '$time');");
            $template->assign('message', 'F&eacute;licitations, votre compte vient d\'être cr&eacute;&eacute; sur le serveur '.$server.' de Halo-Battle.<br /><br />Un courriel de confirmation vient d\'être envoy&eacute; à votre adresse &eacute;lectronique afin de valider votre inscription au jeu en ligne Halo-Battle.<br /><br />N\'attendez-plus, commencez le combat dès à pr&eacute;sent !');
            $template->assign('couleur', 'green');
            $template->display('cms/erreur.tpl');
            exit;
        }
        $mail->SmtpClose();
        unset($mail);
    }
    $base->deconnexion();
}
$template->assign('erreurs', $erreurs);

$template->assign('listegalaxies_nom', $VAR['serveur_name']);
$template->assign('listegalaxies_type', "");
