<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}

$SESS->values['connected'] = false;
$SESS->close();

if (!empty($HB_login)) {
    redirection($_SERVER["REQUEST_URI"]);
    exit('HALO-BATTLE<br /><br />A bientôt !<br />See you soon !');
} else {
    redirection($VAR['first_page']);
    exit('HALO-BATTLE<br /><br />A bientôt !<br />See you soon !');
}
