<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}

if (!empty($_GET['cds']) && !empty($_GET['nom']) && !empty($_GET['race']) && !empty($_GET['mdp']) && !empty($_GET['mail']) && !empty($_GET['ti']) && isset($_GET['placement'])) {
    //Reconstitution du mot de passe
    $_GET['mdp'] = hexstr(gpc('mdp'));
    if ($_GET['cds'] == sha1(gpc('nom').'$'.gpc('race').'£'.gpc('mdp').'#'.gpc('mail').'ß'.gpc('ti').'Ó'.$_SERVER['HTTP_USER_AGENT'].'♀☻'.$_SERVER['REMOTE_ADDR'].gpc('placement'))) {
        $pseudo = gpc('nom');
        $race = gpc('race');
        $mdp = cxor(gpc('mdp'), sha1($pseudo.'£'.$race));
        $mdp = mdp($pseudo, $mdp);
        $alea = $mdp[1];
        $mdp = $mdp[0];
        $mail = gpc('mail');

        $bdd = new bdd();
        $bdd->escape($pseudo);
        $bdd->escape($mdp);
        $bdd->escape($race);
        $bdd->escape($mail);

        //On vérifie que le nom d'utilisateur ou l'adresse électronique n'existe pas déjà
        if ($bdd->query("SELECT id FROM $table_user WHERE pseudo = '$pseudo' OR mail = '$mail';")) {
            erreur("Votre compte est déjà créé sur ce serveur ou votre adresse électronique est déjà associée à un autre compte de se serveur !");
        }

        //Création de l'utilisateur
        $bdd->query("INSERT INTO $table_user (pseudo, mdp, mdp_var, race, mail, last_ip, time_inscription, credits) VALUES ('$pseudo', '$mdp', '$alea', '$race', '$mail', '".$_SERVER['REMOTE_ADDR']."', '".time()."', 1000);");

        $queryUser = $bdd->unique_query("SELECT id FROM $table_user WHERE pseudo = '$pseudo';");
        $bdd->deconnexion();
        if (empty($queryUser)) {
            erreur("Une erreur est survenue lors de la création de votre compte sur le serveur.<br /><br />Si le problème persiste, contactez un administrateur.");
        }

        //Création des bases de la planète
        require_once("Class/planete.php");
        $newPlanete = new Planete(false);

        //Recherche d'une planète disponible
        if (empty($_GET['placement']) || !empty($SESS->values['forceInscript'])) {
            $bdd->reconnexion();
            while (true) {
                $galaxie = mt_rand(1, $VAR['nb_amas']);
                $ss = mt_rand(1, $VAR['nb_systeme']);
                $pos = mt_rand(1, $VAR['nb_planete']);

                //Test pour savoir si la planète est déjà habitée
                if (!$bdd->query("SELECT id FROM $table_planete WHERE galaxie=$galaxie AND ss=$ss AND position=$pos;")) {
                    break;
                }
            }
            $newPlanete->galaxie = $galaxie;
            $newPlanete->ss = $ss;
            $newPlanete->position = $pos;
            unset($galaxie, $ss, $pos);
        } else {
            $bdd->reconnexion();
            //Recherche du joueur ami
            $placement = gpc("placement");
            $bdd->escape($placement);
            $placement_joueur = $bdd->unique_query("SELECT id FROM $table_user WHERE pseudo = '$placement';");

            if (!$placement_joueur && !isset($SESS->values['forceInscript'])) {
                $SESS->values['forceInscript'] = true;
                $SESS->put();
                $bdd->query("DELETE FROM $table_user WHERE id = ".$queryUser["id"].";");
                erreur("Le joueur à côté duquel vous souhaitez être placé n\'est pas encore ou plus inscrit sur ce serveur.<br /><br />Si vous ne souhaitez plus être placé à ses côtés, rechargez la page.");
            }

            $placement_planetes = $bdd->query("SELECT galaxie, ss FROM $table_planete WHERE id_user = ".$placement_joueur["id"].";");
            $nbPlanete = count($placement_planetes);
            $decale = 0;
            while ($decale < 20 && empty($newPlanete->galaxie)) {
                for ($i = 0; $i < $nbPlanete ; $i++) {
                    $galaxie = $placement_planetes[$i]['galaxie'];
                    if (empty($galaxie)) {
                        continue;
                    }

                    //Alternativement, on essaye le système précédent et le système suivant
                    if ($decale%2) {
                        $ss = $placement_planetes[$i]['ss'] - floor($decale/2);
                    } else {
                        $ss = $placement_planetes[$i]['ss'] + $decale/2;
                    }

                    if ($ss >= $VAR['nb_systeme'] || $ss <= 0) {
                        break;
                    }
                    for ($j = 1; $j <= $VAR['nb_planete'] ; $j++) {
                        if (!$bdd->query("SELECT id FROM $table_planete WHERE galaxie=$galaxie AND ss=$ss AND position=$j;")) {
                            $newPlanete->galaxie = $galaxie;
                            $newPlanete->ss = $ss;
                            $newPlanete->position = $j;
                            break;
                        }
                    }
                    if ($newPlanete->galaxie) {
                        break;
                    }
                }
                $decale++;
            }
            unset($decale, $galaxie, $ss, $pos, $i, $j, $nbPlanete, $placement_planetes, $placement_joueur, $placement);

            //Si le système n'a trouvé aucune planète
            if (empty($newPlanete->galaxie)) {
                $SESS->values['forceInscript'] = true;
                $SESS->put();
                $bdd->query("DELETE FROM $table_user WHERE id = ".$queryUser["id"].";");
                erreur("Nous n'avons pas pu trouvé de planète disponible autour de l'une des planètes du joueur que vous avez demandé.<br /><br />Si vous ne souhaitez plus être placé à ses côtés, rechargez la page.");
            }
        }
        $bdd->deconnexion();

        //On termine la création de la planète
        $newPlanete->creer($queryUser['id'], true);
        send_mp($queryUser['id'], "Bienvenue sur Halo-battle !", "Bienvenue sur Halo-battle ".$pseudo.",\nVous pouvez dès maintenant commencer à jouer, mais nous vous conseillons vivement de lire la FAQ afin de vous familiariser avec les principales fonctions du jeu !\n\nN'hésitez pas à vous inscrire sur notre forum : <a href=\"http://www.halo-battle.fr/forum/\">http://www.halo-battle.fr/forum/</a>\n\nCordialement, le staff d'Halo-Battle.");
        unset($queryUser);

        erreur("Félicitations !<br />Votre compte a été créé avec succès, vous pouvez maintenant vous connecter sur ce serveur en utilisant vos identifiants.<br />A tout de suite.", "green", $VAR["first_page"]."?index", 3500);
    } else {
        erreur("Impossible d'authentifier votre inscription !<br /><br />Si le problème persiste, contactez un administrateur.");
    }
} else {
    erreur("Impossible de terminer votre inscription, il manque des données !<br /><br />Si le problème persiste, contactez un administrateur.");
}
