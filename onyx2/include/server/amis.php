<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$titre = 'Amis';

$a = gpc('a');
$nom = gpc('nom', 'post');

//Suppression d'un ami
if (!empty($a)) {
    $d = gpc('d');
    if (!empty($d) && $a == md5(date('GA').'@'.$d)) {
        $f = array_keys($planete->amis, $d);
        unset($planete->amis[$f[0]]);
        $bdd->reconnexion();
        $bdd->query("UPDATE $table_user SET amis = '".serialize($planete->amis)."' WHERE id = $id_user;");
        $bdd->deconnexion();
    }
    header('Location: ?p=amis');
    exit;
}
//Ajout d'un ami dans la liste
elseif (!empty($nom)) {
    $bdd->reconnexion();
    $plan = $bdd->unique_query("SELECT id FROM $table_user WHERE pseudo = '$nom';");
    if ($plan) {
        if (in_array($plan['id'], $planete->amis)) {
            $bdd->deconnexion();
            erreur('Ce joueur est déjà dans votre liste d\'amis.', "red", "?p=amis", 2000);
        }
        $planete->amis[] = $plan['id'];
        $bdd->query("UPDATE $table_user SET amis = '".serialize($planete->amis)."' WHERE id = $id_user;");
    } else {
        $bdd->deconnexion();
        erreur('Joueur introuvable.', "red", "?p=amis", 2000);
    }
    $bdd->deconnexion();

    header('Location: ?p=amis');
    exit;
}
//Affichage de la liste d'amis
else {
    $page = 'amis';

    $destins = array();
    $bdd->reconnexion();
    foreach ($planete->amis as $ami) {
        $res = $bdd->unique_query("SELECT pseudo FROM $table_user WHERE id = ".$ami.";");
        if ($res != false) {
            $destins[] = array($ami, $res['pseudo'], md5(date('GA').'@'.$ami));
        }
    }
    $bdd->deconnexion();
    $template->assign('destins', $destins);
}
