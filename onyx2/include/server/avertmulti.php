<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$page = 'vide';
$titre = 'Multicompte';

$template->assign('titreP', '<span style="color: red;">! ATTENTION AU MULTICOMPTE !</span>');

$bdd->reconnexion();
$multi = $bdd->query("SELECT U.pseudo, R.id_util FROM $table_registre_identification R INNER JOIN $table_user U ON U.id = R.id_util WHERE R.ip = '".$_SERVER["REMOTE_ADDR"]."' GROUP BY R.ip, R.id_util");
$bdd->deconnexion();

$cnt = @count($multi);
$joueurs = '';
for ($i = 0; $i < $cnt; $i++) {
    if ($multi[$i]['id_util'] == $id_user) {
        continue;
    }

    if ($joueurs !=  '') {
        $joueurs .= ', '.$multi[$i]['pseudo'];
    } else {
        $joueurs = $multi[$i]['pseudo'];
    }
}

//Si pas de multi-compte, on redirige vers la page d'accueil
if ($cnt) {
    redirection('./'.$VAR['first_page']);
}

$template->assign('contenu', '<div class="error">Nous avons détecté que plus d\'un compte s\'est connecté sur cette adresse IP.<br />Pour ne pas être considéré comme multi-compte, évitez toute interraction avec '.$joueurs.'<br /><br />Si vous êtes plusieurs dans la même famille à jouer à Halo-Battle dans cette galaxie, <ins>vous devez impérativement</ins> <a href="'.$VAR['link']["operateur"].'">prendre contact avec un opérateur</a> afin que vos comptes ne soient pas considérés comme du multi-compte.<br /><br />Si vous vous connectez depuis un lieu public (cybercafé, aéroport, hôtel, wifi public, ...), vous pouvez prévenir <a href="'.$VAR['link']["operateur"].'">les opérateurs de la galaxie</a> afin que cette connexion ne soit pas considérée comme du multi-compte.<br /><br />Pour plus d\'informations, reportez-vous <a href="?p=regles#comptes">au chapitre <i>Comptes</i> des régles du jeu</a>.</div>');

unset($multi, $cnt, $joueurs);
