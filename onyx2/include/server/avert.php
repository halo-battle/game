<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$page = 'vide';
$titre = 'Avertissement de sécurité';

$template->assign('titreP', '<span style="color: red;">! AVERTISSEMENT !</span>');

$test = intval($SESS->values["avert"]);
$contenu = "";

if ($SESS->values["avert"]& 1) {
    $contenu .= "<h3 style=\"font-weight: bold;\">JavaScript désactivé !</h3>Nous avons détecté que le JavaScript n'était pas activé sur votre navigateur. Nous vous recommandons de l'activé pour avoir accès à l'ensemble du contenu du site, même si celui-ci fonctionne parfaitement sans cette technologie.<br />";
}
if ($SESS->values["avert"]& 2) {
    $contenu .= "<h3 style=\"font-weight: bold;\">Referer désactivé !</h3>Nous avons détecté que votre navigateur n'envoie pas d'informations sur sa navigation. Cela empêche le jeu de valider l'ensemble des requêtes faites par votre navigateur. Votre compte de jeu n'est donc pas protégé contre certaines attaques informatiques.<br />";
}

$template->assign("contenu", '<div class="error">'.$contenu.'<br /><br /><h3><a href="'.$_SERVER["REQUEST_URI"].'">Continuer</a></h3></div>');

unset($SESS->values["avert"]);
$SESS->put();
