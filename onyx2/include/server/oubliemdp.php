<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$page = 'oubliemdp';
$erreur = '';

if (!empty($_GET['i']) && isset($_GET['n'])) {
    $Un = gpc('n');
    $Ui = gpc('i');
    $bdd = new BDD();
    $bdd->escape($Un);
    $bdd->escape($Ui);
    $user = $bdd->unique_query("SELECT * FROM $table_user WHERE pseudo = '$Un' AND mdpNOUV = '$Ui';");
    if ($user) {
        $bdd->query("UPDATE user SET mdp = mdpNOUV, mdpNOUV = '' WHERE pseudo = '$Un'");
        $bdd->deconnexion();
        $template->assign('message', 'Votre mot de passe a été réinitialisé avec succès.');
        $template->assign('script', '<script type="text/javascript">setTimeout(\'document.location.href="?index";\', 3500);</script>');
        $template->assign('couleur', 'green');
        $template->display('cms/erreur.tpl');
        exit;
    } else {
        $bdd->deconnexion();
        $template->assign('message', 'Ce lien a expiré !');
        $template->assign('couleur', 'red');
        $template->display('cms/erreur.tpl');
        exit;
    }
}

if ((isset($_POST['HB_pseudo']) && isset($_POST['HB_mail']) && isset($_POST['HB_captcha'])) || (isset($_GET['ps']) && isset($_GET['m']) && isset($_GET['a']))) {
    $ok = true;
    if ((empty($_POST['HB_pseudo']) || empty($_POST['HB_mail'])) && (empty($_GET['ps']) || empty($_GET['m']))) {
        $ok = false;
        $erreur .= '<p style="color: #FF0000"><b>Pseudo ou adresse électronique vide :</b><br />Veuillez compléter tous les champs de ce formulaire avant de le valider.</p><br />';
    }

    if (!isset($_POST['HB_captcha'])) {
        $_POST['HB_captcha'] = '';
    }
    if (isset($_GET['m']) && is_numeric('0x'.$_GET['m'])) {
        $_GET['m'] = cxor(hexstr(gpc('m')), date('WYDj'));
    } elseif (strtolower($_POST['HB_captcha']) != $SESS->values['_captcha'] && gpc('a') != hash("sha512", gpc('ps').'☻♫☼'.date('W!Y¨D@j').'Ņ♂↨'.gpc('m'))) {
        $ok = false;
        $erreur .= '<p style="color: #FF0000"><b>Code de vérification incorrect :</b><br />Le texte que vous avez recopier ne correspondait pas au texte de l\'image, veuillez recommencer.</p><br />';
    }
    if ($ok) {
        if (isset($SESS->values['_captcha']) && strtolower($_POST['HB_captcha']) == strtolower($SESS->values['_captcha'])) {
            $HB_pseudo = gpc('HB_pseudo', 'post');
            $HB_mail = gpc('HB_mail', 'post');
        } else {
            $HB_pseudo = gpc('ps');
            $HB_mail = gpc('m');
        }

        $bdd = new BDD();
        $bdd->escape($HB_pseudo);
        $bdd->escape($HB_mail);
        $user = $bdd->unique_query("SELECT id, mdp_var FROM $table_user WHERE pseudo = '$HB_pseudo' AND mail = '$HB_mail';");
        if ($user) {
            $code = gen_mdp(8);
            $passNOUV = mdp($HB_pseudo, $code, $user['mdp_var']);
            $bdd->unique_query("UPDATE $table_user SET mdpNOUV = '$passNOUV' WHERE pseudo = '$HB_pseudo';");
            $bdd->deconnexion();

            if (!$test = send_mail($HB_mail, 'Halo-Battle :: ', 'Bonjour '.$HB_pseudo.",\n\nVous recevez ce mail suite à votre demande de réinitialisation de mot de passe. Pour confirmer votre réinitialisation de mot de passe et ainsi pouvoir de nouveau accéder au jeu, cliquez sur le lien ci-après :\n\nhttp://".$_SERVER['HTTP_HOST'].'/'.$VAR['first_page'].'?p=oubliemdp&i='.$passNOUV.'&n='.$HB_pseudo."\n\nVotre nouveau mot de passe est : ".$code."\n\nA bientôt dans Halo-Battle,\nLe staff")) { //Teste si le return code est ok.
                $template->assign('message', 'Erreur lors de l\'envoie du courriel de confirmation !<br /><br />'.$LANG["contactAdmin"]);
                $template->assign('couleur', 'red');
                $template->display('cms/erreur.tpl');
                exit;
            } else {
                $template->assign('message', 'Un nouveau mot de passe a été généré pour votre compte, vous le trouverez dans votre boîte de courrier électronique.<br /><br /><em>Pensez à regarder dans vos courriers indésirables, on ne sait jamais ...</em>');
                $template->assign('couleur', 'green');
                $template->display('cms/erreur.tpl');
                exit;
            }
        } else {
            $bdd->deconnexion();
            $erreur .= '<p style="color: #FF0000; font-weight: bold">Nom d\'utilisateur ou adresse électronique incorrect :<br />L\'adresse électronique que vous avez indiqué ne correspond pas au nom d\'utilisateur. Réessayez.</p><br />';
        }
    }
}

$template->assign('erreurs', $erreur);
