<?php
if (!defined('ONYX')) {
    exit;
}
$titre = 'Classement';
$page = $p;

//Récupération et vérification de la race voulue ou définition d'une race par défaut
if (empty($_GET['r']) || (gpc('r') != 'humain' && gpc('r') != 'covenant')) {
    $_GET['r'] = "global";
}
$race = gpc('r');

require_once("Class/serveur.php");
$serveur = new Serveur();
$classement = $serveur->classement($race);
$template->assign('joueurs', $classement);
$template->assign('user', $planete->id_user);
$template->assign('raceAff', $race);
