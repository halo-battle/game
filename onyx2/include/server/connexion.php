<?php
//On traite la demande de loggin de l'utilisateur
if ((isset($_GET['l']) && isset($_GET['p'])) || (isset($_POST['HB_login']) && isset($_POST['HB_password']))) {
    if (empty($_SERVER["HTTP_USER_AGENT"])) {
        $_SERVER["HTTP_USER_AGENT"] = "";
    }

    //Récupération des données POST ou GET
    if (isset($_POST['HB_login']) && isset($_POST['HB_password']) && gpc('p') == 'connexion') {
        $HB_login = gpc('HB_login', 'post');
        $HB_password = gpc('HB_password', 'post');
        $HB_auth = hash("sha512", $HB_login.'Ņ♂↨'.$HB_password.'☻♫☼'.date('W!Y¨D@j').$_SERVER["HTTP_USER_AGENT"].$_SERVER["REMOTE_ADDR"]);
    } else {
        $HB_login = gpc('l');
        if (is_numeric('0x'.gpc('p'))) {
            $HB_password = cxor(hexstr(gpc('p')), date('WYDj'));
        }
        $HB_auth = gpc('a');
    }

    if (empty($HB_login) || empty($HB_password)) {
        $template->assign('message', $LANG['badNomMdp']);
        $template->assign('couleur', 'red');
        $template->display('cms/erreur.tpl');
        exit;
    } elseif (hash("sha512", $HB_login.'Ņ♂↨'.$HB_password.'☻♫☼'.date('W!Y¨D@j').$_SERVER["HTTP_USER_AGENT"].$_SERVER["REMOTE_ADDR"]) != $HB_auth && hash("sha512", $HB_login.'Ņ♂↨'.$HB_password.'☻♫☼'.date('W!Y¨D@j', time()-300)) != $HB_auth) {
        $template->assign('message', $LANG['badAuthConnect'].'<br /><br />'.$LANG['contactAdmin']);
        $template->assign('couleur', 'red');
        $template->display('cms/erreur.tpl');
        exit;
    } else {
        $ip = $_SERVER["REMOTE_ADDR"];
        if (isset($_SERVER["HTTP_REFERER"])) {
            $referer = $_SERVER["HTTP_REFERER"];
        } else {
            $referer = "";
        }
        if (isset($_SERVER["HTTP_USER_AGENT"])) {
            $agent = $_SERVER["HTTP_USER_AGENT"];
        } else {
            $agent = "";
        }
        $activ_js = gpc("jsa", "post");
        $cookies = gpc("hbsession", "cookie");

        /*if (!(empty($referer) || DEBUG || preg_match("#^http://(\w{1,4}\.)?halo-battle\.(fr|net|com|org|us|en)#", $referer, $osef)))
        {
            elog("Mauvais referer à la connexion : ".$referer." pour l'utilisateur : ".$HB_login." (ip : ".$ip.", agent : ".$agent.")", 2);
            erreur("Alerte de sécurité !<br /><br />La requête de connexion ne provient pas d'un serveur authentique d'Halo-Battle.<br />Veuillez renouveler votre connexion sur un serveur officiel et <ins>changer d'urgence votre mot de passe de connexion</ins> !");
        }
        else*/if (empty($cookies)) {
            erreur("Vous devez accepter les cookies pour vous connecter au jeu !");
        } elseif (empty($activ_js) || empty($referer)) {
            $SESS->values["avert"] = 0;
            if (empty($activ_js)) {
                $SESS->values["avert"] += 1;
            }
            if (empty($referer)) {
                $SESS->values["avert"] += 2;
            }
        }
        unset($referer, $agent, $activ_js, $cookies);


        //Connexion à la base de données
        if (!isset($bdd)) {
            $bdd = new BDD();
        } else {
            $bdd->reconnexion();
        }

        //Sauvegarde du nombre d'essai pour éviter les brute-force
        $bruteforce = $bdd->unique_query("SELECT nombre FROM securite_identification WHERE ip = '$ip';");
        if ($bruteforce['nombre'] >= 10) {
            $fichier = fopen(ONYX."ban.xlist", 'a+');
            fwrite($fichier, $ip."\n\r");
            fclose($fichier);
        }
        $bdd->escape($HB_login);
        if (!($var = $bdd->unique_query("SELECT mdp_var, mdp, auth_level FROM $table_user WHERE pseudo = '$HB_login';")) || ($var["auth_level"] >= 4 && isset($_GET['HB_login']))) {
            if (isset($bruteforce['nombre'])) {
                $bdd->query("UPDATE securite_identification SET nombre = nombre + 1 WHERE ip = '$ip';");
            } else {
                $bdd->query("INSERT INTO securite_identification VALUES ('$ip', '1', '".time()."');");
            }

            $bdd->deconnexion();
            $template->assign('message', $LANG['badNomMdp']);
            $template->assign('couleur', 'red');
            $template->assign('script', '<script type="text/javascript">setTimeout(\'document.location.href="'.$VAR['link']['accueil'].'";\', 2500);</script>');
            $template->display('cms/erreur.tpl');
            exit;
        }
        $enligne = $bdd->unique_query("SELECT COUNT(session) AS enligne FROM sessions WHERE active = true AND var != '0';");
        $bdd->deconnexion();

        $HB_password = mdp($HB_login, $HB_password, $var['mdp_var']);

        //Limiter le nombre de personnes en ligne simutanément
        if ($enligne["enligne"] > 1500) {
            $template->assign('message', $LANG['servSature']);
            $template->assign('couleur', 'red');
            $template->display('cms/erreur.tpl');
            exit;
        }

        //On actualise le nombre de joueur en ligne si besoin
        if (abs($enligne["enligne"]-$header['count'][3]) > 6) {
            Cache::del('headerNB');
        }

        unset($bruteforce, $fichier);
        $time = time();

        $bdd->reconnexion();
        $bdd->escape($HB_password);

        //On vérifie que la requête ne provienne pas d'un proxy
        $proxy = $bdd->unique_query("SELECT ip FROM proxy_list WHERE ip LIKE '$ip' LIMIT 1;");
        $resultat = $bdd->unique_query("SELECT id, race, mv, last_visite, auth_level, raisonmv FROM $table_user WHERE pseudo = '$HB_login' AND mdp = '$HB_password';");

        if ($proxy) {
            erreur("Utilisation de proxy détectée !");
        } elseif ($resultat) {
            $id = $resultat['id'];
            $reqPlan = $bdd->query("SELECT id, file_bat, file_tech, file_cas, file_vais, file_ter, timestamp FROM $table_planete WHERE id_user = '$id' ORDER BY id ASC;");
            $resultatP = $reqPlan[0];
            $race = $resultat['race'];

            if (!$reqPlan) {
                $template->assign('message', $LANG['badPlanete']);
                $template->assign('couleur', 'red');
                $template->display('cms/erreur.tpl');
                exit;
            }

            if ($resultat['mv'] > 0) {
                if (($resultat['last_visite'] + 259200 > time() || $resultat['mv'] == 3) && $resultat['auth_level'] < 2) {
                    if ($resultat['mv'] == 3) {
                        $template->assign('message', sprintf($LANG['banInf'], $resultat['raisonmv']));
                    } elseif ($resultat['mv'] == 2) {
                        $template->assign('message', sprintf($LANG['banVac'], $resultat['raisonmv'], strftime("%A %d %B à %H:%M", $resultat['last_visite']+259200)));
                    } else {
                        $template->assign('message', sprintf($LANG['modVac'], strftime("%A %d %B à %H:%M", $resultat['last_visite']+259200)));
                    }

                    $template->assign('couleur', 'red');
                    $template->display('cms/erreur.tpl');
                    exit;
                }

                $bdd->query("UPDATE $table_user SET mv = 0 WHERE id = $id;");
                $bdd->query("UPDATE $table_planete SET timestamp = '".$time."' WHERE id_user = $id;");
                $bdd->deconnexion();

                //On fait repartir à 0 les files d'attente
                include_once("game/vars.php");
                include_once("game/Class/class.file.php");
                include_once("game/Class/class.user.php");
                include_once("game/Class/class.planete.php");

                foreach ($reqPlan as $plan) {
                    if (!empty($plan['file_bat'])) {
                        $file = unserialize($plan['file_bat']);
                        $fileBat = $file->reajusteVacances($plan['timestamp']);
                    } else {
                        $fileBat = '';
                    }
                    if (!empty($plan['file_tech'])) {
                        $file = unserialize($plan['file_tech']);
                        $fileTech = $file->reajusteVacances($plan['timestamp']);
                    } else {
                        $fileTech = '';
                    }
                    if (!empty($plan['file_cas'])) {
                        $file = unserialize($plan['file_cas']);
                        $fileCas = $file->reajusteVacances($plan['timestamp']);
                    } else {
                        $fileCas = '';
                    }
                    if (!empty($plan['file_vais'])) {
                        $file = unserialize($plan['file_vais']);
                        $fileVais = $file->reajusteVacances($plan['timestamp']);
                    } else {
                        $fileVais = '';
                    }
                    if (!empty($plan['file_ter'])) {
                        $file = unserialize($plan['file_ter']);
                        $fileTer = $file->reajusteVacances($plan['timestamp']);
                    } else {
                        $fileTer = '';
                    }

                    $bdd->reconnexion();
                    $bdd->escape($fileBat);
                    $bdd->escape($fileTech);
                    $bdd->escape($fileCas);
                    $bdd->escape($fileVais);
                    $bdd->escape($fileTer);
                    $bdd->query("UPDATE $table_planete SET file_bat = '$fileBat', file_tech = '$fileTech', file_cas = '$fileCas', file_vais = '$fileVais', file_ter = '$fileTer' WHERE id = ".$plan['id'].";");
                    $bdd->deconnexion();

                    //On met à jour la planète pour tout le reste
                    $plan = new Planete($plan['id'], true);
                    $plan->actualiser();
                }
            }
            //On met à jour toutes les planètes si le nombre de joueurs n'est pas trop important
            elseif ($enligne["enligne"] < 400) {
                $bdd->deconnexion();
                require_once("donnees.php");
                include_once("Class/user.php");
                include_once("Class/planete.php");

                foreach ($reqPlan as $plan) {
                    $plan = new Planete($plan['id'], true);
                }
            }

            unset($plan, $enligne);

            $bdd->reconnexion();
            $bdd->query("UPDATE $table_user SET last_visite = $time, last_ip = '$ip' WHERE id = $id;");
            $bdd->query("INSERT INTO $table_registre_identification (id_util, ip) VALUES (".$id.",'".$ip."');");
            $multi = $bdd->unique_query("SELECT COUNT(*) FROM $table_registre_identification WHERE ip = '$ip' GROUP BY ip, id_util;");
            $message = $bdd->unique_query("SELECT time FROM $table_messages_demarrage ORDER BY time DESC LIMIT 1;");
            $bdd->deconnexion();

            $SESS->values['connected'] = true;
            $SESS->values['id'] = $resultat['id'];
            $SESS->values['race'] = $resultat['race'];
            $SESS->values['idPlan'] = $resultatP['id'];
            $SESS->values['idAsteroide'] = 0;

            //Si on détecte le multi-compte, on interdit l'accès au panneau d'admin
            //if (empty($multi))
            //	$SESS->level = 1;
            //else
            $SESS->level = $resultat['auth_level'];

            $SESS->put($resultat['id']);

            if (!empty($resultat['last_visite']) && $message['time'] > $resultat['last_visite']) {
                redirection('./'.$VAR['first_page'].'?p=demarrage');
            }
            //elseif (empty($multi))
            //	redirection('./'.$VAR['first_page'].'?p=avertmulti');
            else {
                redirection('./'.$VAR['first_page'].'?p=accueil');
            }
        } else {
            $bdd->deconnexion();

            $template->assign('message', $LANG['badNomMdp']);
            $template->assign('couleur', 'red');
            $template->assign('script', '<script type="text/javascript">setTimeout(\'document.location.href="'.$VAR['link']['accueil'].'";\', 2500);</script>');
            $template->display('cms/erreur.tpl');
            exit;
        }
    }
}
