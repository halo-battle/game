<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$titre = 'Options';

if (gpc("p") == "changeopt" && ($Un = intval(gpc('util'))) && $Un == $SESS->values["id"]) {
    $Ui = gpc('auth');
    $bdd = new BDD();
    $bdd->escape($Ui);
    $bdd->query("UPDATE user SET mdp = mdpNOUV, mdpNOUV = '', mailNOUV = '' WHERE id = $Un AND SHA1(mdpNOUV) = '$Ui' AND mdpNOUV != '';");
    if ($bdd->affected() == 1) {
        erreur("Votre nouveau mot de passe est maintenant actif.", "green");
    }

    $bdd->query("UPDATE user SET mail = mailNOUV, mailNOUV = '', mdpNOUV = '' WHERE id = $Un AND SHA1(mailNOUV) = '$Ui' AND mailNOUV != '';");
    if ($bdd->affected() == 1) {
        erreur("Votre nouvelle adresse électronique est maintenant active.", "green");
    } else {
        erreur("Ce lien a expiré !");
    }
} else {
    $onglet = gpc("n");

    if ($onglet == "compte") {
        $page = 'options/2';

        if (isset($_POST["chgmdp"])) {
            $bdd->reconnexion();
            $user = $bdd->unique_query("SELECT mdp, mdp_var FROM $table_user WHERE id = ".$planete->id_user.";");
            $bdd->deconnexion();

            if (mdp($planete->pseudo, gpc('mdp_anc', 'post'), $user["mdp_var"]) == $user["mdp"]) {
                $check = check_mdp($_POST['mdp_nouv'], $planete->pseudo, $_POST['mdp_conf']);
                if (empty($check)) {
                    $new_pass = mdp($planete->pseudo, gpc('mdp_nouv', 'post'), $user["mdp_var"]);
                    $auth = sha1($new_pass);

                    $bdd->reconnexion();
                    $bdd->escape($new_pass);
                    $bdd->query("UPDATE $table_user SET mdpNOUV = '$new_pass' WHERE id = ".$planete->id_user.";");
                    $bdd->deconnexion();

                    if (!send_mail($planete->mail, "Halo-Battle :: Activation de votre nouveau mot de passe", "Bonjour ".$planete->pseudo.",\n\nVous recevez ce mail suite à votre demande de changement de mot de passe.\nCe dernier entrera en vigueur une fois que vous aurez validé ce changement en cliquant sur le lien suivant :\nhttp://".$_SERVER['HTTP_HOST']."/".$VAR['first_page']."?p=changeopt&util=".$planete->id_user."&auth=".$auth."\n\nNotez que pour que le mot de passe soit changé, il faut que vous soyez connecté sur votre compte.")) {
                        erreur('Erreur lors de l\'envoie du courriel de confirmation !<br /><br />Si le probl&egrave;me perciste, <a href="mailto:'.$VAR['mail_admin'].'">contactez un administrateur</a>.');
                    } else {
                        erreur('Un courriel vient d\'être envoy&eacute; à '.$planete->mail.'. Le mot de passe ne sera modifi&eacute; qu\'apr&egrave;s avoir cliqu&eacute; sur le lien d\'activation contenu dans ce courriel.', "green");
                    }
                    unset($new_pass);
                } else {
                    erreur($check, "red", $VAR["menu"]["options"]."&n=compte");
                }
            } else {
                erreur("L'ancien mot de passe que vous avez indiqué ne correspond pas.", "red", $VAR["menu"]["options"]."&n=compte");
            }
            unset($new_pass, $auth, $user);
        } elseif (isset($_POST["chgmail"])) {
            $bdd->reconnexion();
            $user = $bdd->unique_query("SELECT mdp, mdp_var FROM $table_user WHERE id = ".$planete->id_user.";");
            $bdd->deconnexion();

            if (mdp($planete->pseudo, gpc('mdp_mail', 'post'), $user["mdp_var"]) == $user["mdp"]) {
                $nouveauMail = gpc('mail', 'post');
                if (preg_match("#^[A-Za-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $nouveauMail)) {
                    $bdd->reconnexion();
                    $bdd->escape($nouveauMail);
                    $result = $bdd->query("SELECT mail FROM user WHERE mail = '$nouveauMail'");
                    if ($result) {
                        $bdd->deconnexion();
                        erreur('Vous ne pouvez pas utiliser cette adresse mail, elle est déjà utilisée par un autre utilisateur.', "red", '?p=options');
                    } else {
                        $bdd->query("UPDATE user SET mailNOUV = '$nouveauMail' WHERE id = ".$planete->id_user.";");
                        $bdd->deconnexion();
                        $auth = sha1($nouveauMail);

                        //On envoie le mail de confirmation
                        if (!send_mail($nouveauMail, "Halo-Battle :: Activation de votre nouvelle adresse électronique", "Bonjour ".$planete->pseudo.",\n\nVous recevez ce mail suite à votre demande de changement d'adresse électronique. Cette dernière entrera en vigueur une fois que vous aurez validé ce changement en cliquant sur le lien suivant :\nhttp://".$_SERVER['HTTP_HOST']."/".$VAR['first_page']."?p=changeopt&util=".$planete->id_user."&auth=".$auth."\n\nNotez que pour que le changement soit pris en compte, il faut que vous soyez connecté sur votre compte.")) {
                            erreur('Erreur lors de l\'envoie du courriel de confirmation !<br /><br />Si le probl&egrave;me perciste, <a href="mailto:'.$VAR['mail_admin'].'">contactez un administrateur</a>.');
                        } else {
                            erreur('Un courriel vient d\'être envoy&eacute; à '.$nouveauMail.'. L\'adresse &eacute;lectronique ne sera modifi&eacute; qu\'apr&egrave;s avoir cliqu&eacute; sur le lien d\'activation contenu dans ce courriel.', "green");
                        }
                    }
                } else {
                    erreur("L'adresse email que vous avez indiqué semble incorrecte.", "red", $VAR["menu"]["options"]."&n=compte");
                }
            } else {
                erreur("Le mot de passe que vous avez indiqué ne correspond pas.", "red", $VAR["menu"]["options"]."&n=compte");
            }
            unset($nouveauMail, $result, $user);
        } elseif (!empty($_GET["a"]) && gpc("a") == "mv") {
            if (gpc("c") == $SESS->values['idvac']) {
                $bdd->reconnexion();
                $bdd->query("UPDATE user SET mv = 1 WHERE id = ".$planete->id_user.";");
                $bdd->deconnexion();

                $SESS->values['connected'] = false;
                $SESS->put();

                erreur('Le mode vacances est maintenant actif sur votre compte.<br /><br />Vous pourez vous reconnecter sur votre compte à partir de<br />'.strftime("%A %d %B à %H:%M", $planete->last_visite+259200).'<br /><br />Bonnes vacances !', "green", '?index', 10000);
            } else {
                erreur("Impossible d'authentifier le placement en mode vacances !", "red", $VAR["menu"]["options"]."&n=compte");
            }
        } elseif (!empty($_GET["a"]) && gpc("a") == "dl") {
            if (gpc("c") == $SESS->values['idvac']) {
                $bdd->reconnexion();
                $bdd->query("UPDATE user SET mv = -2 WHERE id = ".$planete->id_user.";");
                $bdd->deconnexion();

                erreur('Votre compte sera supprimé le <br />'.strftime("%A %d %B à %H:%M", mktime(date('H', $VAR["time_maintenance"]), date('i', $VAR["time_maintenance"]), 0, date('n', time()+86400*2), date('d', time()+86400*2), date('y', time()+86400*2))).'.', "green");
            } else {
                erreur("Impossible d'authentifier la supression du compte !", "red", $VAR["menu"]["options"]."&n=compte");
            }
        }

        $template->assign('idvac', $SESS->values['idvac'] = sha1(rand(123456789, 9876543210).'ß‘}☻'.time()));
        $SESS->put();
    } elseif ($onglet == "jeu") {
        if (!empty($_POST)) {
            $options = 0;
            foreach ($_POST as $opt) {
                $options += intval($opt);
            }

            $planete->options = $options;
            $planete->addModifUser("options");
        }

        $page = 'options/3';
    } elseif ($onglet == "notif") {
        $page = 'options/4';

        if (isset($_POST["notif"])) {
            $planete->envoyerMail = 0;
            if (!empty($_POST["rapport"])) {
                $planete->envoyerMail += 1;
            }
            if (!empty($_POST["mp"])) {
                $planete->envoyerMail += 2;
            }

            $planete->addModifUser("envoyerMail");
        }
    } else {
        $onglet = "ext";
        $page = 'options/1';
    }

    $template->assign("onglet", $onglet);
}
