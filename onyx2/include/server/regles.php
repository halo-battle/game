<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
if (!empty($SESS->values['connected'])) {
    $page = '../cms/vide';
} else {
    $page = 'vide';
}
$titre = 'Règles du jeu';

$template->assign('contenu', '<h1>R&egrave;gles de Halo-Battle</h1><div style="text-align: left; margin: 15px; font-size: 115%;">
<u>Pr&eacute;ambule :</u> tous les joueurs ayant cr&eacute;&eacute; un compte de jeu, et l&#39;utilisant ou non, s&#39;engagent &agrave; avoir lu et &agrave; respecter les r&egrave;gles de Halo-Battle dans leur int&eacute;gralit&eacute;. Si l&#39;une des r&egrave;gles &eacute;nonc&eacute;es ci-dessous est transgress&eacute;e, merci de vous r&eacute;f&eacute;rer au chapitre des infractions afin de conna&icirc;tre toutes les modalit&eacute;s relatives aux punitions encourues.<br />
<br />
Ce r&egrave;glement peut &ecirc;tre l&#39;objet d&#39;une modification, partielle ou totale, et il est du ressort du joueur de se tenir inform&eacute; de toute modification quelle qu&#39;elle soit. Toute modification sera publi&eacute;e sur le forum avec sa date de mise en application, avant de se trouver effective au sein du jeu.<br />
<br />
Ces r&egrave;gles du jeu ont &eacute;t&eacute; publi&eacute;es le 14/04/2008 et rentrent en vigueur &agrave; partir du 24/04/2008
<br />
<br /><u>Plan des r&egrave;gles :</u>
<br />
<br /><a href="#comportement">I. Comportement</a>
<br /><a href="#comptes">II. Comptes</a>
<br /><a href="#commerce">III. Commerce et ressources</a>
<br /><a href="#mercenariat">IV. Mercenariat</a>
<br /><a href="#guerre">V. Guerre</a>
<br /><a href="#bash">VI. Bash</a>
<br /><a href="#alliances">VII. Alliances</a>
<br /><a href="#sanctions">VIII. Sanctions</a>
<br /><br /><br />
<h3 id="comportement">I. Comportement</h3><br />
<h4>1 - Comportement g&eacute;n&eacute;ral</h4><br />

- La convivialit&eacute; et le respect des autres joueurs sont de rigueur. Par cons&eacute;quent, les propos injurieux, racistes, x&eacute;nophobes, pornographiques et antis&eacute;mites sont prohib&eacute;s. Tous les outils de communications utilis&eacute;s pour et par Halo-Battle sont soumis &agrave; cette r&egrave;gle (jeu, forum et IRC).<br />
- L&#39;utilisation de pseudo et/ou nom de plan&egrave;te en rapport direct avec des propos racistes, x&eacute;nophobes, pornographiques ou encore antis&eacute;mites est totalement prohib&eacute;. Il en va de m&ecirc;me pour toute revendication politique, religieuse ou ayant un rapport avec l&#39;actualit&eacute; (une certaine libert&eacute; peut &ecirc;tre tol&eacute;r&eacute;e mais ceci reste &agrave; l&#39;appr&eacute;ciation du staff).<br />
- Les menaces sortant du cadre du jeu ne seront pas tol&eacute;r&eacute;es et lourdement sanctionn&eacute;es par les op&eacute;rateurs. Elles pourront de plus faire l&#39;objet de poursuites judiciaires de la part de la personne concern&eacute;e devant les juridictions comp&eacute;tentes.<br />
- Utiliser le bouton Signaler pour signaler un message qui ne contient pas d&#39;insultes ou qui n&#39;enfreint pas les r&egrave;gles du jeu est interdit. Le signalement de message ne doit servir qu&#39;&agrave; informer les op&eacute;rateurs d&#39;infractions aux r&egrave;gles du jeu et &agrave; rien d&#39;autre.<br />
- Le spam, les insultes et les messages &agrave; contenus offensants sont interdits, de m&ecirc;me que tout contenu x&eacute;nophobe, antis&eacute;mite ou raciste.<br />
- Toute manoeuvre visant &agrave; exploiter le r&egrave;glement &agrave; son avantage, ou &agrave; le contourner sciemment, est interdite.<br />

<br /><h4>2 - Bugusing</h4><br />

- Tout bug trouv&eacute; doit &ecirc;tre imm&eacute;diatement signal&eacute; au staff de Halo-Battle.<br />
- Il est totalement prohib&eacute; d&#39;exploiter un bug &agrave; son avantage.<br />
- Toute action visant &agrave; paralyser d&#39;une quelconque mani&egrave;re le compte d&#39;un autre joueur est absolument interdit.<br />
- Ne pas signaler un bug d&eacute;couvert est passible de bannissement.<br />

<br /><h3 id="comptes">II. Comptes</h3><br />

<br /><h4>1 - Les comptes de jeu</h4><br />

- Les comptes restent la propri&eacute;t&eacute; de Halo-Battle ; par cons&eacute;quent, ils ne peuvent faire l&#39;objet d&#39;une vente ou faire office de monnaie d&#39;&eacute;change.<br />
- Un compte ne peut &ecirc;tre jou&eacute; que par une seule et m&ecirc;me personne physique dans une seule et m&ecirc;me galaxie (sauf cas relatif &agrave; la surveillance de compte).<br />
- La langue utilis&eacute;e dans Halo-Battle est le fran&ccedil;ais ; toutefois, seul l&#39;anglais est tol&eacute;r&eacute; pour les personnes ne sachant pas ou peu le parler.<br />

<br /><h4>2 - Multicomptes</h4><br />

- Jouer plus d&#39;un compte par galaxie est strictement interdit.<br />
- Pour les joueurs jouant depuis un lieu commun (famille, collocation, &eacute;coles, lieux de travail...) il est imp&eacute;ratif de pr&eacute;venir l&#39;op&eacute;rateur de votre galaxie.<br />
- Aucune interaction, directe ou non, entre comptes de joueurs se connectant r&eacute;guli&egrave;rement de lieux communs n&#39;est autoris&eacute;e.<br />
- Pour les joueurs d&#39;une m&ecirc;me famille, faire partie d&#39;une m&ecirc;me alliance est tol&eacute;r&eacute;. Dans ce cas pr&eacute;cit, merci d&#39;en avertir l&#39;op&eacute;rateur de votre galaxie. Toute exploitation abusive de cette autorisation sera s&eacute;v&eacute;rement sanctionn&eacute;e.<br />
- La r&egrave;gle de la surveillance de compte et le partage de compte s&#39;appliquent normalement : m&ecirc;me connect&eacute; du m&ecirc;me lieu, chaque joueur n&#39;est autoris&eacute; &agrave; surveiller ou jouer que son propre compte.<br />

<br /><h4>3 - Surveillance de compte</h4><br />

- Toute surveillance de compte doit &ecirc;tre obligatoirement signal&eacute;e &agrave; l&#39;op&eacute;rateur de votre galaxie. Le propri&eacute;taire du-dit compte doit indiquer quel compte va &ecirc;tre surveill&eacute;, donner la dur&eacute;e exacte de la surveillance, et donner le pseudonyme du surveillant. Il n&#39;est pas n&eacute;cessaire d&#39;attendre une r&eacute;ponse de l&#39;op&eacute;rateur pour pouvoir commencer la surveillance du compte.<br />
- Une seule et m&ecirc;me personne ne peut surveiller qu&#39;un seul compte &agrave; la fois. Aucun d&eacute;lais minimum n&#39;est exig&eacute; entre la surveillance de deux comptes diff&eacute;rents. Les surveillances de comptes ne peuvent faire l&#39;objet d&#39;aucune r&eacute;mun&eacute;ration.<br />
- Le surveillant ainsi que le joueur qui fait surveiller son compte sont responsables des actions faites via un compte durant une surveillance.<br />
- Le surveillant peut effectuer n&#39;importe quelle op&eacute;ration sur la ou les plan&egrave;tes du propri&eacute;taire durant la surveillance. Est n&eacute;anmoins interdit le transfert de ressources vers des plan&egrave;tes appartenant &agrave; un joueur autre. L&#39;activation du mode vacances est &eacute;galement autoris&eacute;.<br />
- La surveillance du compte prend fin lorsque le propri&eacute;taire du compte se reconnecte &agrave; celui-ci, ou que le d&eacute;lai de surveillance a expir&eacute;. Dans ce cas pr&eacute;cit, le surveillant est tenu de ne plus s&#39;en occuper et de faire passer obligatoirement le compte en mode vacances.<br />

<br /><h4>4 - Don de compte</h4><br />

- Tout changement de compte doit &ecirc;tre au pr&eacute;alable signal&eacute; &agrave; un op&eacute;rateur. Tout changement sans l&#39;intervention de l&#39;op&eacute;rateur sera sanctionn&eacute;.<br />
- Le propri&eacute;taire du mail permanent du compte est tenu d&#39;envoyer un mail informatif aux op&eacute;rateurs en pr&eacute;cisant le pseudonyme du repreneur et le futur mail permanent du compte.<br />
- L&#39;op&eacute;rateur modifiera lui-m&ecirc;me le mail permanent du compte et informera le donneur et le repreneur que le compte est c&eacute;d&eacute;.<br />
- Le repreneur peut se connecter au compte d&egrave;s qu&#39;il en re&ccedil;oit l&#39;autorisation par l&#39;op&eacute;rateur, et peut alors changer l&#39;adresse dynamique, le pseudonyme et le mot de passe du compte.<br />
- Une fois que le repreneur s&#39;est connect&eacute; sur le compte, il est strictement interdit au donneur de s&#39;y reconnecter.<br />
- La m&ecirc;me r&egrave;gle s&#39;applique dans le cas d&#39;un &eacute;change de comptes entre deux joueurs d&#39;une m&ecirc;me galaxie.<br />
- Aucun don de compte ne peut faire l&#39;objet d&#39;une transaction mon&eacute;taire.<br />

<br /><h3 id="commerce">III. Commerce et ressources</h3><br />

- Les joueurs sont libres de commercer comme ils l&#39;entendent, tant que leurs transactions ne se trouvent pas en infraction avec le r&eacute;glement.<br />
- Le commerce doit prendre la forme d&#39;un arrangement entre les diff&eacute;rents joueurs. Chaque joueur engage sa propre responsabilit&eacute; lors de la transaction de ressources. Par cons&eacute;quent, dans le cas de vols, d&#39;arnaques ou de toute autre forme crapuleuse de n&eacute;gociation, il ne peut y avoir de recours aupr&egrave;s d&#39;un op&eacute;rateur.<br />
- Il est interdit d&#39;exercer une pression ou de s&#39;acharner sur un joueur dans le but de lui soutirer ses ressources (sauf si les motifs sont valables, comme dans le cas d&#39;une guerre d&eacute;clar&eacute;e).<br />
- L&#39;envoie de ressources &agrave; des joueurs plus haut class&eacute;s que soi doit &ecirc;tre motiv&eacute; par des motifs valables. Si l&#39;op&eacute;rateur juge l&#39;action outranci&egrave;re, des r&eacute;parations de dommages peuvent &ecirc;tre demand&eacute;es au joueur b&eacute;n&eacute;ficiaire des ressources.<br />
- Les transactions de ressources ne peuvent faire l&#39;objet d&#39;une transaction mon&eacute;taire r&eacute;elle.<br />

<br /><h3 id="mercenariat">IV. Mercenariat</h3><br />

- Le mercenariat est autoris&eacute;. Ces contrats r&eacute;tribuent un joueur ou une alliance en fonction de services rendus.<br />
- Un contrat de mercenariat doit prendre la forme d&#39;un arrangement entre les diff&eacute;rents joueurs et ceux-ci y engagent leur responsabilit&eacute; propre. Par cons&eacute;quent, il ne peut y avoir de recours aupr&egrave;s d&#39;un op&eacute;rateur si, par exemple, le contrat n&#39;est pas ex&eacute;cut&eacute;, ou n&#39;est pas r&eacute;mun&eacute;r&eacute;.<br />
- Toute mission de mercenariat est possible, tant qu&#39;elle n&#39;interf&egrave;re pas avec le pr&eacute;sent r&eacute;glement.<br />
- Un contrat de mercenariat ne peut &ecirc;tre r&eacute;mun&eacute;r&eacute; par des unit&eacute;s mon&eacute;taires r&eacute;elles.<br />

<br /><h3 id="guerre">V. Guerre</h3><br />

- Une guerre ne peut &ecirc;tre d&eacute;clar&eacute;e que contre une alliance ou un joueur sans alliance. Ainsi il est possible de d&eacute;clarer la guerre &agrave; une alliance enti&egrave;re ou &agrave; joueur qui n&#39;a pas d&#39;alliance, mais pas &agrave; un seul joueur d&#39;une alliance ou &agrave; quelques joueurs d&#39;une alliance.<br />
- Il n&#39;est pas n&eacute;cessaire de motiver la d&eacute;cision ou de pr&eacute;venir l&#39;alliance agress&eacute;e pour d&eacute;clarer une guerre.<br />
- Il est obligatoire de d&eacute;clarer officiellement une guerre sur le forum, dans la section appropri&eacute;e, dans les 24 heures qui suivent l&#39;agression. Les joueurs sont libres de formuler les objectifs et les conditions de fin de la guerre.<br />
- Il est strictement interdit d&#39;&eacute;tablir des objectifs portant atteinte aux libert&eacute;s fondamentales dont disposent les joueurs, comme la suppression d&#39;un compte, l&#39;interdiction de jouer, suppression de colonies ou d&#39;unit&eacute;s, etc...<br />
- En cas de guerre, le bash n&#39;est pas interdit.<br />
- Les joueurs sont libres de mener leurs campagnes militaires comme ils l&#39;entendent, pourvu que cela n&#39;entre pas en conflit avec le r&egrave;glement.<br />
- Une guerre prend fin lorsque les bellig&eacute;rants trouvent un accord. Ils devront le signaler obligatoirement, comme pour la d&eacute;claration de guerre.<br />

<br /><h3 id="bash">VI. Bash</h3><br />

- Le bash est relatif &agrave; l&#39;attaque &agrave; outrance de plan&egrave;tes particuli&egrave;res. Attaquer une plan&egrave;te plus de quatre fois en 24 heures est par cons&eacute;quent interdit.<br />
- La r&egrave;gle du bash s&#39;applique aux joueurs actifs et inactifs.<br />
- En cas de guerre, le bash est autoris&eacute;.<br />

<br /><h3 id="alliances">VII. Alliances</h3><br />

- Toute alliance poss&egrave;de le privil&egrave;ge de se constituer comme elle l&#39;entend.<br />
- Toute alliance peut recruter autant de joueurs qu&#39;elle le souhaite.<br />
- Les alliances mixtes sont autoris&eacute;es ; cependant, une race dominante doit y &ecirc;tre repr&eacute;sent&eacute;e et les joueurs de la race oppos&eacute;e ne doivent pas s&#39;y trouver en trop grand nombre sous peine de sanction.<br />
- Toute alliance peut recruter autant de joueurs qu&#39;elle le souhaite.<br />
- Toute alliance peut signer autant de pactes qu&#39;elle le souhaite.<br />
- Toute alliance a le droit de se donner comme mission celle qu&#39;elle entend.<br />
- Il est strictement interdit de copier totalement ou en partie, les textes, images, etc, cr&eacute;&eacute;s par les joueurs des diff&eacute;rentes alliances d&eacute;j&agrave; existantes. Les alliances fond&eacute;es sur le principe du CSNU ou de l&#39;Alliance ne sont pas concern&eacute;es par la clause du plagiat de nom. Pour pouvoir recopier l&#39;un des &eacute;l&eacute;ments se trouvant dans une alliance, il est n&eacute;cessaire d&#39;avoir une autorisation &eacute;crite de ou des auteurs vous autorisant &agrave; y proc&eacute;der sous peine de sanctions.<br />
- Tout recrutement dans les autres alliances d&eacute;j&agrave; existantes est formellement interdit.<br />

<br /><h3 id="sanctions">VIII. Sanctions</h3><br />

- En cas de non respect de ce r&egrave;glement, les op&eacute;rateurs du jeu infligeront un blocage pouvant aller de un jour, &agrave; un blocage d&eacute;finitif selon la gravit&eacute; de la faute, ainsi qu&#39;en cas de r&eacute;cidive.<br />
- Chaque bannissement est mentionn&eacute; dans le pilori du jeu. Par ailleurs, le staff de Halo-Battle se r&eacute;serve le droit de prendre &agrave; l&#39;encontre des joueurs toute autre sanction.<br />
- En cas de r&eacute;clamation, merci d&#39;envoyer un mail &agrave; l&#39;un des op&eacute;rateurs en pr&eacute;cisant votre pseudonyme ingame et votre galaxie.<br />
- Toute manoeuvre visant &agrave; faire bloquer d&eacute;lib&eacute;r&eacute;ment un adversaire est interdite et pourra &ecirc;tre sanctionn&eacute;e par l&#39;op&eacute;rateur.<br />
</div>');
