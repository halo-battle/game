<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$pagea = 'print';
$titre = 'Vérification planète';

    $template->assign('linkpage', 'vplanetes');

if (!empty($_GET['id']) && !empty($_GET['key']) && $_GET['key'] == 'hash_planete') {
    $id_plan = intval(gpc('id'));
    $bdd = new BDD();
    $bdd->query("UPDATE $table_planete SET hash_planete = SHA1(CONCAT('g',planete.galaxie,'s',planete.ss,'p',planete.position)) WHERE id = $id_plan;");

    $req = $bdd->unique_query("SELECT * FROM $table_planete WHERE id = $id_plan;");
    $bdd->deconnexion();
    $template->assign('tableau', $req);
    $template->assign('idPlan', $id_plan);
} elseif (!empty($_GET['id']) && !empty($_GET['key']) && $_GET['key'] != 'id') {
    $pagea = 'print_key';
    $id_plan = intval(gpc('id'));
    $key = intval(gpc('key'));
    $bdd = new BDD();
    $reqp = $bdd->unique_query("SELECT * FROM $table_planete WHERE id = $id_plan;");
    $req = $bdd->unique_query("DESCRIBE $table_planete $key;");
    $bdd->deconnexion();

    $template->assign('tableau', $reqp);
    $template->assign('type', explode('(', $req['Type']));
    $template->assign('idPlan', $id_plan);
    $template->assign('key', $_GET['key']);
} elseif (!empty($_GET['id'])) {
    $id_plan = intval(gpc('id'));
    if (isset($_POST['key']) && isset($_POST['mod']) && $_POST['key'] != 'id') {
        $key = gpc('key', 'post');
        $mod = gpc('mod', 'post');
        $bdd = new BDD();
        $bdd->escape($key);
        $bdd->escape($mod);
        $bdd->query("UPDATE $table_planete SET $key = '$mod' WHERE id = $id_plan;");
        $bdd->deconnexion();
    }
    $bdd = new BDD();
    $req = $bdd->unique_query("SELECT * FROM $table_planete WHERE id = $id_plan;");
    $bdd->deconnexion();
    $template->assign('tableau', $req);
    $template->assign('idPlan', $id_plan);
} elseif (isset($_GET['amas']) && isset($_GET['ss']) && !empty($_GET['pos'])) {
    $amas = intval(gpc('amas'));
    $ss = intval(gpc('ss'));
    $pos = intval(gpc('pos'));

    $bdd = new BDD();
    $req = $bdd->unique_query("SELECT id FROM $table_planete WHERE galaxie = $amas AND ss = $ss AND position = $pos;");
    $bdd->deconnexion();
    header('Location: '.$VAR["menu"]["vplanetes"].'&id='.$req['id']);
    exit;
} elseif (isset($_GET['format'])) {
    preg_match('#^\[?([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})\]?$#', gpc('format'), $position);

    $bdd = new BDD();
    $req = $bdd->unique_query("SELECT id FROM $table_planete WHERE galaxie = ".$position[1]." AND ss = ".$position[2]." AND position = ".$position[3].";");
    $bdd->deconnexion();
    header('Location: '.$VAR["menu"]["vplanetes"].'&id='.$req['id']);
    exit;
} else {
    $bdd = new BDD();
    $req = $bdd->query("SELECT planete.id, nom_planete, pseudo, galaxie, ss, position FROM planete LEFT JOIN user ON (id_user=user.id) LIMIT 1000;");
    $bdd->deconnexion();
    $template->assign('planetes', $req);
    $pagea = 'print_choixP';
}
