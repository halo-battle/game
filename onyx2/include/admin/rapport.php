<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$pagea = 'rapports';
$titre = 'Afficher les rapports d\'un joueur';

    $template->assign('linkpage', 'vrapports');

if (!empty($_GET['id'])) {
    $id_user = intval(gpc('id'));
    $bdd = new BDD();
    $req = $bdd->query("SELECT * FROM $table_mail WHERE destinataire = $id_user AND expediteur = '' ORDER BY temps DESC;");
    $bdd->deconnexion();

    $template->assign('tableau', $req);
    $template->assign('idPlan', $id_user);
    $template->assign('script', '<script>for (j=0; document.getElementsByName(j)[0]; j++) hide(document.getElementsByName(j)[0]);</script>');
} elseif (!empty($_GET['name'])) {
    $name = gpc('name');
    $bdd = new BDD();
    $bdd->escape($name);
    $req = $bdd->unique_query("SELECT id FROM $table_user WHERE pseudo = '".$name."';");
    $bdd->deconnexion();

    header('Location: admin.php?p=vrapports&id='.$req['id']);
    exit;
} else {
    $pagea = 'print_choixU';
}
