<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$titre = "Création d'inscription";
$pagea = "inscription";

if (!empty($_POST["HB_pseudo"]) && !empty($_POST["race"])) {
    if (empty($_POST["HB_conf"]) && empty($_POST["HB_mdp"])) {
        $_POST["HB_mdp"] = $_POST["HB_conf"] = gen_mdp(9);
    }

    if ($_POST['HB_conf'] == $_POST['HB_mdp'] && !empty($_POST['HB_mdp'])) {
        $_POST['HB_mdp'] = cxor(gpc("HB_mdp", "post"), sha1(gpc("HB_pseudo", "post").'£'.gpc("race", "post")));
        $cds = sha1(gpc("HB_pseudo", "post").'$'.gpc("race", "post").'£'.gpc("HB_mdp", "post").'#'.gpc("HB_mail", "post").'ß'.time().'Ó'.$_SERVER['HTTP_USER_AGENT'].'♀☻'.$_SERVER['REMOTE_ADDR'].gpc("HB_placement", "post"));
        if (empty($_POST['mailler'])) {
            erreur('MDP: <em>'.gpc("HB_conf", "post").'</em><br />URL : <a href="?p=njoueur&amp;nom='.gpc("HB_pseudo", "post").'&amp;race='.gpc("race", "post").'&amp;mdp='.strhex(gpc("HB_mdp", "post")).'&amp;mail='.gpc("HB_mail", "post").'&amp;ti='.time().'&amp;placement='.gpc("HB_placement", "post").'&amp;cds='.$cds.'">Lien</a><br /><br />L\'inscription doit avoir lieu par vous même en raison des procédures de sécurités !', "white");
        } else {
            if (send_mail(gpc("HB_mail", "post"), "Halo-Battle :: Inscription sur le serveur ".$VAR['serveur_name'], "Bonjour ".gpc("HB_pseudo", "post")." et bienvenue dans l'univers d'Halo-Battle !\n\nNous sommes ravi de vous annoncer qu'un opérateur vient de vous créer un compte sur le serveur ".$VAR['serveur_name'].".\n\nVoici le mot de passe qui vous servira à vous connecter à ce serveur : ".gpc("HB_conf", "post")."\n\nA bientôt,\nLe staff de Halo-Battle")) {
            }
            header('Location: admin.php?p=njoueur&nom='.gpc("HB_pseudo", "post").'&race='.gpc("race", "post").'&mdp='.strhex(gpc("HB_mdp", "post")).'&mail='.gpc("HB_mail", "post").'&ti='.time().'&placement='.gpc("HB_placement", "post").'&cds='.$cds);
        }
    } else {
        erreur('Mot de passe incorrect !');
    }
}
