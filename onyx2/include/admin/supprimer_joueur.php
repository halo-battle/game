<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$titre = '!!! Supprimer joueur !!!';
$pagea = 'erreur';

    $template->assign('linkpage', 'djoueurs');

if ($SESS->level >= 5 && !empty($_GET['id'])) {
    $id = intval(gpc('id'));
    $bdd = new BDD();
    $req = $bdd->unique_query("SELECT * FROM $table_user WHERE id = $id;");
    if ($req && $req["auth_level"] < 4) {
        $bdd->query("DELETE FROM $table_mail WHERE destinataire = $id;");
        $bdd->query("DELETE FROM $table_user WHERE id = $id;");
        $bdd->query("DELETE FROM $table_flottes WHERE id_user = $id;");
        $bdd->query("DELETE FROM $table_planete WHERE id_user = $id;");
        $bdd->deconnexion();
        $template->assign('message', 'Le joueur '.$id.' ('.$req['pseudo'].') a été supprimé du jeu ainsi que toutes les données le concernant !<br />Vérifiez qu\'il ne soit pas fondateur d\'une alliance ou d\'une mission groupée.');
    } else {
        $bdd->deconnexion();
        $template->assign('message', 'Le joueur n\'a pas été trouvé ou vous n\'avez pas les permissions de le supprimer !');
    }
} elseif (!empty($_GET['name'])) {
    $name = gpc('name');
    $bdd = new BDD();
    $bdd->escape($name);
    $req = $bdd->unique_query("SELECT * FROM $table_user WHERE pseudo = '$name';");
    $bdd->deconnexion();

    header('Location: '.$VAR["menu"]["djoueurs"].'&id='.$req['id']);
    exit;
} else {
    $pagea = 'print_choixU';
}
