<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}

$pagea = 'accueil';
$titre = 'Accueil administration';

$bdd = new BDD();
$mail = $bdd->unique_query("SELECT COUNT(id) AS nombre FROM $table_ope_mail WHERE statut = 0;");
$pilori = $bdd->unique_query("SELECT COUNT(id) AS nombre FROM $table_user WHERE mv > 1;");
$news = $bdd->query("SELECT * FROM $table_ope_news ORDER BY id ASC LIMIT 3;");
$bdd->deconnexion();

$template->assign('nbMail', $mail['nombre']);
$template->assign('nbPilori', $pilori['nombre']);
$template->assign('news', $news);
