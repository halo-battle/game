<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$pagea = 'bandeau';
$titre = 'Bandeau d\'information';

//Ajout d'une information
if (!empty($_GET['i']) && $_GET['i'] == 'add' && !empty($_POST['texte']) && isset($_POST['color'])) {
    $texte = gpc('texte', 'post');
    $color = gpc('color', 'post');
    $bdd = new BDD();
    $bdd->escape($texte);
    $bdd->escape($color);
    $bdd->query("INSERT INTO $table_infoshead (texte, color) VALUES ('$texte', '$color');");
    $bdd->deconnexion();

    header('Location: admin.php?p=bandeau');
    exit;
}
//Modification
elseif (!empty($_GET['i'])) {
    $id = intval(gpc('i'));
    if (!empty($_POST['texte']) && isset($_POST['color'])) {
        $texte = gpc('texte', 'post');
        $color = gpc('color', 'post');
        $bdd = new BDD();
        $bdd->escape($texte);
        $bdd->escape($color);
        $bdd->query("UPDATE $table_infoshead SET texte = '$texte', color = '$color' WHERE id = '$id';");
        $bdd->deconnexion();
        
        header('Location: admin.php?p=bandeau');
        exit;
    }
    $bdd = new BDD();
    $req = $bdd->unique_query("SELECT * FROM $table_infoshead WHERE id = '$id';");
    $reqTT = $bdd->query("SELECT * FROM $table_infoshead ORDER BY id ASC;");
    $bdd->deconnexion();
    $template->assign('mod', $req);
    $template->assign('tableau', $reqTT);
    $template->assign('id', $id);
}
//Demande de suppression
elseif (!empty($_GET['d'])) {
    $id = intval(gpc('d'));
    $bdd = new BDD();
    $bdd->query("DELETE FROM $table_infoshead WHERE id = $id;");
    $bdd->deconnexion();
    
    header('Location: admin.php?p=bandeau');
    exit;
}
//Demande de mise à jour du cache
elseif (isset($_GET['actuCache'])) {
    Cache::del('headerNB');
    
    header('Location: admin.php?p=bandeau');
    exit;
} else {
    //Récupération des lignes du bandeau
    $bdd = new BDD();
    $req = $bdd->query("SELECT * FROM $table_infoshead ORDER BY id ASC;");
    $bdd->deconnexion();

    $template->assign('tableau', $req);
    $template->assign('id', "add");
}
