<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$pagea = 'demarrage';
$titre = 'Page de démarrage';

if (!empty($_GET['i']) && $_GET['i'] == 'add' && !empty($_POST['contenu']) && isset($_POST['titre'])) {
    $titre = gpc('titre', 'post');
    $contenu = gpc('contenu', 'post');
    $time = time();
    $bdd = new BDD();
    $bdd ->escape($titre);
    $bdd ->escape($contenu);
    $bdd ->query("INSERT INTO $table_messages_demarrage (titre, contenu, time) VALUES ('$titre', '$contenu', $time);");
    $bdd ->deconnexion();

    header('Location: admin.php?p=demarrage');
    exit;
} elseif (!empty($_GET['i'])) {
    $id = intval(gpc('i'));
    if (!empty($_POST['contenu']) && isset($_POST['titre'])) {
        $titre = gpc('titre', 'post');
        $texte = gpc('contenu', 'post');
        $time = time();
        $reset = gpc('reset', 'post');
        $bdd = new BDD();
        $bdd->escape($titre);
        $bdd->escape($texte);
        if (!empty($reset)) {
            $bdd->query("UPDATE $table_messages_demarrage SET contenu = '$texte', titre = '$titre', time = $time WHERE id = $id;");
        } else {
            $bdd->query("UPDATE $table_messages_demarrage SET contenu = '$texte', titre = '$titre' WHERE id = $id;");
        }
        $bdd->deconnexion();

        header('Location: admin.php?p=demarrage');
        exit;
    }
    $bdd = new BDD();
    $template->assign('mod', $bdd->unique_query("SELECT * FROM $table_messages_demarrage WHERE id = '$id';"));
    $template->assign('tableau', $bdd->query("SELECT * FROM $table_messages_demarrage;"));
    $bdd->deconnexion();
    $template->assign('id', $id);
}
//Suppression d'un message
elseif (!empty($_GET['d'])) {
    $id = intval(gpc('d'));
    $bdd = new BDD();
    $bdd->query("DELETE FROM $table_messages_demarrage WHERE id = '$id';");
    $bdd->deconnexion();
    
    header('Location: admin.php?p=demarrage');
    exit;
}
//Demande de mise à jour du cache
elseif (isset($_GET['actuCache'])) {
    Cache::del('headerNB');
    
    header('Location: admin.php?p=demarrage');
    exit;
} else {
    $bdd = new BDD();
    $template->assign('tableau', $bdd->query("SELECT * FROM $table_messages_demarrage;"));
    $bdd->deconnexion();
    $template->assign('id', 'add');
}
