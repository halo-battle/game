<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$pagea = 'mail_mass';
$titre = 'Envoie de mail en masse aux joueurs de la galaxie';

    $template->assign('linkpage', 'mail_mass');

if (!empty($_POST['sujet']) || !empty($_POST['message'])) {
    $sujet = "Halo-Battle :: ".gpc('sujet', 'post');
    $message = gpc('message', 'post');
    $bdd = new BDD();
    $users = $bdd->query("SELECT pseudo, mail FROM $table_user;");
    $bdd->deconnexion();

    foreach ($users as $user) {
        $messageJ = str_replace('$pseudo', $user["pseudo"], $message);
        send_mail($user["mail"], $sujet, $messageJ);
        //print 'Mail : '.$user["mail"].' ; sujet : '.$sujet.' ; message : '.$messageJ.'<br />';
    }

    erreur("Tous les mails ont été envoyés avec succès.", "green");
}
