<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$pagea = 'vflotte';
$titre = 'Vérification flottes';

//Vérification des flottes
if (!isset($_GET['ceil'])) {
    $_GET['ceil'] = 5000;
}

$bdd = new BDD();
$res = $bdd->query("SELECT * FROM `$table_flottes` ORDER BY `start_time` DESC");
$bdd->deconnexion();
$tableau = array();
if (isset($resultat)) {
    foreach ($resultat as $res) {
        $user_source = infoPlan($resultat['end_galaxie'], $resultat['end_ss'], $resultat['end_position'], 'id_user');
        if ($resultat['contenu_metal'] + $resultat['contenu_cristal'] + $resultat['contenu_hydrogene'] >= $_GET['ceil']) {
            $color = 'FF0000';
        } elseif ($user_source != $resultat['id_user']) {
            $color = 'DFBF00';
        } else {
            $color = false;
        }
        $tableau[] = array(trouvNom($user_source), trouvNom($resultat['id_user']), infoPlan($resultat['start_galaxie'], $resultat['start_ss'], $resultat['start_position'], 'nom_planete'), '['.$resultat['start_galaxie'].':'.$resultat['start_ss'].':'.$resultat['start_position'].']', infoPlan($resultat['end_galaxie'], $resultat['end_ss'], $resultat['end_position'], 'nom_planete'), '['.$resultat['end_galaxie'].':'.$resultat['end_ss'].':'.$resultat['end_position'].']', $resultat['start_time'], '<acronym title="!!!Détail vaisseaux">'.$resultat['nb_vais'].'</acronym>', $resultat['vitesse'], $resultat['contenu_metal'], $resultat['contenu_cristal'], $resultat['contenu_hydrogene'], $color);
    }
}

$template->assign('flottes', $tableau);
