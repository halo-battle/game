<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$pagea = 'version';
$titre = 'Versions';

//Ajout d'une version
if (!empty($_GET['i']) && $_GET['i'] == 'add' && $SESS->level >= 7 && !empty($_POST['version']) && !empty($_POST['contenu'])) {
    $version = gpc('version', 'post');
    $contenu = gpc('contenu', 'post');
    $temps = time();
    $nom_user = trouvNom($SESS->values['id']);
    $bdd = new BDD();
    $bdd->escape($version);
    $bdd->escape($contenu);
    $bdd->escape($nom_user);
    $bdd->query("INSERT INTO $table_version (version, contenu, temps, pseudo) VALUES ('$version', '$contenu', $temps, '$nom_user');");
    $bdd->deconnexion();

    header('Location: admin.php?p=version');
    exit;
}
//Modification d'une version
elseif (!empty($_GET['i'])) {
    $id = intval(gpc('i'));
    if (!empty($_POST['version']) && !empty($_POST['contenu'])) {
        $version = gpc('version', 'post');
        $contenu = gpc('contenu', 'post');
        $bdd = new BDD();
        $bdd->escape($version);
        $bdd->escape($contenu);
        $bdd->query("UPDATE $table_version SET version = '$version', contenu = '$contenu' WHERE id = $id;");
        $bdd->deconnexion();

        header('Location: admin.php?p=version');
        exit;
    }
    $bdd = new BDD();
    $template->assign('mod', $bdd->unique_query("SELECT * FROM $table_version WHERE id = '$id';"));
    $template->assign('tableau', $bdd->query("SELECT * FROM $table_version ORDER BY temps DESC;"));
    $bdd->deconnexion();
    $template->assign('id', $id);
}
//Suppression d'une version
elseif (!empty($_GET['d']) && $SESS->level >= 7) {
    $id = intval(gpc('d'));
    $bdd = new BDD();
    $bdd->query("DELETE FROM $table_version WHERE id = $id;");
    $bdd->deconnexion();

    header('Location: admin.php?p=version');
    exit;
}
//Demande de mise à jour du cache
elseif (isset($_GET['actuCache'])) {
    Cache::del('versionsDATA');
    
    header('Location: admin.php?p=version');
    exit;
} else {
    $bdd = new BDD();
    $template->assign('tableau', $bdd->query("SELECT * FROM $table_version ORDER BY temps DESC;"));
    $bdd->deconnexion();
    $template->assign('id', 'add');
}
