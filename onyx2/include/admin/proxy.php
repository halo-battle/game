<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$pagea = 'vide';
$titre = 'Mise à jour de la liste des proxys';

function traiterfichier($uri, &$list)
{
    $fp = fopen($uri, "r");
    while (!feof($fp)) {
        $buffer = fgets($fp);
        if (preg_match("#^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}#", $buffer, $match)) {
            $list[] = $match[0];
        }
        //$list[] = substr($buffer, 0, strpos($buffer, ":"));
    }
    fclose($fp);
}

$list = array();

traiterfichier("http://www.proxylists.net/socks4.txt", $list);
traiterfichier("http://www.proxylists.net/socks5.txt", $list);
traiterfichier("http://www.proxylists.net/http_highanon.txt", $list);
traiterfichier("http://www.proxylists.net/http.txt", $list);

traiterfichier("http://www.textproxylists.com/proxy.php?anonymous", $list);
traiterfichier("http://www.textproxylists.com/proxy.php?highanonymity", $list);
traiterfichier("http://www.textproxylists.com/proxy.php?codeen", $list);
traiterfichier("http://www.textproxylists.com/proxy.php?transparent", $list);
traiterfichier("http://www.textproxylists.com/proxy.php?nontransparent", $list);
//traiterfichier("http://www.textproxylists.com/proxy.php?allproxy", $list);

//traiterfichier("http://www.multiproxy.org/txt_anon/proxy.txt", $list);
traiterfichier("http://www.multiproxy.org/txt_all/proxy.txt", $list);

$bdd = new BDD();
$bdd->query("TRUNCATE TABLE  `proxy_list`;");
$bdd->query("INSERT INTO proxy_list VALUES ('".implode("'),('", $list)."');");
$bdd->deconnexion();

erreur("Procédure terminée, ".count($list)." proxys listés.", "green");
