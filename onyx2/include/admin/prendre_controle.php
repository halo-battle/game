<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}
$titre = 'Prise de contrôle d\'un joueur';
$pagea = 'erreur';

    $template->assign('linkpage', 'cjoueurs');

if ($SESS->level >= 5 && !empty($_GET['id'])) {
    $name = intval(gpc('id'));
    $bdd = new BDD();
    $req = $bdd->unique_query("SELECT id, pseudo, auth_level FROM $table_user WHERE id = $name;");
    $bdd->deconnexion();
    if ($req['auth_level'] >= $SESS->level) {
        $template->assign('message', 'Vous ne pouvez pas prendre le contrôle de cet utilisateur !');
    } else {
        $bdd->reconnexion();
        $reqPl = $bdd->unique_query("SELECT id, nom_planete FROM $table_planete WHERE id_user = '".$req['id']."' LIMIT 1;");
        $bdd->deconnexion();

        if (empty($SESS->values['souscontrole'])) {
            $SESS->values['souscontrole'] = array($SESS->values['id'], $SESS->values['idPlan']);
        }
        $SESS->values['id'] = $req['id'];
        $SESS->values['idPlan'] = $reqPl['id'];
        $SESS->put();

        $template->assign('message', 'Vous contrôlez maintenant le joueur '.$req['pseudo'].'.<br />Planète '.$reqPl['nom_planete'].' sélectionnée !');
    }
} elseif (!empty($_GET['name'])) {
    $name = gpc('name');
    $bdd = new BDD();
    $bdd->escape($name);
    $req = $bdd->unique_query("SELECT id FROM $table_user WHERE pseudo = '$name';");
    $bdd->deconnexion();
    header('Location: admin.php?p=cjoueurs&id='.$req['id']);
    exit;
} else {
    $pagea = 'print_choixU';
}
