<?php	//vars.php : contient toutes les valeurs pour construire chaque batiments, unités ou technologies
trigger_error("Utilisation de ce fichier déprécié !");

if (!isset($technolo)) {
    if (empty($race) && empty($sess->values['race'])) {
        $race = "none";
    } elseif (empty($race)) {
        $race = $sess->values['race'];
    }
    include_once("game/noms.php");
}

/*
 * Défenses
 */
    //Attaque
    $defense_at = array(200,800,1500,500,4500,15000,10000,25000);
    //Défense (bouclier)
    $defense_bc = array(100,300,600,400,1000,4000,10000,20000);
    //Coque (pv)
    $defense_pv = array(1000,3000,6000,1800,4000,1000,15000,25000);

    //Débris de métal
    $defense_md = array(300,300,300,300,300,300,300,300);
    //Débris de cristal
    $defense_cd = array(300,300,300,300,300,300,300,300);

/*
 * Vaisseaux
 */
    //Nombre maximal de ressources embarquées
    $nomvais_rs = array(10000,50000,25000,25000,100,50,200,400,800,1000,1500,50000,100000,500);

    //Coefficient vitesse
    $nomvais_vitesseP = array(5,5,6,5,10,8,5,7,6,5,4,2,1,6);
    //Temps de préparation
    $nomvais_vitesseS = array(5,5,6,5,4,4,5,7,6,5,4,3,2,10);
    //Temps de chauffe
    $nomvais_vitesseG = array(5,5,6,5,1,1,5,7,7,7,6,4,3,10);

    //Attaque
    $nomvais_at = array(50,50,50,50,150,200,400,900,1500,3500,6000,7000,10000,100);
    //Défense (bouclier)
    $nomvais_bc = array(0,0,0,50,0,200,500,1250,2500,6000,5000,8000,10000,800);
    //Coque (pv)
    $nomvais_pv = array(1000,1500,1000,1000,1000,1000,2500,5500,9000,15000,25000,35000,50000,3000);
    //Débris de métal
    $nomvais_md = array(300,300,300,300,300,300,300,300,300,300,300,36000);
    //Débris de cristal
    $nomvais_cd = array(300,300,300,300,300,300,300,300,300,300,300,24000);


/*
  Aide-mémoire pour le format des tableaux des nécessités
   - Type général : batiments, technologies, ...
   - ID de l'objet ou branche pour les technologies
   - Niveau requis ou id pour les technologies
   - Bits requis (technologies uniquement)
*/
$neededCaserne =
    array(
        array(
            array('batiments', 9, 1)
        ),
        array(
            array('batiments', 9, 2)
        ),
        array(
            array('batiments', 9, 3)
        ),
        array(
            array('batiments', 9, 5)
        ),
        array(
            array('batiments', 9, 3)
        ),
        array(
            array('batiments', 9, 10)
        ),
        array(
            array('batiments', 9, 2)
        ),
        array(
            array('batiments', 9, 2)
        ),
        array(
            array('batiments', 9, 5)
        )
    );
$neededBatiments =
    array(
        0,
        0,
        0,
        0,
        array(
            array('batiments', 3, 12)
        ),
        0,
        0,
        array(
            array('technologies', 2,3, 9)
        ),
        array(
            array('technologies', 2,4, 17)
        ),
        0,
        0,
        array(
            array('technologies', 1,12, 4096)
        ),
        0,
        array(
            array('technologies', 3,8, 256)
        ),
        array(
            array('technologies', 7,12, 4096)
        ),
        array(
            array('technologies', 3,6, 64)
        ),
        array(
            array('technologies', 3,7, 128)
        ),
        0
    );
$neededTerrestre = array(
    array(
        array('batiments', 7, 1)
    ),
    array(
        array('batiments', 7, 3)
    ),
    array(
        array('batiments', 7, 4)
    ),
    array(
        array('batiments', 7, 6)
    ),
    array(
        array('batiments', 7, 1)
    ),
    array(
        array('batiments', 7, 2)
    ),
    array(
        array('batiments', 7, 3)
    ),
    array(
        array('batiments', 7, 5)
    ),
    //Défenses
    array(
        array('batiments', 7, 1),
        array('technologies', 6, 0, 1)
    ),
    array(
        array('batiments', 7, 3),
        array('technologies', 6, 3, 8)
    ),
    array(
        array('batiments', 7, 4),
        array('technologies', 6, 1, 2)
    ),
    array(
        array('batiments', 7, 4),
        array('technologies', 6, 4, 16)
    ),
    array(
        array('batiments', 7, 8),
        array('technologies', 6, 2, 4)
    ),
    array(
        array('batiments', 7, 8),
        array('technologies', 6, 5, 32)
    ),
    array(
        array('batiments', 7, 8),
        array('technologies', 6, 6, 64)
    ),
    array(
        array('batiments', 7, 10),
        array('technologies', 7, 11, 2048)
    )
);
$neededVaisseaux =
    array(
        array(
            array('batiments', 8, 1),
            array('technologies', 0,3, 8)
        ),
        array(
            array('batiments', 8, 5),
            array('technologies', 0,3, 8),
            array('technologies', 1,6, 64)
        ),
        array(
            array('batiments', 8, 5),
            array('technologies', 8,0, 1)
        ),
        array(
            array('batiments', 8, 5),
            array('technologies', 7,4, 16)
        ),
        array(
            array('batiments', 8, 5),
            array('technologies', 7,0, 1)
        ),
        array(
            array('batiments', 8, 3),
            array('technologies', 7,1, 2)
        ),
        array(
            array('batiments', 8, 5),
            array('technologies', 7,2, 4)
        ),
        array(
            array('batiments', 8, 1),
            array('technologies', 7,3, 8)
        ),
        array(
            array('batiments', 8, 3),
            array('technologies', 7,5, 32)
        ),
        array(
            array('batiments', 8, 6),
            array('technologies', 7,6, 64)
        ),
        array(
            array('batiments', 8, 6),
            array('technologies', 7,1, 128)
        ),
        array(
            array('batiments', 8, 8),
            array('technologies', 7,8, 256)
        ),
        array(
            array('batiments', 8, 10),
            array('technologies', 7,9, 512)
        ),
        array(
            array('batiments', 8, 10),
            array('technologies', 7,10, 1024)
        )
    );

$neededAlli_Batiments = array(
    0,
    array(
        array('batiments', 0, 1)
    ),
    array(
        array('batiments', 0, 3)
    ),
    array(
        array('batiments', 0, 5)
    ),
    array(
        array('batiments', 0, 4)
    ),
    array(
        array('batiments', 0, 2)
    )
);
