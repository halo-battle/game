<?php
if (!defined('INDEX')) {
    header('Location: ../');
    exit;
}

if (DB_TYPE == "postgresql") {
    $table_alliances = '"public"."'.$config['db_prefix'].'alliances"';
    $table_alliances_attente = '"public"."'.$config['db_prefix'].'alliances_attente"';
    $table_alliances_chat = '"public"."'.$config['db_prefix'].'alliances_chat"';
    $table_alliances_creation = '"public"."'.$config['db_prefix'].'alliances_creation"';
    $table_alliances_grade = '"public"."'.$config['db_prefix'].'alliances_grade"';
    $table_bourse = '"public"."'.$config['db_prefix'].'bourse"';
    $table_bourse_ressources = '"public"."'.$config['db_prefix'].'bourse_ressources"';
    $table_bug = '"public"."'.$config['db_prefix'].'bug"';
    $table_classement = '"public"."'.$config['db_prefix'].'classement'.(floor((time()-$VAR['time_maintenance'])/86400)%2).'"';
    $table_classement_alliances = '"public"."'.$config['db_prefix'].'classement_alliances'.(floor((time()-$VAR['time_maintenance'])/86400)%2).'"';
    $table_flottes = '"public"."'.$config['db_prefix'].'flottes"';
    $table_flottes_combats = '"public"."'.$config['db_prefix'].'flottes_combats"';
    $table_flottes_preparation = '"public"."'.$config['db_prefix'].'flottes_preparation"';
    $table_infoshead = '"public"."'.$config['db_prefix'].'infoshead"';
    $table_mail = '"public"."'.$config['db_prefix'].'mail"';
    $table_messages_demarrage = '"public"."'.$config['db_prefix'].'messages_demarrage"';
    $table_ope_faq = '"public"."'.$config['db_prefix'].'ope_faq"';
    $table_ope_mail = '"public"."'.$config['db_prefix'].'ope_mail"';
    $table_ope_modele = '"public"."'.$config['db_prefix'].'ope_modele"';
    $table_ope_news = '"public"."'.$config['db_prefix'].'ope_news"';
    $table_planete = '"public"."'.$config['db_prefix'].'planete"';
    $table_registre_identification = '"public"."'.$config['db_prefix'].'registre_identification"';
    $table_sessions = '"public"."'.$config['db_prefix'].'sessions"';
    $table_user = '"public"."'.$config['db_prefix'].'user"';
    $table_user_inscriptions = '"public"."'.$config['db_prefix'].'user_inscriptions"';
    $table_version = '"public"."'.$config['db_prefix'].'version"';
} elseif (DB_TYPE == "mysql") {
    $table_alliances = $VAR['db_prefix'].'alliances';
    $table_alliances_attente = $VAR['db_prefix'].'alliances_attente';
    $table_alliances_chat = $VAR['db_prefix'].'alliances_chat';
    $table_alliances_creation = $VAR['db_prefix'].'alliances_creation';
    $table_alliances_emprunt = $VAR['db_prefix'].'alliances_emprunt';
    $table_alliances_grade = $VAR['db_prefix'].'alliances_grade';
    $table_alliances_mail = $VAR['db_prefix'].'alliances_mail';
    $table_alliances_pactes = $VAR['db_prefix'].'alliances_pactes';
    $table_bourse = $VAR['db_prefix'].'bourse';
    $table_bourse_ressources = $VAR['db_prefix'].'bourse_ressources';
    $table_bug = $VAR['db_prefix'].'bug';
    $table_classement = $VAR['db_prefix'].'classement'.(floor((time()-$VAR['time_maintenance'])/86400)%2);
    $table_classement_alliances = $VAR['db_prefix'].'classement_alliances'.(floor((time()-$VAR['time_maintenance'])/86400)%2);
    $table_flottes = $VAR['db_prefix'].'flottes';
    $table_flottes_combats = $VAR['db_prefix'].'flottes_combats';
    $table_flottes_preparation = $VAR['db_prefix'].'flottes_preparation';
    $table_infoshead = $VAR['db_prefix'].'infoshead';
    $table_mail = $VAR['db_prefix'].'mail';
    $table_messages_demarrage = $VAR['db_prefix'].'messages_demarrage';
    $table_ope_faq = $VAR['db_prefix'].'ope_faq';
    $table_ope_mail = $VAR['db_prefix'].'ope_mail';
    $table_ope_modele = $VAR['db_prefix'].'ope_modele';
    $table_ope_news = $VAR['db_prefix'].'ope_news';
    $table_planete = $VAR['db_prefix'].'planete';
    $table_registre_identification = $VAR['db_prefix'].'registre_identification';
    $table_sessions = $VAR['db_prefix'].'sessions';
    $table_user = $VAR['db_prefix'].'user';
    $table_user_inscriptions = $VAR['db_prefix'].'user_inscriptions';
    $table_version = $VAR['db_prefix'].'version';
}
