{include file='game/header.tpl'}
		<h2>Bourse des ressources</h2>
		<h3>Les prix sont donnés en milliers de crédits</h3>
		<h3>Les achats et les ventes se font par paquet de 1000 actions (le prix est recalculé chaque 1000 actions)</h3>
{foreach from=$bourse item=action}
		<dl class="bourse">
		    <dt><strong>{$LANG[$race].ressources.noms[$action.id]|ucfirst}</strong> <a href="{$menu.marche}&amp;d={$action.id}"><img src="bourser.php?a={$action.id}&amp;r={$planete->race}" class="imglink" alt="{$LANG[$race].ressources.noms[$action.id]}" /></a></dt>
		    <dd>
			<form action="{$menu.marche}" method="post">
				<fieldset>
					<input type="hidden" name="ressource" value="{$action.id}" />
					<div><strong>Total :</strong> <em>{$action.dispo|separerNombres}</em></div>
				    <label for="buy{$action.id}">Acheter pour <em>{$action.prixA|separerNombres}</em> : <input type="text" class="text" id="buy{$action.id}" name="nbb" value="0" /></label>
				    <input type="submit" class="submit" value="OK" name="buy" /><br />
				    <label for="sell{$action.id}">Vendre pour <em>{$action.prixV|separerNombres}</em> : <input type="text" class="text" id="sell{$action.id}" name="nbs" value="0" /></label>
				    <input type="submit" class="submit" value="OK" name="sell" />
				</fieldset>
			</form>
		    </dd>
		</dl>
{/foreach}
{include file='game/footer.tpl'}