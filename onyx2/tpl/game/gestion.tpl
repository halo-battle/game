{include file='game/header.tpl'}
{if $smarty.const.SURFACE == 'planete'}
			<h2>Politique</h2>
			<form action="{$menu.gestion}" method="post">
				<fieldset class="form">
					<label for="politique">Politique actuelle : <select name="politique" id="politique">{html_options options=$politiques selected=$planete->politique}</select></label><br /><br />
					<label>Dernier changement de régime : <em>{$planete->politique_lastchange|date_format:"%d/%m/%y"}</em></label><br /><br />
					<label>Moral global de la population : <em>{$planete->moral*100}%</em></label><br /><br />
					<input class="submit" type="submit" value="Ok" />
				</fieldset>
			</form>
{/if}
			<h2>Gestion</h2>
			<div id="gestion">
				<table>
					<thead>
						<tr>
							<th></th>
{foreach from=$planetes item=plan}
							<td><img src="{$url_images}images/planetes/{$plan.image}.jpg" alt="{$plan.image}" title="{$plan.nom_planete}" width="80" height="80" /></td>
{/foreach}
						</tr>
						<tr>
							<th></th>
{foreach from=$planetes item=plan}
							<th>{$plan.nom_planete|escape}</th>
{/foreach}
						</tr>
					</thead>
					<tbody>
						<tr>
							<th>{$LANG[$race].ressources.noms.metal|ucfirst}</th>
{foreach from=$planetes item=plan}
{if $plan.id == $planeteEC.id}{assign var='plan' value=$planeteEC}{/if}
							<td>{$plan.metal|separerNombres}</td>
{/foreach}
						</tr>
						<tr>
							<th>{$LANG[$race].ressources.noms.cristal|ucfirst}</th>
{foreach from=$planetes item=plan}
{if $plan.id == $planeteEC.id}{assign var='plan' value=$planeteEC}{/if}
							<td>{$plan.cristal|separerNombres}</td>
{/foreach}
						</tr>
						<tr>
							<th>{$LANG[$race].ressources.noms.hydrogene|ucfirst}</th>
{foreach from=$planetes item=plan}
{if $plan.id == $planeteEC.id}{assign var='plan' value=$planeteEC}{/if}
							<td>{$plan.hydrogene|separerNombres}</td>
{/foreach}
						</tr>
						<tr>
							<th>{$LANG[$race].ressources.noms.energie|ucfirst}</th>
{foreach from=$planetes item=plan}
{if $plan.id == $planeteEC.id}{assign var='plan' value=$planeteEC}{/if}
							<td>{$plan.energie|separerNombres}</td>
{/foreach}
						</tr>
						<tr>
							<th>Population</th>
{foreach from=$planetes item=plan}
{if $plan.id == $planeteEC.id}{assign var='plan' value=$planeteEC}{/if}
							<td>{$plan.population|separerNombres}</td>
{/foreach}
						</tr>
						<tr>
							<th>Moral</th>
{foreach from=$planetes item=plan}
							<td>{$plan.moral*100|separerNombres}%</td>
{/foreach}
						</tr>
					</tbody>
				</table>
			</div>
{include file='game/footer.tpl'}