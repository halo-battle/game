{include file='game/header.tpl'}
{include file='game/diplomatie/common.tpl'}
			<h2>Déclarations de guerre</h2>
{if $guerres}
			<ul>
	{foreach from=$guerres item=pacte}
				<li><a href="{$menu.alliance}&amp;v={$pacte.alliance_id}">[{$pacte.tag}] {$pacte.nom_alliance}</a> déclarée le {$pacte.time_creation|date_format:"%d/%m/%y"} et terminiée le {$pacte.time_fin|date_format:"%d/%m/%y"}</li>
	{/foreach}
			</ul>
{else}
			<h3>Aucune guerre n'a été déclarée</h3>
{/if}
			<br />
			<h2>Pactes de non agression</h2>
{if $pnas}
			<ul>
	{foreach from=$pnas item=pacte}
				<li><a href="{$menu.alliance}&amp;v={$pacte.alliance_id}">[{$pacte.tag}] {$pacte.nom_alliance}</a> créé le {$pacte.time_creation|date_format:"%d/%m/%y"} et abrogé le {$pacte.time_fin|date_format:"%d/%m/%y"}</li>
	{/foreach}
			</ul>
{else}
			<h3>Aucun pacte de ce type n'a encore été signé</h3>
{/if}
			<br />
			<h2>Pactes commerciaux</h2>
{if $pcs}
			<ul>
	{foreach from=$pcs item=pacte}
				<li><a href="{$menu.alliance}&amp;v={$pacte.alliance_id}">[{$pacte.tag}] {$pacte.nom_alliance}</a> créé le {$pacte.time_creation|date_format:"%d/%m/%y"} et abrogé le {$pacte.time_fin|date_format:"%d/%m/%y"}</li>
	{/foreach}
			</ul>
{else}
			<h3>Aucun pacte de ce type n'a encore été signé</h3>
{/if}
			<br />
			<h2>Pactes militaires</h2>
{if $pms}
			<ul>
	{foreach from=$pms item=pacte}
				<li><a href="{$menu.alliance}&amp;v={$pacte.alliance_id}">[{$pacte.tag}] {$pacte.nom_alliance}</a> créé le {$pacte.time_creation|date_format:"%d/%m/%y"} et abrogé le {$pacte.time_fin|date_format:"%d/%m/%y"}</li>
	{/foreach}
			</ul>
{else}
			<h3>Aucun pacte de ce type n'a encore été signé</h3>
{/if}
{include file='game/footer.tpl'}