{include file='game/header.tpl'}
{include file='game/diplomatie/common.tpl'}
			<h2>Demandes de pactes</h2>
{if $demandes}
	{foreach from=$demandes item=demande}
			<h4><strong>{$demande.time_creation|date_format:"%d/%m/%y %H:%M"} ::</strong> L'alliance <a href="{$menu.alliance}&amp;v={$demande.alliance_id}">[{$demande.tag}] {$demande.nom_alliance}</a> veut {if $demande.type == 0}engager un cesser le feu{else}passer un pacte {if $demande.type == 1}de non-agression{elseif $demande.type == 2}commercial{else}militaire{/if}{/if}.{if $planete->permissions_alliance &8} <a href="{$menu.diplomatie}&amp;a={$demande.id}">Accepter</a> - <a href="{$menu.diplomatie}&amp;r={$demande.id}">Rejeter</a>{/if}</h4>
	{/foreach}
{else}
			<h3>Aucune demande en cours</h3>
{/if}
			<br />
			<h2>Actualités des pactes et des guerres</h2>
{if $actus_alli}
{foreach from=$actus_alli item=actu_alli}
			{if $actu_alli.time_fin && $actu_alli.type != 0}<h3>{$actu_alli.time_fin|date_format:"%d/%m/%y %H:%M"} :: Le pacte {if $actu_alli.type == 1}de non-agression{elseif $actu_alli.type == 2}commercial{else}militaire{/if} avec  l'alliance <a href="{$menu.alliance}&amp;v={$actu_alli.alliance_id}">[{$actu_alli.tag}] {$actu_alli.nom_alliance}</a> a été abrogé</h3>
			{else}{if ($planete->id == $actu_alli.id_alliance1 && $actu_alli.type != 0) || ($planete->id == $actu_alli.id_alliance2 && $actu_alli.type == 0 && $actu_alli.accepter == 1) || ($planete->id == $actu_alli.id_alliance1 && $actu_alli.type == 0 && $actu_alli.time_demand != $actu_alli.time_creation|date_format:"%Y-%m-%d %H:%M:%S")}<h3><strong>{$actu_alli.time_creation|date_format:"%d/%m/%y %H:%M"} ::</strong> L'alliance <a href="{$menu.alliance}&amp;v={$actu_alli.alliance_id}">[{$actu_alli.tag}] {$actu_alli.nom_alliance}</a> {if $actu_alli.type == 0 && $actu_alli.time_demand == $actu_alli.time_creation|date_format:"%Y-%m-%d %H:%M:%S"}nous a déclaré la guerre{else}{if $actu_alli.accepte == 2}a refusé notre{elseif $actu_alli.accepte == 1}a accepté notre{else}n'a pas encore répondu à notre demande de{/if} {if $actu_alli.type == 0}cesser le feu{else}pacte {if $actu_alli.type == 1}de non-agression{elseif $actu_alli.type == 2}commercial{else}militaire{/if}{/if}{/if}.</h3>
			{else}<h3><strong>{$actu_alli.time_creation|date_format:"%d/%m/%y %H:%M"} ::</strong> Nous avons {if $actu_alli.type == 0 && $actu_alli.time_demand == $actu_alli.time_creation|date_format:"%Y-%m-%d %H:%M:%S"}déclaré la guerre à{else}{if $actu_alli.accepte == 1}accepté{else}refusé{/if} le {if $actu_alli.type == 0}cesser le feu{else}pacte {if $actu_alli.type == 1}de non-agression{elseif $actu_alli.type == 2}commercial{else}militaire{/if}{/if} proposé par{/if} l'alliance <a href="{$menu.alliance}&amp;v={$actu_alli.alliance_id}">[{$actu_alli.tag}] {$actu_alli.nom_alliance}</a></h3>
			{/if}{/if}
{/foreach}
{else}
			<h3>Aucune actualité disponible</h3>
{/if}
			<br />
			<h2>Actualités de la galaxie</h2>
{if $actus_world}
{foreach from=$actus_world item=actu_world}
			<h3><strong>{$actu_world.time_creation|date_format:"%d/%m/%y %H:%M"} ::</strong> L'alliance <a href="{$menu.alliance}&amp;v={$actu_world.alliance1_id}">[{$actu_world.tag1}] {$actu_world.nom_alliance1}</a> {if $actu_world.type == 0}a déclaré la guerre à{else}a engagé un pacte {if $actu_world.type == 1}de non-agression{elseif $actu_world.type == 2}commercial{else}militaire{/if} avec{/if} l'alliance <a href="{$menu.alliance}&amp;v={$actu_world.alliance2_id}">[{$actu_world.tag2}] {$actu_world.nom_alliance2}</a>.</h3>
{/foreach}
{else}
			<h3>Aucune actualité disponible</h3>
{/if}
{include file='game/footer.tpl'}