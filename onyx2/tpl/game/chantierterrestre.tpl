{include file='game/header.tpl'}
		<ul class="onglets">
		{section name=nb start=0 loop=$planete->batiments.7}
                         {assign var=i value=$smarty.section.nb.index}
			<li{if $lieu == $i} class="hilight"{/if}><a href="{$menu.chantierterrestre}&amp;k={$i}&amp;n={$onglet}">Chantier {$i+1}</a></li>
		{/section}
		</ul>

		<ul class="onglets">
			<li{if $onglet != "defenses"} class="hilight"{/if}><a href="{$menu.chantierterrestre}&amp;k={$lieu}">Unités</a></li>
			<li{if $onglet == "defenses"} class="hilight"{/if}><a href="{$menu.chantierterrestre}&amp;k={$lieu}&amp;n=defenses">Défenses</a></li>
		</ul>
		<h2>File d'attente</h2>
		<ul id="file">
{capture name='expFile'}
{assign var='keyF' value=$lieu}
{foreach from=$files.$keyF item=element key=keyE}
	{if $element.5}<li><strong>Prochaine {if $element.6}unit&eacute;{else}d&eacute;fense{/if} </strong> : <span class="countdown">{$element.4|countdown}</span></li>{/if}
	<li>{$element.1} {if $element.1 > 1}{$LANG[$race].terrestre.noms_pluriel[$element.0]}{else}{$LANG[$race].terrestre.noms_sing[$element.0]}{/if} {if $element.2} (démolition){/if} - <span{if $element.5} class="countdown"{/if}>{$element.3|countdown}</span> - <a href="{$menu.chantierterrestre}&amp;n={$onglet}&amp;k={$lieu}&amp;a={$keyF}&amp;b={$keyE}">Annuler un</a> - <a href="{$menu.chantierterrestre}&amp;n={$onglet}&amp;k={$lieu}&amp;a={$keyF}&amp;b={$keyE}&amp;s={$element.1}">Annuler tous</a></li>
{/foreach}
{/capture}
{if $files && $smarty.capture.expFile}
	{$smarty.capture.expFile}
{else}
			<li>Aucune unit&eacute; dans la file d'attente</li>
{/if}
		</ul>

		<h3><a href="{$menu.arbre}&amp;q=chantierterrestre">Arbre des technologies</a></h3><br />
		<h2>{$LANG[$race].batiments.noms_sing.7|ucfirst}</h2>
{if $unites}
		<div id="constructions">
{foreach from=$unites item=unite}
	        <dl>
				<dt>{$LANG[$race].terrestre.noms_sing[$unite.id]|ucfirst}{if $unite.nombre > 0} ({$unite.nombre} unité{if $unite.nombre > 1}s{/if}){/if}</dt>
				<dd class="description"><a href="?p=description&amp;d={$unite.id}#body"><img src="{$url_images}images/terrestre/{$unite.image}" alt="{$LANG[$race].terrestre.noms_sing[$unite.id]|ucfirst}" /></a><p>{$LANG[$race].terrestre.descriptions[$unite.id]}</p></dd>
				<dd>
					{if $unite.nombre > 0}<strong>Nombre actuel :</strong> {$unite.nombre}<br /><br />{/if}
					{if $unite.nec_metal > 0}<strong>Coût {$LANG[$race].ressources.noms.metal} :</strong> {$unite.nec_metal|separerNombres}<br />{/if}
					{if $unite.nec_cristal > 0}<strong>Coût {$LANG[$race].ressources.noms.cristal} :</strong> {$unite.nec_cristal|separerNombres}<br />{/if}
					{if $unite.nec_hydrogene > 0}<strong>Coût {$LANG[$race].ressources.noms.hydrogene} :</strong> {$unite.nec_hydrogene|separerNombres}<br />{/if}
					<strong>Temps de construction :</strong> {$unite.temps}<br />
					<br />
					{if $unite.nec_hydrogene > $planete->hydrogene || $unite.nec_cristal > $planete->cristal || $unite.nec_metal > $planete->metal}<span class="lack">Ressources insuffisantes</span>
					{else}<form action="{$menu.chantierterrestre}&amp;n={$onglet}&amp;k={$lieu}&amp;v={$unite.id}" method="post"><fieldset>
					<label for="terr{$unite.id}">Nombre : <input type="text" class="text" name="terr{$unite.id}" id="terr{$unite.id}" value="0" /></label>
					<input type="submit" class="submit" value="OK" />
				    </fieldset></form>{/if}
				</dd>
			</dl>
{/foreach}
		</div>
{else}	<div id="erreur">Aucune unité ou défense ne peut être entraînée actuellement.</div>{/if}
{include file='game/footer.tpl'}