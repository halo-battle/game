<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
<title>{if $titre}.: {$titre} - Halo-Battle :.{else}.: Halo-Battle :.{/if}</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
{if $race == "covenant"}<link rel="stylesheet" href="{$url_images}css/covenant/covenant.css" type="text/css" media="all" title="Covenants" />{elseif $auth_level >= 3}<link rel="alternate stylesheet" href="{$url_images}css/covenant/covenant.css" type="text/css" media="all" title="Covenants" />{/if}
{if $race == "humain"}<link rel="stylesheet" href="{$url_images}css/csnu/csnu.css" type="text/css" media="all" title="CSNU" />{elseif $auth_level >= 3}<link rel="alternate stylesheet" href="{$url_images}css/csnu/csnu.css" type="text/css" media="all" title="CSNU" />{/if}
<link rel="shortcut icon" type="image/x-icon" href="{$url_images}favicon.ico" />
{$scripth}
</head>
<body>
<div id="top"></div>
<div id="all">
	<div id="header">
		<div><span id="date">{$smarty.now|date_format:"/%e%m%y/%H%M%S"}</span> | {$LANG.hd_sup.tempsDeJeu} {$tpsdejeu} | <a href="?p=version">Version {$version}</a>{if $multi} | <a href="?p=avertmulti" style="color: #FF0000;">! MULTICOMPTE !</a>{/if}{if $header.messagedemarrage} | <a href="?p=demarrage">{$header.messagedemarrage}</a>{/if}{if $planete->mv < 0} | <a href="?p=accueil" style="color: #FF0000;">Votre compte sera supprimé d'ici 48h</a>{/if}{if $alertMail.0 + $alertMail.1 >= 1} | <a href="{$menu.messages}{if $planete->options& 16 && $alertMail.1 < $alertMail.0}&amp;n=recus"{elseif $alertMail.1 > $alertMail.0}&amp;n=rapports{/if}" style="color: #FF0000;">Vous avez des {if $alertMail.1 > $alertMail.0}rapports{else}messages{/if} non lu !</a>{/if}</div><a href="{$link.RSS}" id="rss">RSS :: s'abonner</a>
		<h1><a href="{$link.self}{$first_page}"><span></span>Halo Battle</a></h1>
{if $page == "admin"}
		<ul>
			<li><a href="{$link.self}{$first_page}">RETOUR AU JEU</a></li>
			<li><a href="{$menu.webmail}">WEBMAIL</a></li>
			<li><a href="{$menu.cjoueurs}">CONTROLE JOU.</a></li>
			<li><a href="{$menu.vjoueurs}">VERIFIER JOUEUR</a></li>
			<li><a href="{$menu.vplanetes}">VERIFIER PLANETE</a></li>
			<li><a href="{$link.self}{$link.operateur}">{$LANG.hd_menu.operateurs}</a></li>
			<li><a href="?p=bdd">ADMIN. BDD</a></li>
			<li><a href="?p=mail_mass">MAILLING MASSE</a></li>
		</ul>
{else}
		<ul>
			<li><a href="{$link.accueil}">{$LANG.hd_menu.accueil}</a></li>
			<li><a href="{$link.blog}">{$LANG.hd_menu.blog}</a></li>
			<li><a href="{$link.forum}">{$LANG.hd_menu.forum}</a></li>
			<li><a href="{$menu.chat}">{$LANG.hd_menu.chat}</a></li>
			<li>{if $auth_level >= 4}<a href="admin.php">CONTROLES</a>{else}<a href="{$menu.faq}">{$LANG.hd_menu.faq}</a>{/if}</li>
			<li><a href="{$menu.messages}{if $planete->options& 16 && $alertMail.1 < $alertMail.0}&amp;n=recus"{elseif $alertMail.1 > $alertMail.0}&amp;n=rapports{/if}"{if $alertMail.0 + $alertMail.1 >= 1} style="color:#FF0000;"{/if}>{$LANG.hd_menu.messagerie}</a></li>
			<li><a href="{$link.operateur}">{$LANG.hd_menu.operateurs}</a></li>
		</ul>
{/if}
	</div>
	<div id="banner">
	{foreach from=$header.infos item=info}
		<div class="bannerleft"{if $info.color} style="color: {$info.color};"{/if}>{$info.texte}</div>
	{/foreach}
		<div id="bannerright">{$LANG.cntRaces|sprintf:$header.count.1:$header.count.0:$header.count.2}</div>
	</div>
	<div id="corps">
		<div id="menu"><span class="top"></span>
		{if $page == "admin"}
			{include file='admin/menu.tpl'}
		{elseif $smarty.const.SURFACE == "asteroide"}
			{include file='game/menu_asteroide.tpl'}
		{else}
			{include file='game/menu_joueurs.tpl'}
		{/if}
		<span class="bottom"></span></div>
        </div>

{if $page != "admin" && $page != "vide" && $page != "amis" && $page != "flotten" && $page != "messagerie" && $page != "pilori" && $page != "envoyer" && $page != "bugs" && $page != "carte" && $page != "classement" && $page != "options" && $page != "simulation" && $page != "vp" && $page != "operateur" && !preg_match("#alliance/#", $page) && !preg_match("#diplomatie/#", $page)}
	<div id="head">
		<span class="top"></span>

		<a href="?p=rename" class="imglink"><img src="{$url_images}images/{if $smarty.const.SURFACE == "planete"}planetes{else}asteroides{/if}/{$planete->image}.jpg" width="70" height="70" alt="{$planete->nom_planete|escape}" /></a>
		<form action="{$link.changeplanete}" method="post">
			<p>Bonjour, <span>{$planete->pseudo}.</span><br />Votre position actuelle :</p>
			<fieldset>
				<select name="planete">
					<optgroup label="Plan&egrave;tes">{foreach from=$planetes item=plan}
						<option {if $smarty.const.SURFACE == 'planete' && $plan.id == $planete->id}selected="selected" {/if}value="{$plan.id}">{$plan.nom_planete|escape} [{$plan.galaxie}:{$plan.ss}:{$plan.position}]</option>
					{/foreach}</optgroup>
					{if $planete->alliance}<optgroup label="Astéroïdes">
						<option value="A{$planete->id_alliance}" {if $smarty.const.SURFACE == 'asteroide'}selected="selected" {/if}>{$planete->alliance->nom_asteroide|escape} [{$planete->alliance->galaxie}:{$planete->alliance->ss}:A]</option>
					</optgroup>{/if}
				</select>
				<noscript><fieldset><input class="submit" type="submit" value="GO" /></fieldset></noscript>
			</fieldset></form>
		<div>
			<div><strong>{$LANG[$race].ressources.noms.metal|ucfirst}</strong><br /><span{if $planete->metal >= $planete->cap} class="lack"{/if}>{$planete->metal|separerNombres}</span></div>
			<div><strong>{$LANG[$race].ressources.noms.cristal|ucfirst}</strong><br /><span{if $planete->cristal >= $planete->cap} class="lack"{/if}>{$planete->cristal|separerNombres}</span></div>
			<div><strong>{$LANG[$race].ressources.noms.hydrogene|ucfirst}</strong><br /><span{if $planete->hydrogene >= $planete->cap} class="lack"{/if}>{$planete->hydrogene|separerNombres}</span></div>
			{if $smarty.const.SURFACE == "asteroide"}<div><strong>{$LANG[$race].ressources.noms.credits|ucfirst}</strong><br />{$planete->credits_alliance|separerNombres}</div>
			{else}
				{if $planete->options& 64 || $page == "laboratoire" || $page == "marche"}<div><strong>{$LANG[$race].ressources.noms.credits|ucfirst}</strong><br /><span>{$planete->credits|separerNombres}</span></div>
				{else}<div><strong>{$LANG[$race].ressources.noms.energie|ucfirst}</strong><br /><span{if $planete->energieConso >= $planete->energie} class="lack"{/if}>{$planete->energieConso|separerNombres}/{$planete->energie|separerNombres}</span></div>{/if}
			{/if}
		</div>

		<span class="bottom"></span>
	</div>
{/if}

	{if $page != "admin" && $page != "operateur"}<div id="pub">{$pub}</div>{/if}

	<div id="body">
		<span class="top"></span>
