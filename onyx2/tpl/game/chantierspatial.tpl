{include file='game/header.tpl'}
{if $smarty.const.SURFACE == "planete"}
	{assign var='max' value=$planete->batiments.8}
{else}
	{assign var='max' value=$planete->batiments.3}
{/if}
		<ul class="onglets">
		{section name=nb start=0 loop=$max}
                         {assign var=i value=$smarty.section.nb.index}
			<li{if $lieu == $i} class="hilight"{/if}><a href="{$menu.chantierspatial}&amp;k={$i}">Chantier {$i+1}</a></li>
		{/section}
		</ul>

		<h2>File d'attente</h2>
		<ul id="file">
{capture name='expFile'}
{assign var='keyF' value=$lieu}
{foreach from=$files[$lieu] item=element key=keyE}
	{if $element.5}<li><strong>Prochain vaisseau </strong> : <span class="countdown">{$element.4|countdown}</span></li>{/if}
	<li>{$element.1} {if $element.1 > 1}{$LANG[$race].vaisseaux.noms_pluriel[$element.0]}{else}{$LANG[$race].vaisseaux.noms_sing[$element.0]}{/if} {if $element.2} (démolition){/if} - <span{if $element.5} class="countdown"{/if}>{$element.3|countdown}</span> - <a href="{$menu.chantierspatial}&amp;n={$onglet}&amp;k={$lieu}&amp;a={$keyF}&amp;b={$keyE}">Annuler un</a> - <a href="{$menu.chantierspatial}&amp;n={$onglet}&amp;k={$lieu}&amp;a={$keyF}&amp;b={$keyE}&amp;s={$element.1}">Annuler tous</a></li>
{/foreach}
{/capture}
{if $files && $smarty.capture.expFile}
	{$smarty.capture.expFile}
{else}
			<li>Aucun vaisseau dans la file d'attente</li>
{/if}
		</ul>
		<h3><a href="{$menu.arbre}&amp;q=vaisseaux">Arbre des technologies</a></h3><br />
		<h2>{$LANG[$race].batiments.noms_sing.8|ucfirst}</h2>
{if $vaisseaux}
		<div id="constructions">
{foreach from=$vaisseaux item=vaisseau}
	        <dl>
				<dt>{$LANG[$race].vaisseaux.noms_sing[$vaisseau.id]|ucfirst}{if $vaisseau.nombre > 0} ({$vaisseau.nombre} unité{if $vaisseau.nombre > 1}s{/if}){/if}</dt>
				<dd class="description"><a href="?p=description&amp;v={$vaisseau.id}#body"><img src="{$url_images}images/vaisseaux/{$vaisseau.image}" alt="{$LANG[$race].vaisseaux.noms_sing[$vaisseau.id]|ucfirst}" /></a><p>{$LANG[$race].vaisseaux.descriptions[$vaisseau.id]}</p></dd>
				<dd>
					{if $vaisseau.nombre > 0}<strong>Nombre actuel :</strong> {$vaisseau.nombre}<br /><br />{/if}
					{if $vaisseau.nec_metal > 0}<strong>Coût {$LANG[$race].ressources.noms.metal} :</strong> {$vaisseau.nec_metal|separerNombres}<br />{/if}
					{if $vaisseau.nec_cristal > 0}<strong>Coût {$LANG[$race].ressources.noms.cristal} :</strong> {$vaisseau.nec_cristal|separerNombres}<br />{/if}
					{if $vaisseau.nec_hydrogene > 0}<strong>Coût {$LANG[$race].ressources.noms.hydrogene} :</strong> {$vaisseau.nec_hydrogene|separerNombres}<br />{/if}
					<strong>Temps de construction :</strong> {$vaisseau.temps}<br />
					<br />
					{if $vaisseau.nec_hydrogene > $planete->hydrogene || $vaisseau.nec_cristal > $planete->cristal || $vaisseau.nec_metal > $planete->metal}<span class="lack">Ressources insuffisantes</span>
					{else}<form action="{$menu.chantierspatial}&amp;k={$lieu}&amp;v={$vaisseau.id}" method="post"><fieldset>
					<label for="vais{$vaisseau.id}">Nombre : <input type="text" class="text" name="vais{$vaisseau.id}" id="vais{$vaisseau.id}" value="0" /></label>
					<input type="submit" class="submit" value="OK" />
				    </fieldset></form>{/if}
				</dd>
			</dl>
{/foreach}
		</div>
{else}	<div id="erreur">Aucun vaisseau à construire actuellement.</div>{/if}
{include file='game/footer.tpl'}