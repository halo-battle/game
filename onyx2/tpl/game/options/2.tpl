{include file='game/header.tpl'}
{include file='game/options/common.tpl'}
		<h2>Changer de mot de passe</h2>
		<form action="{$menu.options}&amp;n=compte" method="post"><fieldset class="form">
			<label for="ancien">Ancien mot de passe : <input class="text" type="password" id="ancien" name="mdp_anc" /></label><br />
			<label for="nouveau">Nouveau mot de passe : <input class="text" type="password" id="nouveau" name="mdp_nouv" /></label><br />
			<label for="confirm">Confirmez le mot de passe : <input class="text" type="password" id="confirm" name="mdp_conf" /></label><br />
			<p>Le nouveau mot de passe sera modifié après validation par mail. Veuillez vous assurez que votre adresse actuelle est toujours valide.</p><br />
			<input class="submit" type="submit" name="chgmdp" value="OK" />
		</fieldset></form>
		<h2>Changer d'adresse électronique</h2>
		<form action="{$menu.options}&amp;n=compte" method="post"><fieldset class="form">
			<label>Adresse actuelle : <em>{$planete->mail}</em></label><br />
			<label for="mail">Nouveau mail : <input class="text" type="text" id="mail" name="mail" /></label><br />
			<label for="mdp_mail">Mot de passe actuel : <input class="text" type="password" id="mdp_mail" name="mdp_mail" /></label><br /><br />
			<p>L'adresse électronique sera modifiée une fois validée depuis votre nouvelle adresse.</p><br />
			<input class="submit" type="submit" name="chgmail" value="OK" />
		</fieldset></form>
		<h2>Mode vacances</h2>
		Si vous devez vous absenter de votre compte pour une durée supérieure à 3 jours, vous pouvez mettre votre compte en mode vacances.<br />Vous ne pouvez pas vous faire attaquer (les attaques déjà lancées ne seront pas annulées !) durant cette période, mais vous ne produirez pas de resources sur vos planètes. Vous retrouverez vos files d'attentes dans l'état dans lequel vous les avez laissées.<br />
		<br /><a href="{$menu.options}&amp;n=compte&amp;a=mv&amp;c={$idvac}" onclick="return confirm('Êtes-vous sûr de vouloir passer votre compte en mode vacances ?\nIl ne sera plus accessible durant les trois prochains jours !');">Activer le mode vacances maintenant</a>
		<h2>Supprimer son compte</h2>
		{if $planete->mv < 0}<span style="color: #FF0000;">Vous avez demandé la suppression de votre compte. Il sera supprimé sous 48h.</span>
		{else}<br /><a href="{$menu.options}&amp;n=compte&amp;a=dl&amp;c={$idvac}" onclick="return confirm('Êtes-vous sûr de vouloir supprimer votre compte ?');">Supprimer mon compte</a>{/if}
{include file='game/footer.tpl'}