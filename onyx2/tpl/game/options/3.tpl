{include file='game/header.tpl'}
{include file='game/options/common.tpl'}
		<h2>Recevoir des notifications par mail</h2>
		<form action="{$menu.options}&amp;n=jeu" method="post">
			<fieldset class="form">
				<input type="hidden" name="init" value="0" />
				<label for="ch_creen">Afficher les crédits à la place de l'énergie sur toutes les pages <input type="checkbox" name="ch_creen" id="ch_creen" value="64"{if $planete->options & 64} checked="checked"{/if} class="text" /></label>
				<label for="planacc">Afficher l'image de la planète sur la page d'accueil <input type="checkbox" name="planacc" id="planacc" value="1"{if $planete->options & 1} checked="checked"{/if} class="text" /></label>
				<label for="fileacc">Afficher les files d'attentes de la planète sur la page d'accueil <input type="checkbox" name="fileacc" id="fileacc" value="2"{if $planete->options & 2} checked="checked"{/if} class="text" /></label>
				<label for="fileref">NONPROG > Rafraîchir automatiquement les pages à la fin de la construction/entra&icirc;nement <input type="checkbox" name="fileref" id="fileref" value="4"{if $planete->options & 4} checked="checked"{/if} class="text" /></label>
				<label for="res_auto">Autogérer mes productions de ressources au rendement maximal <input type="checkbox" name="res_auto" id="res_auto" value="8"{if $planete->options & 8} checked="checked"{/if} class="text" /></label>
				<label for="rappdef">Afficher la page des rapports par défaut au lieu de la page des messages privés reçus <input type="checkbox" name="rappdef" id="rappdef" value="16"{if $planete->options & 16} checked="checked"{/if} class="text" /></label>
				<label for="fl_prep">Afficher les flottes préparées <input type="checkbox" name="fl_prep" id="fl_prep" value="32"{if $planete->options & 32} checked="checked"{/if} class="text" /></label>
				<label for="fl_safe">Autoriser l'envoie de flotte à un multi-compte <input type="checkbox" name="fl_safe" id="fl_safe" value="128"{if $planete->options & 128} checked="checked"{/if} disabled="disabled" class="text" /></label>
				<input type="submit" class="submit" value="Ok" />
			</fieldset>
		</form>
{include file='game/footer.tpl'}