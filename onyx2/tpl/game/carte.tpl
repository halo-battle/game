{include file='game/header.tpl'}
		<h2>Navigation</h2>
		<form action="{$menu.carte}" method="get"><fieldset id="navigation">
			<input type="hidden" value="carte" name="p" />
			<label for="amas">Amas : <input type="text" class="text" name="amas" id="amas" value="{$position.0}" /></label>
			<label for="systeme">Système solaire : <input type="text" class="text" name="systeme" id="systeme" value="{$position.1}" /></label>
			<input type="submit" class="submit" value="GO" /><br />
			<a href="{$menu.carte}&amp;amas={$position.4}&amp;systeme={$position.5}" class="button" title="système solaire précédent">précédent</a>
			<a href="{$menu.carte}&amp;amas={$position.2}&amp;systeme={$position.3}" class="button" title="système solaire suivant">suivant</a>
		</fieldset></form>

		<h2>Système Solaire</h2>
		<table id="carte">
			<thead>
				<tr>
					<td colspan="7">Système: {$position.0}:{$position.1}</td>
				</tr>
				<tr>
					<th>Position</th>
					<th></th>
					<th>Nom planète<br />Alliance<br />Joueur</th>
					<th>Race</th>
					<th>Gouvernement</th>
					<th>Débris {$LANG[$race].ressources.noms.metal} | {$LANG[$race].ressources.noms.cristal}</th>
					<th>Fonctions</th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$carte item=plan}
				<tr{if $plan.4} class="{$plan.4}"{/if}>
					<td>{$plan.0}</td>
					<td style="width: 45px;">{if $plan.7}<img src="{$url_images}images/planetes/{$plan.7}.jpg" width="45" alt="{$plan.1|escape}" />{else}&nbsp;{/if}</td>
					<td>{if $plan.1}{$plan.1|escape}<br />{if $plan.6}[{$plan.6}]<br />{/if}{$plan.5}{else}&nbsp;{/if}</td>
					<td style="width: 45px;">{if $plan.4}<img src="{$url_images}images/carte/{$plan.4}.jpg" width="45" alt="{$plan.4}" />{else}&nbsp;{/if}</td>
					<td style="width: 45px;">{if $plan.4}<img src="{$url_images}images/carte/{$plan.6}.jpg" width="45" alt="{$plan.6}" />{else}&nbsp;{/if}</td>
					<td>{if $plan.2}{$plan.2}{else}0{/if} | {if $plan.3}{$plan.3}{else}0{/if}</td>
					<td>{if $plan.5}<a href="?p=envoyer&amp;d={$plan.5}" class="msg"><span>Message</span></a>{else}&nbsp;{/if}</td>
				</tr>
				{/foreach}
			</tbody>
		</table>
{include file='game/footer.tpl'}