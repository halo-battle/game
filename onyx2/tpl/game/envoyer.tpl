{include file='game/header.tpl'}
		<h2>Messagerie</h2>
		<ul class="onglets">
			<li{if $onglet != "rapports" && $onglet != "send" && $onglet != "envoyer"} class="hilight"{/if}><a href="{$menu.messages}&amp;n=recus">Reçus</a>{if $alertMail.0} ({$alertMail.0}){/if}</li>
			<li{if $onglet == "rapports"} class="hilight"{/if}><a href="{$menu.messages}&amp;n=rapports">Rapports</a>{if $alertMail.1} ({$alertMail.1}){/if}</li>
			<li{if $onglet == "send"} class="hilight"{/if}><a href="{$menu.messages}&amp;n=send">Envoyés</a></li>
			<li{if $onglet == "envoyer"} class="hilight"{/if}><a href="{$menu.messages}&amp;n=envoyer">Ecrire</a></li>
		</ul><br /><br />
		<form action="{$menu.messages}&amp;n=envoyer" method="post">
			<fieldset id="envoyer">
				<label for="amis">Amis :
					<select name="amis" id="amis">
						<option value="">--</option>
						{html_options output=$amis values=$amis}
					</select>
				</label><br />
				<label for="destinataire">Destinataire(s) : <input type="text" class="text" name="destinataire" id="destinataire" value="{$destinataire|escape}" /></label><br />
				<label for="objet">Objet : <input type="text" class="text" name="objet" id="objet" value="{$objet|escape}" maxlength="50" /></label><br />
				<textarea name="message" cols="40" rows="10"></textarea><br />
				<input type="submit" class="submit" value="OK" />
			</fieldset>
		</form>
{include file='game/footer.tpl'}