{include file='game/header.tpl'}
			<h2>Opérateurs de cette galaxie</h2>
			<ul>
{foreach from=$operateurs item=operateur}
			<li>{$operateur.pseudo} <i>({if $operateur.auth_level == 2}Modérateur{elseif $operateur.auth_level == 3}Maître de la bourse{elseif $operateur.auth_level == 4}Opérateur en formation{elseif $operateur.auth_level == 5}Opérateur{elseif $operateur.auth_level == 6}Super-opérateur{elseif $operateur.auth_level == 7}Administrateur{elseif $operateur.auth_level == 8}Codeur{/if})</i></li>
{/foreach}
			</ul>
			<h2>Problèmes connus</h2>
			<strong>Lisez les questions/réponses ci-dessous. Ne contactez un opérateur que si vous ne pouvez pas résoudre votre problème/demande.</strong><br />
			<strong>N'oubliez pas de contacter les opérateurs pour les raisons indiquées dans les <a href="?p=regles">règles du jeu</a>.</strong>
			<div id="messagerie">
{foreach from=$questions item=question}
				<dl>
					<dt>{$question.0}</dt>
					<dd>{$question.1}{if $auth_level > 4}<div style="text-align: right"><a href="{$menu.operateur}&amp;a=top&amp;i={$question.2}"><i>Haut</i></a> - <a href="{$menu.operateur}&amp;a=bas&amp;i={$question.2}"><i>Bas</i></a> - <a href="{$menu.operateur}&amp;a=del&amp;i={$question.2}"><i>Supprimer</i></a></div>{/if}</dd>
				</dl>
{/foreach}
			</div>
			{if $auth_level >= 5}
			<h2>Ajouter un problème connu</h2>
			<form action="{$menu.operateur}&amp;post" method="post"><fieldset class="form">
				<label for="O_titre">Problème : <input class="text" type="text" name="O_titre" id="O_titre" /></label>
				<label for="O_desc">Solution :
				<textarea id="O_desc" name="O_description" cols="40" rows="10"></textarea></label>
				<input class="submit" type="submit" value="OK" />
			</fieldset></form>
			{else}<h2>Prendre contact avec les opérateurs de la galaxie</h2>
			<form action="{$menu.operateur}&amp;post" method="post"><fieldset class="form">
				<label for="titre">Titre/objet : <input class="text" type="text" name="titre" id="titre" /></label>
				<label for="desc">Corps :
				<textarea id="desc" name="description" cols="40" rows="10"></textarea></label>
				<input class="submit" type="submit" value="OK" />
			</fieldset></form>{/if}
{include file='game/footer.tpl'}