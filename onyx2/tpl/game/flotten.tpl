{include file='game/header.tpl'}
			<h2>Détails de la flotte : {$flotte->nom|escape}</h2>
			<form action="{$menu.flotte}&amp;n={$flotte->id_flotte}" method="post">
				<fieldset class="options">
					<label for="nom">Nom de la flotte :</label><input class="text" type="text" id="nom" name="nomflotte" value="{$flotte->nom|escape}" /><br />
					<input class="submit" type="submit" value="GO" />
				</fieldset>
			</form>
			<h2>Composition de la flotte</h2>
			<br /><table style="margin: auto;">
			{foreach from=$flotte->vaisseaux item=vaiss key=key}
				{if $vaiss}
				<tr>
					<td>{if $vaiss > 1}{$LANG[$race].vaisseaux.noms_pluriel.$key|ucfirst}{else}{$LANG[$race].vaisseaux.noms_sing.$key|ucfirst}{/if}</td>
					<td>{$vaiss|separerNombres}</td>
				</tr>
				{/if}
			{/foreach}
				<tr>
					<th>Total</th>
					<th>{$flotte->nb_vais}</th>
				</tr>
			</table><br />
			<h2>Contenu de la flotte</h2>
			<br /><table style="margin: auto;">
				<tr>
					<td>{$LANG[$race].ressources.noms.metal|ucfirst}</td>
					<td>{$flotte->contenu.0|separerNombres}</td>
				</tr>
				<tr>
					<td>{$LANG[$race].ressources.noms.cristal|ucfirst}</td>
					<td>{$flotte->contenu.1|separerNombres}</td>
				</tr>
				<tr>
					<td>{$LANG[$race].ressources.noms.hydrogene|ucfirst}</td>
					<td>{$flotte->contenu.2|separerNombres}</td>
				</tr>
				<tr>
					<td>Maximum</td>
					<td>{$flotte->contenuMax|separerNombres}</td>
				</tr>
			</table><br />
			<h2>Détails de la mission</h2>
			<table>
				<tbody>
					<tr>
						<td>Mission</td>
						<td>{$flotte->mission}</td>
					</tr>
					<tr>
						<td>Etat de la mission</td>
{if $flotte->effectue == 2}
						<td>En cours</td>
{elseif $flotte->effectue == 1}
						<td>Effectuée</td>
{else}
						<td>Déplacement</td>
{/if}
					</tr>
					<tr>
						<td style="border: none;"></td>
					</tr>
					<tr>
						<td>Plan&egrave;te source</td>
						<td>{$flotte->start_planete->nom_planete|escape} [{$flotte->start_planete->galaxie}:{$flotte->start_planete->ss}:{$flotte->start_planete->position}]</td>
					</tr>
					<tr>
						<td>Date de départ</td>
						<td>{$flotte->start_time|date_format:"/%a %d %b %y/ %H%M %S"}</td>
					</tr>
					<tr>
						<td style="border: none;"></td>
					</tr>
					<tr>
						<td>Plan&egrave;te de destination</td>
						<td>{if isset($flotte->end_planete->nom_planete)}{$flotte->end_planete->nom_planete|escape} [{$flotte->end_planete->galaxie}:{$flotte->end_planete->ss}:{$flotte->end_planete->position}]{else}[{$flotte->end_planete}]{/if}</td>
					</tr>
					<tr>
						<td>Temps de déplacement</td>
						<td>{$flotte->end_time|date_format:"%k:%M:%S"}</td>
					</tr>
					<tr>
						<td>Date {if $flotte->mission != 6 && $flotte->statut != 1}d'arrivée{else}de retour{/if}</td>
						<td>{$flotte->ret_time|date_format:"/%a %d %b %y/ %H%M %S"}</td>
					</tr>
				</tbody>
			</table>
			{if $flotte->mission != 6}<a href="{$menu.flotte}&amp;n={$flotte->id_flotte}&amp;a={$ret_fleet}">Annuler la mission et rappeler la flotte</a>{/if}
{include file='game/footer.tpl'}