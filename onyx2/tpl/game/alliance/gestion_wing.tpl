{include file='game/header.tpl'}
{include file='game/alliance/gestion_common.tpl'}
			<h2>Gestion des wings</h2>
{if $wings}
			<table>
				<tbody>
{foreach from=$wings item=wing}
					<tr>
						<td>{$wing.nom_alliance}</td>
						<td>{$wing.race}</td>
						<td>{$wing.nbMembres}</td>
						<td>{if $wing.pseudo_fondateur}{$wing.pseudo_fondateur}{else}Attente de l'acceptation{/if}</td>
						<td>{if $wing.pseudo_fondateur}<a href="{$menu.messages}&amp;n=envoyer&amp;d={$wing.pseudo_fondateur}" class="msg"><span>Message</span></a>{/if}</td>
						<td><a href="{$menu.gestion}&amp;g=wing&amp;u={$membre.id}"><em>supprimer</em></a></td>
					</tr>
{/foreach}
				</tbody>
				<thead>
					<tr>
						<th>Nom de la wing</th>
						<th>Race</th>
						<th>Nombre de membres</th>
						<th>Fondateur</th>
						<th>MP fondateur</th>
						<th>Actions</th>
					</tr>
				</thead>
			</table>
{else}
			<div id="erreur">L'alliance ne possède aucune wing.</div>
{/if}
			<h2>Créer une wing</h2>
			<form method="post" action="{$menu.gestion}&amp;g=wings">
				<fieldset class="options">
					<label for="nom">Nom de la wing : <input type="text" class="text" name="nom" id="nom" maxlength="32" /></label><br />
					<label for="tag">Tag de la wing : <input type="text" class="text" name="tag" id="tag" maxlength="5" /></label><br />
					<label for="race">Race de la wing : <select name="race" id="race"><option value="humain">Humaine</option><option value="covenant">Covenante</option></select></label><br />
					<label for="fondateur">Membre fondateur : <input type="text" class="text" name="fondateur" id="fondateur" maxlength="64" /></label><br />
					<em>Le membre fondateur ne doit faire actuellement parti d'aucune alliance.<br />La wing sera créée une fois que le fondateur aura accepté votre demande.</em><br />
					<input type="submit" class="submit" value="Go" />
				</fieldset>
			</form>
{include file='game/footer.tpl'}