{include file='game/header.tpl'}
			<h2>Alliance en fondation</h2>
			<h3><strong>Nom :</strong> {$alliance.nom_alliance}</h3>
			<h3><strong>Tag :</strong> {$alliance.tag|upper}</h3><br />
			<h2>Signatures</h2>
			<h3><strong>Actuellement :</strong> {$nbSignatures}</h3>
			<h3><strong>N&eacute;cessaire :</strong> {$smarty.const.nb_signatures}</h3>
			<h3><strong>Signataires :</strong> {foreach from=$pseudos key=k item=pseudo}{if $k != 0}, {/if}{$pseudo.pseudo}{/foreach}</h3><br />
			<h2>Autre</h2>
			<h3><a href="?p=alliances&amp;q=fonder&amp;r=quit">Quitter l'alliance</a></h3>
			<h3><strong>Lien de signature :</strong> <a href="http://{$url_serveur}/{$first_page}?p=alliances&amp;signer={$alliance.lien}">http://{$url_serveur}/{$first_page}?p=alliances&amp;signer={$alliance.lien}</a></h3>
{include file='game/footer.tpl'}