{include file='game/header.tpl'}
{include file='game/alliance/gestion_common.tpl'}
			<h2>Gestion des membres</h2>
			<table>
				<tbody>
{foreach from=$membres item=membre}
					<tr>
						<td>{$membre.pseudo}</td>
						<td>{$membre.race}</td>
						<td>{$membre.last_visite|date_format:"%d/%m/%y %H:%M:%S"}</td>
						<td><form action="{$menu.gestion}&amp;g=membres&amp;i={$membre.id}" method="post"><p><select name="grade" onchange="this.form.submit();"><option label="Aucun" value="0">Aucun</option>{foreach from=$grades item=grade}<option label="{$grade.nom}" value="{$grade.id}"{if $grade.id == $membre.id_grade_alliance} selected="selected"{/if}>{$grade.nom}</option>{/foreach}</select><noscript> <input type="submit" class="submit" value="GO" /></noscript></p></form></td>
						<td><a href="{$menu.messages}&amp;n=envoyer&amp;d={$membre.pseudo}" class="msg"><span>Message</span></a></td>
						<td><a href="{$menu.gestion}&amp;g=membres&amp;u={$membre.id}"><em>Exclure</em></a></td>
					</tr>
{/foreach}
				</tbody>
				<thead>
					<tr>
						<th>Pseudo</th>
						<th>Race</th>
						<th>Derni&egrave;re connexion </th>
						<th>Grade</th>
						<th>MP</th>
						<th>Actions</th>
					</tr>
				</thead>
			</table>
			<h2>Gestion des postulants</h2>
{if $postulants}
			<div id="messagerie">
		{foreach from=$postulants item=postulant}
			    <dl class="new">
					<dt><a href="?p=...&amp;i={$postulant.id_user}">{$postulant.pseudo}</a> le {$postulant.timestamp|date_format:"%d/%m/%y %H:%M:%S"}</dt>
					<dd>{$postulant.message|escape|nl2br}<br /><div style="text-align: right;"><a href="{$menu.gestion}&amp;g=membres&amp;pa={$postulant.id}"><em>Accepter</em></a> | <a href="{$menu.gestion}&amp;g=membres&amp;pr={$postulant.id}"><em>Refuser</em></a></div></dd>
			    </dl>
		{/foreach}
			</div>
{else}
			<div id="erreur">Aucun postulant actuellement</div>
{/if}
{include file='game/footer.tpl'}