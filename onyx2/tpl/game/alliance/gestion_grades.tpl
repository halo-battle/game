{include file='game/header.tpl'}
{include file='game/alliance/gestion_common.tpl'}
			<h2>Gestion des grades existants</h2>
			<table>
				<tbody>
{foreach from=$grades item=grade}
					<tr>
						<td><a href="{$menu.gestion}&amp;g=grades&amp;i={$grade.id}">{$grade.nom|escape}</a></td>
						<td>{if $grade.auth & 1}Oui{else}Non{/if}</td>
						<td>{if $grade.auth & 2}Oui{else}Non{/if}</td>
						<td>{if $grade.auth & 4}Oui{else}Non{/if}</td>
						<td>{if $grade.auth & 8}Oui{else}Non{/if}</td>
						<td>{if $grade.auth & 16}Oui{else}Non{/if}</td>
						<td>{if $grade.auth & 32}Oui{else}Non{/if}</td>
						<td>{if $grade.auth & 64}Oui{else}Non{/if}</td>
						<td>{if $grade.auth & 128}Oui{else}Non{/if}</td>
						<td>{if $grade.auth & 256}Oui{else}Non{/if}</td>
						<td>{if $grade.auth & 512}Oui{else}Non{/if}</td>
						<td><a href="{$menu.gestion}&amp;g=grades&amp;d={$grade.id}" onclick="return confirm('Êtes-vous sûr de vouloir supprimer ce grade ?');">Supprimer</a></td>
					</tr>
{/foreach}
				</tbody>
				<thead>
					<tr>
						<th>Nom</th>
						<th>Bâtiments</th>
						<th>Vaisseaux</th>
						<th>Flottes</th>
						<th>Diplomatie</th>
						<th>Crédits</th>
						<th>Membres</th>
						<th>Wings</th>
						<th>Alliance</th>
						<th>Chat</th>
						<th>Messagerie</th>
						<th>Actions</th>
					</tr>
				</thead>
			</table>
			<h2>{if $grade_mod}Modifier{else}Cr&eacute;er{/if} un grade</h2><br />
			<form action="{$menu.gestion}&amp;g=grades{if $grade_mod}&amp;i={$grade_mod.id}{/if}" method="post">
				<fieldset class="options">
					<label for="nom">Nom :</label> <input type="text" name="nom" id="nom" value="{$grade_mod.nom|escape}" size="30" maxlength="32" tabindex="1" /><br /><br />
					<input type="checkbox" name="gbats" id="gbats" value="1"{if $grade_mod.auth & 1} checked="checked"{/if} /> <label for="gbats">Gestion des bâtiments</label><br />
					<input type="checkbox" name="gvais" id="gvais" value="1"{if $grade_mod.auth & 2} checked="checked"{/if} /> <label for="gvais">Gestion des vaisseaux</label><br />
					<input type="checkbox" name="gflot" id="gflot" value="1"{if $grade_mod.auth & 4} checked="checked"{/if} /> <label for="gflot">Gestion des flottes</label><br />
					<input type="checkbox" name="gdipl" id="gdipl" value="1"{if $grade_mod.auth & 8} checked="checked"{/if} /> <label for="gdipl">Gestion de la diplomatie</label><br />
					<input type="checkbox" name="gcred" id="gcred" value="1"{if $grade_mod.auth & 16} checked="checked"{/if} /> <label for="gcred">Gestion des crédits</label><br /><br />
					<input type="checkbox" name="gmemb" id="gmemb" value="1"{if $grade_mod.auth & 32} checked="checked"{/if} /> <label for="gmemb">Gestion des membres</label><br />
					<input type="checkbox" name="gwing" id="gwing" value="1"{if $grade_mod.auth & 64} checked="checked"{/if} /> <label for="gwing">Gestion des wings</label><br />
					<input type="checkbox" name="galli" id="galli" value="1"{if $grade_mod.auth & 128} checked="checked"{/if} /> <label for="galli">Gestion de l'alliance</label><br />
					<input type="checkbox" name="gchat" id="gchat" value="1"{if $grade_mod.auth & 256} checked="checked"{/if} /> <label for="gchat">Participe au chat</label><br />
					<input type="checkbox" name="gmess" id="gmess" value="1"{if $grade_mod.auth & 512} checked="checked"{/if} /> <label for="gmess">Utilisation de la messagerie d'alliance</label><br />
					<br /><input type="submit" class="submit" value="GO" tabindex="3" />
				</fieldset>
			</form>
{include file='game/footer.tpl'}