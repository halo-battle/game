{include file='game/header.tpl'}
			<h2>[{$alliance.tag|upper|escape}] {$alliance.nom_alliance|capitalize|escape}</h2>
			<table cellpadding="2" cellspacing="0" style="text-align: center; width: 100%;">
				<tbody>
					<tr>
						<td colspan="2"><img src="{$alliance.image}" alt="" /></td>
			    	</tr>
					<tr>
						<td style="text-align: right; vertical-align: middle;"><strong>Nom : </strong></td>
				   	 	<td style="width: 70%;">{$alliance.nom|escape} ({$alliance.tag|upper|escape})</td>
				  	</tr>
				  	<tr>
				    	<td style="text-align: right;"><strong>Membres : </strong></td>
				    	<td>{$nbmembres} (<a href="{$menu.alliance}&amp;m={$alliance.id}">Liste des membres</a>)</td>
					</tr>
					<tr>
						<td style="text-align: right;"><strong>Votre grade :</strong></td>
						<td>{$grade}</td>
					</tr>
					<tr>
						<td style="text-align: right;"><strong>Status des inscriptions :</strong></td>
				    	<td>{if $alliance.etat_inscription}Ouvertes{if !$planete->id_alliance} - <a href="{$menu.alliance}&amp;postuler={$alliance.id}">Postuler</a>{/if}{else}Ferm&eacute;e{/if}</td>
					</tr>
					</tr>
				</tbody>
			</table>
			<h2>Présentation</h2>
			{$alliance.presentation|nl2br}
{include file='game/footer.tpl'}