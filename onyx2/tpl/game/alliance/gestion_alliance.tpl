{include file='game/header.tpl'}
{include file='game/alliance/gestion_common.tpl'}
			<h2>Designation de l'alliance</h2><br />
			<label>Nom de l'alliance :</label> {$planete->nom_alliance}<br />
			<label>Tag de l'alliance :</label> {$planete->tag}<br />
			<label>Race de l'alliance :</label> {$planete->race}<br /><br />
			<em>Pour changer le tag ou le nom de votre alliance, veuillez contacter un opérateur.</em><br /><br /><br />
			<form method="post" action="{$menu.gestion}&amp;g=alliance">
				<fieldset class="options">
					<label for="asteroide_name">Nom de l'astéroïde : <input type="text" class="text" name="asteroide_name" id="asteroide_name" value="{$planete->nom_asteroide}" maxlength="32" /></label><br /><br />
					<input type="submit" class="submit" value="Go" /><br /><br />
				</fieldset>
			</form>
			<h2>Communication et défense de l'alliance</h2><br />
			<form method="post" action="{$menu.gestion}&amp;g=alliance">
				<fieldset class="options">
					<label for="texte_interne">Message interne :<br /><textarea name="texte_interne" id="texte_interne">{$planete->details.texte_interne|escape}</textarea></label><br /><br />
					<label for="defcon">Defcon : <select name="defcon" id="defcon"><option value="1"{if $planete->details.defcon == 1} selected="selected"{/if}>Niveau 1</option><option value="2"{if $planete->details.defcon == 2} selected="selected"{/if}>Niveau 2</option><option value="3"{if $planete->details.defcon == 3} selected="selected"{/if}>Niveau 3</option><option value="4"{if $planete->details.defcon == 4} selected="selected"{/if}>Niveau 4</option><option value="5"{if $planete->details.defcon == 5} selected="selected"{/if}>Niveau 5</option></select></label><br />
					<label for="defcon_txt"><textarea name="defcon_txt" id="defcon_txt">{$planete->details.defcon_txt|escape}</textarea></label><br /><br />
					<input type="submit" class="submit" value="Go" /><br /><br />
				</fieldset>
			</form>
			<h2>Paramètres des moyens de communication</h2><br />
			<form method="post" action="{$menu.gestion}&amp;g=alliance">
				<fieldset class="options">
					<label for="url_forum">URL du forum de l'alliance : <input name="url_forum" id="url_forum" maxlength="64" class="text" type="text" value="{$planete->url_forum}" /></label><br />
					<br />
					<label for="url_chat">Adresse sur serveur IRC de l'alliance : <input name="url_chat" id="url_chat" maxlength="64" class="text" type="text" value="{$planete->url_chat}" /></label><br />
					<label for="port_chat">Port du serveur IRC : <input name="port_chat" id="port_chat" maxlength="64" class="text" type="text" value="{$planete->details.port_chat}" /></label><br />
					<label for="pass_chat">Mot de passe du serveur sur le serveur : <input name="pass_chat" id="pass_chat" maxlength="64" class="text" type="password" value="{$planete->details.pass_chat}" /></label><br />
					<label for="chan_chat">Channel de l'alliance sur le serveur : <input name="chan_chat" id="chan_chat" maxlength="64" class="text" type="text" value="{$planete->details.chan_chat}" /></label><br />
					<em>Laissez l'adresse du serveur IRC vide pour utiliser le chat par défaut du jeu.</em><br /><br /><br />
					<input type="submit" class="submit" value="Go" /><br /><br />
				</fieldset>
			</form>
			<h2>Réglages des inscriptions</h2><br />
			<form method="post" action="{$menu.gestion}&amp;g=alliance">
				<fieldset class="options">
					<label for="etatinscriptions"><input type="checkbox" name="etatinscriptions" id="etatinscriptions" {if $planete->details.etat_inscription} checked="checked"{/if} /> Ouvrir les inscriptions</label><br /><br />
					<label for="message_inscription">Message affiché pour les inscriptions (activées ou désactivées) :<br /><textarea name="message_inscription" id="message_inscription">{$planete->details.message_inscription|escape}</textarea></label><br /><br />
					<br />
					<label for="presentation">Texte de présentation de l'alliance :<br /><textarea name="presentation" id="presentation">{$planete->details.presentation|escape}</textarea></label><br /><br />
					<br />
					<input type="submit" class="submit" value="Go" /><br /><br />
				</fieldset>
			</form>
			{if $planete->fondateur == $planete->id_user}
			<h2>Changer de fondateur</h2><br />
			<form method="post" action="{$menu.gestion}&amp;g=alliance">
				<fieldset class="options">
					Vous êtres actuellement le fondateur de l'alliance.<br /><br />
					<label for="newfondateur">Nouveau fondateur :</label> <select id="newfondateur" name="newfondateur">
					{foreach from=$membres item=membre}
						<option value="{$membre.id}">{$membre.pseudo}</option>
					{/foreach}
					</select>
					<br />
					<input type="submit" class="submit" value="Go" /><br /><br />
				</fieldset>
			</form>
			{/if}
{include file='game/footer.tpl'}