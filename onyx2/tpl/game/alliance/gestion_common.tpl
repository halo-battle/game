		<ul class="onglets">
			{if $planete->permissions_alliance & 32}<li{if $onglet == "membres"} class="hilight"{/if}><a href="{$menu.gestion}&amp;g=membres">Membres</a></li>{/if}
			{if $planete->permissions_alliance & 128}<li{if $onglet == "grades"} class="hilight"{/if}><a href="{$menu.gestion}&amp;g=grades">Grades</a></li>{/if}
			{if $planete->permissions_alliance & 64}<li{if $onglet == "wings" || ($onglet == "alliance" && $planete->wing)} class="hilight"{/if}><a href="{$menu.gestion}&amp;g=wings">Wings</a></li>{/if}
			{if $planete->permissions_alliance & 128 && !$planete->wing}<li{if $onglet == "alliance"} class="hilight"{/if}><a href="{$menu.gestion}&amp;g=alliance">Alliance</a></li>{/if}
		</ul>