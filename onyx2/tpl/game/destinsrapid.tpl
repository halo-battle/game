{include file='game/header.tpl'}
			<h2>Destinations rapides existantes</h2>
			<table>
				<thead>
					<tr>
						<th>Nom de la planète</th>
						<th>Coordonnées de la planète</th>
						<th>Supprimer</th>
					</tr>
				</thead>
				<tbody>
{foreach from=$destins item=destin}
					<tr>
						<td>{$destin.1|escape}</td>
						<td>{$destin.2}</td>
						<td><a href="?p=destinationsrapides&amp;d={$destin.0}&amp;a={$destin.3}">Supprimer</a></td>
					</tr>
{/foreach}
				</tbody>
			</table>
			<h2>Ajouter une destination rapide</h2>
			<form action="?p=destinationsrapides" method="post">
				<fieldset class="navigation"><br />
					<label for="amas">Coordonnées de la planète à ajouter : <input class="text" type="text" id="amas" name="amas" maxlength="2" />:<input class="text" type="text" id="ss" name="ss" maxlength="2" />:<input class="text" type="text" id="plan" name="pos" maxlength="2" /></label><br /><br />
					<input class="submit" type="submit" value="Ok" />
				</fieldset>
			</form>
{include file='game/footer.tpl'}