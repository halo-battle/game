{include file='game/header.tpl'}
		<h2>{$titreR}</h2>
		<ul class="onglets">
			<li{if $onglet != "rapports" && $onglet != "send" && $onglet != "envoyer"} class="hilight"{/if}><a href="?p={$link_P}&amp;n=recus">Reçus{if $alertMail.0} ({$alertMail.0}){/if}</a></li>
			<li{if $onglet == "rapports"} class="hilight"{/if}><a href="?p={$link_P}&amp;n=rapports">Rapports{if $alertMail.1} ({$alertMail.1}){/if}</a></li>
			<li{if $onglet == "send"} class="hilight"{/if}><a href="?p={$link_P}&amp;n=send">Envoyés</a></li>
			<li{if $onglet == "envoyer"} class="hilight"{/if}><a href="?p={$link_P}&amp;n=envoyer">Ecrire</a></li>
		</ul><br /><br />
		{$avertissement}
		<form action="{$menu.messagerie}" method="post" id="messagerie"><fieldset>
{foreach from=$messages item=message key=key}
		    <dl{if $onglet != "send" && $message.vu} class="new"{/if}>
				<dt><input type="checkbox" class="checkbox" name="m{$key}" value="{$message.id}" /> {if $onglet != "rapports"}<a href="?p={$link_P}&amp;n=envoyer&amp;d={if $onglet == "send"}{$message.destinataire}&amp;o=Fwd{else}{$message.expediteur}&amp;o=Re{/if}:%20{$message.sujet}">{/if}{$message.sujet}{if $onglet != "rapports"}</a>{/if}{if !empty($message.expediteur)} {if $onglet == "send"}vers {$message.destinataire}{else}de {$message.expediteur}{/if}{/if} le {$message.temps|date_format:"%d/%m/%y à %H:%M"}</dt>
				<dd>{if $onglet == "rapports"}{$message.contenu|nl2br}{else}{$message.contenu|escape|bbcode|nl2br}{/if}{if $onglet != "send" && $onglet != "rapports"}<div style="text-align: right;"><a href="?p={$link_P}&amp;n=envoyer&amp;d={$message.expediteur}&amp;o=Re:%20{$message.sujet}"><i>Répondre</i></a> | <a href="?p=messagerie&amp;avertir={$message.id}"><i>Avertir un opérateur</i></a></div>{/if}</dd>
		    </dl>
{/foreach}
			{if $onglet != "send" && $messages}<strong>Supprimer le sélection :  <input class="submit" type="submit" value="OK" /></strong>
			{elseif !$messages}<div id="erreur">Aucun {if $onglet == "rapports"}rapport{else}message{/if}</div>{/if}
		</fieldset></form>
{include file='game/footer.tpl'}