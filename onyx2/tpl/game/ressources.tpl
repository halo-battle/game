{include file='game/header.tpl'}
			<h2>Ressources : Tableau r&eacute;capitulatif des productions par heure</h2>
			<form method="post" action="{$menu.ressources}"><fieldset id="ressources">
				<table>
					<thead><tr><th> </th><th>Coefficient</th><th>{$LANG[$race].ressources.noms.metal|ucfirst}</th><th>{$LANG[$race].ressources.noms.cristal|ucfirst}</th><th>{$LANG[$race].ressources.noms.hydrogene|ucfirst}</th><th>{$LANG[$race].ressources.noms.energie|ucfirst}</th></tr></thead>
					<tbody>
					<tr><td>{$planete->batiments.0} {$LANG[$race].batiments.noms_sing.0|ucfirst}</td><td><input type="text" name="coeff_metal" class="text" value="{$ressources_coef.0}" maxlength="3" size="3" />%</td><td>{$ressources_prod.0|separerNombres}</td><td>0</td><td>0</td><td class="lack">-{$ressources_conso.0|separerNombres}</td></tr>
					{if $planete->batiments.1}<tr><td>{$planete->batiments.1} {$LANG[$race].batiments.noms_sing.1|ucfirst}</td><td><input type="text" name="coeff_cristal" class="text" value="{$ressources_coef.1}" maxlength="3" size="3" />%</td><td>0</td><td>{$ressources_prod.1|separerNombres}</td><td>0</td><td class="lack">-{$ressources_conso.1|separerNombres}</td></tr>{/if}
					{if $planete->batiments.2}<tr><td>{$planete->batiments.2} {$LANG[$race].batiments.noms_sing.2|ucfirst}</td><td><input type="text" name="coeff_hydrogene" class="text" value="{$ressources_coef.2}" maxlength="3" size="3" />%</td><td>0</td><td>0</td><td>{$ressources_prod.2|separerNombres}</td><td class="lack">-{$ressources_conso.2|separerNombres}</td></tr>{/if}
					<tr><td>{$planete->batiments.3} {$LANG[$race].batiments.noms_sing.3|ucfirst}</td><td><input type="text" name="coeff_cs" class="text" value="{$ressources_coef.3}" maxlength="3" size="3" />%</td><td>0</td><td>0</td><td>0</td><td>{$ressources_prod.3|separerNombres}</td></tr>
					{if $planete->batiments.4}<tr><td>{$planete->batiments.4} {$LANG[$race].batiments.noms_sing.4|ucfirst}</td><td><input type="text" name="coeff_ce" class="text" value="{$ressources_coef.4}" maxlength="3" size="3" />%</td><td>0</td><td>0</td><td>0</td><td>{$ressources_prod.4|separerNombres}</td></tr>{/if}
					<tr><td>{$planete->batiments.10} {$LANG[$race].batiments.noms_sing.10|ucfirst}</td><td>-</td><td>{$ressources_silo.1|separerNombres}</td><td>{$ressources_silo.1|separerNombres}</td><td>{$ressources_silo.1|separerNombres}</td><td>0</td></tr>
					<tr><th>Total</th><td>-</td><td>{$ressources_prod.0|separerNombres}</td><td>{$ressources_prod.1|separerNombres}</td><td>{if $planete->batiments.3}{$ressources_toto.0|separerNombres}{else}0{/if}</td><td{if $planete->energieConso >= $planete->energie} class="lack"{/if}>{$ressources_conso.4|separerNombres}</td></tr>
					</tbody>
				</table>
				<input type="submit" class="submit" value="GO" />
			</fieldset></form>
{if $planete->batiments.4}
			<h2>{$LANG[$race].batiments.noms_sing.4|ucfirst} : augmentation de la capacité</h2>
			<form method="post" action="{$menu.ressources}">
				<fieldset class="form">
					<label for="nb">Capacité à acheter pour {math equation="7000-x*1250" x=$planete->batiments[4]} {$LANG[$race].ressources.noms.hydrogene} : <input type="text" id="nb" name="anb" value="0" size="2" /></label>
					<input type="submit" class="submit" value="GO" />
				</fieldset>
			</form>
{/if}
{include file='game/footer.tpl'}