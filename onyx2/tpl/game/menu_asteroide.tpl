<ul>
	<li><a href="{$menu.accueil}"{if $page == "accueil"} class="hilight"{/if}>Accueil</a></li>
	<li><a href="{$menu.alli_chat}"{if $page == "alliance/chat_ajax" || $page == "alliance/chat_irc"} class="hilight"{/if}>Chat de l'alliance</a></li>
	{if $planete->url_forum}<li><a href="{$planete->url_forum}" class="external">Forum de l'alliance</a></li>{/if}
	<li>
		<a href="{$menu.alli_messagerie}"{if preg_match("#Messagerie d'alliance#", $titre)} class="hilight"{/if}>Messagerie d'alliance</a>
		<ul>
			<li><a href="{$menu.alli_messagerie}&amp;n=recus"{if preg_match("#Messagerie  d'alliance#", $titre) && $onglet != "rapports" && $onglet != "send" && $onglet != "envoyer"} class="hilight"{/if}>Reçus{if $alertMail.0} ({$alertMail.0}){/if}</a></li>
			<li><a href="{$menu.alli_messagerie}&amp;n=rapports"{if $onglet == "rapports" && preg_match("#Messagerie  d'alliance#", $titre)} class="hilight"{/if}>Rapports{if $alertMail.1} ({$alertMail.1}){/if}</a></li>
			<li><a href="{$menu.alli_messagerie}&amp;n=send"{if $onglet == "send" && preg_match("#Messagerie  d'alliance#", $titre)} class="hilight"{/if}>Envoyés</a></li>
			<li><a href="{$menu.alli_messagerie}&amp;n=envoyer"{if $onglet == "envoyer"} class="hilight"{/if}>Ecrire</a></li>
		</ul>
	</li>
</ul>
{if $planete->permissions_alliance & 1 || $planete->permissions_alliance & 2}
<ul>
	{if $planete->permissions_alliance & 1}<li><a href="{$menu.batiments}"{if $page == "batiments_alli"} class="hilight"{/if}>B&acirc;timents</a></li>{/if}
	{if $planete->permissions_alliance & 2}<li><a href="{$menu.chantierspatial}"{if $page == "chantierspatial"} class="hilight"{/if}>{$LANG[$race].alli_batiments.noms_sing.3|ucfirst}</a></li>{/if}
</ul>
{/if}
<ul>
	<li><a href="{$menu.ressources}"{if $page == "ressources_alli"} class="hilight"{/if}>Ressources</a></li>
{if $planete->permissions_alliance &32 || $planete->permissions_alliance &64 || $planete->permissions_alliance &128}
	<li>
		<a href="{$menu.gestion}"{if preg_match("#gestion#", $page)} class="hilight"{/if}>Gestion</a>
		<ul>
			{if $planete->permissions_alliance & 32}<li><a href="{$menu.gestion}&amp;g=membres"{if $onglet == "membres" && preg_match("#gestion#", $page)} class="hilight"{/if}>Membres</a></li>{/if}
			{if $planete->permissions_alliance & 128}<li><a href="{$menu.gestion}&amp;g=grades"{if $onglet == "grades" && preg_match("#gestion#", $page)} class="hilight"{/if}>Grades</a></li>{/if}
			{if $planete->permissions_alliance & 64}<li><a href="{$menu.gestion}&amp;g=wings"{if $onglet == "wings" && preg_match("#gestion#", $page)} class="hilight"{/if}>Wings</a></li>{/if}
			{if $planete->permissions_alliance & 128 && !$planete->wing}<li><a href="{$menu.gestion}&amp;g=alliance"{if $onglet == "alliance" && preg_match("#gestion#", $page)} class="hilight"{/if}>Alliance</a></li>{/if}
		</ul>
	</li>
{/if}
	<li>
		<a href="{$menu.diplomatie}"{if preg_match("#diplomatie/#", $page)} class="hilight"{/if}>Diplomatie</a>
		<ul>
			<li><a href="{$menu.diplomatie}&amp;o=actus"{if $onglet == "actus" && preg_match("#diplomatie/#", $page)} class="hilight"{/if}>Actualités</a></li>
			<li><a href="{$menu.diplomatie}&amp;o=encours"{if $onglet == "encours" && preg_match("#diplomatie/#", $page)} class="hilight"{/if}>En place</a></li>
			<li><a href="{$menu.diplomatie}&amp;o=archives"{if $onglet == "archives" && preg_match("#diplomatie/#", $page)} class="hilight"{/if}>Résiliés/annulés</a></li>
			{if $planete->permissions_alliance & 8}<li><a href="{$menu.diplomatie}&amp;o=new"{if $onglet == "new" && preg_match("#diplomatie/#", $page)} class="hilight"{/if}>Nouveau</a></li>{/if}
		</ul>
	</li>
	<li><a href="{$menu.flotte}"{if $page == "flotte" || $page == "flotte1" || $page == "flotte2" || $page == "flotten"} class="hilight"{/if}>Flottes</a></li>
	<!--<li><a href="?p=simulateur"{if $page == "simulateur_combat"} class="hilight"{/if}>Simulateur de combat</a></li>-->
	<li><a href="{$menu.carte}"{if $page == "carte"} class="hilight"{/if}>Carte spatiale</a></li>
	<li>
		<a href="{$menu.options}"{if preg_match("#options/#", $page)} class="hilight"{/if}>Options</a>
		<ul>
			<li><a href="{$menu.options}&amp;n=ext"{if $onglet == "ext" && preg_match("#options/#", $page)} class="hilight"{/if}>Extérieures</a></li>
			<li><a href="{$menu.options}&amp;n=compte"{if $onglet == "compte" && preg_match("#options/#", $page)} class="hilight"{/if}>Compte</a></li>
			<li><a href="{$menu.options}&amp;n=jeu"{if $onglet == "jeu" && preg_match("#options/#", $page)} class="hilight"{/if}>Jeu</a></li>
			<li><a href="{$menu.options}&amp;n=notif"{if $onglet == "notif" && preg_match("#options/#", $page)} class="hilight"{/if}>Notifications</a></li>
		</ul>
	</li>
</ul>
<ul>
	<li><a href="{$menu.messages}"{if preg_match("#Messagerie - #", $titre) || $page == "envoyer"} class="hilight"{/if}>
		{if $alertMail.0 + $alertMail.1 >= 1}<b>Messages {if $alertMail.0}({$alertMail.0}) {/if}et rapports{if $alertMail.1} ({$alertMail.1}){/if}</b>{else}Messages et rapports{/if}</a>
		<ul>
			<li><a href="{$menu.messages}&amp;n=recus"{if preg_match("#Messagerie - #", $titre) && $onglet != "rapports" && $onglet != "send" && $onglet != "envoyer"} class="hilight"{/if}>Reçus{if $alertMail.0} ({$alertMail.0}){/if}</a></li>
			<li><a href="{$menu.messages}&amp;n=rapports"{if $onglet == "rapports" && preg_match("#Messagerie - #", $titre)} class="hilight"{/if}>Rapports{if $alertMail.1} ({$alertMail.1}){/if}</a></li>
			<li><a href="{$menu.messages}&amp;n=send"{if $onglet == "send" && preg_match("#Messagerie - #", $titre)} class="hilight"{/if}>Envoyés</a></li>
			<li><a href="{$menu.messages}&amp;n=envoyer"{if $onglet == "envoyer"} class="hilight"{/if}>Ecrire</a></li>
		</ul>
	</li>
	<li><a href="{$menu.classement}"{if $page == "classement"} class="hilight"{/if}>Classement</a></li>
	<li><a href="{$menu.chat}"{if $page == "chat"} class="hilight"{else} class="external"{/if}>Chat</a></li>
	<li><a href="{$menu.forums}" class="external">Forums</a></li>
	<li><a href="{$menu.bugs}"{if $page == "bugs"} class="hilight"{/if}>Rapports de bug</a></li>
	<li><a href="{$menu.faq}"{if $page == "faq" || preg_match('/aide/', $page)} class="#hilight#"{/if}>F.A.Q.</a></li>
	<li><a href="{$menu.deconnexion}">Déconnexion</a></li>
</ul>