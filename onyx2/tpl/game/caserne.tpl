{include file='game/header.tpl'}
		<ul class="onglets">
		{section name=nb start=0 loop=$planete->batiments.9}
                         {assign var=i value=$smarty.section.nb.index}
			<li{if $lieu == $i} class="hilight"{/if}><a href="{$menu.caserne}&amp;k={$i}">Caserne {$i+1}</a></li>
		{/section}
		</ul>

		<h2>File d'attente</h2>
		<ul id="file">
{capture name='expFile'}
{assign var='keyF' value=$lieu}
{foreach from=$files.$keyF item=element key=keyE}
	{if $element.5}<li><strong>Prochaine unit&eacute; </strong> : <span class="countdown">{$element.4|countdown}</span></li>{/if}
	<li>{$element.1} {if $element.1 > 1}{$LANG[$race].caserne.noms_pluriel[$element.0]}{else}{$LANG[$race].caserne.noms_sing[$element.0]}{/if} {if $element.2} (démolition){/if} - <span{if $element.5} class="countdown"{/if}>{$element.3|countdown}</span> - <a href="{$menu.caserne}&amp;n={$onglet}&amp;k={$lieu}&amp;a={$keyF}&amp;b={$keyE}">Annuler un</a> - <a href="{$menu.caserne}&amp;n={$onglet}&amp;a={$keyF}&amp;k={$lieu}&amp;b={$keyE}&amp;s={$element.1}">Annuler tous</a></li>
{/foreach}
{/capture}
{if $files && $smarty.capture.expFile}
	{$smarty.capture.expFile}
{else}
			<li>Aucune unit&eacute; dans la file d'attente</li>
{/if}
		</ul>
		<h3><a href="{$menu.arbre}&amp;q=caserne">Arbre des technologies</a></h3><br />
		<h2>{$LANG[$race].batiments.noms_sing.9|ucfirst}</h2>
{if $unites}
		<div id="constructions">
{foreach from=$unites item=unite}
	        <dl>
				<dt>{$LANG[$race].caserne.noms_sing[$unite.id]|ucfirst}{if $unite.nombre > 0} ({$unite.nombre} unité{if $unite.nombre > 1}s{/if}){/if}</dt>
				<dd class="description"><a href="?p=description&amp;c={$unite.id}#body"><img src="{$url_images}images/caserne/{$unite.image}" alt="{$LANG[$race].caserne.noms_sing[$unite.id]|ucfirst}" /></a><p>{$LANG[$race].caserne.descriptions[$unite.id]}</p></dd>
				<dd>
					{if $unite.nombre > 0}<strong>Nombre actuel :</strong> {$unite.nombre}<br /><br />{/if}
					{if $unite.nec_metal > 0}<strong>Coût {$LANG[$race].ressources.noms.metal} :</strong> {$unite.nec_metal|separerNombres}<br />{/if}
					{if $unite.nec_cristal > 0}<strong>Coût {$LANG[$race].ressources.noms.cristal} :</strong> {$unite.nec_cristal|separerNombres}<br />{/if}
					{if $unite.nec_hydrogene > 0}<strong>Coût {$LANG[$race].ressources.noms.hydrogene} :</strong> {$unite.nec_hydrogene|separerNombres}<br />{/if}
					<strong>Temps de construction :</strong> {$unite.temps}<br />
					<br />
					{if $unite.nec_hydrogene > $planete->hydrogene || $unite.nec_cristal > $planete->cristal || $unite.nec_metal > $planete->metal}<span class="lack">Ressources insuffisantes</span>
					{else}<form action="{$menu.caserne}&amp;c={$unite.id}&amp;k={$lieu}" method="post"><fieldset>
					<label for="cas{$unite.id}">Nombre : <input type="text" class="text" name="cas{$unite.id}" id="cas{$unite.id}" value="0" /></label>
					<input type="submit" class="submit" value="OK" />
				    </fieldset></form>{/if}
				</dd>
			</dl>
{/foreach}
		</div>
{else}	<div id="erreur">Aucune unit&eacute; à entra&icirc;ner actuellement.</div>{/if}
{include file='game/footer.tpl'}