<ul>
	<li><a href="{$menu.accueil}"{if $page == "accueil"} class="hilight"{/if}>Accueil</a></li>
	<li>
		<a href="{$menu.batiments}"{if $page == "batiments"} class="hilight"{/if}>B&acirc;timents</a>
		<ul>
			<li><a href="{$menu.batiments}"{if $page == "batiments" && $onglet == 0} class="hilight"{/if}>Tous</a></li>
			<li><a href="{$menu.batiments}&amp;n=1"{if $page == "batiments" && $onglet&1} class="hilight"{/if}>Mines/centrales</a></li>
			<li><a href="{$menu.batiments}&amp;n=2"{if $page == "batiments" && $onglet&2} class="hilight"{/if}>Civils</a></li>
			<li><a href="{$menu.batiments}&amp;n=4"{if $page == "batiments" && $onglet&4} class="hilight"{/if}>Militaires</a></li>
		</ul>
	</li>
	<li><a href="{$menu.caserne}"{if $page == "caserne"} class="hilight"{/if}>{$LANG[$race].batiments.noms_sing.9|ucfirst}</a></li>
	<li><a href="{$menu.chantierspatial}"{if $page == "chantierspatial"} class="hilight"{/if}>{$LANG[$race].batiments.noms_sing.8|ucfirst}</a></li>
	<li>
		<a href="{$menu.chantierterrestre}"{if $page == "chantierterrestre"} class="hilight"{/if}>{$LANG[$race].batiments.noms_sing.7|ucfirst}</a>
		<ul>
			<li><a href="{$menu.chantierterrestre}"{if $page == "chantierterrestre" && $onglet != "defenses"} class="hilight"{/if}>Unités</a></li>
			<li><a href="{$menu.chantierterrestre}&amp;n=defenses"{if $page == "chantierterrestre" && $onglet == "defenses"} class="hilight"{/if}>Défenses</a></li>
		</ul>
	</li>
	{if $planetes.0.id == $planete->id}<li>
		<a href="{$menu.laboratoire}"{if $page == "laboratoire"} class="hilight"{/if}>{$LANG[$race].batiments.noms_sing.6|ucfirst}</a>
		<ul>
			<li><a href="{$menu.laboratoire}&amp;n=0"{if $page == "laboratoire" && $onglet == 0} class="hilight"{/if}>Industrie</a></li>
			<li><a href="{$menu.laboratoire}&amp;n=1"{if $page == "laboratoire" && ($onglet == 1 || $onglet == 2)} class="hilight"{/if}>Ingénierie</a></li>
			<li><a href="{$menu.laboratoire}&amp;n=3"{if $page == "laboratoire" && $onglet == 3} class="hilight"{/if}>Politique</a></li>
			<li><a href="{$menu.laboratoire}&amp;n=4"{if $page == "laboratoire" && $onglet == 4} class="hilight"{/if}>Armement</a></li>
			<li><a href="{$menu.laboratoire}&amp;n=5"{if $page == "laboratoire" && ($onglet == 5 || $onglet == 6)} class="hilight"{/if}>Défenses</a></li>
			<li><a href="{$menu.laboratoire}&amp;n=7"{if $page == "laboratoire" && $onglet == 7} class="hilight"{/if}>Projets expérimentaux</a></li>
			<li><a href="{$menu.laboratoire}&amp;n=8"{if $page == "laboratoire" && $onglet == 8} class="hilight"{/if}>Expansion</a></li>
		</ul>
	</li>{/if}
	<li>
		<a href="{$menu.arbre}"{if $page == "arbre" || $page == "description"} class="hilight"{/if}>Arbre technologique</a>
	</li>
</ul>
<ul>
	<li><a href="{$menu.ressources}"{if $page == "ressources"} class="hilight"{/if}>Ressources</a></li>
	<li><a href="{$menu.gestion}"{if $page == "gestion" || $page == "rename"} class="hilight"{/if}>Gestion</a></li>
	<li><a href="{$menu.marche}"{if $page == "marche" || $page == "bourse_ressources" || $page == "bourseDetails"} class="hilight"{/if}>March&eacute;</a></li>
	<li><a href="{$menu.flotte}"{if $page == "flotte" || $page == "flotte1" || $page == "flotte2" || $page == "flotten"} class="hilight"{/if}>Flottes</a></li>
	<!--<li><a href="?p=simulateur"{if $page == "simulateur_combat"} class="hilight"{/if}>Simulateur de combat</a></li>-->
	<li><a href="{$menu.carte}"{if $page == "carte"} class="hilight"{/if}>Carte spatiale</a></li>
	<li>
		<a href="{$menu.options}"{if preg_match("#options/#", $page)} class="hilight"{/if}>Options</a>
		<ul>
			<li><a href="{$menu.options}&amp;n=ext"{if $onglet == "ext" && preg_match("#options/#", $page)} class="hilight"{/if}>Extérieures</a></li>
			<li><a href="{$menu.options}&amp;n=compte"{if $onglet == "compte" && preg_match("#options/#", $page)} class="hilight"{/if}>Compte</a></li>
			<li><a href="{$menu.options}&amp;n=jeu"{if $onglet == "jeu" && preg_match("#options/#", $page)} class="hilight"{/if}>Jeu</a></li>
			<li><a href="{$menu.options}&amp;n=notif"{if $onglet == "notif" && preg_match("#options/#", $page)} class="hilight"{/if}>Notifications</a></li>
		</ul>
	</li>
</ul>
<ul>
	<li><a href="{$menu.messages}{if $planete->options& 16 && $alertMail.1 < $alertMail.0}&amp;n=recus"{elseif $alertMail.1 > $alertMail.0}&amp;n=rapports{/if}"{if $page == "messagerie" || $page == "envoyer"} class="hilight"{/if}>
		{if $alertMail.0 + $alertMail.1 >= 1}<b>Messages {if $alertMail.0}({$alertMail.0}) {/if}et rapports{if $alertMail.1} ({$alertMail.1}){/if}</b>{else}Messages et rapports{/if}</a>
		<ul>
			<li><a href="{$menu.messages}&amp;n=recus"{if $page == "messagerie" && $onglet != "rapports" && $onglet != "send" && $onglet != "envoyer"} class="hilight"{/if}>Reçus{if $alertMail.0} ({$alertMail.0}){/if}</a></li>
			<li><a href="{$menu.messages}&amp;n=rapports"{if $onglet == "rapports" && $page == "messagerie"} class="hilight"{/if}>Rapports{if $alertMail.1} ({$alertMail.1}){/if}</a></li>
			<li><a href="{$menu.messages}&amp;n=send"{if $onglet == "send" && $page == "messagerie"} class="hilight"{/if}>Envoyés</a></li>
			<li><a href="{$menu.messages}&amp;n=envoyer"{if $onglet == "envoyer"} class="hilight"{/if}>Ecrire</a></li>
		</ul>
	</li>
	<li><a href="{$menu.alliance}"{if preg_match("#alliance/#", $page)} class="hilight"{/if}>Alliance</a></li>
	<li><a href="{$menu.classement}"{if $page == "classement"} class="hilight"{/if}>Classement</a></li>
	<li><a href="{$menu.chat}"{if $page == "chat"} class="hilight"{else} class="external"{/if}>Chat</a></li>
	<li><a href="{$menu.forums}" class="external">Forums</a></li>
	<li><a href="{$menu.bugs}"{if $page == "bugs"} class="hilight"{/if}>Rapports de bug</a></li>
	<li><a href="{$menu.faq}"{if $page == "faq" || preg_match('/aide/', $page)} class="#hilight#"{/if}>F.A.Q.</a></li>
	<li><a href="{$menu.deconnexion}">Déconnexion</a></li>
</ul>
