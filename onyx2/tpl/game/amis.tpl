{include file='game/header.tpl'}
			<h2>Amis existants</h2>
			<table>
				<thead>
					<tr>
						<th>Nom du joueur</th>
						<th>Supprimer</th>
					</tr>
				</thead>
				<tbody>
{foreach from=$destins item=destin}
					<tr>
						<td>{$destin.1|escape}</td>
						<td><a href="?p=amis&amp;d={$destin.0}&amp;a={$destin.2}">Supprimer</a></td>
					</tr>
{/foreach}
				</tbody>
			</table>
			<h2>Ajouter un ami</h2>
			<form action="?p=amis" method="post">
				<fieldset class="form">
					<label for="nom">Nom du joueur : <input class="text" type="text" id="nom" name="nom" maxlength="32" /></label><br />
					<input class="submit" type="submit" value="Ok" />
				</fieldset>
			</form>
{include file='game/footer.tpl'}