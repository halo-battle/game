{include file='game/header.tpl'}
			<h2>Flottes en action</h2>
			<table>
				<thead>
					<tr><th>Nom</th><th>Mission</th><th>Nb.vaisseau(x)</th><th>Destination</th><th>Heure d'arrivée</th><th>Retour</th><th>Heure de retour</th></tr>
				</thead>
				<tbody>
{if $flottesEC}
{foreach from=$flottesEC item=flotteEC}
					<tr>
						<td><a href="{$menu.flotte}&amp;n={$flotteEC->id_flotte}">{$flotteEC->nom|escape}</a></td>
						<td>{$flotteEC->txtMission($flotteEC->mission)}</td>
						<td>{$flotteEC->nb_vais}</td>
						<td>{if $flotteEC->end_type == "2"}{$flotteEC->end_planete->nom_alliance} [{$flotteEC->end_planete->galaxie}:{$flotteEC->end_planete->ss}:A]{elseif $flotteEC->end_type == "1"}[{$flotteEC->end_planete}]{elseif $flotteEC->end_type == "3"}[{$flotteEC->end_planete}:A]{else}{$flotteEC->end_planete->nom_planete} [{$flotteEC->end_planete->galaxie}:{$flotteEC->end_planete->ss}:{$flotteEC->end_planete->position}]{/if}</td>
						<td>{$flotteEC->printTime_end()|date_format:"%d/%m %H:%M:%S"}</td>
						{if $flotteEC->printTime_ret()}<td>{if $flotteEC->start_type == "2"}{$flotteEC->start_planete->nom_alliance} [{$flotteEC->start_planete->galaxie}:{$flotteEC->start_planete->ss}:A]{elseif $flotteEC->start_type == "1"}[{$flotteEC->start_planete}]{elseif $flotteEC->start_type == "3"}[{$flotteEC->start_planete}:A]{else}{$flotteEC->start_planete->nom_planete} [{$flotteEC->start_planete->galaxie}:{$flotteEC->start_planete->ss}:{$flotteEC->start_planete->position}]{/if}</td>
						<td>{$flotteEC->printTime_ret()|date_format:"%d/%m %H:%M:%S"}</td>
						{else}<td>-</td><td>-</td>{/if}
					</tr>
{/foreach}
{else}
					<tr>
						<td colspan="7">Aucune flotte en action</td>
					</tr>
{/if}
				</tbody>
			</table>
{if $flottesEP && $planete->options& 32}
			<h2>Flottes en préparation</h2>
			<table>
				<thead>
					<tr><th>Statut</th><th>Nom</th><th>Mission</th><th>Nb.vaisseau(x)</th><th>Destination</th></tr>
				</thead>
				<tbody>
{foreach from=$flottesEP key=key item=flotteEP}
					<tr>
						<td>{$flotteEP.statut}</td>
						<td><a href="{$menu.flotte}&amp;c={$key}">{if $flotteEP.nom}{$flotteEP.nom|escape}{else}Sans nom{/if}</a></td>
						<td>{$flotteEP.mission}</td>
						<td>{$flotteEP.nbVaisseaux}</td>
						<td>{if $flotteEP.end_position}[{$flotteEP.end_galaxie}:{$flotteEP.end_systeme}:{$flotteEP.end_position}]{else}Indéterminé{/if}</td>
					</tr>
{/foreach}
				</tbody>
			</table>
			<p>Cette liste sera perdue après votre déconnexion</p>
{/if}
			<h2>Vaisseaux disponibles sur cette planète</h2>
			<form action="{$menu.flotte}" method="post">
				<table>
					<thead>
						<tr><th>Vaisseaux</th><th>Nombre disponible</th><th>Max</th><th>A envoyer<br />Flottes : {$nbflotte}/{$nbflottemax}</th></tr>
					</thead>
					<tfoot>
						<tr><td colspan="4"><input class="button" type="button" value="Tous les vaisseaux" onclick="vaisseaux(true)" />  <input class="button" type="button" value="Aucun vaisseau" onclick="vaisseaux(false)" /></td></tr>
						<tr><td colspan="4">{$action}</td></tr>
					</tfoot>
					<tbody>
{foreach from=$planete->vaisseaux key=key item=vaisseau}
					{if $vaisseau > 0}
						<tr>
							<td>{if $vaisseau > 1}{$LANG[$race].vaisseaux.noms_pluriel[$key]|ucfirst}{else}{$LANG[$race].vaisseaux.noms_sing[$key]|ucfirst}{/if}</td>
							<td>{$vaisseau|separerNombres}<span id="v_{$key}" style="display: none;">{$vaisseau}</span></td>
							<td><input class="button" type="button" value="Max" onclick="max('{$key}')" /></td>
							<td><input class="text" type="text" name="v{$key}" value="0" /></td>
						</tr>
					{/if}
{/foreach}
					</tbody>
				</table>
			</form>
{include file='game/footer.tpl'}
