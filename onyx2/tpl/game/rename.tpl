{include file='game/header.tpl'}
		<h2>Abandonner la planète</h2>
		{if $abandonH}<b>Attention, cette action est irréversible ; vous perdrez tous les batiments, vaisseaux et unités présents sur cette planète, les flottes ayant une mision qui a pour origine votre planète se perdront dans l'espace.</b><br /><br />Si vous êtes sur de vouloir abandonner cette planète : <a href="?p=rename&amp;a={$abandonH}&amp;i={$planete->id}" onclick="return confirm('Êtes-vous sûr de vouloir abandonner cette planète ?');">cliquez sur ce lien</a>.<br /><br />
		{else}Vous ne pouvez pas abandonner cette planète car vous n'en avez pas d'autre.<br /><br />{/if}
		<h2>Renommer la planète</h2>
		<dl id="about">
			<dt><img src="{$url_images}images/planetes/{$planete->image}.jpg" alt="{$planete->nom_planete|escape}" /></dt>
			<dd>
				<form action="?p=rename" method="post"><fieldset class="form">
					<label for="rename">Nom de la planète ({$planete->galaxie}:{$planete->ss}:{$planete->position}) : <input type="text" class="text" name="planete" id="rename" maxlength="18" value="{$planete->nom_planete|escape}" /></label><br />
					<input type="submit" class="submit" value="OK" />
			    </fieldset></form>
			</dd>
		</dl>
{include file='game/footer.tpl'}