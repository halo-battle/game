{include file='game/header.tpl'}
			<h2>Arbre Technologique {$raceAff}</h2>
			Race : <a href="{$menu.arbre}&amp;r=humain">Humains</a> - <a href="{$menu.arbre}&amp;r=covenant">Covenants</a>
			<div class="techno">
				<div{if $defaut == 'batiments'} class="hightlight"{/if}><a href="{$menu.arbre}&amp;q=batiments&amp;r={$raceAff}">Bâtiments</a>
					<div><table><thead>
					<tr><th>Batiments (niveau)</th><th>Technologies nécessaires (niveau actuel)</th></tr>
					</thead><tbody>
{if $smarty.const.SURFACE == "planete"}
{foreach from=$batiments key=key item=batiment}
						<tr><td><a href="?p=description&amp;b={$key}&amp;r={$raceAff}#menu">{$LANG[$raceAff].batiments.noms_sing.$key|ucfirst}</a> ({$batiment.niveau}) :</td><td>{$batiment.etat}</td></tr>
{/foreach}
{else}
{foreach from=$batiments key=key item=batiment}
						<tr><td><a href="?p=description&amp;a={$key}&amp;r={$raceAff}#menu">{$LANG[$raceAff].alli_batiments.noms_sing.$key|ucfirst}</a> ({$batiment.niveau}) :</td><td>{$batiment.etat}</td></tr>
{/foreach}
{/if}
					</tbody></table></div>
					</div>

				<div{if $defaut == 'technologies'} class="hightlight"{/if}><a href="{$menu.arbre}&amp;q=technologies&amp;r={$raceAff}">Technologies</a>
					<div><table><thead>
					<tr><th>Branche</th><th>Technologies (niveau du joueur / niveau max)</th><th>Technologies nécessaires (niveau actuel)</th></tr>
				</thead><tbody>
{counter start=0 assign=branche print=false}
{foreach from=$technologies item=technologie}
			{foreach from=$technologies[$branche] item=technologie}
						{assign var="id" value=$technologie.id}
						<tr><td>{$LANG[$raceAff].technologies.branches.$branche|ucfirst}</td><td><a href="?p=description&amp;b={$branche}&amp;t={$id}&amp;r={$raceAff}#menu">{$LANG[$raceAff].technologies.noms_sing.$branche.$id|ucfirst}</a> ({$technologie.niveau}/{$technologie.niveau_max}) :</td><td>{$technologie.etat}</td></tr>
			{/foreach}
			{counter}
{/foreach}
					</tbody></table></div>
					</div>

				<div{if $defaut == 'caserne'} class="hightlight"{/if}><a href="{$menu.arbre}&amp;q=caserne&amp;r={$raceAff}">{$LANG[$raceAff].batiments.noms_sing.9|ucfirst}</a>
					<div><table><thead>
					<tr><th>Unité</th><th>Technologies nécessaires</th></tr>
					</thead><tbody>
{foreach from=$caserne key=key item=unite}
						<tr><td><a href="?p=description&amp;c={$key}&amp;r={$raceAff}#menu">{$LANG[$raceAff].caserne.noms_sing.$key|ucfirst}</a> ({$unite.niveau}) :</td><td>{$unite.etat|ucfirst}</td></tr>
{/foreach}
					</tbody></table></div>
					</div>

				<div{if $defaut == 'chantierterrestre'} class="hightlight"{/if}><a href="{$menu.arbre}&amp;q=chantierterrestre&amp;r={$raceAff}">{$LANG[$raceAff].batiments.noms_sing.7|ucfirst}</a>
					<div><table><thead>
					<tr><th>Véhicules/Défenses</th><th>Technologies nécessaires (niveau actuel)</th></tr>
					</thead><tbody>
{foreach from=$unites key=key item=unite}
						<tr><td><a href="?p=description&amp;d={$key}&amp;r={$raceAff}#menu">{$LANG[$raceAff].terrestre.noms_sing.$key|ucfirst}</a> ({$unite.niveau}) :</td><td>{$unite.etat}</td></tr>
{/foreach}
					</tbody></table></div>
					</div>

				<div{if $defaut == 'vaisseaux'} class="hightlight"{/if}><a href="{$menu.arbre}&amp;q=vaisseaux&amp;r={$raceAff}">{$LANG[$raceAff].batiments.noms_sing.8|ucfirst}</a>
					<div><table><thead>
					<tr><th>Vaisseaux</th><th>Technologies nécessaires (niveau actuel)</th></tr>
					</thead><tbody>
{foreach from=$vaisseaux key=key item=vaisseau}
						<tr><td><a href="?p=description&amp;v={$key}&amp;r={$raceAff}#menu">{$LANG[$raceAff].vaisseaux.noms_sing.$key|ucfirst}</a> ({$vaisseau.niveau}) :</td><td>{$vaisseau.etat}</td></tr>
{/foreach}
					</tbody></table></div>
				</div>
			</div>
{include file='game/footer.tpl'}
