{include file='game/header.tpl'}
			<h3><a href="?p=rename">{if ($smarty.const.SURFACE == "planete" && !$planete->nom_planete) || ($smarty.const.SURFACE == "asteroide" && !$planete->nom_asteroide)}Sans nom{/if}{if $smarty.const.SURFACE == "planete"}{$planete->nom_planete|escape}{else}{$planete->nom_asteroide|escape}{/if}</a> (<span>{if $smarty.const.SURFACE == "planete"}{$planete->pseudo}{else}[{$planete->tag|upper}] {$planete->nom_alliance}{/if}</span>)</h3>
		{if $planete->options & 1}
			<img src="{$url_images}images/{if $smarty.const.SURFACE == "planete"}planetes{else}asteroides{/if}/{$planete->image}.jpg" width="200" height="200" alt="Astéroïde" /><br />
		{/if}
            <em>Heure du serveur : {$smarty.now|date_format:"/%a %d %b %Y/%H%M %S"}</em>
			<h2>&Eacute;vénements</h2>
{capture name='events'}
{if $multi}<strong style="color: #FF0000;"><a href="?p=avertmulti">Multicompte :</a> évitez toute interaction avec {foreach from=$multi item=cmpt key=key}{if $cmpt.id_util != $planete->id_user}{if $key > 0}, {/if}{$cmpt.pseudo}{/if}{/foreach} durant cette session.</strong><br />{/if}
{if $planete->mv < 0}<strong style="color: #FF0000;">Une demande de suppression de votre compte est en cours. Il sera supprimé sous 48h.<br />Si vous n'avez pas demandé la suppression de votre compte, <a href="{$link.operateur}">contactez un opérateur</a> au plus vite.</strong><br />{/if}
{foreach from=$radar item=flottes key=key}
{foreach from=$flottes.0 item=flotte key=key}
{if $flotte.mission == 1}<h4 class="attaque">attaque: Une flotte ennemie en provenance de {$flotte.nom_planete|escape} [{$flotte.start_galaxie}:{$flotte.start_ss}:{$flotte.start_position}] atteint la planète {$flottes.1.0|escape} [{$flottes.1.1}:{$flottes.1.2}:{$flottes.1.3}] dans <span id="reste{$key}">{$flotte.arrive_time}</span><script type="text/javascript">reste({$flotte.arrive_time},'reste{$key}', true);</script></h4>
{elseif $flotte.mission == 2}<h4 class="attaque">transport: Une flotte en provenance de {$flotte.nom_planete|escape} [{$flotte.start_galaxie}:{$flotte.start_ss}:{$flotte.start_position}] atteint la planète {$flottes.1.0|escape} [{$flottes.1.1}:{$flottes.1.2}:{$flottes.1.3}] dans <span id="reste{$key}">{$flotte.arrive_time}</span><script type="text/javascript">reste({$flotte.arrive_time},'reste{$key}', true);</script></h4>
{elseif $flotte.mission == 6 || $flotte.mission == 7}<h4 class="attaque">stationnement/don: Une flotte en provenance de {$flotte.nom_planete|escape} [{$flotte.start_galaxie}:{$flotte.start_ss}:{$flotte.start_position}] atteint la planète {$flottes.1.0|escape} [{$flottes.1.1}:{$flottes.1.2}:{$flottes.1.3}] dans <span id="reste{$key}">{$flotte.arrive_time}</span><script type="text/javascript">reste({$flotte.arrive_time},'reste{$key}', true);</script></h4>
{/if}
{/foreach}
{/foreach}
{if $alertMail.0 + $alertMail.1 >= 1}<h4>messagerie: {if $alertMail.0}<a href="?p=messagerie">{$alertMail.0} message{if $alertMail.0 > 1}s{/if}{/if}{if $alertMail.0 != 0 && $alertMail.1 != 0}</a> et {else} non lu</a>{/if}{if $alertMail.1}<a href="?p=messagerie&amp;n=rapports">{$alertMail.1} rapport{if $alertMail.1 > 1}s{/if} non lu</a>{/if}</h4>
{/if}
{/capture}
{if $smarty.capture.events}
	{$smarty.capture.events}
{else}
	<h4>Rien à signaler !</h4>
{/if}
			<h2>Infos</h2>
		{if $smarty.const.SURFACE == "planete"}
			<h4>{php}dDonnees::nameVilles($this->_tpl_vars['planete']->batiments[17]){/php}</h4>
			<h4>Diamètre: {$diametre|separerNombres} km <ins>({$planete->casesRest} case{if $planete->casesRest > 1}s{/if} disponible{if $planete->casesRest > 1}s{/if} sur {$planete->cases})</ins></h4>
		{/if}
			<h4>Race : {$planete->race}</h4>
			<h4>position: <a href="?p=carte&amp;galaxie={$planete->galaxie}&amp;ss={$planete->ss}">{$planete->galaxie}.{$planete->ss}.{$planete->position}</a></h4>
		{if $smarty.const.SURFACE == "planete"}
			<h4>population: {$planete->population|separerNombres}</h4>
			<h4>crédits: {$planete->credits|separerNombres}</h4>
			<h4>Classement: <a href="?p=classement&amp;j={$planete->id}&amp;c={$planete->place_points}#moi">{if $planete->place_points == '0'}non classé{else}{$planete->place_points}{if $planete->place_points == '1'}er{else}ème{/if}{/if} ({$planete->points|separerNombres} points)</a></h4>
		{else}
			<h4>nombre de membres: {$planete->details.nb_membres|separerNombres}</h4>
			<h4>fondateur: {$planete->fondateur->pseudo}</h4>
			<h4>crédits: {$planete->credits_alliance|separerNombres}</h4>
			<h4>Classement: <a href="?p=classement&amp;j={$planete->id}&amp;c={$planete->place_points}#moi">{if $planete->place_points == '0'}non classé{else}{$planete->place_points}{if $planete->place_points == '1'}er{else}ème{/if}{/if} ({$planete->points_alliance|separerNombres} points)</a></h4>
		{/if}
			{if $alliance}<h4>Alliance: <a href="?p=alliances">[{$alliance.tag|upper}] {$alliance.nom}</a></h4>{/if}
		{if $smarty.const.SURFACE == "planete" && $planete->options & 2}
{capture name='expFile'}
{assign var='i' value=false}
{if $fileBat}
<h4>
{foreach from=$fileBat item=file key=keyF}
	{foreach from=$file item=element key=keyE}
		{if $keyE < 3}{if $i == true} - {else}{assign var='i' value=true}<a href="{$menu.batiments}">Bâtiments</a> : {/if}{$LANG[$race].batiments.noms_sing[$element.0]}{if $element.1} (démolition){/if}{elseif $keyE == 3}...{/if}
	{/foreach}
{/foreach}
</h4>
{/if}
{assign var='i' value=false}
{if $fileCas}
<h4>
{foreach from=$fileCas item=file key=keyF}
	{if $file}
	{foreach from=$file item=element key=keyE}
		{if $keyE < 3}{if $i == true} - {else}{assign var='i' value=true}<a href="{$menu.caserne}">{$LANG[$race].batiments.noms_sing.9|ucfirst}</a> : {/if}{$element.1} {if $element.1 > 1}{$LANG[$race].caserne.noms_pluriel[$element.0]}{else}{$LANG[$race].caserne.noms_sing[$element.0]}{/if}{if $element.2} (démantèlement){/if}{elseif $keyE == 3}...{/if}
	{/foreach}
	<br />
	{/if}
{/foreach}
</h4>
{/if}
{assign var='i' value=false}
{if $fileVais}
<h4>
{foreach from=$fileVais item=file key=keyF}
	{if $file}
	{foreach from=$file item=element key=keyE}
		{if $keyE < 3}{if $i == true} - {else}{assign var='i' value=true}<a href="{$menu.chantierspatial}">{$LANG[$race].batiments.noms_sing.8|ucfirst}</a> : {/if}{$element.1} {if $element.1 > 1}{$LANG[$race].vaisseaux.noms_pluriel[$element.0]}{else}{$LANG[$race].vaisseaux.noms_sing[$element.0]}{/if}{if $element.2} (démantèlement){/if}{elseif $keyE == 3}...{/if}
	{/foreach}
	<br />
	{/if}
{/foreach}
</h4>
{/if}
{assign var='i' value=false}
{if $fileTer}
<h4>
{foreach from=$fileTer item=file key=keyF}
	{foreach from=$file item=element key=keyE}
		{if $keyE < 3}{if $i == true} - {else}{assign var='i' value=true}<a href="{$menu.chantierterrestre}">{$LANG[$race].batiments.noms_sing.7|ucfirst}</a> : {/if}{$element.1} {if $element.1 > 1}{$LANG[$race].terrestre.noms_pluriel[$element.0]}{else}{$LANG[$race].terrestre.noms_sing[$element.0]}{/if}{if $element.2} (démantèlement){/if}{elseif $keyE == 3}...{/if}
	{/foreach}
{/foreach}
</h4>
{/if}
{assign var='i' value=false}
{if $fileTech}
<h4>
{foreach from=$fileTech item=file key=keyF}
	{foreach from=$file item=element key=keyE}
		{if $keyE < 3}{if $i == true} - {else}{assign var='i' value=true}<a href="{$menu.laboratoire}">{$LANG[$race].batiments.noms_sing.6|ucfirst}</a> : {/if}{$LANG[$race].technologies.noms_sing[$element.0][$element.1]}{elseif $keyE == 3}...{/if}
	{/foreach}
{/foreach}
</h4>
{/if}
{/capture}
	<h2>En cours sur la planète</h2>
{if $smarty.capture.expFile}
	{$smarty.capture.expFile}
{else}
	<h4>Tous les chantiers sont terminés !</h4>
{/if}
		{elseif $smarty.const.SURFACE == "asteroide"}
			<h2>Infos de l'alliance</h2>
			{$planete->details.texte_interne}
		{/if}
{include file='game/footer.tpl'}
