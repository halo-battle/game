{include file='game/header.tpl'}
			<h2>Aide de Halo-Battle :: Avertissements</h2>
			<h3>Avertissement aux utilisateurs de téléviseurs à rétroprojection</h3><br />
			Les images fixes peuvent endommager de manière irréversible le tube cathodique ou marquer à jamais les luminophores qui constituent l'écran de ces téléviseurs.<br />C'est pourquoi il est conseillé d'éviter d'utiliser des jeux vidéo trop souvent ou de façon prolongée avec les téléviseurs à rétroprojection.<br /><br />
			<h3>Prévention des risques d'épilepsie</h3><br />
			<h4>A lire avant toute utilisation d'un jeu vidéo par vous-même ou par votre enfant.</h4><br />
			Chez certaines personnes, la stimulation visuelle par certains effets stroboscopiques ou motifs lumineux peut déclencher une crise d'épilepsie ou une perte de connaissance et ce, y compris dans la vie de tous les jours.<br />
			Chez ces personnes, le simple fait de regarder la télévision ou de jouer à un jeu vidéo peut suffire à déclencher une crise. Les symptômes peuvent même se déclarer chez un individu sans antécédents médicaux ou n'ayant jamais souffert de crise d'épilepsie.<br />
			Si vous-même ou un membre de votre famille avez déjà présenté des symptômes liés à l'épilepsie (crise d'épilepsie ou de perte de connaissance) à la suite d'une exposition à des effets lumineux stroboscopiques, veuillez consulter votre médecin avant de commencer à jouer.<br />
			Nous conseillons vivement aux parents de prêter une attention soutenue à leurs enfants lorsqu'ils utilisent un jeu vidéo. Si vous ou votre enfant ressentez l'un des symptômes suivants en cours de jeu : vertiges, troubles de la vue, contractions oculaires ou musculaires incontrôlées, perte de connaissance, désorientation, mouvements involontaires ou convulsions, veuillez cesser IMMEDIATEMENT la partie et consulter votre médecin.<br /><br />
			<h3>Règles à respecter pour jouer dans les meilleures conditions possibles</h3>
			<ul>
				<li>S'installer confortablement en position assise, le plus loin possible de l'écran.
				<li>Jouer de préférence sur un écran de petite taille.</li>
				<li>Eviter de jouer en cas de fatigue ou de manque de sommeil.</li>
				<li>Veiller à ce que la pièce soit bien éclairée.</li>
				<li>Observer des pauses de 10 à 15 minutes par heure de jeu.</li>
			</ul><br />
			<h3>Prévention des risques liés aux troubles du sommeil</h3><br />
			Pour votre santé, veillez à observer un compte d'heures minimales allouées à votre repos. Jouer plusieurs heures d'affilées comporte un risque de déréglement de votre horloge interne et/ou de troubles du sommeil. Si vous ou votre enfant ressentez l'un des symptômes suivants en cours de jeu : fatigue, vertiges, troubles de la vue, contractions oculaires ou musculaires incontrôlées, perte de connaissance, insomnie, stress, veuillez cesser IMMEDIATEMENT de jouer et prenez du repos.<br />
			Pour bien dormir, observez ces quelques règles :
			<ul>
				<li>Couchez-vous et levez-vous à heures régulières.</li>
				<li>Pas de grasse matinée : levez-vous dès le réveil.</li>
				<li>Dînez léger et pas trop tard, en mangeant un laitage.</li>
				<li>Observer les premiers signes annonciateurs du sommeil, ne luttez pas contre le sommeil.</li>
				<li>Maintenir une température basse (entre 15 et 18 °C) ainsi qu'un bon degré d'hygrométrie.<br />Opter pour l'obscurité, elle favorise le sommeil. Choisir un lit confortable (ni trop ferme ni trop mou) et des couvertures légères.</li>
				<li>Evitez la prise de produits stimulants : coca-cola, thé, café, vitamine C…</li>
				<li>La régularité du sommeil reste le meilleur gage de qualité.</li>
			</ul>
{include file='game/footer.tpl'}