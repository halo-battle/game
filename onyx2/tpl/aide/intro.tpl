{include file='game/header.tpl'}
			<h2>Aide de Halo-Battle :: Introduction</h2>
			<i>Même la plus puissante des nations finit par succomber un jour...</i><br /><br />
			Neuvième Age de la Réclamation. La majestueuse Grande Bonté n'est plus. La voici désormais en proie au Parasite, gangrénée et empoisonnée de l'intérieur. Le glas de la fin du collectif Covenant se fait entendre par-delà les étoiles, alors que les trompettes de la victoire résonnent sur Terre...<br /><br />
			<i>... le chaos est total...</i><br /><br />
			La galaxie n'est plus que ruine ; les épaves des vaisseaux des batailles passées flottent lamentablement dans l'espace à présent. Alors que le Covenant doit faire face à sa plus grave crise de son histoire, les Humains rassemblent leurs dernières forces dans une tentative désespérée de survivre.<br /><br />
			<i>... mais le passé ne meurt jamais.</i><br /><br />
			Tandis que certains Covenants pactisent avec les Humains et d'autres veulent se venger de la trahison odieuse qu'ils ont subis de la part de leurs prophètes, beaucoup d'entre eux vouent encore une haine viscérale contre les Humains, et la guerre devient inévitable. Dans chaque camp, tout le monde s'apprête à livrer les batailles de la dernière chance.<br /><br />
			<i>Préparez-vous à vous battre pour votre cause.</i><br /><br />
			Quel que soit votre camp, que vous soyez Humain ou Covenant, vous aussi devez vous préparer à l'inévitable conflit qui se prépare. Si vous espérez un tant soit peu survivre, il vous faudra développer votre économie, rechercher de nouvelles technologies innovatrices et surtout préparer votre armée. Sachez que vos frères d'armes comptent sur vous.<br /><br />
			<i>Qu'importe les moyens, seule la victoire compte.</i><br />
{include file='game/footer.tpl'}