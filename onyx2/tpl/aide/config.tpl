{include file='game/header.tpl'}
			<h2>Aide de Halo-Battle :: Configuration requise</h2>
			<br /><h3>Configuration nécessaire</h3><br />
			Pour jouer à Halo-Battle, un simple navigateur internet est suffisant. Assurez-vous donc de disposer du matériel informatique adéquat ainsi que d'une connexion internet.<br />
			<br /><h3>Configuration recommandée</h3><br />
			Pour jouer dans des conditions optimales :<br />
			<ul>
				<li>Connexion 512 k</li>
				<li>Navigateur internet Mozilla Firefox ou Opéra</li>
				<li>JavaScript</li>
			</ul>
			<br /><h2>Démarrage du jeu</h2>
			Pour pouvoir jouer à Halo-Battle, rendez-vous sur la page d'accueil du site puis cliquez sur l'onglet « Jeu » pour accéder au menu principal. Renseignez dans les champs appropriés votre login de joueur puis votre mot de passe pour accéder à votre compte.<br />
			Il faut être impérativement inscrit pour pouvoir jouer. Si vous ne l'êtes pas, cliquez sur l'onglet « S'inscrire » via la page d'accueil du site, puis laissez-vous guider. Si vous ne comprenez pas ce que l'on vous demande, il vous suffit de cliquer dans la case appropriée et un texte d'aide apparaîtra sur votre gauche.
{include file='game/footer.tpl'}