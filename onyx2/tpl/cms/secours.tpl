{include file='cms/header.tpl'}
		<div id="connexion" class="block">
			<div class="header">Connexion</div>
			<div class="corps">
				Sélectionnez votre galaxie :<br /><br />
{foreach from=$servers_name item=server_name key=key}
				<a href="{$server_url.$key}">Galaxie {$server_name}</a><br />
{/foreach}
			</div>
		</div>

		<div id="inscription" class="block"><span>Le serveur de base de données est indisponible</span> <p><em>Le serveur de base de données n'étant actuellement pas en mesure d'assurer la génération des pages web, ni le processus de connexion ou d'inscription, utilisez les liens du cadre ci-contre pour accèder à la page de connexion de secours de votre serveur.<br /><br />Veuillez nous excuser pour la gène occasionnée, nous faisons notre possible pour rétablir le site au plus vite.<br /><br /><i>Le Staff</i></em></p></div>
{include file='cms/footer.tpl'}