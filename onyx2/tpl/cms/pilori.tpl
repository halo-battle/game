{include file='cms/header.tpl'}
		<table style="width: 85%; margin: auto;" id="pilori">
			<thead>
				<tr>
					<th>Utilisateur</th>
					<th>Raison</th>
					<th>Fin du banissement</th>
					<th>Opérateur</th>
				</tr>
			</thead>
			<tbody>
{foreach from=$joueurs item=joueur}
				<tr>
					<td>{$joueur.pseudo}</td>
					<td>{$joueur.raisonmv|escape}</td>
					<td>{if $joueur.mv == 3 || $joueur.last_visite == 2147483647}Définitif{else}{$joueur.last_visite+259200|date_format:"%a %d %B %Y %H:%M"|escape}{/if}</td>
					<td>{if $joueur.operateurmv}{mailto address=$joueur.mail encode="hex" text=$joueur.operateurmv}{else}Inconnu{/if}</td>
				</tr>
{/foreach}
			</tbody>
		</table>
{include file='cms/footer.tpl'}