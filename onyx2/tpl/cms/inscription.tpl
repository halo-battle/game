{include file='cms/header.tpl'}
<script src="js/inscription.js" type="text/javascript"></script>
		<div id="dernierminute" class="block">
			<div class="header">Informations</div>
			<div class="corps" id="description"><p><b>Bienvenue dans l&#39;univers de Halo&#45;Battle</b><br /><br /><p>Afin de pouvoir jouer sur l'un des serveurs mis &agrave; votre disposition, veuillez remplir les champs ci-contre.<br /><br />Vous trouverez ici des informations pour vous aider tout au long de votre inscription<noscript> (requiert d&#39;avoir JavaScript d&#39;activ&eacute;)</noscript>.</p><br /><b>A bient&ocirc;t dans le jeu</b><br /><i>Le staff de Halo&#45;Battle</i></p>
			</div>
		</div>
		<div id="statistiques" class="block">
			<div class="header">Formulaire d&#39;inscription</div>
			<div class="corpsi">
				<form action="{$menu.inscription}" method="post">
						<span><label for="codeact">Code d'activation :</label> <input class="text" name="codeact" id="codeact" type="text" maxlength="16" value="{$smarty.post.codeact}" /><br />
						<br /><span><label for="pseudo">Pseudo :</label> <input class="text" name="HB_pseudo" id="pseudo" type="text" maxlength="16" value="{$smarty.post.HB_pseudo}" />
						<br /><label for="mdp">Mot de passe :</label> <input class="text" name="HB_mdp" id="mdp" type="password" maxlength="32" />
						<br /><label for="conf">Confirmation :</label> <input class="text" name="HB_conf" id="conf" type="password" maxlength="32" />
						<br /><label for="mail">Adresse &eacute;lectronique :</label> <input class="text" name="HB_mail" id="mail" type="text" value="{$smarty.post.HB_mail}" maxlength="64" />
						<br /><br />Race :<br />
						<table style="margin-left: auto; margin-right: auto; width: 75%;">
							<tr>
								<td>
									<label id="humain" for="humaini">
										<img src="images/humain.jpg" alt="Humain"/><br />
										<input type="radio" name="race" id="humaini" value="humain"{if $smarty.post.race == "humain"} selected="selected"{/if} /> Humain
									</label>
								</td>
								<td>
									<label id="covenant" for="covenanti">
										<img src="images/covenant.jpg" alt="Covenant"/><br />
										<input type="radio" name="race" id="covenanti" value="covenant"{if $smarty.post.race == "covenant"} selected="selected"{/if} /> Covenant
									</label>
								</td>
							</tr>
						</table>
						<br /><label for="servers">Galaxie :</label> <select name="servers" id="servers"><option value="1"> Alpha</option></select>
						<br /><label for="placement">Placement pr&eacute;f&eacute;rentiel :</label> <input class="text" name="HB_placement" id="placement" type="text" value="{$smarty.post.HB_placement}" />
						<br /><br /><label for="captcha">Recopiez ce texte :</label> <input class="text" name="HB_captcha" id="captcha" type="text" /> <img src="captcha/image.php" alt="Génération ..." id="gen" />
						<br /><br /><input type="checkbox" name="regles" id="regles" value="1" /> <label for="regles">J&#39;accepte les <a href="?p=conditions" onclick="window.open(this.href); return false;">conditions g&eacute;n&eacute;rales d&#39;utilisation</a> et les <a href="?p=regles" onclick="window.open(this.href); return false;">r&egrave;gles du jeu</a></label>
						<br /><br /><input class="submit" value="OK" type="submit">
						</span>
				</form>
			</div>
		</div>
		{if $erreurs}
		<div id="connexion" class="block">
			<div class="header"><b>Impossible de valider votre inscription</b></div>
			<div class="corps" id="description">
				<p>
					{$erreurs}
				</p>
			</div>
		</div>
		{/if}
{include file='cms/footer.tpl'}