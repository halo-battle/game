{include file='cms/header.tpl'}
		<div id="dernierminute" class="block">
			<div class="header">Informations</div>
			<div class="corps" id="description"><b>Oublie de mot de passe</b><br /><br /><p>Compl&eacute;tez le formualaire ci-contre avec votre nom d&#39;utilisateur et votre adresse &eacute;lectronique. Un nouveau mot de passe sera envoy&eacute; &agrave; votre adresse, vous permettant ainsi d&#39;avoir de nouveau acc&egrave;s &agrave; votre compte.<br /><br />Pensez &agrave; changer ce nouveau mot de passe que vous ne retiendrez pas facilement, via la page option du jeu.</p>
			</div>
		</div>
		<div id="statistiques" class="block">
			<div class="header">Formulaire de r&eacute;initialisation de mot de passe</div>
			<div class="corpsi">
				<form action="{$link.oubliemdp}" method="post">
						<span><label for="HB_pseudo">Pseudo :</label> <input class="text" name="HB_pseudo" id="pseudo" type="text"{if $smarty.post.HB_pseudo} value="{$smarty.post.HB_pseudo}"{/if} />
						<br /><label for="HB_mail">Adresse &eacute;lectronique :</label> <input class="text" name="HB_mail" id="mail" type="text"{if $smarty.post.HB_mail} value="{$smarty.post.HB_mail}"{/if} />
						<br /><br /><label for="HB_captcha">Recopiez ce texte :</label> <input class="text" name="HB_captcha" id="captcha" type="text" /> <a href="javascript:document.getElementById('gen').src = 'captcha/image.php?a='+Math.random();"><img src="captcha/image.php" alt="Génération ..." id="gen" /></a>
						<br /><br /><input class="submit" value="OK" type="submit">
						</span>
				</form>
			</div>
		</div>
		{if $erreurs}
		<div id="connexion" class="block">
			<div class="header"><b>Impossible de changer le mot de passe :</b></div>
			<div class="corps" id="description">
				<p>
					{$erreurs}
				</p>
			</div>
		</div>
		{/if}
{include file='cms/footer.tpl'}