<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="fr" />

<title> ::Halo-Battle:: - {$titre}</title>
<link rel="stylesheet" href="{$url_images}templates/cms/default.css" type="text/css" media="all" />
<link rel="shortcut icon" type="image/x-icon" href="{$url_images}favicon.ico" />
</head>

<body>
<div id="cornertop"></div>
<div id="all">
	<div id="header">
		<div id="header_link"><span id="date">{$smarty.now|date_format:"%A %e %B %Y %H:%M"}</span> | <a href="{$link.lastNews}">{$lastNews}</a> | <a href="{$link.lastTopic}">{$lastTopic}</a> | <a href="{$link.lastUser}">{$lastUser}</a> | <a href="{$link.lastDownload}">{$lastDownload}</a> | <a href="{$link.lastLink}">{$lastLink}</a></div><a href="{$link.RSS}" id="rss">{$LANG.hd_menu.rss}</a>
		<a href="{$link.self}" id="img"><span></span>Halo Battle</a>
		<div id="buttons"><div><a href="{$link.accueil}">{$LANG.hd_menu.accueil}</a></div> <div><a href="{$link.jeu}">{$LANG.hd_menu.jeu}</a></div> <div><a href="{$link.inscription}">{$LANG.hd_menu.inscrire}</a></div> <div><a href="{$link.forum}">{$LANG.hd_menu.forum}</a></div> <div><a href="{$link.blog}">{$LANG.hd_menu.blog}</a></div> <div><a href="http://www.halo.fr/">{$LANG.hd_menu.hfr}</a></div></div>
		<form action="#" method="post"><fieldset id="recherche"><input class="text" type="text" name="recherche" value="{$LANG.hd_menu.search}" maxlength="128" tabindex="10" onfocus="this.value = this.value!='{$LANG.hd_menu.search}'?this.value:''" onblur="this.value=this.value==''?'{$LANG.hd_menu.search}':this.value" /><input value="OK" class="submit" type="submit" tabindex="11" /><br /><a href="#" tabindex="12">{$LANG.hd_menu.advSearch}</a></fieldset></form>
	</div>
	<div id="banner">
	{foreach from=$header.infos item=info}
		<div class="bannerleft"{if $info.color} style="color: {$info.color};"{/if}>{$info.texte}</div>
	{/foreach}
		<span id="bannerright">{$LANG.cntRaces|sprintf:$header.count.1:$header.count.0:$header.count.2}</span></div>
	<div id="corps">
