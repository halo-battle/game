{include file='cms/header.tpl'}
		<div id="connexion" class="block">
			<div class="header">{$LANG.accueil.login.titre}</div>
			<div class="corps">
				<form action="?p=connexion" method="post">
					<fieldset>
						<input type="hidden" name="jsa" id="jsa" value="0" />
						<span>
							<input type="text" class="text" name="HB_login" id="HB_login" maxlength="32" tabindex="1" />
							<input type="password" class="password" name="HB_password" id="HB_password" maxlength="32" tabindex="2" />
							<input type="submit" class="submit" value="GO" tabindex="3" />
						</span>
						<span>
							<label>{$LANG.accueil.login.galaxie}:</label> {$serveur_name}
							<a href="{$link.oubliemdp}" tabindex="4">{$LANG.accueil.login.forgotMdp}</a>
						</span>
					</fieldset>
				</form>
			</div>
		</div>

		<div id="inscription" class="block">{$LANG.accueil.inscription.texte|sprintf:$link.inscription:$link.inscription:$link.inscription}</div>
		<div id="dernierminute" class="block">
			<a href="http://www.halo.fr/" onclick="window.open(this.href); return false;"><img src="{$url_images}images/hfr.jpg" alt="Halo.fr" /></a>
		</div>
		<script type="text/javascript">document.getElementById("jsa").value=1; document.getElementById("HB_login").focus();</script>
{include file='cms/footer.tpl'}