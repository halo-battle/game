{include file='game/header.tpl'}
			<h2>Evénements</h2>
			<em>Heure du serveur: {$smarty.now|date_format:"/%a %d %b %Y/%H%M %S"}</em>
			<h4>Version du serveur : <a href="./?p=version">{$version}</a></h4>
{foreach from=$news item=new key=key}
			<h1><strong>{$new.titre}</strong> : {$new.contenu|escape|nl2br}</h1>
{/foreach}
			<h2>Infos</h2>
			<h4><a href="{$menu.courrier}">Messagerie :</a> {$nbMail} message{if $nbMail > 1}s{/if} en attente</h4>
			<h4><a href="./{$menu.pilori}">Pilori :</a> {$nbPilori} joueur{if $nbPilori > 1}s{/if} dans le pilori de la galaxie</h4>
{include file='game/footer.tpl'}