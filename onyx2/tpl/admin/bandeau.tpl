{include file='game/header.tpl'}
			<h2>Informations visibles</h2>
			<table>
				<thead>
					<tr>
						<th>Action</th>
						<th>Texte</th>
					</tr>
				</thead>
				<tbody>
{foreach from=$tableau item=ligne key=key}
					<tr>
						<td><a href="{$menu.bandeau}&amp;i={$ligne.id}">EDIT</a> | <a href="{$menu.bandeau}&amp;d={$ligne.id}">SUPPR</a></td>
						<td{if $ligne.color} style="color: {$ligne.color};"{/if}><b>{$ligne.texte|escape}</b></td>
					</tr>
{/foreach}
				</tbody>
			</table>
			<h2>{if $id == "add"}Ajouter{else}Modifier{/if} une information{if $id != "add"} : {$id}{/if}</h2>
			<form action="{$menu.bandeau}&amp;i={$id}" method="post">
				<fieldset class="options">
					<label for="texte">Texte :</label> <input type="text" name="texte" id="texte" value="{$mod.texte|escape}" size="40" maxlength="255" tabindex="1" /><br /><br />
					<label for="color">Couleur :</label> <input type="text" name="color" id="color" value="{$mod.color|escape}" size="25" maxlength="255" tabindex="2" /><br /><br />
					<input type="submit" class="submit" value="GO" tabindex="3" />
				</fieldset>
			</form>
			<h2>Gestion du cache</h2>
			<a href="{$menu.bandeau}&amp;actuCache">Mettre à jour le cache</a>
{include file='game/footer.tpl'}