{include file='game/header.tpl'}
			<h2>Pages de démarrage</h2>
			<h3>La première news est celle qui sera affichée à la connexion des joueurs</h3>
			<table>
				<thead>
					<tr>
						<th>Action</th>
						<th>Titre</th>
						<th>Date de visibilité</th>
					</tr>
				</thead>
				<tbody>
{foreach from=$tableau item=ligne key=key}
					<tr>
						<td><a href="{$menu.demarrage}&amp;i={$ligne.id}">EDIT</a> | <a href="{$menu.demarrage}&amp;d={$ligne.id}">SUPPR</a></td>
						<td><b>{$ligne.titre|escape}</b></td>
						<td>{$ligne.time|date_format:"%d/%m/%y %H:%M:%S"}</td>
					</tr>
{/foreach}
				</tbody>
			</table>
			<h2>Gestion du cache</h2>
			<a href="{$menu.demarrage}&amp;actuCache">Mettre à jour le cache</a>
			<h2>{if $id == "add"}Ajouter{else}Modifier{/if} une page de démarrage{if $id != "add"} : {$id}{/if}</h2>
			<form action="{$menu.demarrage}&amp;i={$id}" method="post">
				<fieldset class="options">
					<label for="titre">Titre :</label> <input type="text" name="titre" id="titre" value="{$mod.titre}" tabindex="1" /><br />
					<label for="contenu">Texte :</label><br /><textarea name="contenu" id="contenu" rows="10" cols="10" tabindex="2">{$mod.contenu}</textarea><br />
					{if $mod.titre}<input type="checkbox" name="reset" id="reset" value="1" tabindex="3" /><label for="reset"> Actualiser la date et l'heure d'écriture</label> <i>Les utilisateurs ayant déjà vu la page la revéront de nouveau.</i><br /><br /><br />{/if}
					<input type="submit" class="submit" value="GO" tabindex="4" />
				</fieldset>
			</form>
{include file='game/footer.tpl'}