{include file='game/header.tpl'}
			<h2>Vérification des IP</h2>
			<a href="{$menu.vip}&amp;v={$numpage-1}">&lt;&lt;&lt;</a> page {$numpage}/{$nbpage} <a href="{$menu.vip}&amp;v={$numpage+1}">&gt;&gt;&gt;</a>
			<table style="text-align: center; margin: auto;">
				<thead>
					<tr>
						<th>Adresses IP</th>
						<th>Nom d'utilisateur</th>
						<th>Date & heure</th>
						<th>Traçage IP</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
{foreach from=$ips item=ip}
					<tr{if $ip.6} style="background-color: #{$ip.6};"{/if}>
						<td><a href="{$menu.vip}&amp;ip={$ip.0}">{$ip.0}</a></td>
						<td><a href="{$menu.vip}&amp;util={$ip.2}">{$ip.3} ({$ip.2})</a></td>
						<td>{$ip.4|date_format:"%d/%m/%y %H:%M:%S"}</td>
						<td>{$ip.1}</td>
						<td><a href="{$menu.vip}&amp;util={$ip.2}&amp;act=multiok">Déclaré</a><br /><a href="{$menu.sjoueurs}&amp;id={$ip.2}&amp;sanc=d&amp;raisonmv=Multi-compte">Définitivement</a></td>
					</tr>
{/foreach}
				</tbody>
			</table>
			<a href="{$menu.vip}&amp;v={$numpage-1}">&lt;&lt;&lt;</a> page {$numpage}/{$nbpage} <a href="{$menu.vip}&amp;v={$numpage+1}">&gt;&gt;&gt;</a>
{include file='game/footer.tpl'}