{include file='game/header.tpl'}
		{if $smarty.post.req || $smarty.get.req}
			<h2>Réponse de la base de données - <a href="#req">Aller à la requête</a></h2>
			<h3><strong>Requête :</strong> <em>{$smarty.post.req|escape}</em></h3>
			{if $affected}
			<h3><strong>Enregistrement(s) affecté(s) :</strong> {$affected}</h3>
			{/if}
			{if $erreur}
			<h3 style="color: #FF0000;"><strong>Erreur :</strong> <em>{$erreur}</em></h3>
			{/if}
	{if $reponses}
	<div id="gestion">
		<table style="empty-cells:show;">
			<tr>
		{foreach from=$reponses.0 key=key item=reponse}
				<th>{$key}</th>
		{/foreach}
			</tr>
		{foreach from=$reponses item=reponse}
			<tr>
				{foreach from=$reponse item=ligne}
					<td>{if $lien}<a href="?p=bdd&amp;req={$lien}{$ligne}">{/if}{$ligne}</td>
				{/foreach}
			</tr>
		{/foreach}
		</table>
	</div>
	{/if}
		{/if}
			<h2>Administration de la base de données</h2>
			<form action="?p=bdd" method="post">
				<fieldset class="options">
					<label for="req">Requête :</label><br /><textarea name="req" id="req" rows="5" cols="15" tabindex="2">{if $smarty.get.req}{$smarty.get.req}{/if}{$smarty.post.req|escape}</textarea><br />
					<input type="submit" class="submit" value="GO" tabindex="3" />
				</fieldset>
			</form>
{include file='game/footer.tpl'}