{include file='game/header.tpl'}
			<h2>{$titre}</h2>
			<table>
				<tr>
					<td>
						<form action="admin.php" method="get">
							<fieldset class="options" style="margin: 0;">
								<label for="idplan">ID de la planète :</label> <input type="text" value="" id="idplan" name="id" tabindex="1" /><br />
								<input type="hidden" value="{$linkpage}" name="p" /><br />
								<input type="submit" class="submit" value="GO" />
							</fieldset>
						</form>
					</td>
				</tr>
				<tr>
					<td>
						<form action="admin.php" method="get">
							<fieldset class="options" style="margin: 0;">
								<label for="amas">Amas :</label> <input type="text" id="amas" name="amas" maxlength="2" tabindex="2" /><br /><br />
								<label for="ss">Système :</label> <input type="text" id="ss" name="ss" maxlength="3" tabindex="3" /><br /><br />
								<label for="pos">Position :</label> <input type="text" id="pos" name="pos" maxlength="2" tabindex="4" /><br /><br />
								<input type="hidden" value="{$linkpage}" name="p" /><br />
								<input type="submit" class="submit" value="GO" />
							</fieldset>
						</form>
					</td>
				</tr>
				<tr>
					<td>
						<form action="admin.php" method="get">
							<fieldset class="options" style="margin: 0;">
								<label for="format">Affichage ([a:s:p]) :</label> <input type="text" id="format" name="format" maxlength="10" tabindex="5" /><br />
								<input type="hidden" value="{$linkpage}" name="p" /><br />
								<input type="submit" class="submit" value="GO" />
							</fieldset>
						</form>
					</td>
				</tr>
			</table>
			<br />
			<h2>Liste des planètes existantes</h2>
			<table>
				<tr>
					<th>Planète</th>
					<th>Joueur</th>
					<th>Position</th>
					<th></th>
				</tr>
				{foreach from=$planetes item=planete}
				<tr>
					<td>{$planete.nom_planete}</td>
					<td>{$planete.pseudo}</td>
					<td>{$planete.galaxie}:{$planete.ss}:{$planete.position}</td>
					<td><a href="admin.php?p={$linkpage}&id={$planete.id}"><input type="submit" class="submit" value="GO" /></a></td>
				<tr>
				{/foreach}
			</table>
{include file='game/footer.tpl'}
