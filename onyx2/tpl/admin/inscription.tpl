{include file='game/header.tpl'}
	<div class="corpsi">
		<form action="" method="post">
				<p><label for="pseudo">Pseudo :</label> <input class="text" name="HB_pseudo" id="pseudo" type="text" maxlength="16" value="{$smarty.post.HB_pseudo}" tabindex="1" />
				<br /><label for="mdp">Mot de passe :</label> <input class="text" name="HB_mdp" id="mdp" type="password" maxlength="32" tabindex="2" /> <em>Laisser vide pour en générer un mot de passe aléatoire</em>
				<br /><label for="conf">Confirmation :</label> <input class="text" name="HB_conf" id="conf" type="password" maxlength="32" tabindex="3" />
				<br /><label for="mail">Adresse &eacute;lectronique :</label> <input class="text" name="HB_mail" id="mail" type="text" value="{$smarty.post.HB_mail}" maxlength="64" tabindex="4" />
				<br /><br />Race :<br />
				</p>
				<table style="margin-left: auto; margin-right: auto; width: 75%;">
					<tr>
						<td>
							<label id="humain" for="humaini">
								<img src="images/humain.jpg" alt="Humain"/><br />
								<input type="radio" name="race" id="humaini" value="humain"{if $smarty.post.race == "humain"} selected="selected"{/if} tabindex="4" /> Humain
							</label>
						</td>
						<td>
							<label id="covenant" for="covenanti">
								<img src="images/covenant.jpg" alt="Covenant"/><br />
								<input type="radio" name="race" id="covenanti" value="covenant"{if $smarty.post.race == "covenant"} selected="selected"{/if} tabindex="5" /> Covenant
							</label>
						</td>
					</tr>
				</table>
				<p>
				<br /><label for="servers">Galaxie :</label> <select name="servers" id="servers" tabindex="6"><option value="1"> Alpha</option></select>
				<br /><label for="placement">Placement pr&eacute;f&eacute;rentiel :</label> <input class="text" name="HB_placement" id="placement" type="text" tabindex="7" value="{$smarty.post.HB_placement}" />
				<br /><input class="text" name="mailler" id="mailler" type="checkbox" tabindex="8" value="1" /> <label for="mailler">Inscrire automatiquement et envoyer un mail au joueur !</label>
				<br /><br /><input class="submit" value="OK" type="submit" tabindex="9" />
				</p>
		</form>
	</div>
{include file='game/footer.tpl'}