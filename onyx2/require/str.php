<?php
function hexstr($var)
{
    return pack('H*', $var);
}

function strhex($var)
{
    return @array_shift(unpack('H*', $var));
}

function bitstr($var)
{
    $r = null;
    for ($i = 0; $i < strlen($var); $i += 8) {
        $r .= chr(bindec(substr($var, $i, 8)));
    }
    return $r;
}

function bithex($var)
{
    $r = null;
    for ($i = 0; $i < strlen($var); $i += 4) {
        $r .= base_convert(substr($var, $i, 4), 2, 16);
    }
    return $r;
}

function cxor($msg, $cle)
{
    $xor = null;
    for ($i = 0; $i < strlen($msg);$i++) {
        $xor .= substr($msg, $i, 1) ^ substr($cle, $i % strlen($cle), 1);
    }
    return $xor;
}

function random($l=128)
{
    $r = null;
    for ($i = 1;$i <= $l/128 && $l/128 <= 32; $i++) {
        $var = time().microtime().mt_rand().md5(time()).md5(microtime()).md5(mt_rand()).sha1(time()).sha1(microtime()).sha1(mt_rand());
        $r .= md5(cxor(md5($var, true), sha1($var, true)));
    }
    return $r;
}

function uniquehash($var, $length=128, $raw=false)
{
    $hashs = array('tiger192,4','haval256,5','md5','snefru','gost','ripemd160','whirlpool');
    $r = hash('sha512', $var, true);

    foreach ($hashs as $algo) {
        $r = cxor(strrev($r), hash($algo, strrev($r), true));
    }

    if ($length % 8 == 0 && $length >=128 && $length <= 512) {
        $r = substr($r, 0, $length/8);
    }
    if (!$raw) {
        $r = strhex($r);
    }

    return $r;
}

function temphash($var, $length=128, $raw=false)
{
    if (!$val = Cache::read('_temphash')) {
        Cache::set('_temphash', $val = hexstr(random()));
    }

    return uniquehash(cxor($var, $val), $length, $raw);
}

function decode_ip($int_ip)
{
    $hexipbang = explode('.', chunk_split($int_ip, 2, '.'));
    return hexdec($hexipbang[0]). '.' . hexdec($hexipbang[1]) . '.' . hexdec($hexipbang[2]) . '.' . hexdec($hexipbang[3]);
}

function encode_ip($dotquad_ip=false)
{
    if (!$dotquad_ip) {
        $dotquad_ip = $_SERVER['REMOTE_ADDR'];
    }

    $ip_sep = explode('.', $dotquad_ip);
    if (empty($ip_sep[3])) {
        $ip_sep = explode('.', "127.0.0.1");
    }
    return sprintf('%02x%02x%02x%02x', $ip_sep[0], $ip_sep[1], $ip_sep[2], $ip_sep[3]);
}

function url($string, $external=false)
{
    if ($external) {
        return htmlspecialchars($string);
    }

    global $VAR;

    if (!empty($VAR['rewrite_url'])) {
        $masque = $VAR['rewrite_url']['masque'];
        $replace = $VAR['rewrite_url']['replace'];
        $var = preg_replace($masque, $replace, $string);
        return htmlspecialchars($var);
    } else {
        return htmlspecialchars($string);
    }
}
