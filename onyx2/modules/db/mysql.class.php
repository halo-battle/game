<?php

class BDD
{
    public $connected;

    private $session;

    private $reponse;

    public $host;

    public $user;

    private $password;

    public $database;

    public $num_rows;

    public $nodb;


    public function __construct($profile=null)
    {
        if ($profile === false) {
            return false;
        }

        global $db_config;

        if (empty($profile)) {
            if (!$db_config['profile']) {
                return false;
            }
            $profile = &$db_config['profile'];
        }

        if (!ctype_alnum($profile)) {
            trigger_error('Le nom du profil contient des caracteres illegaux', E_USER_ERROR);
        }

        if ($db_config['profile']) {
            require(ONYX.'db/'.$profile.'.profile.php');

            $db = &$___profile['db'];
            $host = &$___profile['host'];
            $user = &$___profile['user'];
            $pass = &$___profile['pass'];
        }

        if ($db_config['crypt']) {
            $pass = dbpass($pass, $db_config['crypt']);
        }

        return $this->connexion($host, $user, $pass, $db);
    }

    public function connexion($host, $user, $pass, $db=null)
    {
        if ($this->session) {
            $this->deconnexion();
        }

        $this->reponse = null;

        $this->session = mysqli_connect($host, $user, $pass);

        if (!$this->session) {
            elog('Connexion impossible a la base de donnee : '.$this->erreur(), 2);
            if (function_exists($this->nodb)) {
                call_user_func($this->nodb);
            }
            return false;
        }

        mysqli_query($this->session, 'SET CHARACTER SET "utf8"');

        $this->host = $host;
        $this->user = $user;
        $this->password = $pass;
        if ($db) {
            $this->db($db);
        }

        $this->connected = true;
    }

    public function reconnexion()
    {
        if (!empty($this->host) && !empty($this->user) && !empty($this->password) && !empty($this->database)) {
            $this->connexion($this->host, $this->user, $this->password, $this->database);
        }
    }

    public function deconnexion()
    {
        if (!$this->session) {
            return false;
        }

        $r =  mysqli_close($this->session);
        $this->session = false;
        $this->connected = false;
        return $r;
    }

    public function erreur()
    {
        if (!$this->session) {
            return false;
        }

        return mysqli_error($this->session);
    }

    public function db($db=null)
    {
        if (!$this->session) {
            return false;
        }

        return $this->database = mysqli_select_db($this->session, $db) ? $db : $this->database;
    }

    public function escape(&$var)
    {
        if (!$this->session) {
            return false;
        }
        $var = mysqli_real_escape_string($this->session, $var);
        return $var;
    }

    public function query($query)
    {
        if (!$this->session) {
            return false;
        }

        $this->reponse = mysqli_query($this->session, $query);

        global $db_config;

        if (!$this->reponse && $db_config['log']) {
            elog('Erreur Mysql: " '.$this->erreur().' ", avec la requète: { '.$query.' }.', 1);
        }

        $this->num_rows = @mysqli_num_rows($this->reponse);

        if ($this->num_rows == 0) {
            return null;
        } elseif ($this->num_rows >= 1) {
            for ($i=0; $var = mysqli_fetch_assoc($this->reponse); $i++) {
                $sortie[$i] = $var;
            }
            return $sortie;
        } else {
            return false;
        }
    }

    public function unique_query($query)
    {
        if (!$this->session) {
            return false;
        }

        $this->reponse = mysqli_query($this->session, $query);

        global $db_config;

        if (!$this->reponse && $db_config['log']) {
            elog('Erreur Mysql: " '.$this->erreur().' ", avec la requète: { '.$query.' }.', 1);
        }

        $this->num_rows = @mysqli_num_rows($this->reponse);

        if ($this->num_rows == 0) {
            return null;
        } elseif ($this->num_rows >= 1) {
            return mysqli_fetch_assoc($this->reponse);
        } else {
            return false;
        }
    }

    public function affected()
    {
        if (!$this->session) {
            return false;
        }

        return mysqli_affected_rows($this->session);
    }

    public function update_classement()
    {
        if (!$this->session) {
            return false;
        }
        global $table_user;
        $query = "SET @p=0; UPDATE $table_user SET place_points = (@p:=@p+1) ORDER BY points DESC;";
        mysqli_multi_query($this->session, $query);
    }
}
