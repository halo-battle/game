<?php

if (!defined("ONYX")) {
    exit;
}

if (!empty($OPT["type"]) && time() > $OPT["start"] && (empty($OPT["auto_end"]) || time() <= $OPT["start"] + $OPT["duree"])) {
    $template = new Template();
    $template->assign('date', utf8_encode(strftime("%A %d %B %Y %H:%M")));
    $template->assign('datehb', utf8_encode(str_replace('.', '', strftime("/%a %d %b %Y/%H%M %S"))));
    $template->assign("link", array_map("url", $VAR["link"]));
    $template->assign("url_serveur", $_SERVER["HTTP_HOST"]);
    $template->assign("url_images", $VAR["url_images"]);
    $template->assign("header", Cache::read("headerNB"));
    $template->assign("LANG", $LANG);

    $template->assign('titre', $LANG["modules"]["fermerServeur"][$OPT['type']]["titre"]);
    if (!empty($OPT['auto_end'])) {
        $reste = ($OPT["start"] + $OPT["duree"]) - time();
        $min = floor($reste/60);
        if ($min > 1) {
            $reste = "Il reste ".$min." minutes";
        } elseif ($min == 1) {
            $reste = "Il reste ".$min." minute";
        } else {
            $reste = "Il reste ".$reste." secondes";
        }
        $reste .= " de maintenance";
    } else {
        if (empty($OPT["duree"])) {
            $reste = "La maintenance doit durer quelques minutes";
        } else {
            $reste = "La maintenance doit durer environ ".ceil($OPT["duree"]/60)." minutes";
        }
    }

    $template->assign('texte', str_replace('$main_serveur', $VAR["main_serveur"], str_replace('$server_name', $VAR["serveur_name"], str_replace('$reste', $reste, $LANG["modules"]["fermerServeur"][$OPT['type']]["texte"]))));

    $template->display('cms/maj.tpl');
    exit;
}
