<?php

if (!defined('ONYX')) {
    exit;
}

if (defined('DB_TYPE')) {
    switch (DB_TYPE) {
                case 'mysql':
                case 'postgresql':
                    
                    $session_config = $OPT;
                    
                    require_once(DB_TYPE.'.class.php');
                    break;
                
                default: trigger_error('Base de donnee inconnue', E_USER_ERROR);
            }
}
