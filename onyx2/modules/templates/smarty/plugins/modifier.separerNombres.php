<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty plugin
 *
 * Type:     modifier<br>
 * Name:     separenombre<br>
 * Date:     Aug 22, 2008
 * Purpose:  separe number
 * Example:  {$text|separenombre}
 * @version  1.0
 * @author   Nemunaire <nemunaire at gmail dot com>
 * @param string
 * @return string
 */
function smarty_modifier_separerNombres($string)
{
    return number_format(floor($string), 0, ',', ' ');
}

/* vim: set expandtab: */
