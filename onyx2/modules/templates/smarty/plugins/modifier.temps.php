<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty plugin
 *
 * Type:     modifier<br>
 * Name:     temps<br>
 * Date:     Nov 15, 2020
 * Purpose:  Affiche un temps à partir d'un nombre de seconde
 * Example:  {$nombre_de_secondes|temps}
 * @version  1.0
 * @author   Nigel
 * @param int
 * @return string
 */
function smarty_modifier_temps($secondes)
{
    return gmdate("H:i:s", $secondes);
}

/* vim: set expandtab: */
