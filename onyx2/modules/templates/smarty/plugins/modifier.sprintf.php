<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty plugin
 *
 * Type:     modifier<br>
 * Name:     sprintf<br>
 * Date:     Jan 29, 2009
 * Purpose:  separe number
 * Example:  {$text|sprintf:$LANG['test']}
 * @version  1.0
 * @author   Nemunaire <nemunaire at gmail dot com>
 * @param string
 * @return string
 */
function smarty_modifier_sprintf($string, $var0 = null, $var1 = null, $var2 = null, $var3 = null, $var4 = null, $var5 = null, $var6 = null, $var7 = null, $var8 = null, $var9 = null)
{
    for ($i = -1; $i<9; $i++) {
        if (empty(${'var'.($i+1)})) {
            break;
        }
    }

    if ($i == 0) {
        return sprintf($string, $var0);
    } elseif ($i == 1) {
        return sprintf($string, $var0, $var1);
    } elseif ($i == 2) {
        return sprintf($string, $var0, $var1, $var2);
    } elseif ($i == 3) {
        return sprintf($string, $var0, $var1, $var2, $var3);
    } elseif ($i == 4) {
        return sprintf($string, $var0, $var1, $var2, $var3, $var4);
    } elseif ($i == 5) {
        return sprintf($string, $var0, $var1, $var2, $var3, $var4, $var5);
    } elseif ($i == 6) {
        return sprintf($string, $var0, $var1, $var2, $var3, $var4, $var5, $var6);
    } elseif ($i == 7) {
        return sprintf($string, $var0, $var1, $var2, $var3, $var4, $var5, $var6, $var7);
    } elseif ($i == 8) {
        return sprintf($string, $var0, $var1, $var2, $var3, $var4, $var5, $var6, $var7, $var8);
    } elseif ($i == 9) {
        return sprintf($string, $var0, $var1, $var2, $var3, $var4, $var5, $var6, $var7, $var8, $var9);
    } else {
        return $string;
    }
}

/* vim: set expandtab: */
