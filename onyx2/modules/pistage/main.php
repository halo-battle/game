<?php

if (!defined('ONYX')) {
    exit;
}

$pistage = false;
$file = null;

if (!empty($OPT['ips'])) {
    $ips = explode(';', $OPT['ips']);
    if (in_array($_SERVER["REMOTE_ADDR"], $ips)) {
        $pistage = true;
    }
}

if (!empty($OPT['ids'])) {
    $SESS = new Session();

    $ids = explode(';', $OPT['ids']);
    if (isset($SESS->values['id']) && in_array($SESS->values['id'], $ids)) {
        $pistage = true;
        $file = $SESS->values['id'].".log";
    }
}

if ($pistage) {
    elog(var_export($_REQUEST, true), 0, ONYX."log/users", $file);
}

unset($pistage, $SESS, $ips, $ids);
